package com.lavsystems.mycityssports

import android.app.Application
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.lavsystems.mycityssports.di.dataSource.dataSourceModule
import com.lavsystems.mycityssports.di.network.networkModule
import com.lavsystems.mycityssports.di.repository.repositoryModule
import com.lavsystems.mycityssports.di.utils.utilsModule
import com.lavsystems.mycityssports.di.viewmodel.viewModelModule
import com.lavsystems.mycityssports.utils.AppOpenAdsManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@ExperimentalCoroutinesApi
class MyCitysSportsApplication : Application() {

//    private var appOpenAdsManager: AppOpenAdsManager? = null

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MyCitysSportsApplication)
            koin.loadModules(
                listOf(
                    utilsModule,
                    networkModule,
                    dataSourceModule,
                    repositoryModule,
                    viewModelModule,

                   // databaseModule
                )
            )
           // koin.createRootScope()
        }

        MobileAds.initialize(this) {
//            appOpenAdsManager = AppOpenAdsManager(
//                this@MyCitysSportsApplication
//            )
        }

//        MobileAds.setRequestConfiguration(
//            RequestConfiguration.Builder()
//                .setTestDeviceIds(listOf("618EF5F1CED8CAA104ACCD9AF6B57A05"))
//                .build()
//        )
    }

    //fun getAppOpenAdsManager() = appOpenAdsManager
}