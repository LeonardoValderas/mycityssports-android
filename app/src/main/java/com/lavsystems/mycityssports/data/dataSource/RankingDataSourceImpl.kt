package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.service.RankingService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.*

class RankingDataSourceImpl(
    private val service: RankingService,
    preferences: PreferencesRepository
): RankingDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<RankingDto>>> {
        val preferencesData = getPreferencesDataOrThrowException()
        return service.getRanking(
            preferencesData.cityId,
            preferencesData.institutionId,
            preferencesData.sportId,
            preferencesData.tournamentId,
            preferencesData.divisionSubDivision.divisionId,
            preferencesData.divisionSubDivision.subDivisionName
        )
    }

    override fun getFromLocal(): Flow<RankingDto?> {
        getCache(RANKING)?.let {
            return flowOf(it as RankingDto)
        }
        return flowOf(null)
    }

    override fun saveLocal(table: RankingDto?) {
        table?.let {
            setCache(RANKING, it)
        } ?: run {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(RANKING)
    }
}