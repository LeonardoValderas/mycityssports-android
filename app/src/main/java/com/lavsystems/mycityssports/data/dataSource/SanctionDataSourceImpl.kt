package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.db.dao.SponsorDao
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.api.service.SanctionService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.SanctionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.*

class SanctionDataSourceImpl(
    private val service: SanctionService,
    preferences: PreferencesRepository
) : SanctionDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<SanctionDto>>> {
        val preferencesData = getPreferencesDataOrThrowException()
        return service.getSanctions(
            preferencesData.cityId,
            preferencesData.institutionId,
            preferencesData.tournamentId,
            preferencesData.sportId,
            preferencesData.divisionSubDivision.divisionId,
            preferencesData.divisionSubDivision.subDivisionName
        )
    }

    override fun getFromLocal(): Flow<SanctionDto?> {
        getCache(SANCTION)?.let {
            return flowOf(it as SanctionDto)
        }

        return flowOf(null)
    }

    override fun saveLocal(sanctions: SanctionDto?) {
        sanctions?.let {
            setCache(SANCTION, it)
        } ?: run  {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(SANCTION)
    }
}