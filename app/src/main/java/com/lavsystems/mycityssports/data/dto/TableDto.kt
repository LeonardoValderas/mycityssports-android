package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TableDto(
    val table: TableModel?,
    val sponsors: MutableList<SponsorModel>
){
    constructor(): this(TableModel(), arrayListOf())
}