package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.network.api.service.InstitutionService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.flow.*

class InstitutionDataSourceImpl(
    private val service: InstitutionService,
    preferences: PreferencesRepository
) : InstitutionDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<MutableList<InstitutionModel>>>> {
        val preferencesData = getPreferencesData()
        return service.getInstitutions(
            preferencesData.cityId
        )
    }

    override fun getFromLocal(): Flow<MutableList<InstitutionModel>?> {
        getCache(INSTITUTION)?.let {
            return flowOf(it as MutableList<InstitutionModel>)
        }
        return flowOf(null)
    }

    override fun saveLocal(news: MutableList<InstitutionModel>?) {
        news?.let {
            setCache(INSTITUTION, it)
        } ?: run {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(INSTITUTION)
    }
}