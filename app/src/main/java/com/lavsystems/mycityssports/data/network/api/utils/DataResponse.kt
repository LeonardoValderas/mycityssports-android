package com.lavsystems.mycityssports.data.network.api.utils

import com.lavsystems.mycityssports.data.network.exception.ExceptionHandle

data class DataResponse<out T>(val status: StatusResponse,
                               val data: T?,
                               val message: String?,
                               val resourceString: Int?,
                               val exceptionHandle: ExceptionHandle?
) {
    companion object {
        fun <T> SUCCESS(data: T?): DataResponse<T> {
            return DataResponse(
                StatusResponse.SUCCESS,
                data,
                null,
                null,
                null
            )
        }

        fun <T> ERROR(message: String?, resourceString: Int?): DataResponse<T> {
            return DataResponse(
                StatusResponse.ERROR,
                null,
                message,
                resourceString,
                null
            )
        }

        fun <T> FAILURE(exceptionHandle: ExceptionHandle?): DataResponse<T> {
            return DataResponse(
                StatusResponse.FAILURE,
                null,
                null,
                null,
                exceptionHandle
            )
        }

        fun <T> LOADING(): DataResponse<T> {
            return DataResponse(
                StatusResponse.LOADING,
                null,
                null,
                null,
                null
            )
        }
    }
}