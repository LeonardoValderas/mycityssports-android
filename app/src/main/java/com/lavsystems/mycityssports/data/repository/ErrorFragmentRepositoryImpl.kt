package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.BuildConfig
import com.lavsystems.mycityssports.data.network.api.service.ErrorFragmentService
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder
import com.lavsystems.mycityssports.utils.preferences.Preferences

class ErrorFragmentRepositoryImpl(private val service: ErrorFragmentService, private val preferences: Preferences) :
    ErrorFragmentRepository {

    override fun sendErrorToAnalytic(stackTrace: String) {
        if(!BuildConfig.DEBUG)
            FirestoreBuilder.Builder()
                .className(ConstantsUtils.ERROR_FRAGMENT_REPOSITORY)
                .method("sendErrorToAnalytic")
                .type(FirestoreBuilder.EXCEPTION)
                .message(stackTrace)
                .brand()
                .model()
                .send()
        }
}