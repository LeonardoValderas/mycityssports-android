package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.flow.Flow

interface NewsDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<NewsDto>>>
    fun getFromLocal(): Flow<NewsDto?>
    fun saveLocal(news: NewsDto?)
    fun preferencesIsEmpty(): Boolean
    fun removeLocal()
}