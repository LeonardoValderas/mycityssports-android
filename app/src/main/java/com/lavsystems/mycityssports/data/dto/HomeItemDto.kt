package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HomeItemDto(
    val institutions: MutableList<InstitutionModel>,
    val items: MutableList<HomeDto>

){
    constructor(): this(mutableListOf(), mutableListOf())
}