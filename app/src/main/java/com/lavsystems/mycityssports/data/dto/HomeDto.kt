package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HomeDto(
    val institution: InstitutionModel,
    val sports: MutableList<SportModel>,
    val tournaments: MutableList<TournamentModel>
){
    constructor(): this(InstitutionModel(), arrayListOf(), arrayListOf())
}