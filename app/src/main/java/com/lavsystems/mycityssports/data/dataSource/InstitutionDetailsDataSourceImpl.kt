package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.api.service.InstitutionService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.flow.Flow

class InstitutionDetailsDataSourceImpl(
    private val service: InstitutionService
): InstitutionDetailsDataSource {
    override fun getFromApi(institutionId: String): Flow<ApiResponse<ApiResponseData<InstitutionModel>>> {
        return service.getInstitution(institutionId)
    }
}