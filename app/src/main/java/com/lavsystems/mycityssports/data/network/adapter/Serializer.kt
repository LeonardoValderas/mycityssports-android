package com.lavsystems.mycityssports.data.network.adapter

import com.squareup.moshi.Moshi
import java.util.*
///import com.squareup.moshi.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

object Serializer {
    @JvmStatic
    val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        //.add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        .build()
}