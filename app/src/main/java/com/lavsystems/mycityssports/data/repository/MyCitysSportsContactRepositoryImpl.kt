package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.MyCitysSportsContactDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class MyCitysSportsContactRepositoryImpl(private val dataSource: MyCitysSportsContactDataSource) : MyCitysSportsContactRepository {
    override fun getContact(): Flow<Resource<ApiResponseData<MyCitysSportsContactModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}