package com.lavsystems.mycityssports.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

@Dao
interface SponsorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdateSponsor(vararg sponsors: SponsorModel)

    @Query("SELECT * FROM sponsor_table")
    fun getSponsors(): Flow<MutableList<SponsorModel>>

    @Query("DELETE FROM sponsor_table")
    suspend fun deleteAll()
}