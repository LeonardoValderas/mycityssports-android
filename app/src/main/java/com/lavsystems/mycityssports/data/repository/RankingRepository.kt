package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.mvvm.model.RankingModel
import kotlinx.coroutines.flow.Flow

interface RankingRepository {
    fun getRanking(isRefresh: Boolean): Flow<Resource<RankingDto>>
}