package com.lavsystems.mycityssports.data.network.api.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lavsystems.mycityssports.mvvm.model.FixturePointItemModel
import java.lang.reflect.Type
import java.util.*

class FixturePointItemConverters {
    var gson = Gson()

    @TypeConverter
    fun stringToFixturePointItemList(data: String?): MutableList<FixturePointItemModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<MutableList<FixturePointItemModel?>?>() {}.type
        return gson.fromJson<MutableList<FixturePointItemModel?>>(data, listType)
    }

    @TypeConverter
    fun fixturePointItemListToString(fixtureItemModels: MutableList<FixturePointItemModel?>?): String? {
        return gson.toJson(fixtureItemModels)
    }
}