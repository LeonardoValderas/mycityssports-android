package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.network.api.service.FcmService
import com.lavsystems.mycityssports.data.network.request.FcmRequest
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CountryEnum
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.flow.Flow
import org.koin.java.KoinJavaComponent.get

class FcmDataSourceImpl(
    private val preferences: PreferencesRepository
) : FcmDataSource, BaseRepository(preferences) {

    override fun getFcmModel(): Flow<FcmModel?> {
        return preferences.getFcmModel()
    }

    /**
     * Situation: when user is in Arg the token is saved in arg database
     * so when the user change for other country the app does not save the token again because of that the push notification does not found the fmc model
     * in Br database
     */
    override fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean> {
        return preferences.saveFcmModel(fcmModel)
    }

    override fun saveToken(fcmModel: FcmModel): Flow<ApiResponse<ApiResponseData<FcmModel>>> {
        val service = get(FcmService::class.java)
        return service.saveFcmModel(FcmRequest(fcmModel.token, fcmModel.id))
    }

    override fun updateToken(fcmModel: FcmModel): Flow<ApiResponse<ApiResponseData<FcmModel>>> {
        val service = get(FcmService::class.java)
        return service.updateFcmModel(fcmModel.id, fcmModel)
    }

    override fun getCountry(): CountryModel? {
        return getCurrentCountry()
    }
}