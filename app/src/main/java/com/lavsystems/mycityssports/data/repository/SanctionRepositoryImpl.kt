package com.lavsystems.mycityssports.data.repository


import com.lavsystems.mycityssports.data.dataSource.SanctionDataSource
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import com.lavsystems.mycityssports.mvvm.model.SanctionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class SanctionRepositoryImpl(private val dataSource: SanctionDataSource) : SanctionRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getSanctions(isRefresh: Boolean): Flow<Resource<SanctionDto>> {
        if(dataSource.preferencesIsEmpty())
            return flowOf(Resource.success(SanctionDto()))

        return networkBoundResource(
            fetchFromLocal = {
                dataSource.getFromLocal()
            },
            shouldFetchFromRemote = {
                it == null
            },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                    dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ ->

            },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }

//    override fun getSponsors(): Flow<MutableList<SponsorModel>> {
//        return dataSource.getSponsors()
//    }
}
