package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import com.lavsystems.mycityssports.utils.preferences.Preferences
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class PreferencesRepositoryImpl(private val preferences: Preferences) :
    PreferencesRepository {

    override fun saveInstitutionId(id: String): Flow<Boolean> {
        return flowOf(preferences.saveInstitutionId(id))
    }

    override fun getInstitutionId(): Flow<String?> {
        return flowOf(preferences.getInstitutionId())
    }

    override fun removeInstitutionId(): Flow<Boolean> {
        return flowOf(preferences.removeInstitutionId())
    }

    override fun saveCountry(countryModel: CountryModel): Flow<Boolean> {
        return flowOf(preferences.saveCountry(countryModel))
    }

    override fun getCountry(): Flow<CountryModel?> {
        val country = preferences.getCountry()
        return flowOf(country)
    }

    override fun removeCountry(): Flow<Boolean> {
        return flowOf(preferences.removeCountry())
    }

    override fun getCountryCode(): Flow<Int?> {
        preferences.getCountry()?.let{
            return flowOf(it.code)
        }
        return flowOf(null)
    }

    override fun saveCity(cityModel: CityModel): Flow<Boolean> {
        return flowOf(preferences.saveCity(cityModel))
    }

    override fun getCity(): Flow<CityModel?> {
        val city = preferences.getCity()
        return flowOf(city)
    }

    override fun cityExists(): Flow<Boolean> {
        val city = preferences.getCity()
        val exists = city != null
        return flowOf(exists)
    }

    override fun removeCity(): Flow<Boolean> {
        return flowOf(preferences.removeCity())
    }

    override fun removeCountryAndCity(): Flow<Boolean> {
        return flowOf(preferences.removeCountryAndCity())
    }

    override fun saveCityId(id: String): Flow<Boolean> {
        return flowOf(preferences.saveCityId(id))
    }

    override fun getCityId(): Flow<String?> {
        return flowOf(preferences.getCityId())
    }

    override fun removeCityId(): Flow<Boolean> {
        return flowOf(preferences.removeCityId())
    }

    override fun saveSportId(id: String): Flow<Boolean> {
        return flowOf(preferences.saveSportId(id))
    }

    override fun getSportId(): Flow<String?> {
        return flowOf(preferences.getSportId())
    }

    override fun removeSportId(): Flow<Boolean> {
        return flowOf(preferences.removeSportId())
    }

    override fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Flow<Boolean> {
        return flowOf(preferences.saveDivisionSubDivision(divisionSubDivision))
    }

    override fun getDivisionSubDivision(): Flow<DivisionSubDivisionModel?> {
        return flowOf(preferences.getDivisionSubDivision())
    }

    override fun removeDivisionSubDivision(): Flow<Boolean> {
        return flowOf(preferences.removeDivisionSubDivision())
    }

    override fun saveTournamentId(id: String): Flow<Boolean> {
        return flowOf(preferences.saveTournamentId(id))
    }

    override fun getTournamentId(): Flow<String?> {
        return flowOf(preferences.getTournamentId())
    }

    override fun removeTournamentId(): Flow<Boolean> {
        return flowOf(preferences.removeTournamentId())
    }

    override fun showHomeInformationAlert(): Flow<Boolean> {
        return flowOf(preferences.showHomeInformationAlert())
    }

    override fun homeInformationAlertDisplayed(): Flow<Boolean> {
        return flowOf(preferences.homeInformationAlertDisplayed())
    }

    override fun getAllPreferenceIds(): Flow<PreferenceDataDto> {
        var preferenceDataDto: PreferenceDataDto
        runBlocking {
            val institutionId = getInstitutionId().single()
            val city = getCity().single()
            val sportId = getSportId().single()
            val divisionSubDivision = getDivisionSubDivision().single()
            val tournamentId = getTournamentId().single()
            val fcmId = getFcmId().single()
            preferenceDataDto = PreferenceDataDto(
                institutionId ?: "",
                city?.id ?: "",
                sportId ?: "",
                divisionSubDivision ?: DivisionSubDivisionModel(),
                tournamentId ?: "",
                fcmId ?: ""
            )
        }
        return flowOf(
            preferenceDataDto
        )
    }

    override fun getAllPreferenceIdsOrThrowException(): Flow<PreferenceDataDto> {
        var preferenceDataDto: PreferenceDataDto
        runBlocking {
            val institutionId = getInstitutionId().single()
            val city = getCity().single()
            val sportId = getSportId().single()
            val divisionSubDivision = getDivisionSubDivision().single()
            val tournamentId = getTournamentId().single()

            preferenceDataDto = PreferenceDataDto(
                institutionId ?: "",
                city?.id ?: "",
                sportId ?: "",
                divisionSubDivision ?: DivisionSubDivisionModel(),
                tournamentId ?: "",
                ""
            )
        }
        if (preferenceDataDto.isAnyImportantValueEmpty)
            throw NoSuchFieldException()

        return flowOf(preferenceDataDto)
    }

    override fun countryAndCityExists(): Flow<Boolean> {
        val country = preferences.getCountry()
        val city = preferences.getCity()
        val cityExists = city != null
        val countryExists = country != null
        return flowOf(cityExists && countryExists)
    }

    override fun getFcmModel(): Flow<FcmModel?> {
        return flowOf(preferences.getFcmModel())
    }

    override fun getFcmId(): Flow<String?> {
        return flowOf(preferences.getFcmId())
    }

    override fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean> {
        return flowOf(preferences.saveFcmModel(fcmModel))
    }

    override fun countryExists(): Flow<Boolean> {
        val country = preferences.getCountry()
        val countryExists = country != null
        return flowOf(countryExists)
    }
}
