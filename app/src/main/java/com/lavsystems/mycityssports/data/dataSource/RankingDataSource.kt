package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.flow.Flow

interface RankingDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<RankingDto>>>
    fun getFromLocal(): Flow<RankingDto?>
    fun saveLocal(table: RankingDto?)
    fun removeLocal()
    fun preferencesIsEmpty(): Boolean
}