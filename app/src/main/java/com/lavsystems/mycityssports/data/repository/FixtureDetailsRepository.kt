package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.flow.Flow

interface FixtureDetailsRepository {
     fun getCity(): Flow<CityModel?>
     fun getFixtureDetails(fixtureId: String): Flow<Resource<ApiResponseData<FixtureItemModel>>>
}