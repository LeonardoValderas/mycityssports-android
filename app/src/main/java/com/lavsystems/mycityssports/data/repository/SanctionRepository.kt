package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import kotlinx.coroutines.flow.Flow

interface SanctionRepository {
    fun getSanctions(isRefresh: Boolean): Flow<Resource<SanctionDto>>
}