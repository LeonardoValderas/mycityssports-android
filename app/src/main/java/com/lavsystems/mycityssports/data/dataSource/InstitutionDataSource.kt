package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

interface InstitutionDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<MutableList<InstitutionModel>>>>
    fun getFromLocal(): Flow<MutableList<InstitutionModel>?>
    fun saveLocal(news: MutableList<InstitutionModel>?)
    fun preferencesIsEmpty(): Boolean
    fun isInstitutionOrCityEmpty(): Boolean
    fun removeLocal()
}