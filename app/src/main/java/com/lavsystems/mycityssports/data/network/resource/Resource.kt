package com.lavsystems.mycityssports.data.network.resource

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val e: Throwable?,
    val code: Int?
    //val show: Boolean?
) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null,
                null,
                null
            )
        }

        fun <T> error(msg: String, e: Throwable?, data: T? = null, code: Int): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg,
                e,
                code
            )
        }

        fun <T> loading(): Resource<T> {
            return Resource(
                Status.LOADING,
                null,
                null,
                null,
                null
            )
        }
    }
}