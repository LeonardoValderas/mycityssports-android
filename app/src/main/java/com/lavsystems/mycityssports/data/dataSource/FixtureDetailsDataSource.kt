package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.flow.Flow

interface FixtureDetailsDataSource {
    fun getFromApi(fixtureId: String): Flow<ApiResponse<ApiResponseData<FixtureItemModel>>>
    fun getCity(): Flow<CityModel?>
}