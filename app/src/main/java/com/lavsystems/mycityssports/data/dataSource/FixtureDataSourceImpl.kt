package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.network.api.service.FixtureService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.koin.java.KoinJavaComponent.get

class FixtureDataSourceImpl(
    preferences: PreferencesRepository
): FixtureDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<FixtureModel>>> {
        val preferencesData = getPreferencesDataOrThrowException()
        val service = get(FixtureService::class.java)
        return service.getFixturesData(
            preferencesData.cityId,
            preferencesData.institutionId,
            preferencesData.sportId,
            preferencesData.tournamentId,
            preferencesData.divisionSubDivision.divisionId,
            preferencesData.divisionSubDivision.subDivisionName,
           // preferencesData.divisionId,
        )
    }

    override fun getFromLocal(): Flow<FixtureModel?> {
        getCache(FIXTURE)?.let {
            return flowOf(it as FixtureModel)
        }
        return flowOf(null)
    }

    override fun saveLocal(fixture: FixtureModel?) {
        fixture?.let {
            setCache(FIXTURE, it)
        } ?: run {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(FIXTURE)
    }
}