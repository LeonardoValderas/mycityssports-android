package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.TableModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface TableService {

    @GET("tables/cities/{cityId}/institutions/{institutionId}/sports/{sportId}/tournaments/{tournamentId}/divisions/{divisionId}/{subDivision}")
    fun getTable(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("sportId") sportId: String?,
        @Path("tournamentId") tournamentId: String?,
        @Path("divisionId") divisionId: String?,
        @Path("subDivision") subDivision: String?,
    ): Flow<ApiResponse<ApiResponseData<TableDto>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): TableService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(TableService::class.java)
        }
    }
}