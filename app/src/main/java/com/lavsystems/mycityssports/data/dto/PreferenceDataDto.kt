package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel

data class PreferenceDataDto(
    val institutionId: String,
    val cityId: String,
    val sportId: String,
    val divisionSubDivision: DivisionSubDivisionModel,
    val tournamentId: String,
    val fcmId: String
) {
    constructor() : this("", "", "", DivisionSubDivisionModel(), "", "")

    val isAnyValueEmpty: Boolean
        get() {
            return (institutionId.isEmpty() || cityId.isEmpty() || sportId.isEmpty() || divisionSubDivision.divisionId.isEmpty() || tournamentId.isEmpty())
        }

    val isAnyImportantValueEmpty: Boolean
        get() {
            return (institutionId.isEmpty() || cityId.isEmpty() || sportId.isEmpty())
        }


    val isInstitutionOrCityEmpty: Boolean
        get() {
            return (institutionId.isEmpty() || cityId.isEmpty())
        }

    override fun toString(): String {
        return  "$institutionId$cityId$sportId$divisionSubDivision$tournamentId"
    }
}
