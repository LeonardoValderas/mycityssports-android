package com.lavsystems.mycityssports.data.network.response

import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CityResponse(
    val success: Boolean,
    val message: String,
    val model: CityModel?,
    val error: String?
)