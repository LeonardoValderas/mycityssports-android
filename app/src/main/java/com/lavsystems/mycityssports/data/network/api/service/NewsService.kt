package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsService {

    @GET("news/cities/{cityId}/institutions/{institutionId}/sports/{sportId}/divisions/{divisionId}")
    fun getNews(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("sportId") sportId: String?,
        @Path("divisionId") divisionId: String?,
        @Query(value = "start", encoded = true) start: Int,
        @Query(value = "size", encoded = true) size: Int
    ): Flow<ApiResponse<MutableList<NewsModel>>>


    @GET("news/cities/{cityId}/institutions/{institutionId}/tournaments/{tournamentId}/sports/{sportId}/divisions/{divisionId}/{subDivision}")
    fun getNews(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("tournamentId") tournamentId: String?,
        @Path("sportId") sportId: String?,
        @Path("divisionId") divisionId: String?,
        @Path("subDivision") subDivision: String?
    ): Flow<ApiResponse<ApiResponseData<NewsDto>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): NewsService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(NewsService::class.java)
        }
    }
}