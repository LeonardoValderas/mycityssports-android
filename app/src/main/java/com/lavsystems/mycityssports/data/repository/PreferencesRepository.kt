package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.flow.Flow

interface PreferencesRepository {
    fun saveInstitutionId(id: String): Flow<Boolean>
    fun getInstitutionId(): Flow<String?>
    fun removeInstitutionId(): Flow<Boolean>

    fun saveCountry(countryModel: CountryModel): Flow<Boolean>
    fun getCountry(): Flow<CountryModel?>
    fun removeCountry(): Flow<Boolean>
    fun countryExists(): Flow<Boolean>
    fun getCountryCode(): Flow<Int?>

    fun saveCity(cityModel: CityModel): Flow<Boolean>
    fun getCity(): Flow<CityModel?>
    fun removeCity(): Flow<Boolean>
    fun removeCountryAndCity(): Flow<Boolean>
    fun cityExists(): Flow<Boolean>


    fun saveCityId(id: String): Flow<Boolean>
    fun getCityId(): Flow<String?>
    fun removeCityId(): Flow<Boolean>

    fun saveSportId(id: String): Flow<Boolean>
    fun getSportId(): Flow<String?>
    fun removeSportId(): Flow<Boolean>

    fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Flow<Boolean>
    fun getDivisionSubDivision(): Flow<DivisionSubDivisionModel?>
    fun removeDivisionSubDivision(): Flow<Boolean>

    fun saveTournamentId(id: String): Flow<Boolean>
    fun getTournamentId(): Flow<String?>
    fun removeTournamentId(): Flow<Boolean>

    fun showHomeInformationAlert(): Flow<Boolean>
    fun homeInformationAlertDisplayed(): Flow<Boolean>

    fun getAllPreferenceIds(): Flow<PreferenceDataDto>
    fun getAllPreferenceIdsOrThrowException(): Flow<PreferenceDataDto>

    fun countryAndCityExists(): Flow<Boolean>

    fun getFcmModel(): Flow<FcmModel?>
    fun getFcmId(): Flow<String?>
    fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean>
}