package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface SponsorService {

    @GET("sponsors/{sponsorId}")
    fun getSponsor(
        @Path("sponsorId") sponsorId: String?
    ): Flow<ApiResponse<ApiResponseData<SponsorModel>>>

//    @GET("sponsors/institutions/{institutionId}")
//    fun getSponsors(
//        @Path("institutionId") institutionId: String?
//    ): Flow<ApiResponse<ApiResponseData<MutableList<SponsorModel>>>>

    @GET("sponsors/cities/{cityId}/groups")
    fun getSponsorsByCity(
        @Path("cityId") cityId: String?
    ): Flow<ApiResponse<ApiResponseData<MutableList<SponsorDto>>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): SponsorService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(SponsorService::class.java)
        }
    }
}