package com.lavsystems.mycityssports.data.network.adapter

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.awaitResponse
import java.lang.reflect.Type

class FlowCallAdapter<T : Any>(
    private val responseType: Type
) : CallAdapter<T, Flow<ApiResponse<T>>> {

    override fun responseType() = responseType

    //@ExperimentalCoroutinesApi
    override fun adapt(call: Call<T>): Flow<ApiResponse<T>> = flow {

        val response = call.awaitResponse()
        println(response)
        emit(ApiResponse.create(response))

    }.catch { error ->
        println(error)
        emit(ApiResponse.create(error))
    }
}