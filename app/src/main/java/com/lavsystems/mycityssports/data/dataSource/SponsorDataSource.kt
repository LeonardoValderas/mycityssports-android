package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

interface SponsorDataSource {
    fun getFromApi(sponsorId: String): Flow<ApiResponse<ApiResponseData<SponsorModel>>>
    fun getSponsorsByCity() : Flow<ApiResponse<ApiResponseData<MutableList<SponsorDto>>>>
}