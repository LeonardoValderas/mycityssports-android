package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.request.FcmRequest
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface FcmService {

    @POST("fcms")
    fun saveFcmModel(
        @Body fcmRequest: FcmRequest
    ): Flow<ApiResponse<ApiResponseData<FcmModel>>>

    @PUT("fcms/{fcmId}")
    fun updateFcmModel(
        @Path("fcmId") fcmId: String?,
        @Body fcmModel: FcmModel
    ): Flow<ApiResponse<ApiResponseData<FcmModel>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): FcmService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(FcmService::class.java)
        }
    }
}