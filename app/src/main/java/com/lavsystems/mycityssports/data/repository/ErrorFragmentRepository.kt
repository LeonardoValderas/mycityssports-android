package com.lavsystems.mycityssports.data.repository


interface ErrorFragmentRepository {
    //fun sendBundleErrorToAnalytic(bundle: Bundle?)
    fun sendErrorToAnalytic(stackTrace: String)
}