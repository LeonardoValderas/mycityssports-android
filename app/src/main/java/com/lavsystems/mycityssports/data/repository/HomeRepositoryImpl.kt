package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.dataSource.HomeDataSource
import com.lavsystems.mycityssports.data.db.dao.SponsorDao
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.model.VersionAppModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
class HomeRepositoryImpl(
    private val dataSource: HomeDataSource
): HomeRepository {
    override fun getHomeData(): Flow<Resource<ApiResponseData<HomeItemDto>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getHomeData() },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun removeCity(): Flow<Boolean> {
        return dataSource.removeCity()
    }

    override fun removeCountryAndCity(): Flow<Boolean> {
        return dataSource.removeCountryAndCity()
    }

    override fun saveInstitutionId(institutionId: String): Flow<Boolean> {
        return dataSource.saveInstitutionId(institutionId)
    }

    override fun getInstitutionId(): Flow<String?> {
        return dataSource.getInstitutionId()
    }

    override fun saveSportId(sportId: String): Flow<Boolean> {
        return dataSource.saveSportId(sportId)
    }

    override fun getSportId(): Flow<String?> {
        return dataSource.getSportId()
    }

    override fun removeSportId(): Flow<Boolean> {
        return dataSource.removeSportId()
    }

    override fun saveTournamentId(tournamentId: String): Flow<Boolean> {
        return dataSource.saveTournamentId(tournamentId)
    }

    override fun getTournamentId(): Flow<String?> {
        return dataSource.getTournamentId()
    }

    override fun removeTournamentId(): Flow<Boolean> {
        return dataSource.removeTournamentId()
    }

    override fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Flow<Boolean> {
        return dataSource.saveDivisionSubDivision(divisionSubDivision)
    }

    override fun getDivisionSubDivision(): Flow<DivisionSubDivisionModel?> {
        return dataSource.getDivisionSubDivision()
    }

    override fun removeDivisionSubDivision(): Flow<Boolean> {
        return dataSource.removeDivisionSubDivision()
    }

    override fun showHomeInformationAlert(): Flow<Boolean> {
        return dataSource.showHomeInformationAlert()
    }

    override fun homeInformationAlertDisplayed(): Flow<Boolean> {
        return dataSource.homeInformationAlertDisplayed()
    }

    override fun needUpdateVersionApp(): Flow<Resource<ApiResponseData<VersionAppModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.needUpdateVersionApp() },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}
