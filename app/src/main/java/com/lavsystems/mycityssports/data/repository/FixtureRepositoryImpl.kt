package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.FixtureDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class FixtureRepositoryImpl(private val dataSource: FixtureDataSource) :
    FixtureRepository {

    override fun getFixtures(isRefresh: Boolean
    ): Flow<Resource<FixtureModel>> {
        if(dataSource.preferencesIsEmpty())
            return flowOf(Resource.success(null))

        return networkBoundResource(
            fetchFromLocal = { dataSource.getFromLocal() },
            shouldFetchFromRemote = {  it == null },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ -> },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }
}
