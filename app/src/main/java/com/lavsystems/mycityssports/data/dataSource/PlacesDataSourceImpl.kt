package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.service.PlaceService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import kotlinx.coroutines.flow.*
import org.koin.java.KoinJavaComponent.get


class PlacesDataSourceImpl(
    //private val service: PlaceService,
    private val preferences: PreferencesRepository
) : PlacesDataSource, BaseRepository(preferences) {
    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<PlaceDto>>> {
         val service = get(PlaceService::class.java)
        return service.getPlacesData()
    }

    override fun saveCity(city: CityModel): Flow<Boolean> {
        return preferences.saveCity(city)
    }

    override fun saveCountry(country: CountryModel): Flow<Boolean> {
        return preferences.saveCountry(country)
    }
}