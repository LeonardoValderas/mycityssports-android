package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsDto(
    val news: MutableList<NewsModel>,
    val sponsors: MutableList<SponsorModel>
){
    constructor(): this(arrayListOf(), arrayListOf())
}