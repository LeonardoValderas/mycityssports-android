package com.lavsystems.mycityssports.data.network.exception

enum class TypeErrorEnum {
    REQUEST,
    INTERNET,
    RESPONSE_STATUS,
    RESPONSE_NULL,
    TIME_OUT,
    ID_INSTITUTION_NULL,
    ERROR,
    HTTP_500_599,
    HTTP_400_499
}