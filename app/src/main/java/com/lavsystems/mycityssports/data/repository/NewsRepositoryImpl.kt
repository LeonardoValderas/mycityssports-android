package com.lavsystems.mycityssports.data.repository

import android.util.Log
import com.lavsystems.mycityssports.data.dataSource.NewsDataSource
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class NewsRepositoryImpl(private val dataSource: NewsDataSource) : NewsRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getNews(isRefresh: Boolean): Flow<Resource<NewsDto>> {
        if(dataSource.preferencesIsEmpty())
            return flowOf(Resource.success(NewsDto()))

        return networkBoundResource(
            fetchFromLocal = {
                dataSource.getFromLocal()
            },
            shouldFetchFromRemote = {
                it == null
            },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                    dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ ->

            },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }
}
