package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import kotlinx.coroutines.flow.Flow

interface PlacesDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<PlaceDto>>>
    fun saveCity(city: CityModel):Flow<Boolean>
    fun saveCountry(country: CountryModel):Flow<Boolean>
}