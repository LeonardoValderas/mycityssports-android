package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.SplashDataSource
import kotlinx.coroutines.flow.Flow

class SplashRepositoryImpl(private val dataSource: SplashDataSource) : SplashRepository {
//    override fun cityExists(): Flow<Boolean> {
//        return dataSource.cityExists()
//    }

    override fun countryAndCityExists(): Flow<Boolean> {
        return dataSource.countryAndCityExists()
    }
}