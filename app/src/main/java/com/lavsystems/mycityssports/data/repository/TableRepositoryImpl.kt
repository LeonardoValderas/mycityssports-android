package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.TableDataSource
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class TableRepositoryImpl(private val dataSource: TableDataSource) :
    TableRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getTable(isRefresh: Boolean
    ): Flow<Resource<TableDto>> {
        if(dataSource.preferencesIsEmpty())
            return flowOf(Resource.success(null))

        return networkBoundResource(
            fetchFromLocal = { dataSource.getFromLocal() },
            shouldFetchFromRemote = {  it == null },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                println("save date ${it.data}")
                dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ -> },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }
}
