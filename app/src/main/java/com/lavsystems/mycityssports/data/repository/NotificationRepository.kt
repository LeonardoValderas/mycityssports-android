package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import kotlinx.coroutines.flow.Flow

interface NotificationRepository {
    fun getNotificationInstitutions(): Flow<Resource<ApiResponseData<MutableList<NotificationModel>>>>
    fun addNotificationInstitution(institutionId: String): Flow<Resource<ApiResponseData<NotificationModel>>>
    fun removeNotificationInstitution(institutionId: String): Flow<Resource<ApiResponseData<NotificationModel>>>
}