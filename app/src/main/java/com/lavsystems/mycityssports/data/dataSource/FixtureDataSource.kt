package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import kotlinx.coroutines.flow.Flow

interface FixtureDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<FixtureModel>>>
    fun getFromLocal(): Flow<FixtureModel?>
    fun saveLocal(fixture: FixtureModel?)
    fun removeLocal()
    fun preferencesIsEmpty(): Boolean

}