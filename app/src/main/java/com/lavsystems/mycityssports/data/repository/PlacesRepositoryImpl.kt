package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.dataSource.PlacesDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
class PlacesRepositoryImpl(
    private val dataSource: PlacesDataSource
) : PlacesRepository {

    override fun getCountries(): Flow<MutableList<CountryModel>> {
        return flowOf(
         mutableListOf(
                CountryModel(
                    id = "01",
                    name = "Argentina",
                    code = 1
                ),
                CountryModel(
                    id = "02",
                    name = "Brasil",
                    code = 2
                )
            )
        )
    }

    override fun getPlacesData(): Flow<Resource<ApiResponseData<PlaceDto>>> {

        return networkBoundResourceWithoutCache(
            fetchFromRemote = {
                dataSource.getFromApi()
                              },
            processRemoteResponse = {
                //(it.body as ApiResponseData).data
            },
            onFetchFailed = { e, s ->
                print(e)
            }
        ).flowOn(Dispatchers.IO)
    }

    override fun saveCity(city: CityModel): Flow<Boolean> {
        return dataSource.saveCity(city)
    }

    override fun saveCountry(country: CountryModel): Flow<Boolean> {
        return dataSource.saveCountry(country)
    }
}
