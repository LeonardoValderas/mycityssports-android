package com.lavsystems.mycityssports.data.network.request

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NotificationInstitutionRequest(
    val fcmId: String,
    val institutionId: String
)