package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import kotlinx.coroutines.flow.Flow

interface TableRepository {
    fun getTable(isRefresh: Boolean): Flow<Resource<TableDto>>
}