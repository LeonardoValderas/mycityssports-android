package com.lavsystems.mycityssports.data.network.api.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import java.lang.reflect.Type
import java.util.*


class FixtureItemConverters {
    var gson = Gson()

    @TypeConverter
    fun stringToFixtureItemList(data: String?): MutableList<FixtureItemModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<MutableList<FixtureItemModel?>?>() {}.type
        return gson.fromJson<MutableList<FixtureItemModel?>>(data, listType)
    }

    @TypeConverter
    fun fixtureItemListToString(fixtureItems: MutableList<FixtureItemModel?>?): String? {
        return gson.toJson(fixtureItems)
    }
}