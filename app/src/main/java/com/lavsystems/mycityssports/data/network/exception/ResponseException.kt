package com.lavsystems.mycityssports.data.network.exception

class ResponseException(private val codeStatus: Int, private val messageError: String): Throwable(){
    override val cause: Throwable?
        get() = Throwable(codeStatus.toString())

    override val message: String?
        get() = messageError
}