package com.lavsystems.mycityssports.data.dataSource

import kotlinx.coroutines.flow.Flow

interface SplashDataSource {
    //fun cityExists(): Flow<Boolean>
    fun countryAndCityExists(): Flow<Boolean>
}