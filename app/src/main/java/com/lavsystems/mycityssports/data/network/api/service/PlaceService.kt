package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface PlaceService {
    @GET("places")
    fun getPlacesData(): Flow<ApiResponse<ApiResponseData<PlaceDto>>>

    @GET("places/{cityId}")
    fun getCity(
        @Path("cityId") cityId: String?
    ): Flow<ApiResponse<CityModel>?>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): PlaceService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(PlaceService::class.java)
        }
    }
}