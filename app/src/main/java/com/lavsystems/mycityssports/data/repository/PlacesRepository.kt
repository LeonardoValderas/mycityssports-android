package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import kotlinx.coroutines.flow.Flow

interface PlacesRepository {
     fun getCountries(): Flow<MutableList<CountryModel>>
     fun getPlacesData(): Flow<Resource<ApiResponseData<PlaceDto>>>
     fun saveCity(city: CityModel): Flow<Boolean>
     fun saveCountry(country: CountryModel): Flow<Boolean>
}