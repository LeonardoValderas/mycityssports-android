package com.lavsystems.mycityssports.data.network.exception

import java.io.Serializable

interface ExceptionHandle: Serializable {
    fun getMessageIntResource(): Int
    fun getExceptionStackTrace(): String
}