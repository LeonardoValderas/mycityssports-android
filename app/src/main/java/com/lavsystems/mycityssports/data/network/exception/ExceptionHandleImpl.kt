package com.lavsystems.mycityssports.data.network.exception

import android.database.sqlite.SQLiteConstraintException
import com.lavsystems.mycityssports.R
import kotlinx.coroutines.TimeoutCancellationException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownServiceException

class ExceptionHandleImpl(private val t: Throwable) : ExceptionHandle {
    private fun getTypeError(): TypeErrorEnum {
        return when(t){
            is IllegalStateException -> TypeErrorEnum.ERROR
            is NoConnectivityException -> TypeErrorEnum.INTERNET
            is SocketTimeoutException -> TypeErrorEnum.INTERNET
            is SQLiteConstraintException -> TypeErrorEnum.ERROR
            is ConnectException -> TypeErrorEnum.ERROR
            is UnknownServiceException -> TypeErrorEnum.ERROR
            is TimeoutCancellationException -> TypeErrorEnum.TIME_OUT
            is HttpException -> {
                if(t.code() in 500..599){
                    TypeErrorEnum.HTTP_500_599
                } else {
                    TypeErrorEnum.HTTP_400_499
                }
            }
            else -> TypeErrorEnum.ERROR
        }
    }

    private fun getTypeErrorEnum(): TypeErrorEnum {
        return getTypeError()
    }

    override fun getMessageIntResource(): Int {
        return when (getTypeErrorEnum()) {
            TypeErrorEnum.INTERNET -> R.string.without_internet
            TypeErrorEnum.RESPONSE_STATUS -> R.string.known_error
            TypeErrorEnum.HTTP_400_499 -> R.string.response_400
            TypeErrorEnum.HTTP_500_599 -> R.string.response_500
            TypeErrorEnum.RESPONSE_NULL -> R.string.response_null_error
            TypeErrorEnum.REQUEST -> R.string.request_error
            TypeErrorEnum.TIME_OUT -> R.string.request_timeout_error
            TypeErrorEnum.ID_INSTITUTION_NULL-> R.string.known_error
            else -> R.string.known_error
        }
    }

    override fun getExceptionStackTrace(): String {
        return t.stackTraceToString()
    }

}