package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.api.service.FixtureService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

class FixtureDetailsDataSourceImpl(
    private val service: FixtureService,
    private val preferences: PreferencesRepository
): FixtureDetailsDataSource{
    override fun getFromApi(fixtureId: String): Flow<ApiResponse<ApiResponseData<FixtureItemModel>>> {
        return service.getFixture(fixtureId)
    }

    override fun getCity(): Flow<CityModel?> {
        return preferences.getCity().flowOn(Dispatchers.IO)
    }
}