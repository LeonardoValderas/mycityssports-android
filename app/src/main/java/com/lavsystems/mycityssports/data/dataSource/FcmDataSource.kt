package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.flow.Flow

interface FcmDataSource {
    fun getFcmModel(): Flow<FcmModel?>
    fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean>
    fun saveToken(fcmModel: FcmModel): Flow<ApiResponse<ApiResponseData<FcmModel>>>
    fun updateToken(fcmModel: FcmModel): Flow<ApiResponse<ApiResponseData<FcmModel>>>
    fun getCountry(): CountryModel?
}