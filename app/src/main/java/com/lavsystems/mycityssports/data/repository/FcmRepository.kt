package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.flow.Flow

interface FcmRepository {
    fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean>
    fun getFcmModel(): Flow<FcmModel?>
    fun saveFcmToken(fcmModel: FcmModel): Flow<Resource<ApiResponseData<FcmModel>>>
    fun updateFcmToken(fcmModel: FcmModel): Flow<Resource<ApiResponseData<FcmModel>>>
    fun getCurrentCountry(): CountryModel?
}