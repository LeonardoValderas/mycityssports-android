package com.lavsystems.mycityssports.data.network.api.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lavsystems.mycityssports.mvvm.model.FixtureZoneItemModel
import java.lang.reflect.Type
import java.util.*

class FixtureZoneItemConverters {
    var gson = Gson()

    @TypeConverter
    fun stringToFixtureZoneItemList(data: String?): MutableList<FixtureZoneItemModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<MutableList<FixtureZoneItemModel?>?>() {}.type
        return gson.fromJson<MutableList<FixtureZoneItemModel?>>(data, listType)
    }

    @TypeConverter
    fun fixtureZoneItemListToString(fixtureItemModels: MutableList<FixtureZoneItemModel?>?): String? {
        return gson.toJson(fixtureItemModels)
    }
}