package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.model.VersionAppModel
import kotlinx.coroutines.flow.Flow

interface HomeDataSource {
    fun getHomeData(): Flow<ApiResponse<ApiResponseData<HomeItemDto>>>
    fun removeCity(): Flow<Boolean>
    fun removeCountryAndCity(): Flow<Boolean>
    fun removeTournamentId(): Flow<Boolean>
    fun saveInstitutionId(institutionId: String): Flow<Boolean>
    fun getInstitutionId(): Flow<String?>
    fun getSportId(): Flow<String?>
    fun removeSportId(): Flow<Boolean>
    fun saveSportId(sportId: String): Flow<Boolean>
    fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Flow<Boolean>
    fun getDivisionSubDivision(): Flow<DivisionSubDivisionModel?>
    fun removeDivisionSubDivision(): Flow<Boolean>
    fun saveTournamentId(tournamentId: String): Flow<Boolean>
    fun getTournamentId(): Flow<String?>
    fun showHomeInformationAlert(): Flow<Boolean>
    fun homeInformationAlertDisplayed(): Flow<Boolean>
    fun needUpdateVersionApp(): Flow<ApiResponse<ApiResponseData<VersionAppModel>>>
}