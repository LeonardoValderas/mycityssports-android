package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SanctionDto(
    val sanctions: MutableList<SanctionModel>,
    val sponsors: MutableList<SponsorModel>
){
    constructor(): this(mutableListOf(), mutableListOf())
}