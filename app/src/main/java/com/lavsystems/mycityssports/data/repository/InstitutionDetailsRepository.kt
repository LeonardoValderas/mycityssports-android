package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

interface InstitutionDetailsRepository {
    fun getInstitution(institutionId: String): Flow<Resource<ApiResponseData<InstitutionModel>>>
}