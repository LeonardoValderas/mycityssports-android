package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

interface MyCitysSportsContactDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<MyCitysSportsContactModel>>>
}