package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.FcmDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class FcmRepositoryImpl(private val dataSource: FcmDataSource) :
    FcmRepository {

    override fun saveFcmModel(fcmModel: FcmModel): Flow<Boolean> {
        return dataSource.saveFcmModel(fcmModel)
    }

    override fun getFcmModel(): Flow<FcmModel?> {
        return dataSource.getFcmModel()
    }

    override fun saveFcmToken(fcmModel: FcmModel): Flow<Resource<ApiResponseData<FcmModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.saveToken(fcmModel) },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun updateFcmToken(fcmModel: FcmModel): Flow<Resource<ApiResponseData<FcmModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.updateToken(fcmModel) },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun getCurrentCountry(): CountryModel? {
        return dataSource.getCountry()
    }
}
