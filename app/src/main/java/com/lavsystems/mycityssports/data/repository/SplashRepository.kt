package com.lavsystems.mycityssports.data.repository

import kotlinx.coroutines.flow.Flow

interface SplashRepository {
    //fun cityExists(): Flow<Boolean>
    fun countryAndCityExists(): Flow<Boolean>
}