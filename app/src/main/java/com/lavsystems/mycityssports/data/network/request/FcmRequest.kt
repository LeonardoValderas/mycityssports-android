package com.lavsystems.mycityssports.data.network.request

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FcmRequest(
    val token: String,
    val fcmId: String
)