package com.lavsystems.mycityssports.data.network.exception

import java.io.IOException

class NoConnectivityException: IOException()