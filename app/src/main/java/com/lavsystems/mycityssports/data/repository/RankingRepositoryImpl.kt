package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.RankingDataSource
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class RankingRepositoryImpl(private val dataSource: RankingDataSource) :
    RankingRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getRanking(isRefresh: Boolean
    ): Flow<Resource<RankingDto>> {
        if(dataSource.preferencesIsEmpty())
            return flowOf(Resource.success(null))

        return networkBoundResource(
            fetchFromLocal = { dataSource.getFromLocal() },
            shouldFetchFromRemote = {  it == null },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ -> },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }
}
