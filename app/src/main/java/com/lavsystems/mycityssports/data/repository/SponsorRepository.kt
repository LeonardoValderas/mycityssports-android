package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

interface SponsorRepository {
    fun getSponsor(sponsorId: String): Flow<Resource<ApiResponseData<SponsorModel>>>
    fun getSponsorsByCity(): Flow<Resource<ApiResponseData<MutableList<SponsorDto>>>>
}