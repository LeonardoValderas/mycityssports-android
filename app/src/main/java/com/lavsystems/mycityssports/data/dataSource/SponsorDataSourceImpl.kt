package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.service.SponsorService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow

class SponsorDataSourceImpl(
    private val service: SponsorService,
    preferences: PreferencesRepository
): SponsorDataSource, BaseRepository(preferences) {
    override fun getFromApi(sponsorId: String): Flow<ApiResponse<ApiResponseData<SponsorModel>>> {
        return service.getSponsor(sponsorId)
    }

    override fun getSponsorsByCity(): Flow<ApiResponse<ApiResponseData<MutableList<SponsorDto>>>> {
        return service.getSponsorsByCity(getPreferencesData().cityId)
    }
}