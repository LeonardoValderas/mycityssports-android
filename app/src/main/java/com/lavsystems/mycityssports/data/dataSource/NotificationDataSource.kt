package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import kotlinx.coroutines.flow.Flow

interface NotificationDataSource {
    fun getNotificationInstitutions(): Flow<ApiResponse<ApiResponseData<MutableList<NotificationModel>>>>
    fun addNotificationInstitution(institutionId: String): Flow<ApiResponse<ApiResponseData<NotificationModel>>>
    fun removeNotificationInstitution(institutionId: String): Flow<ApiResponse<ApiResponseData<NotificationModel>>>
    fun preferencesIsEmpty(): Boolean
    fun isInstitutionOrCityEmpty(): Boolean
}