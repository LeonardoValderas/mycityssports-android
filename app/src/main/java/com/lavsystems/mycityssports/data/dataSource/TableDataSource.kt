package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.TableModel
import kotlinx.coroutines.flow.Flow

interface TableDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<TableDto>>>
    fun getFromLocal(): Flow<TableDto?>
    fun saveLocal(table: TableDto?)
    fun removeLocal()
    fun preferencesIsEmpty(): Boolean
}