package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.NotificationDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class NotificationRepositoryImpl(private val dataSource: NotificationDataSource) : NotificationRepository {

    override fun getNotificationInstitutions(): Flow<Resource<ApiResponseData<MutableList<NotificationModel>>>> {
        if(dataSource.isInstitutionOrCityEmpty())
            return flowOf(Resource.success(ApiResponseData(mutableListOf())))

        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getNotificationInstitutions() },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun addNotificationInstitution(institutionId: String): Flow<Resource<ApiResponseData<NotificationModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.addNotificationInstitution(institutionId) },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun removeNotificationInstitution(institutionId: String): Flow<Resource<ApiResponseData<NotificationModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.removeNotificationInstitution(institutionId) },
            processRemoteResponse = {
            },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}
