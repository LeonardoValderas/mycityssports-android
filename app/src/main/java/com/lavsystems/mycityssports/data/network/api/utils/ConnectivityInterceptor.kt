package com.lavsystems.mycityssports.data.network.api.utils

import okhttp3.Interceptor

interface ConnectivityInterceptor: Interceptor