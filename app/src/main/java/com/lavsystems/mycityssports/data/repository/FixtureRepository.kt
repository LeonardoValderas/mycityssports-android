package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.flow.Flow

interface FixtureRepository {
    fun getFixtures(isRefresh: Boolean): Flow<Resource<FixtureModel>>
}