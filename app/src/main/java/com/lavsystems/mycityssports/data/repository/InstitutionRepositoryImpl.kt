package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.InstitutionDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn

class InstitutionRepositoryImpl(private val dataSource: InstitutionDataSource) : InstitutionRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getInstitutions(isRefresh: Boolean): Flow<Resource<MutableList<InstitutionModel>>> {
        if(dataSource.isInstitutionOrCityEmpty())
            return flowOf(Resource.success(mutableListOf()))

        return networkBoundResource(
            fetchFromLocal = {
                dataSource.getFromLocal()
            },
            shouldFetchFromRemote = {
                it == null
            },
            fetchFromRemote = { dataSource.getFromApi() },
            processRemoteResponse = { },
            saveRemoteData = {
                    dataSource.saveLocal(it.data)
            },
            onFetchFailed = { _, _ ->

            },
            isRefresh = isRefresh
        ).flowOn(Dispatchers.IO)
    }
}
