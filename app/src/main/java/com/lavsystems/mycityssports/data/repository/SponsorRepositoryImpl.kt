package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.SponsorDataSource
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class SponsorRepositoryImpl(private val dataSource: SponsorDataSource) : SponsorRepository {
    override fun getSponsor(sponsorId: String): Flow<Resource<ApiResponseData<SponsorModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getFromApi(sponsorId) },
            processRemoteResponse = { },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun getSponsorsByCity(): Flow<Resource<ApiResponseData<MutableList<SponsorDto>>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getSponsorsByCity() },
            processRemoteResponse = { },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

}