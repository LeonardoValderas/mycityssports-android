package com.lavsystems.mycityssports.data.network.resource


import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
inline fun <REMOTE> networkBoundResourceWithoutCache(
    crossinline fetchFromRemote: () -> Flow<ApiResponse<REMOTE>>,
    crossinline processRemoteResponse: (response: ApiSuccessResponse<REMOTE>) -> Unit = { Unit },
    crossinline onFetchFailed: (errorBody: String?, statusCode: Int) -> Unit = { _: String?, _: Int -> Unit }
) = flow<Resource<REMOTE>> {

    fetchFromRemote().onStart {
        emit(Resource.loading())
    }.catch { e ->
        Resource.error(
            e.message ?: "Error desconocido",
            e,
            null,
            0
        )
    }.onCompletion {
       // emit(Resource.loading(false))
    }.collect { apiResponse ->
        when (apiResponse) {
            is ApiSuccessResponse -> {
                processRemoteResponse(apiResponse)
                apiResponse.body?.let {
                    emit(Resource.success(it))
                }
            }

            is ApiErrorResponse -> {
                onFetchFailed(apiResponse.errorMessage, apiResponse.statusCode)
                emit(
                    Resource.error(
                        apiResponse.errorMessage,
                        null,
                        null,
                        apiResponse.statusCode
                    )
                )
            }
            else -> {}
        }
    }
}