package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.api.service.HomeService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.VersionAppModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.koin.java.KoinJavaComponent.get

class HomeDataSourceImpl(
    //private val service: HomeService,
    private val preferences: PreferencesRepository
) : HomeDataSource, BaseRepository(preferences) {

    override fun getHomeData(): Flow<ApiResponse<ApiResponseData<HomeItemDto>>> {
        val service = get(HomeService::class.java)
        val id: String? = getCity()?.id
        return service.getHomeData(id ?: "")
    }

    override fun removeCity(): Flow<Boolean> {
        return preferences.removeCity()
    }

    override fun removeCountryAndCity(): Flow<Boolean> {
        return preferences.removeCountryAndCity()
    }

    override fun removeTournamentId(): Flow<Boolean> {
        return preferences.removeTournamentId()
    }

    override fun saveInstitutionId(institutionId: String): Flow<Boolean> {
        return preferences.saveInstitutionId(institutionId)
    }

    override fun getInstitutionId(): Flow<String?> {
        return preferences.getInstitutionId()
    }

    override fun getSportId(): Flow<String?> {
        return preferences.getSportId()
    }

    override fun removeSportId(): Flow<Boolean> {
        return preferences.removeSportId()
    }

    override fun saveSportId(sportId: String): Flow<Boolean> {
        return preferences.saveSportId(sportId)
    }

    override fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Flow<Boolean> {
        return preferences.saveDivisionSubDivision(divisionSubDivision)
    }

    override fun getDivisionSubDivision(): Flow<DivisionSubDivisionModel?> {
        return preferences.getDivisionSubDivision()
    }

    override fun removeDivisionSubDivision(): Flow<Boolean> {
        return preferences.removeDivisionSubDivision()
    }

    override fun saveTournamentId(tournamentId: String): Flow<Boolean> {
        return preferences.saveTournamentId(tournamentId)
    }

    override fun getTournamentId(): Flow<String?> {
        return preferences.getTournamentId()
    }

    override fun showHomeInformationAlert(): Flow<Boolean> {
        return preferences.showHomeInformationAlert()
    }

    override fun homeInformationAlertDisplayed(): Flow<Boolean> {
        return preferences.homeInformationAlertDisplayed()
    }

    override fun needUpdateVersionApp(): Flow<ApiResponse<ApiResponseData<VersionAppModel>>> {
        val service = get(HomeService::class.java)
        return service.getVersionApp()
    }

    private fun getCity(): CityModel? {
        var city: CityModel? = null
        runBlocking {
            preferences.getCity()
                .flowOn(Dispatchers.IO)
                .collect{
                    city = it
                }
        }

        return city
    }
}