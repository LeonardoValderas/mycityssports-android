package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.network.api.service.NotificationService
import com.lavsystems.mycityssports.data.network.request.NotificationInstitutionRequest
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import kotlinx.coroutines.flow.*

class NotificationDataSourceImpl(
    private val service: NotificationService,
    preferences: PreferencesRepository
) : NotificationDataSource, BaseRepository(preferences) {

    override fun getNotificationInstitutions(): Flow<ApiResponse<ApiResponseData<MutableList<NotificationModel>>>> {
        val preferencesData = getPreferencesData()
        return service.getNotificationInstitutions(
            preferencesData.fcmId,
            preferencesData.cityId
        )
    }

    override fun addNotificationInstitution(institutionId: String): Flow<ApiResponse<ApiResponseData<NotificationModel>>> {
        val preferencesData = getPreferencesData()
        return service.addNotificationInstitution(
            NotificationInstitutionRequest(preferencesData.fcmId, institutionId)
        )
    }

    override fun removeNotificationInstitution(institutionId: String): Flow<ApiResponse<ApiResponseData<NotificationModel>>> {
        val preferencesData = getPreferencesData()
        return service.removeNotificationInstitution(
            NotificationInstitutionRequest(preferencesData.fcmId, institutionId)
        )
    }
}