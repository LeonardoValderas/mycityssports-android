package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface RankingService {

    @GET("rankings/cities/{cityId}/institutions/{institutionId}/sports/{sportId}/tournaments/{tournamentId}/divisions/{divisionId}/{subDivision}")
    fun getRanking(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("sportId") sportId: String?,
        @Path("tournamentId") tournamentId: String?,
        @Path("divisionId") divisionId: String?,
        @Path("subDivision") subDivision: String?,
    ): Flow<ApiResponse<ApiResponseData<RankingDto>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): RankingService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(RankingService::class.java)
        }
    }
}