package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface SanctionService {

    @GET("sanctions/cities/{cityId}/institutions/{institutionId}/tournaments/{tournamentId}/sports/{sportId}/divisions/{divisionId}/{subDivision}")
    fun getSanctions(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("tournamentId") tournamentId: String?,
        @Path("sportId") sportId: String?,
        @Path("divisionId") divisionId: String?,
        @Path("subDivision") subDivision: String?,
    ): Flow<ApiResponse<ApiResponseData<SanctionDto>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): SanctionService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(SanctionService::class.java)
        }
    }
}