package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.flow.Flow

interface SanctionDataSource {
    fun getFromApi(): Flow<ApiResponse<ApiResponseData<SanctionDto>>>
    fun getFromLocal(): Flow<SanctionDto?>
    fun saveLocal(sanctionDto: SanctionDto?)
    fun preferencesIsEmpty(): Boolean
    fun removeLocal()
}