package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.network.api.service.MyCitysSportsContactService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.flow.Flow

class MyCitysSportsContactDataSourceImpl(
    private val service: MyCitysSportsContactService
): MyCitysSportsContactDataSource {
    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<MyCitysSportsContactModel>>> {
        return service.getContact()
    }
}