package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankingDto(
    val ranking: RankingModel?,
    val sponsors: MutableList<SponsorModel>
){
    constructor(): this(RankingModel(), arrayListOf())
}