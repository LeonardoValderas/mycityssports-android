package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.flow.Flow

interface InstitutionRepository {
    fun getInstitutions(isRefresh: Boolean): Flow<Resource<MutableList<InstitutionModel>>>
}