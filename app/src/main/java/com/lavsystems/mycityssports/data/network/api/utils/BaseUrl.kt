package com.lavsystems.mycityssports.data.network.api.utils

interface BaseUrl {
    fun getProductionBaseUrl(): String
    fun getLocalHostBaseUrl(): String
}