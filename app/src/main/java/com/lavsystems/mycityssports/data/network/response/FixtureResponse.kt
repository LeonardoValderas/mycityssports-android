package com.lavsystems.mycityssports.data.network.response

import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixtureResponse(
    val code: Int,
    val success: Boolean,
    val message: String,
    val models: FixtureModel?,
    val error: String?
)