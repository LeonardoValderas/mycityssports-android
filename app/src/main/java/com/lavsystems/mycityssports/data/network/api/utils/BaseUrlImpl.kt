package com.lavsystems.mycityssports.data.network.api.utils

import com.lavsystems.mycityssports.utils.preferences.Preferences

class BaseUrlImpl(private val preferences: Preferences): BaseUrl {
    override fun getProductionBaseUrl(): String {
        preferences.getCountry()?.let {
            return if(it.code == 1)  "https://mcs-dash-be-prod.herokuapp.com/omapi/" else  "https://mcs-dash-br-be-prod.herokuapp.com/omapi/"
        }
        return ""
    }

    override fun getLocalHostBaseUrl(): String {
        preferences.getCountry()?.let {
            return if(it.code == 1)  "http://10.0.2.2:3003/omapi/" else "http://10.0.2.2:3004/omapi/"
        }
        return ""
    }
}