package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface MyCitysSportsContactService {

    @GET("mcs/contacts")
    fun getContact(): Flow<ApiResponse<ApiResponseData<MyCitysSportsContactModel>>>
    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): MyCitysSportsContactService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(MyCitysSportsContactService::class.java)
        }
    }
}