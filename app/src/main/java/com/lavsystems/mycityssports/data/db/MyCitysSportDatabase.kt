package com.lavsystems.mycityssports.data.db

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lavsystems.mycityssports.data.db.dao.SponsorDao

//@Database(entities = [SponsorModel::class], version = 1, exportSchema = true)
abstract class MyCitysSportDatabase : RoomDatabase() {

    abstract fun sponsorDao(): SponsorDao

    companion object {
        @Volatile
        private var instance: MyCitysSportDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                MyCitysSportDatabase::class.java,
                "my_citys_sports.db"
            ).build()
    }
}