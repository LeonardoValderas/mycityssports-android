package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface InstitutionService {

    @GET("institutions/cities/{cityId}")
    fun getInstitutions(
        @Path("cityId") cityId: String?
    ): Flow<ApiResponse<ApiResponseData<MutableList<InstitutionModel>>>>

    @GET("institutions/{institutionId}")
    fun getInstitution(
        @Path("institutionId") institutionId: String?
    ): Flow<ApiResponse<ApiResponseData<InstitutionModel>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): InstitutionService {
            return Retrofit
                .invoke(connectivityInterceptor,baseUrl)
                .create(InstitutionService::class.java)
        }
    }
}