package com.lavsystems.mycityssports.data.network.response

import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixtureDetailsResponse(
    val code: Int,
    val success: Boolean,
    val message: String,
    val model: FixtureItemModel?,
    val error: String?
)