package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface FixtureService {

    @GET("fixtures/{fixtureId}")
    fun getFixture(
        @Path("fixtureId") fixtureId: String?
    ): Flow<ApiResponse<ApiResponseData<FixtureItemModel>>>

    @GET("fixtures/cities/{cityId}/institutions/{institutionId}/sports/{sportId}/tournaments/{tournamentId}/divisions/{divisionId}/{subDivision}")
    fun getFixturesData(
        @Path("cityId") cityId: String?,
        @Path("institutionId") institutionId: String?,
        @Path("sportId") sportId: String?,
        @Path("tournamentId") tournamentId: String?,
        @Path("divisionId") divisionId: String?,
        @Path("subDivision") subDivisions: String?,
    ): Flow<ApiResponse<ApiResponseData<FixtureModel>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): FixtureService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(FixtureService::class.java)
        }
    }
}