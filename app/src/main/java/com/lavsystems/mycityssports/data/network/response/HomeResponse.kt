package com.lavsystems.mycityssports.data.network.response

import com.lavsystems.mycityssports.data.dto.HomeItemDto

data class HomeResponse(
    val success: Boolean,
    val message: String,
    val models: HomeItemDto?,
    val error: String?
)