package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.api.service.NewsService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.*

class NewsDataSourceImpl(
    private val service: NewsService,
    preferences: PreferencesRepository
) : NewsDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<NewsDto>>> {
        val preferencesData = getPreferencesDataOrThrowException()
        return service.getNews(
            preferencesData.cityId,
            preferencesData.institutionId,
            preferencesData.tournamentId,
            preferencesData.sportId,
            preferencesData.divisionSubDivision.divisionId,
            preferencesData.divisionSubDivision.subDivisionName
        )
    }

    override fun getFromLocal(): Flow<NewsDto?> {
        getCache(NEWS)?.let {
            return flowOf(it as NewsDto)
        }

        return flowOf(null)
    }

    override fun saveLocal(news: NewsDto?) {
        news?.let {
            setCache(NEWS, it)
        } ?: run {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(NEWS)
    }
}