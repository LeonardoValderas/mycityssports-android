package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.InstitutionDataSource
import com.lavsystems.mycityssports.data.dataSource.InstitutionDetailsDataSource
import com.lavsystems.mycityssports.data.dataSource.SponsorDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class InstitutionDetailsRepositoryImpl(private val dataSource: InstitutionDetailsDataSource) : InstitutionDetailsRepository {
    override fun getInstitution(institutionId: String): Flow<Resource<ApiResponseData<InstitutionModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getFromApi(institutionId) },
            processRemoteResponse = { },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}