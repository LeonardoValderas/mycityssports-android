package com.lavsystems.mycityssports.data.network.response


import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiResponseData<T>(
    val code: Int,
    val success: Boolean,
    val message: String,
    val data: T?,
    val error: String?
){
    constructor(): this(200, true, "Success", null, null)
    constructor(data: T?): this(200, true, "Success", data, null)
}