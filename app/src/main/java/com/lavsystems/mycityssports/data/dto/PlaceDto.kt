package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.StateModel
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PlaceDto(
    val states: MutableList<StateModel>,
    val cities: MutableList<CityModel>)