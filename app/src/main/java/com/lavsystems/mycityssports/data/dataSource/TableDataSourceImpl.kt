package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseRepository
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.service.TableService
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.model.TableModel
import kotlinx.coroutines.flow.*

class TableDataSourceImpl(
    private val service: TableService,
    preferences: PreferencesRepository
): TableDataSource, BaseRepository(preferences) {

    override fun getFromApi(): Flow<ApiResponse<ApiResponseData<TableDto>>> {
        val preferencesData = getPreferencesDataOrThrowException()
        return service.getTable(
            preferencesData.cityId,
            preferencesData.institutionId,
            preferencesData.sportId,
            preferencesData.tournamentId,
            preferencesData.divisionSubDivision.divisionId,
            preferencesData.divisionSubDivision.subDivisionName
        )
    }

    override fun getFromLocal(): Flow<TableDto?> {
        getCache(TABLE)?.let {
            return flowOf(it as TableDto)
        }
        return flowOf(null)
    }

    override fun saveLocal(table: TableDto?) {
        table?.let {
            setCache(TABLE, it)
        } ?: run  {
            removeLocal()
        }
    }

    override fun removeLocal() {
        removeCache(TABLE)
    }
}