package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dataSource.FixtureDetailsDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResource
import com.lavsystems.mycityssports.data.network.resource.networkBoundResourceWithoutCache
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

@ExperimentalCoroutinesApi
class FixtureDetailsRepositoryImpl(
    private val dataSource: FixtureDetailsDataSource
) : FixtureDetailsRepository {

    override fun getCity(): Flow<CityModel?> {
        return dataSource.getCity()
    }

    override fun getFixtureDetails(fixtureId: String): Flow<Resource<ApiResponseData<FixtureItemModel>>> {
        return networkBoundResourceWithoutCache(
            fetchFromRemote = { dataSource.getFromApi(fixtureId) },
            processRemoteResponse = { },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}