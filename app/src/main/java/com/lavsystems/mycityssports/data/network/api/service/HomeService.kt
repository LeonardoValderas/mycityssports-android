package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.VersionAppModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface HomeService {

    @GET("homes/cities/{cityId}")
    fun getHomeData(@Path("cityId") cityId: String?): Flow<ApiResponse<ApiResponseData<HomeItemDto>>>

    @GET("versionApps")
    fun getVersionApp(): Flow<ApiResponse<ApiResponseData<VersionAppModel>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): HomeService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(HomeService::class.java)
        }
    }
}