package com.lavsystems.mycityssports.data.network.api.utils

enum class StatusResponse {
    SUCCESS,
    LOADING,
    ERROR,
    FAILURE
}