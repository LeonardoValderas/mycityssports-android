package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import kotlinx.coroutines.flow.Flow

interface NewsRepository {
    fun getNews(isRefresh: Boolean): Flow<Resource<NewsDto>>
}