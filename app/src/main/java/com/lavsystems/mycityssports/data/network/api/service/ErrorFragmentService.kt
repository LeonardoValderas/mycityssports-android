package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.HomeResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface ErrorFragmentService {

    @GET("homes")
    fun getHomeData(): Flow<ApiResponse<HomeResponse?>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): ErrorFragmentService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(ErrorFragmentService::class.java)
        }
    }
}