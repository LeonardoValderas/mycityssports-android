package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.utils.adapter.Page

data class LoadMoreFixtureDto(val idAdapter: String, val numberMatch: Int, val page: Page) {
    constructor(): this("", -1, Page())
}