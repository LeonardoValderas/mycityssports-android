package com.lavsystems.mycityssports.data.dto

import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SponsorDto(
    val institution: String,
    val sponsors: MutableList<SponsorModel>
): ExpandableRecyclerViewAdapter.ExpandableGroup<Any>(){
    constructor(): this("", mutableListOf())

    override fun getExpandingItems(): MutableList<Any> {
      return sponsors as MutableList<Any>
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SponsorDto

        if (institution != other.institution) return false
        if (sponsors != other.sponsors) return false

        return true
    }

    override fun hashCode(): Int {
        var result = institution.hashCode()
        result = 31 * result + sponsors.hashCode()
        return result
    }

    override fun asReversed() {
        sponsors.reverse()
    }
}