package com.lavsystems.mycityssports.data.network.api.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lavsystems.mycityssports.mvvm.model.FixtureInstanceItemModel
import java.lang.reflect.Type
import java.util.*

class FixtureInstanceItemConverters {
    var gson = Gson()

    @TypeConverter
    fun stringToFixtureInstanceItemList(data: String?): MutableList<FixtureInstanceItemModel?>? {
        if (data == null) {
            return Collections.emptyList()
        }
        val listType: Type =
            object : TypeToken<MutableList<FixtureInstanceItemModel?>?>() {}.type
        return gson.fromJson<MutableList<FixtureInstanceItemModel?>>(data, listType)
    }

    @TypeConverter
    fun fixtureInstanceItemListToString(fixtureItemModels: MutableList<FixtureInstanceItemModel?>?): String? {
        return gson.toJson(fixtureItemModels)
    }
}