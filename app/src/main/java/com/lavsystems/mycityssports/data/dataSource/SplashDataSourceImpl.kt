package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.Flow

class SplashDataSourceImpl(
    private val preferences: PreferencesRepository
): SplashDataSource {

//    override fun cityExists(): Flow<Boolean> {
//        return preferences.cityExists()
//    }

    override fun countryAndCityExists(): Flow<Boolean> {
        return preferences.countryAndCityExists()
    }
}