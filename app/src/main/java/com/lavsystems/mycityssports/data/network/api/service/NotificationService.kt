package com.lavsystems.mycityssports.data.network.api.service

import com.lavsystems.mycityssports.data.network.api.Retrofit
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.request.FcmRequest
import com.lavsystems.mycityssports.data.network.request.NotificationInstitutionRequest
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.*

interface NotificationService {

    @GET("notifications/fcms/{fcmId}/cities/{cityId}")
    fun getNotificationInstitutions(
        @Path("fcmId") fcmId: String?,
        @Path("cityId") cityId: String?
    ): Flow<ApiResponse<ApiResponseData<MutableList<NotificationModel>>>>

    @POST("notifications/fcms")
    fun addNotificationInstitution(
        @Body notificationInstitutionRequest: NotificationInstitutionRequest
    ): Flow<ApiResponse<ApiResponseData<NotificationModel>>>

    @PUT("notifications/fcms")
    fun removeNotificationInstitution(
        @Body notificationInstitutionRequest: NotificationInstitutionRequest
    ): Flow<ApiResponse<ApiResponseData<NotificationModel>>>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: BaseUrl
        ): NotificationService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(NotificationService::class.java)
        }
    }
}