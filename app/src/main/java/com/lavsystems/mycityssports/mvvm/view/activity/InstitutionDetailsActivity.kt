package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityInstitutionDetailsBinding
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntent
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.InstitutionDetailsViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.INSTITUTION_ID_BUDLE
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_HOME_PHONE
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_PHONE
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.permissions.Permissions
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class InstitutionDetailsActivity :
    BaseActivity<ActivityInstitutionDetailsBinding, InstitutionDetailsViewModel>(),
    OnMapReadyCallback {

    private val viewModel: InstitutionDetailsViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.activity_institution_details
    override fun getBindingVariable(): Int = BR.vm

    override fun initViewModel(): InstitutionDetailsViewModel = viewModel
    private lateinit var mMap: GoogleMap
    private var institutionModel: InstitutionModel? = null
    lateinit var contactIntent: InformationContactIntent

    override fun init() {
        initToolbar()
        getInstitutionIdFromIntentBundle()
        initAdView()
    }

    private fun initContactUtils(){
        institutionModel?.let {
            contactIntent = InformationContactIntentImpl(this@InstitutionDetailsActivity, InstitutionModel.WITHOUT_INFO)
        }
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = getString(R.string.institution_title)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun initAdView() {
        getRootDataBinding().adViewInstitutionDetails.loadAd(AdRequest.Builder().build())
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private fun getInstitutionIdFromIntentBundle() {
        getBundleIntent()?.let { bundle ->
            bundle.getString(INSTITUTION_ID_BUDLE, "")?.let {
                getViewModel().getInstitution(it)
            } ?: kotlin.run {
                closeActivityForError()
            }
        } ?: kotlin.run {
            closeActivityForError()
        }
    }


    override fun initObservers() {
        with(getViewModel()) {
            institution.observe(this@InstitutionDetailsActivity, { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let { i ->
                            institutionModel = i
                            initContactUtils()
                            institutionToBinding(i)
                        } ?: run {
                            closeActivityForError()
                        }
                        hideLoading()
                    }

                    StatusResponse.LOADING -> {
                        showLoading(getString(R.string.loading_institution_data_details))
                    }

                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            closeActivityForError()
                        } ?: run {
                            closeActivityForError()
                        }

                        hideLoading()
                    }
                    else -> {
                        closeActivityForError()
                    }
                }
            })
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun institutionToBinding(institution: InstitutionModel) {
        getRootDataBinding().model = institution
    }

    private fun closeActivityForError() {
        showToast(getString(R.string.institution_error))
        finish()
    }

    private fun getBundleIntent(): Bundle? {
        return intent?.extras
    }

    fun onClickHomePhone(view: View) {
        contactIntent.intentHomePhone(institutionModel?.phone)
    }

    fun onClickHome(view: View) {
        contactIntent.intentPhone(institutionModel?.mobile)
    }

    fun onClickWhatsapp(view: View) {
        contactIntent.intentWhatsapp(institutionModel?.mobile)
    }

    fun onClickEmail(view: View) {
        contactIntent.intentEmail(institutionModel?.email)
    }

    fun onClickFacebook(view: View) {
        contactIntent.intentFacebook(institutionModel?.facebook)
    }

    fun onClickInstagram(view: View) {
        contactIntent.intentInstagram(institutionModel?.instagram)
    }

    fun onClickWeb(view: View) {
        contactIntent.intentWeb(institutionModel?.web)
    }

    fun onClickMap(view: View) {
        contactIntent.intentMap(
            institutionModel?.address?.position,
            institutionModel?.name
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                PERMISSION_CALL_HOME_PHONE -> contactIntent.intentHomePhone(institutionModel?.phone)
                PERMISSION_CALL_PHONE -> contactIntent.intentPhone(institutionModel?.mobile)
            }
        }else {
            if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_DENIED) {
                when (requestCode) {
                    PERMISSION_CALL_PHONE,
                    PERMISSION_CALL_HOME_PHONE -> Permissions.shouldShowRequestPermissionRationale(
                        this,
                        R.id.cl_institution_details,
                        permissions.first(),
                        getString(R.string.permission_call_dialog_message))
                }
            }
        }
    }
}
