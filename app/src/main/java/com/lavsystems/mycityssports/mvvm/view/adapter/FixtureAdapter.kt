package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.data.dto.LoadMoreFixtureDto
import com.lavsystems.mycityssports.databinding.FixtureRecyclerItemBinding
import com.lavsystems.mycityssports.databinding.LoadMoreProgressBarBinding
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixture
import com.lavsystems.mycityssports.utils.adapter.*
import com.lavsystems.mycityssports.utils.listener.RecyclerOnScrollListener
import com.lavsystems.mycityssports.utils.uicomponents.RecyclerViewBuilder
import java.lang.IndexOutOfBoundsException

@Suppress("UNCHECKED_CAST")
class FixtureAdapter(private val listener: OnAdapterListenerFixture) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    RecyclerOnScrollListener<Any>, // pass adapter and page
    OnAdapterListener<Any> {

    private var fullFixtures = mutableListOf<Any>()
    private val adaptersMap = mutableMapOf<String, FixtureItemAdapterOld>()

    companion object {
        const val FULL_FIXTURE = 1
        const val LOADING = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            LOADING -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LoadMoreProgressBarBinding.inflate(layoutInflater, parent, false)
                ViewHolderLoading(binding)

            }
            else -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FixtureRecyclerItemBinding.inflate(layoutInflater, parent, false)
                ViewHolderFullFixture(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolderFullFixture -> holder.bind(fullFixtures[position] as FixtureItemModel)
        }
    }

    override fun getItemCount(): Int = fullFixtures.size

    override fun getItemViewType(position: Int): Int {
        return when (getItemModelFromFullFixture(position)) {
            is ProgressbarItem -> LOADING
            else -> FULL_FIXTURE
        }
    }

    private fun getItemModelFromFullFixture(int: Int): Any {
        return fullFixtures[int]
    }

    override fun getItemId(position: Int): Long {
        val fullFixture = getItemModelFromFullFixture(position)
        return when (fullFixture) {
            is FixtureItemModel -> fullFixture.round.toLong()
            else -> position.toLong()
        }
    }

    inner class ViewHolderFullFixture(private var binding: FixtureRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(fullFixture: FixtureItemModel) {
            with(binding) {
                model = fullFixture

                // YoYo.with(Techniques.FadeIn).playOn(root)
                //init fixture adapter for number match
                //
                // initFixture(binding.rvFixture, fullFixture)

                //loadSponsors(fullFixture.sponsors as MutableList<Any>)

                executePendingBindings()
            }
        }

//        private fun loadSponsors(sponsors: MutableList<Any>) {
//            val recycler = binding.rvSponsor
//            if (isSponsorPosition() && sponsors?.size > 0)
//                initSponsor(recycler, sponsors)
//            else
//                recycler.visibility = View.GONE
//        }

        private fun isSponsorPosition(): Boolean {
            return bindingAdapterPosition == 0 || bindingAdapterPosition % 6 == 0
        }

        private fun initFixture(recycler: RecyclerView,
                                fixtures: MutableList<FixtureModel>) {
            initRecyclerFixtureAdapter(recycler, fixtures as MutableList<Any>)
        }

        private fun initRecyclerFixtureAdapter(recyclerView: RecyclerView, fixtures: MutableList<Any>) {
            RecyclerViewBuilder
                .builder<Any>(recyclerView)
                .setLinearLayoutManagerOrientation(isHorizontal = true)
                .setDividerItemDecoration(isHorizontal = true)
                .setHasFixedSize(true)
                .setEndlessRecyclerOnScrollListener(Page(), 0, this@FixtureAdapter) // change to 5
                .setAdapter(FixtureItemAdapterOld(this@FixtureAdapter))
                .build().also {
                    if (fixtures.contains(AdMobItem()))
                        it.addItems(fixtures, false)
                    else
                        it.addItems(getFixturesWithAds(fixtures), false)
                }

        }

        private fun getFixturesWithAds(fixtures: MutableList<Any>): MutableList<Any> {
            val adsPosition = getAdsPosition()
            if (adsPosition != -1)
                fixtures.add(adsPosition, AdMobItem())

            return fixtures
        }

        private fun getAdsPosition(): Int {
            return when (bindingAdapterPosition) { //ver
                0 -> 3
                4 -> 2
                8 -> 1
                else -> -1
            }
        }

//        private fun initRecyclerSponsorAdapter(recyclerView: RecyclerView, sponsors: MutableList<Any>) {
//            recyclerView.visibility = View.VISIBLE
//            RecyclerViewBuilder
//                .builder<Any>(recyclerView)
//                .setLinearLayoutManagerOrientation(isHorizontal = true)
//                .setDividerItemDecoration(isHorizontal = true)
//                .setHasFixedSize(true)
//                .setHasStableIds(true)
//                .setAdapter(SponsorAdapter(this@FullFixtureAdapter))
//                .build().also {
//                    it.addItems(sponsors)
//                }
//        }

//        private fun initSponsor(recycler: RecyclerView,
//                                sponsors: MutableList<Any>) {
//            initRecyclerSponsorAdapter(recycler, sponsors)
//        }
    }

    fun addLoading() {
        if (itemCount > 0)
            addFooterLoading()
    }

    private fun removeLoadingIfExists() {
        try {
            val deleted = getFullFixtures().removeAll { t -> t is ProgressbarItem }
            if (deleted)
                notifyDataSetChanged()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    private fun addFooterLoading() {
        try {
            getFullFixtures().add(itemCount, ProgressbarItem())
            notifyItemInserted(itemCount)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    fun removeLoading() {
        if (itemCount > 0)
            removeLoadingIfExists()
    }

    override fun scrollUp(isUp: Boolean) {
        // in this case the scroll is horizontal. I dont use
        //listener.scrollUp(isUp)
    }

    override fun onLoadMore(page: Page, adapter: RecyclerView.Adapter<DataBindingViewHolder<Any>>?) {
        if (adapter is FixtureItemAdapterOld) {
            adapter.let {
                val id = it.id
                if (!containsAdapter(id))
                    adaptersMap.set(id, it)

                val loadMoreDto = LoadMoreFixtureDto(id, it.getNumberMatch(), page)
                listener.onLoadMore(loadMoreDto)
            }
        }

        // sponsor doesnt has end more scroll
    }

    private fun getFullFixtures(): MutableList<Any> {
        return fullFixtures
    }

    private fun containsAdapter(id: String): Boolean {
        return adaptersMap.containsKey(id)
    }

    fun addMoreFixtureForAdapter(idAdapter: String, fixtures: MutableList<Any>) {
        val adapter = adaptersMap.get(idAdapter)
        adapter?.apply {
            addItems(fixtures, false)
        }
    }

    fun notifyHasntMoreItems(idAdapter: String) {
        val adapter = getAdapterById(idAdapter)
        adapter?.apply {
            notifyHasntMoreItems()
        }
    }

    fun addFixtureFooterLoading(idAdapter: String) {
        val adapter = getAdapterById(idAdapter)

        adapter?.apply {
            showLoading()
            addFooter(ProgressbarItem())
        }
    }

    fun removeFixtureFooterLoading(idAdapter: String) {
        val adapter = getAdapterById(idAdapter)
        adapter?.apply {
            removeLoadingIfExists()
            hideLoading()
        }
    }

    private fun getAdapterById(idAdapter: String): FixtureItemAdapterOld? {
        return adaptersMap.get(idAdapter)
    }

    override fun onItemClick(view: View, position: Int, t: Any) {
        if (t is FixtureModel)
            listener.onItemFixture(view, position, t)
//        else if (t is SponsorModel)
//            listener.onItemSponsor(view, position, t)
    }

    inner class ViewHolderLoading(binding: LoadMoreProgressBarBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }
}