package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankingItemModel(
    @Json(name = "_id")
    val id: String,
    val position: Int,
    val team: TeamModel,
    val player: PlayerModel,
    val point: Int,
    val comment: String = "",
    val source: String = ""
) {
    constructor() : this("", 0, TeamModel(), PlayerModel(), 0)


    companion object {
        const val SOURCE = "Fuente: "
        const val COMMENT = "* "
    }

    val pointString: String
        get() = point.toString()

    val positionString: String
        get() = position.toString()

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source

    val commentFormatted: String
        get() = if(comment.isEmpty()) comment else COMMENT + comment

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RankingItemModel

        if (id != other.id) return false
        if (team != other.team) return false
        if (player != other.player) return false
        if (point != other.point) return false
        if (comment != other.comment) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + team.hashCode()
        result = 31 * result + player.hashCode()
        result = 31 * result + point
        result = 31 * result + source.hashCode()
        result = 31 * result + comment.hashCode()
        return result
    }
}


