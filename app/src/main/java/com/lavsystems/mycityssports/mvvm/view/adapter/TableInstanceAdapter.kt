package com.lavsystems.mycityssports.mvvm.view.adapter

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.ParentRowBinding
import com.lavsystems.mycityssports.databinding.TableInstanceItemBinding
import com.lavsystems.mycityssports.mvvm.model.TableInstanceItemModel
import com.lavsystems.mycityssports.mvvm.model.TableInstanceModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem

class TableInstanceAdapter(
    parents: MutableList<TableInstanceModel>,
    private val listener: OnAdapterListenerFixtureItem
) : ExpandableRecyclerViewAdapter<
        Any,
        TableInstanceModel,
        TableInstanceAdapter.PViewHolder,
        TableInstanceAdapter.CViewHolder<Any>>(
    parents,
    ExpandingDirection.VERTICAL,
    true,
    false
) {

    override fun onCreateParentViewHolder(parent: ViewGroup, viewType: Int): PViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ParentRowBinding.inflate(layoutInflater, parent, false)
        return PViewHolder(
            binding.root
        )
    }

    override fun onCreateChildViewHolder(child: ViewGroup, viewType: Int, position: Int): CViewHolder<Any> {
        val layoutInflater = LayoutInflater.from(child.context)
        val binding = TableInstanceItemBinding.inflate(layoutInflater, child, false)
        return CViewHolder(
            binding.root, binding
        )
    }

    override fun onBindParentViewHolder(
        parentViewHolder: PViewHolder,
        expandableType: TableInstanceModel,
        position: Int
    ) {
        parentViewHolder.containerView.findViewById<TextView>(R.id.tv_title).text = expandableType.tournamentInstance.name
        if (expandableType.isExpanded) {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.GONE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.VISIBLE
        } else {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.VISIBLE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.GONE
        }
    }

    override fun onBindChildViewHolder(
        childViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: TableInstanceModel,
        position: Int
    ) {
        when (expandedType) {
            is TableInstanceItemModel -> childViewHolder.bind(expandedType, listener)
            else -> childViewHolder.bindAdMob(expandedType)
        }
    }

    override fun onExpandedClick(
        expandableViewHolder: PViewHolder,
        expandedViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: TableInstanceModel
    ) {
        when (expandedType) {
            is TableInstanceItemModel -> {
                listener.onItemFixture(
                    expandableViewHolder.containerView,
                    expandedViewHolder.bindingAdapterPosition,
                    expandedType.fixture
                )
            }
        }
    }

    override fun onExpandableClick(
        expandableViewHolder: PViewHolder,
        expandableType: TableInstanceModel
    ) {
    }

    class PViewHolder(v: View) : ExpandableRecyclerViewAdapter.ExpandableViewHolder(v)

    class CViewHolder<T>(v: View, private val binding: ViewDataBinding) :
        ExpandableRecyclerViewAdapter.ExpandedViewHolder(v) {
        fun bind(item: T, listener: OnAdapterListenerFixtureItem) {
            binding.run {
                setVariable(BR.model, item)
//                root.setOnClickListener {
//                    listener.onItemFixture(it, layoutPosition, (item as FixtureItemModel).id)
//                }
                executePendingBindings()
            }
        }

        fun bindAdMob(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }
        fun bindSponsor(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }
    }
}