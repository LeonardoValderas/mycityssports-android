package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.SanctionRepository
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class SanctionViewModel(
    private val repository: SanctionRepository
) : BaseViewModel() {

    companion object {
        const val SANCTIONS = "Sanctions"
        const val SANCTIONS_UNKNOWN_ERROR = "Error unknown in get sanctions."
    }

    private val _sanctions = MutableLiveData<DataResponse<SanctionDto>>()
    val sanctions: LiveData<DataResponse<SanctionDto>>
        get() = _sanctions

    init {
        getSanctions(false)
    }

    fun getSanctions(isRefresh: Boolean) {
        try {
            repository.getSanctions(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _sanctions.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, SANCTIONS)
                    _sanctions.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _sanctions.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _sanctions.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, SANCTIONS)
                            } ?: run {
                                simpleErrorLog(it.message ?: SANCTIONS_UNKNOWN_ERROR,
                                    SANCTIONS
                                )
                            }
                            _sanctions.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, SANCTIONS)
            _sanctions.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}