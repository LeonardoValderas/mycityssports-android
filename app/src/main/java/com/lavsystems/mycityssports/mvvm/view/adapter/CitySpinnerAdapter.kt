package com.lavsystems.mycityssports.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.CityItemBinding
import com.lavsystems.mycityssports.mvvm.model.CityModel

class CitySpinnerAdapter constructor(
    context: Context,
    resource: Int,
    private var cities: MutableList<CityModel>
) : ArrayAdapter<CityModel>(context, resource, cities) {

    private val citiesAux = mutableListOf<CityModel>()

    override fun getItem(position: Int): CityModel? {
        return cities[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return cities.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = CityItemBinding.inflate(inflater, parent, false)
        val country = getCityModelForPosition(position)
        bind(binding, country)
        return binding.root
    }

    fun getCityModelForPosition(position: Int): CityModel {
        return cities[position]
    }

    private fun bind(binding: CityItemBinding, cities: CityModel) {
        with(binding) {
            model = cities
            YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    fun setCities(cities: MutableList<CityModel>) {
        this.citiesAux.clear()
        this.citiesAux.addAll(cities)
        //this.cities.addAll(cities)

        //notifyDataSetChanged()
    }

    fun setCitiesForStateId(stateId: String) {
        if (citiesAux.isNullOrEmpty())
            return

        val newCities = citiesAux.filter { it.state.id == stateId }
        cities.clear()
        cities.addAll(newCities)
        notifyDataSetChanged()
    }

    fun hasCities(): Boolean{
        return !cities.isNullOrEmpty()
    }
}




