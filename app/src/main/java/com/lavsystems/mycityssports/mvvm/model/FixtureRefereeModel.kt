package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixtureRefereeModel(
    val referee: RefereeModel,
    val refereeFunction: RefereeFunctionModel
) {
    constructor(): this(RefereeModel(), RefereeFunctionModel())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FixtureRefereeModel

        if (referee != other.referee) return false
        if (refereeFunction != other.refereeFunction) return false

        return true
    }

    override fun hashCode(): Int {
        var result = referee.hashCode()
        result = 31 * result + refereeFunction.hashCode()
        return result
    }

}