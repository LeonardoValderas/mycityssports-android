package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.DivisionItemBinding
import com.lavsystems.mycityssports.mvvm.model.DivisionModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerDivision

class DivisionAdapter(private val listener: OnAdapterListenerDivision) :
    RecyclerView.Adapter<DivisionAdapter.ViewHolder>() {

    private var divisions = mutableListOf<DivisionModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DivisionAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DivisionItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(divisions[position], listener)

    override fun getItemCount(): Int = divisions.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getItemForPosition(position: Int): DivisionModel {
        return divisions[position]
    }

    fun addDivisions(divisions: MutableList<DivisionModel>) {
        this.divisions.clear()
        this.divisions.addAll(divisions)
        notifyDataSetChanged()
    }

    fun selectItemIfExistFromDivisionIdOrSetFirstPosition(divisionId: String): Int {
        var indexDivision = -1
        run loop@{
            divisions.forEachIndexed { index, division ->
                if (division.id == divisionId) {
                    indexDivision = index
                    return@loop
                }
            }
        }

        deselectAllModels()

        if (indexDivision == -1)
            selectModel(0)
        else
            selectModel(indexDivision)

        return indexDivision
    }

    fun deselectAllModels() {
        divisions.forEach { division -> division.selected = false }
        notifyDataSetChanged()
    }

    fun selectModel(position: Int) {
        divisions[position].selected = true
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: DivisionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(division: DivisionModel, listener: OnAdapterListenerDivision) {
            with(binding) {
                model = division
                YoYo.with(Techniques.FadeIn).playOn(root)
                root.setOnClickListener {
                    listener.onItemDivisionClick(it, layoutPosition, division)
                    YoYo.with(Techniques.Pulse).playOn(root)
                }

                executePendingBindings()
            }
        }
    }
}