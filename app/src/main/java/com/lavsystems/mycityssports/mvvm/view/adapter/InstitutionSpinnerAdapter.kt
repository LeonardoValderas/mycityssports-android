package com.lavsystems.mycityssports.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.InstitutionSpinnerItemBinding
import com.lavsystems.mycityssports.databinding.SimpleSpinnerItemBinding
import com.lavsystems.mycityssports.databinding.SimpleSpinnerItemSportBinding
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SportModel

class InstitutionSpinnerAdapter(
    context: Context,
    resource: Int,
    var institutionsList: MutableList<InstitutionModel>
) :
    ArrayAdapter<InstitutionModel>(context, resource, R.id.tv_text, institutionsList) {

    override fun getItem(position: Int): InstitutionModel? {
        return institutionsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return institutionsList.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = SimpleSpinnerItemBinding.inflate(inflater, parent, false)
        val institution = getInstitutionModelForPosition(position)
        bindView(binding, institution)
        return binding.root

//
//        val v = super.getView(position, convertView, parent)
//        v?.run {
//            val ivInstitution = findViewById<CircularImageView>(R.id.iv_institution)
//            val item = getItem(position)
//            Glide.with(context)
//                .load(item?.urlImage)
//                //.placeholder(placeHolder)
//                // .override(50, 50)
//                //.dontAnimate()
//                .error(com.lavsystems.mycityssports.R.drawable.ic_image_error)
//                .into(ivInstitution)
//        }
//
//        return v!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = InstitutionSpinnerItemBinding.inflate(inflater, parent, false)
        val country = getInstitutionModelForPosition(position)
        bind(binding, country)
        return binding.root
    }

    fun getIndexForInstitutionId(idInstitution: String): Int {
        institutionsList.forEachIndexed { index, institution ->
            if (institution.id == idInstitution)
                return index
        }

        return -2
    }

    fun getInstitutionModelForPosition(position: Int): InstitutionModel {
        return institutionsList[position]
    }

    private fun bind(binding: InstitutionSpinnerItemBinding, Institution: InstitutionModel) {
        with(binding) {
            model = Institution
            YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    fun setInstitutions(Institutions: MutableList<InstitutionModel>) {
        this.institutionsList = Institutions
        notifyDataSetChanged()
    }

    private fun bindView(binding: SimpleSpinnerItemBinding, institution: InstitutionModel) {
        with(binding) {
            model = institution
            YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }
}




