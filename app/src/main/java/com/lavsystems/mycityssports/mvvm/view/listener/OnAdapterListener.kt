package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View

interface OnAdapterListener<T> {
    fun onItemClick(view: View, position: Int, t: T)
}