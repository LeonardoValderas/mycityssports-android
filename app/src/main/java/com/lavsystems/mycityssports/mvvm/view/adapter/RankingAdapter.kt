package com.lavsystems.mycityssports.mvvm.view.adapter

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.AdsmobItemRankingBinding
import com.lavsystems.mycityssports.databinding.ParentRowBinding
import com.lavsystems.mycityssports.databinding.RankingItemBinding
import com.lavsystems.mycityssports.mvvm.model.RankingItemModel
import com.lavsystems.mycityssports.mvvm.model.RankingsModel

class RankingAdapter(
    parents: MutableList<RankingsModel>
) : ExpandableRecyclerViewAdapter<
        Any,
        RankingsModel,
        RankingAdapter.PViewHolder,
        RankingAdapter.CViewHolder<Any>>(
    parents,
    ExpandingDirection.VERTICAL,
    true,
    false
) {

    override fun onCreateParentViewHolder(parent: ViewGroup, viewType: Int): PViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ParentRowBinding.inflate(layoutInflater, parent, false)
        return PViewHolder(
            binding.root
        )
    }

    override fun onCreateChildViewHolder(child: ViewGroup, viewType: Int, position: Int): CViewHolder<Any> {
        val layoutInflater = LayoutInflater.from(child.context)

        val binding = when (viewType) {
            ITEM -> {
                RankingItemBinding.inflate(layoutInflater, child, false)
            }
            else -> {
                AdsmobItemRankingBinding.inflate(layoutInflater, child, false)
            }
        }
        return CViewHolder(
            binding.root, binding
        )
    }

    override fun onBindParentViewHolder(
        parentViewHolder: PViewHolder,
        expandableType: RankingsModel,
        position: Int
    ) {
            parentViewHolder.containerView.findViewById<TextView>(R.id.tv_title).text = expandableType.rankingType.name
    }

    override fun onBindChildViewHolder(
        childViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: RankingsModel,
        position: Int
    ) {
        when (expandedType) {
            is RankingItemModel -> childViewHolder.bind(expandedType)
            else -> childViewHolder.bindAdMob(expandedType)
        }
    }

    override fun onExpandedClick(
        expandableViewHolder: PViewHolder,
        expandedViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: RankingsModel
    ) {
    }

    override fun onExpandableClick(
        expandableViewHolder: PViewHolder,
        expandableType: RankingsModel
    ) {
    }

    class PViewHolder(v: View) : ExpandableRecyclerViewAdapter.ExpandableViewHolder(v)

    class CViewHolder<T>(v: View, private val binding: ViewDataBinding) :
        ExpandableRecyclerViewAdapter.ExpandedViewHolder(v) {
        fun bind(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }

        fun bindAdMob(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }
    }
}