package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CardModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val resourceDrawable: String,
    val charLetter: CharLetter
) {
    constructor(): this("", "", "", CharLetter.Y)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CardModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (resourceDrawable != other.resourceDrawable) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + resourceDrawable.hashCode()
        return result
    }

     enum class CharLetter(val value: String){
        Y("YELLOW"),
        R("RED"),
        G("GREEN"),
        W("WHITE"),
        B("BLACK"),
        U("BLUE")
    }
}