package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class TournamentModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val year: String,
    val winPoint: Int,
    val tournamentType: TournamentTypeModel,
    val sports: MutableList<String>,
    val divisions: MutableList<DivisionModel>,
    val subDivisions: MutableList<String>,
    val tournamentZones: MutableList<TournamentZoneModel>,
    val tournamentInstances: MutableList<TournamentInstanceModel>,
    val institution: String,
    var selected: Boolean = false
) {
   constructor() : this("", "", "", 0, TournamentTypeModel(), arrayListOf(),  arrayListOf(),  arrayListOf(), arrayListOf(), arrayListOf(), "", false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TournamentModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (year != other.year) return false
        if (winPoint != other.winPoint) return false
        if (tournamentType != other.tournamentType) return false
        if (sports != other.sports) return false
        if (divisions != other.divisions) return false
        if (subDivisions != other.subDivisions) return false
        if (tournamentZones != other.tournamentZones) return false
        if (tournamentInstances != other.tournamentInstances) return false
        if (institution != other.institution) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + year.hashCode()
        result = 31 * result + winPoint
        result = 31 * result + tournamentType.hashCode()
        result = 31 * result + sports.hashCode()
        result = 31 * result + divisions.hashCode()
        result = 31 * result + subDivisions.hashCode()
        result = 31 * result + tournamentZones.hashCode()
        result = 31 * result + tournamentInstances.hashCode()
        result = 31 * result + institution.hashCode()
        return result
    }

    override fun toString(): String {
        return "$name $year"
    }

}