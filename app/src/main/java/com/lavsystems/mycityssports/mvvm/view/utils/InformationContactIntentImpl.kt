package com.lavsystems.mycityssports.mvvm.view.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.webkit.URLUtil
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.mvvm.model.PositionModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_HOME_PHONE
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_LOCATION_FINE
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder
import com.lavsystems.mycityssports.utils.permissions.Permissions.needPermissionForAction
import com.lavsystems.mycityssports.utils.uicomponents.ViewComponents
import java.util.*

class InformationContactIntentImpl(private val activity: Activity, private val emptyInfo: String) :
    InformationContactIntent {

    companion object {
        const val TEXT_PLAIN = "text/plain"
        const val WHATSAPP = "com.whatsapp"
        const val GOOGLE_MAPS = "http://maps.google.com/maps?daddr=%f,%f (%s)"
        const val MAIL_TO = "mailto:"
        const val TEL = "tel:"
        const val FACEBOOK_PACKAGE = "com.facebook.katana"
        const val INSTAGRAM_PACKAGE = "com.instagram.android"
        const val TWITTER_PACKAGE = "com.twitter.android"
        const val SNAPCHAT_PACKAGE = "com.snapchat.android"
    }

    override fun intentHomePhone(phone: String?) {
        phone?.let {
            if (isWithoutInfo(it) || needPermissionForAction(
                    activity,
                    Manifest.permission.CALL_PHONE,
                    PERMISSION_CALL_HOME_PHONE
                )
            )
                return@let

            callPhone(it)
        }
    }

    override fun intentPhone(phone: String?) {
        phone?.let {
            if (isWithoutInfo(it) || needPermissionForAction(
                    activity,
                    Manifest.permission.CALL_PHONE,
                    PERMISSION_CALL_HOME_PHONE
                )
            )
                return@let
            callPhone(it)
        }
    }

    override fun intentWhatsapp(whatsapp: String?) {
        whatsapp?.let {
            if (isWithoutInfo(it))
                return@let

            try {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = TEXT_PLAIN
                shareIntent.setPackage(WHATSAPP)
                activity.startActivity(shareIntent)
            } catch (e: Exception) {
                sendLog("intentWhatsapp", e)
                ViewComponents.showToast(
                    activity,
                    activity.getString(R.string.error_contatc_action)
                )
            }
        }
    }

    override fun intentMap(position: PositionModel?, name: String?) {
        position?.let {
            if (it.latitude.isEmpty() || it.longitude.isEmpty() || needPermissionForAction(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    PERMISSION_CALL_LOCATION_FINE
                ))
                return@let
            try {
                val lat = position.latitude.toDouble()
                val lon = position.longitude.toDouble()
                val url = String.format(
                    Locale.ENGLISH,
                    GOOGLE_MAPS, lat, lon, name
                )


                if (appIsInstalled(activity.getString(R.string.map_package_name)))
                    openRedSocialFromApp(url)
                else
                    openRedSocialFromBrowser(url)
            } catch (e: Exception) {
                sendLog("intentMap", e)
                ViewComponents.showToast(
                    activity,
                    activity.getString(R.string.error_contatc_action)
                )
            }
        }
        /*    }
            val uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", 12f, 2f, "Where the party is at")
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            val intent = Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("geo:0,0?q=37.423156,-122.084917 ($name)"))
            activity.startActivity(intent)*/
    }

    override fun intentWeb(web: String?) {
        web?.let {
            if (isWithoutInfo(it))
                return@let
            try {
                if (urlIsValidate(it)) {
                    val browse = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                    activity.startActivity(browse)
                } else {
                    ViewComponents.showToast(
                        activity,
                        activity.getString(R.string.error_contatc_action)
                    )
                }
            } catch (e: Exception) {
                sendLog("intentWeb", e)
                ViewComponents.showToast(
                    activity,
                    activity.getString(R.string.error_contatc_action)
                )
            }
        }
    }

    override fun intentFacebook(facebook: String?) {
        facebook?.let {
            if (isWithoutInfo(it))
                return@let
            try {
                //val face = activity.getString(R.string.facebook_url)
                val url = replaceWhiteSpace(it)
                //val url = face.plus(user)

                if (urlIsValidate(url)) {
                    if (appIsInstalled(activity.getString(R.string.facebook_packege_name)))
                        openRedSocialFromApp(url)
                    else
                        openRedSocialFromBrowser(url)
                } else {
                    ViewComponents.showToast(
                        activity,
                        activity.getString(R.string.error_contatc_action)
                    )
                }
            } catch (e: Exception) {
                sendLog("intentFacebook", e)
                ViewComponents.showToast(
                    activity,
                    activity.getString(R.string.error_contatc_action)
                )
            }
        }
    }

    override fun intentInstagram(instagram: String?) {
        instagram?.let {
           // val instagramUrl = activity.getString(R.string.instagram_url)
            //val user = replaceWhiteSpace(it)
            //val url = instagramUrl.plus(user)

            if (urlIsValidate(it)) {
                if (appIsInstalled(activity.getString(R.string.instagram_package_name)))
                    openRedSocialFromApp(it)
                else
                    openRedSocialFromBrowser(it)
            } else {
                ViewComponents.showToast(activity, activity.getString(R.string.error_contatc_action))
            }
        }

    }

    override fun intentEmail(email: String?) {
        email?.let {
            if (isWithoutInfo(it))
                return
            try {
                val intent = Intent(Intent.ACTION_SENDTO)
                intent.data = Uri.parse("$MAIL_TO$it")
                activity.startActivity(intent);
            } catch (e: Exception) {
                sendLog("intentEmail", e)
                ViewComponents.showToast(
                    activity,
                    activity.getString(R.string.error_contatc_action)
                )
            }
        }
    }

    private fun callPhone(number: String) {
        try {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("$TEL$number"))
            activity.startActivity(intent)
        } catch (e: Exception) {
            sendLog("callPhone", e)
            ViewComponents.showToast(activity, activity.getString(R.string.error_contatc_action))
        }
    }

    private fun openRedSocialFromApp(urlApp: String) {
        try {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(urlApp)))
        } catch (e: Exception) {
            sendLog("openRedSocialFromApp", e)
            ViewComponents.showToast(activity, activity.getString(R.string.error_contatc_action))
        }
    }

    private fun openRedSocialFromBrowser(url: String) {
        try {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (e: Exception) {
            sendLog("openRedSocialFromBrowser", e)
            ViewComponents.showToast(activity, activity.getString(R.string.error_contatc_action))
        }
    }

    private fun appIsInstalled(packageName: String): Boolean {
        return activity.packageManager.getLaunchIntentForPackage(packageName) != null
    }

    private fun setPackageRed(red: Int): String {
        var pack = ""
        when (red) {
            4 //face
            -> pack = FACEBOOK_PACKAGE
            5 //insta
            -> pack = INSTAGRAM_PACKAGE
            6 //twitter
            -> pack = TWITTER_PACKAGE
            7 //snap
            -> pack = SNAPCHAT_PACKAGE
        }
        return pack
    }

    private fun replaceWhiteSpace(user: String): String {
        return user.trim()
    }

    private fun urlIsValidate(url: String): Boolean {
        return URLUtil.isValidUrl(url)
    }

    private fun isWithoutInfo(info: String): Boolean {
        return info.isEmpty() || info == emptyInfo
    }

    private fun sendLog(method: String, e:Exception){
        FirestoreBuilder.Builder()
            .className(ConstantsUtils.INFORMATION_CONTACT_INTENT)
            .method(method)
            .type(FirestoreBuilder.EXCEPTION)
            .message(e.stackTraceToString())
            .brand()
            .model()
            .send()
    }
}