package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.repository.ErrorFragmentRepository
import com.lavsystems.mycityssports.utils.ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE
import com.lavsystems.mycityssports.utils.ConstantsUtils.FRAGMENT_POSITION_BUNDLE
import com.lavsystems.mycityssports.utils.OpenForTesting

@OpenForTesting
class ErrorFragmentViewModel(private val repository: ErrorFragmentRepository) : BaseViewModel() {
    private val _resourceStringId = MutableLiveData<Int>()
    val resourceStringId: LiveData<Int>
        get() = _resourceStringId

    fun setResourceStringIdFromBundle() {
        val resource = getExceptionHandleBundle().getMessageIntResource()
        _resourceStringId.value = resource
    }

    fun getFragmentPosition(): Int {
        getBundle()?.getInt(FRAGMENT_POSITION_BUNDLE)?.let {
            return it
        }

        return 0
    }

//    fun sendBundleErrorToAnalytic() {
//        repository.sendBundleErrorToAnalytic(getBundle())
//    }

    fun sendErrorToAnalytic() {
        repository.sendErrorToAnalytic(getExceptionHandleBundle().getExceptionStackTrace())
    }

    private fun getExceptionHandleBundle(): ExceptionHandleImpl {
        return getBundle()?.getSerializable(FRAGMENT_EXCEPTION_HANDLE_BUNDLE) as ExceptionHandleImpl
    }
}