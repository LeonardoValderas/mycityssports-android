package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.DivisionSubDivisionItemBinding
import com.lavsystems.mycityssports.mvvm.model.DivisionModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerDivisionSubDivision

class DivisionSubDivisionAdapter(private val listener: OnAdapterListenerDivisionSubDivision) :
    RecyclerView.Adapter<DivisionSubDivisionAdapter.ViewHolder>() {

    private var divisions = mutableListOf<DivisionSubDivisionModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DivisionSubDivisionAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DivisionSubDivisionItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(divisions[position], listener)

    override fun getItemCount(): Int = divisions.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getItemForPosition(position: Int): DivisionSubDivisionModel {
        return divisions[position]
    }

    fun addDivisions(divisions: MutableList<DivisionSubDivisionModel>) {
        this.divisions.clear()
        this.divisions.addAll(divisions)
        notifyDataSetChanged()
    }

    fun selectItemIfExistFromDivisionIdOrSetFirstPosition(divisionSubDivision: DivisionSubDivisionModel): Int {
        var indexDivision = -1
        run loop@{
            divisions.forEachIndexed { index, division ->
                if (division.divisionId == divisionSubDivision.divisionId && division.subDivisionName == divisionSubDivision.subDivisionName) {
                    indexDivision = index
                    return@loop
                }
            }
        }

        deselectAllModels()

        if (indexDivision == -1)
            selectModel(0)
        else
            selectModel(indexDivision)

        return indexDivision
    }

    fun deselectAllModels() {
        divisions.forEach { division -> division.selected = false }
        notifyDataSetChanged()
    }

    fun selectModel(position: Int) {
        divisions[position].selected = true
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: DivisionSubDivisionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(division: DivisionSubDivisionModel, listener: OnAdapterListenerDivisionSubDivision) {
            with(binding) {
                model = division
                YoYo.with(Techniques.FadeIn).playOn(root)
                root.setOnClickListener {
                    listener.onItemDivisionClick(it, layoutPosition, division)
                    YoYo.with(Techniques.Pulse).playOn(root)
                }

                executePendingBindings()
            }
        }
    }
}