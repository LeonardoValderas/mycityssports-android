package com.lavsystems.mycityssports.mvvm.view.adapter

import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import com.lavsystems.mycityssports.mvvm.model.SanctionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import com.lavsystems.mycityssports.utils.adapter.DataBindingAdapter
import com.lavsystems.mycityssports.utils.adapter.ProgressbarItem
import kotlin.random.Random

class SanctionAdapter(listener: OnAdapterListener<Any>) : DataBindingAdapter<Any>(listener) {

    private var hasAds = false
    private var hasSponsor = false

    override fun getAnimateRoot(): YoYo.AnimationComposer? {
        //return YoYo.with(Techniques.FadeIn)
        return null
    }

    override fun getAnimateListener(): YoYo.AnimationComposer? {
        //return YoYo.with(Techniques.Pulse)
        return null
    }

    private var items = mutableListOf<Any>()
    private var sponsors = mutableListOf<Any>()

    override fun getItemModel(int: Int): Any {
        return items[int]
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addSponsors(sponsors: MutableList<Any>) {
        this.sponsors = sponsors
    }

    override fun getItemId(position: Int): Long {
        val item = getItemModel(position)
        return when (item) {
            is SanctionModel -> item.id.toLong()
            is SponsorModel -> item.id.toLong()
            else -> (item as AdMobItem).id.toLong()
        }
    }

    override fun addItems(items: MutableList<Any>, clear: Boolean) {
        if (clear) {
            this.items.clear()
            hasAds = false
            hasSponsor = false
            notifyDataSetChanged()
        }
        this.items.addAll(items)
        addSponsor()
        addAdView()
        notifyDataSetChanged()
    }

    override fun getItems(): MutableList<Any> {
        return items
    }

    private fun addAdView() {
        if (!hasAds && items.isNotEmpty()) {
            if (items.count() <= 3) {
                items.add(items.count(), AdMobItem())
            } else {
                items.add(3, AdMobItem())
            }
            hasAds = true
        }
    }

    private fun addSponsor() {
        if (!hasSponsor && items.isNotEmpty() && sponsors.isNotEmpty()) {
            val size = items.count()
            if (size == 1) {
                items.add(items.size, sponsors.random())
            } else {
                for (i in 2..size step 2) {
                    val indexSponsor = if (i != 2) i + 1 else i
                    items.add(indexSponsor, sponsors.random())
                }
            }
            hasSponsor = true
        }
    }

    private fun getIndexSponsor(): Int {
        return (0 until sponsors.size).random()
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItemModel(position)) {
            is AdMobItem -> R.layout.adsmob_item_sanction
            is ProgressbarItem -> R.layout.load_more_progress_bar
            is SponsorModel -> R.layout.sponsor_news_item
            else -> R.layout.sanction_item
        }
    }
}
