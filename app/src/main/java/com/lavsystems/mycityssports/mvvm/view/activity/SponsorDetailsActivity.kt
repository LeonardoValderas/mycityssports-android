package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivitySponsorDetailsBinding
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntent
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.SponsorViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_HOME_PHONE
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_PHONE
import com.lavsystems.mycityssports.utils.ConstantsUtils.SPONSOR_ID_BUDLE
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.permissions.Permissions
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class SponsorDetailsActivity :
    BaseActivity<ActivitySponsorDetailsBinding, SponsorViewModel>(),
    OnMapReadyCallback {

    private val viewModel: SponsorViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.activity_sponsor_details
    override fun getBindingVariable(): Int = BR.vm

    override fun initViewModel(): SponsorViewModel = viewModel
    private lateinit var mMap: GoogleMap
    private var sponsorModel: SponsorModel? = null
    lateinit var contactIntent: InformationContactIntent

    override fun init() {
        initToolbar()
        getSponsorIdFromIntentBundle()
        initAdView()
    }

    private fun initContactUtils() {
        sponsorModel?.let {
            contactIntent = InformationContactIntentImpl(this, SponsorModel.WITHOUT_INFO)
        }
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = getString(R.string.sponsor_title)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun initAdView() {
        getRootDataBinding().adViewSponsorDetails.loadAd(AdRequest.Builder().build())
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private fun getSponsorIdFromIntentBundle() {
        getBundleIntent()?.let { bundle ->
            bundle.getString(SPONSOR_ID_BUDLE, "")?.let {
                getViewModel().getSponsor(it)
            } ?: kotlin.run {
                closeActivityForError()
            }
        } ?: kotlin.run {
            closeActivityForError()
        }
    }


    override fun initObservers() {
        with(getViewModel()) {
            sponsor.observe(this@SponsorDetailsActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let { s ->
                            sponsorModel = s
                            initContactUtils()
                            sponsorToBinding(s)
                        } ?: run {
                            closeActivityForError()
                        }
                        hideLoading()
                    }

                    StatusResponse.LOADING -> {
                        showLoading(getString(R.string.loading_sponsor_data_details))
                    }

                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            closeActivityForError()
                        } ?: run {
                            closeActivityForError()
                        }

                        hideLoading()
                    }
                    else -> {
                        closeActivityForError()
                    }
                }
            })
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun sponsorToBinding(sponsor: SponsorModel) {
        getRootDataBinding().model = sponsor
    }

    private fun closeActivityForError() {
        showToast(getString(R.string.sponsor_error))
        finish()
    }

    private fun getBundleIntent(): Bundle? {
        return intent?.extras
    }

    fun onClickHomePhone(view: View) {
        contactIntent.intentHomePhone(sponsorModel?.phone)
    }

    fun onClickHome(view: View) {
        contactIntent.intentPhone(sponsorModel?.mobile)
    }

    fun onClickWhatsapp(view: View) {
        contactIntent.intentWhatsapp(sponsorModel?.mobile)
    }

    fun onClickEmail(view: View) {
        contactIntent.intentEmail(sponsorModel?.email)
    }

    fun onClickFacebook(view: View) {
        contactIntent.intentFacebook(sponsorModel?.facebook)
    }

    fun onClickInstagram(view: View) {
        contactIntent.intentInstagram(sponsorModel?.instagram)
    }

    fun onClickWeb(view: View) {
        contactIntent.intentWeb(sponsorModel?.web)
    }

    fun onClickMap(view: View) {
        contactIntent.intentMap(
            sponsorModel?.address?.position,
            sponsorModel?.name
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                PERMISSION_CALL_HOME_PHONE -> contactIntent.intentHomePhone(sponsorModel?.phone)
                PERMISSION_CALL_PHONE -> contactIntent.intentPhone(sponsorModel?.mobile)
            }
        } else {
            if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_DENIED) {
                when (requestCode) {
                    PERMISSION_CALL_PHONE,
                    PERMISSION_CALL_HOME_PHONE -> Permissions.shouldShowRequestPermissionRationale(
                        this,
                        R.id.cl_sponsor_details,
                        permissions.first(),
                        getString(R.string.permission_call_dialog_message)
                    )
                }
            }
        }
    }
}
