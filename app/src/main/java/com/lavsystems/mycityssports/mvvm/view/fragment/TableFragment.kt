package com.lavsystems.mycityssports.mvvm.view.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.databinding.FragmentTableBinding
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.model.TableModel
import com.lavsystems.mycityssports.mvvm.view.activity.FixtureDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.activity.SponsorDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem
import com.lavsystems.mycityssports.mvvm.view.utils.ConcatAdapterUtils
import com.lavsystems.mycityssports.mvvm.viewmodel.TableViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.ref.WeakReference

@ExperimentalCoroutinesApi
@Suppress("UNCHECKED_CAST")
class TableFragment : BaseFragment<FragmentTableBinding, TableViewModel>(),
    OnAdapterListenerFixtureItem, OnAdapterListener<SponsorModel> {

    private val viewModel: TableViewModel by viewModel()
    private var concatAdapter = ConcatAdapter()
    override fun initViewModel(): TableViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_table
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initSwipeRefresh()
    }

    override fun initObservers() {
        with(getViewModel()) {
            table.observe(this@TableFragment, { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data.let { t ->
                            t?.let {
                                it.table?.let { tableModel ->
                                    if (isModelEmpty(tableModel))
                                        showEmptyTitle()
                                    else
                                        hideEmptyTitle()
                                    val sponsor = getSponsorModel(t.sponsors)
                                    // Using Databiding dont work. I needed to create one xml Admob for each fragment
                                    concatAdapter =
                                        ConcatAdapterUtils<TableModel>(WeakReference(context), this@TableFragment).apply {
                                            setModel(
                                                t.table!!, AdMobAdapter(
                                                    R.layout.adsmob_table_item,
                                                    AdMobItem()
                                                ),
                                                if (sponsor == null) null else SponsorItemAdapter(
                                                    R.layout.sponsor_item,
                                                    sponsor,
                                                    this@TableFragment
                                                )
                                            )
                                        }.getConcatAdapter()
                                    initRecyclerView()
                                } ?: run {
                                    showEmptyTitle()
                                }
                            } ?: run {
                                showEmptyTitle()
                            }
                        }
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        getCommunicator()?.showLoadingUI(R.string.loading_standing_data)
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            getCommunicator()?.showErrorFragment(it)
                        } ?: run {
                            val exceptionHandle =
                                ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                            getCommunicator()?.showErrorFragment(exceptionHandle)
                        }

                        hideLoading()
                    }
                    else -> {
                        val exceptionHandle =
                            ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                        getCommunicator()?.showErrorFragment(exceptionHandle)
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun isModelEmpty(tableModel: TableModel): Boolean{
        return tableModel.tournamentInstances.isEmpty() &&
                tableModel.tournamentZones.isEmpty() &&
                tableModel.tournamentPoints.isEmpty()
    }

    private fun getSponsorModel(sponsors: MutableList<SponsorModel>): SponsorModel? {
        if (sponsors.isNotEmpty()) {
            return sponsors[(0 until sponsors.size).random()]
        }

        return null
    }


    override fun getFromHome() {
        getViewModel().getTable(false)
    }

    private fun initRecyclerView() {
        getViewDataBinding().iRvTable.rvContainer.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = concatAdapter
        }
    }

    @ExperimentalCoroutinesApi
    private fun initSwipeRefresh() {
        swipeRefreshLayout = getViewDataBinding().iRvTable.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getTable(true)
        }
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getCommunicator()?.hideLoadingUI()
    }

    private fun showEmptyTitle() {
        getViewDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getViewDataBinding().empty = false
    }

    private fun setFabVisibility(show: Boolean) {
        if (show)
            getCommunicator()?.showFAB()
        else
            getCommunicator()?.hideFAB()
    }

    override fun onItemFixture(view: View, position: Int, fixtureId: String) {
        val intent = Intent(activity?.applicationContext, FixtureDetailsActivity::class.java)
        intent.putExtra(ConstantsUtils.FIXTURE_ID_BUDLE, fixtureId)
        val vl = view.findViewById<View>(R.id.iv_local_team)
        val vv = view.findViewById<View>(R.id.iv_visit_team)
        val pairHome = android.util.Pair(vl, getString(R.string.fixture_details_transition_local))
        val pairVisitors =
            android.util.Pair(vv, getString(R.string.fixture_details_transition_visita))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(activity, pairHome, pairVisitors)
        startActivity(intent, transitionActivityOptions.toBundle())
    }

    override fun onItemSponsor(view: View, position: Int, sponsorId: String) {

    }

    override fun onItemClick(view: View, position: Int, sponsor: SponsorModel) {
        val intent = Intent(activity?.applicationContext, SponsorDetailsActivity::class.java)
        intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, sponsor.id)
        val vs = view.findViewById<View>(R.id.iv_sponsor)
        val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(activity, pairSponsor)
        startActivity(intent, transitionActivityOptions.toBundle())
    }
}
