package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.DateTimeUtils
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SanctionModel(
    @Json(name = "_id")
    val id: String,
    val sport: SportModel,
    val team: TeamModel,
    val division: DivisionModel,
    val player: PlayerModel,
    val motive: String,
    val sanction: String,
    val expiration: String,
    val source: String = ""
) {
    constructor() : this("", SportModel(), TeamModel(), DivisionModel(), PlayerModel(), "", "", "", "")


    companion object {
        const val SOURCE = "Fuente: "
    }

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SanctionModel

        if (id != other.id) return false
        if (sport != other.sport) return false
        if (team != other.team) return false
        if (division != other.division) return false
        if (player != other.player) return false
        if (motive != other.motive) return false
        if (sanction != other.sanction) return false
        if (expiration != other.expiration) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + sport.hashCode()
        result = 31 * result + team.hashCode()
        result = 31 * result + division.hashCode()
        result = 31 * result + player.hashCode()
        result = 31 * result + motive.hashCode()
        result = 31 * result + sanction.hashCode()
        result = 31 * result + expiration.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}


