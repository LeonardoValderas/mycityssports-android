package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TableInstanceItemModel(
    val fixture: String,
    val homeTeam: TeamModel,
    val homeResult: String?,
    val homeResultPenalty: String?,
    val visitorsTeam: TeamModel,
    val visitorsResult: String?,
    val visitorsResultPenalty: String?,
    val comment: String = ""
) {
    constructor() : this(
        "",
        TeamModel(),
        null,
        null,
        TeamModel(),
        null,
        null
    )

    companion object {
        const val COMMENTE_CHAR = "* "
    }

    val homeResultPenaltyFormat: String
        get() = if(homeResultPenalty.isNullOrBlank()) "" else "($homeResultPenalty)"

    val visitorsResultPenaltyFormat: String
         get() = if(visitorsResultPenalty.isNullOrBlank()) "" else "($visitorsResultPenalty)"

    val commentFormatted: String
        get() = if(comment.isEmpty()) COMMENTE_CHAR + " - " else COMMENTE_CHAR + comment

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TableInstanceItemModel

        if (fixture != other.fixture) return false
        if (homeTeam != other.homeTeam) return false
        if (homeResult != other.homeResult) return false
        if (homeResultPenalty != other.homeResultPenalty) return false
        if (visitorsTeam != other.visitorsTeam) return false
        if (visitorsResult != other.visitorsResult) return false
        if (visitorsResultPenalty != other.visitorsResultPenalty) return false
        if (comment != other.comment) return false
        return true
    }

    override fun hashCode(): Int {
        var result = fixture.hashCode()
        result = 31 * result + homeTeam.hashCode()
        result = 31 * result + (homeResult?.hashCode() ?: 0)
        result = 31 * result + (homeResultPenalty?.hashCode() ?: 0)
        result = 31 * result + visitorsTeam.hashCode()
        result = 31 * result + (visitorsResult?.hashCode() ?: 0)
        result = 31 * result + (visitorsResultPenalty?.hashCode() ?: 0)
        result = 31 * result + (comment.hashCode() ?: 0)
        return result
    }
}