package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.InstitutionRepository
import com.lavsystems.mycityssports.data.repository.NewsRepository
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class InstitutionViewModel(
    private val repository: InstitutionRepository
) : BaseViewModel() {

    companion object {
        const val INSTITUTIONS = "Institutions"
        const val INSTITUTIONS_UNKNOWN_ERROR = "Error unknown in get institutions data."
    }

    private val _institutions = MutableLiveData<DataResponse<MutableList<InstitutionModel>>>()
    val institutions: LiveData<DataResponse<MutableList<InstitutionModel>>>
        get() = _institutions

    init {
        getInstitutions(false)
    }

    fun getInstitutions(isRefresh: Boolean) {
        try {
            repository.getInstitutions(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _institutions.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, INSTITUTIONS)
                   _institutions.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _institutions.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _institutions.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, INSTITUTIONS)
                            } ?: run {
                                simpleErrorLog(it.message ?: INSTITUTIONS_UNKNOWN_ERROR,
                                    INSTITUTIONS
                                )
                            }
                            _institutions.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, INSTITUTIONS)
            _institutions.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}