package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View
import com.lavsystems.mycityssports.mvvm.model.TournamentModel

interface OnAdapterListenerTournament {
    fun onItemTournamentClick(view: View, position: Int, tournament: TournamentModel)
}