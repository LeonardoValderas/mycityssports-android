package com.lavsystems.mycityssports.mvvm.view.activity

import android.app.ActivityOptions
import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.ads.AdRequest
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityInstitutionBinding
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.view.adapter.InstitutionAdapter
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.viewmodel.InstitutionViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.INSTITUTION_ID_BUDLE
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.recycler.ItemOffsetDecoration
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel


@ExperimentalCoroutinesApi
class InstitutionActivity : BaseActivity<ActivityInstitutionBinding, InstitutionViewModel>(),
    OnAdapterListener<InstitutionModel> {

    private val viewModel: InstitutionViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_institution
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): InstitutionViewModel = viewModel
    private val institutionAdapter = InstitutionAdapter(this)
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initToolbar()
        initRecyclerView()
        initAdView()
        initSwipeRefresh()
        tryAgainEmptyList()
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            institutions.observe(this@InstitutionActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            if (it.isNotEmpty())
                                hideEmptyTitle()
                            else
                                showEmptyTitle()

                            institutionAdapter.addInstitutions(it)
                        } ?: run {
                            showEmptyTitle()
                        }
                        hideFriendlyError()
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(
                            resource.message ?: getString(R.string.loading_institution_data)
                        )
                    }
                    StatusResponse.ERROR -> {
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                     val error = resource.exceptionHandle?.let {
                            getString(it.getMessageIntResource())
                        } ?: run {
                            getString(R.string.known_error)
                        }
                        showFriendlyError(error)
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun showFriendlyError(subtitle: String) {
        handleFriendlyError(true, subtitle)
    }

    private fun hideFriendlyError() {
        handleFriendlyError(false, "")
    }

    private fun handleFriendlyError(show: Boolean, subtitle: String) {
        getRootDataBinding().clErrorVisibility = show
        getRootDataBinding().title = getString(R.string.error_title_fragment)
        getRootDataBinding().subtitle = subtitle
    }

    private fun initRecyclerView() {
        getRootDataBinding().iRvInstitutions.rvContainer.apply {
            clipToPadding = false
            //setHasFixedSize(true)
            //layoutManager = GridLayoutManager(this@InstitutionActivity, 2, LinearLayoutManager.VERTICAL, false)
            layoutManager = GridLayoutManager(this@InstitutionActivity, 2)
            //val spacingInPixels = resources.getDimensionPixelSize(R.dimen.item_offset)

            addItemDecoration(ItemOffsetDecoration(context, R.dimen.item_offset))
            adapter = institutionAdapter
        }
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout = getRootDataBinding().iRvInstitutions.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getInstitutions(true)
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun showEmptyTitle() {
        getRootDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getRootDataBinding().empty = false
    }

    private fun tryAgainEmptyList() {
        getRootDataBinding().iFriendlyError.btnTryAgain.setOnClickListener {
            startActivity(Intent(this, InstitutionActivity::class.java))
            finish()
        }
    }

    private fun initAdView() {
        getRootDataBinding().adViewInstitution.loadAd(AdRequest.Builder().build())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onItemClick(view: View, position: Int, item: InstitutionModel) {
        val intent = Intent(this@InstitutionActivity, InstitutionDetailsActivity::class.java)
        intent.putExtra(INSTITUTION_ID_BUDLE, item.id)
        val vs = view.findViewById<View>(R.id.iv_institution)
        val pairSponsor = android.util.Pair(vs, getString(R.string.institution_transition))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(this@InstitutionActivity, pairSponsor)
        startActivity(intent, transitionActivityOptions.toBundle())
    }
}
