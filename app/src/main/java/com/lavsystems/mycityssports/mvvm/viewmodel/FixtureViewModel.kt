package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.FixtureRepository
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class FixtureViewModel(private val repository: FixtureRepository) : BaseViewModel() {
    companion object {
        const val FIXTURES = "Fixtures"
        const val FIXTURES_UNKNOWN_ERROR = "Error unknown in get fixture data."
    }
    private val _fixture = MutableLiveData<DataResponse<FixtureModel>>()
    val fixture: LiveData<DataResponse<FixtureModel>>
        get() = _fixture

    init {
        getFixture(false)
    }

    final fun getFixture(isRefresh: Boolean) {
        try {
            repository.getFixtures(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _fixture.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, FIXTURES)
                    _fixture.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _fixture.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _fixture.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, FIXTURES)
                            } ?: run {
                                simpleErrorLog(it.message ?: FIXTURES_UNKNOWN_ERROR,
                                    FIXTURES
                                )
                            }
                            _fixture.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, FIXTURES)
            _fixture.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}
