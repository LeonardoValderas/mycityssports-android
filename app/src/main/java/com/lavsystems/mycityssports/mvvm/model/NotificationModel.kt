package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NotificationModel(
    var institution: InstitutionModel,
    var selected: Boolean
) {
    constructor(): this(InstitutionModel(), false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NotificationModel

        if (institution != other.institution) return false
        if (selected != other.selected) return false

        return true
    }

    override fun hashCode(): Int {
        var result = institution.hashCode()
        result = 31 * result + selected.hashCode()
        return result
    }
}