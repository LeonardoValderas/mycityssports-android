package com.lavsystems.mycityssports.mvvm.model

import androidx.databinding.BaseObservable
import java.io.Serializable

data class PlacesModel(
    val stateModel: StateModel,
    val cities: MutableList<CityModel>
):BaseObservable(), Serializable{
    constructor():this(StateModel(), mutableListOf())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PlacesModel

        if (stateModel != other.stateModel) return false
        if (cities != other.cities) return false

        return true
    }

    override fun hashCode(): Int {
        var result = stateModel.hashCode()
        result = 31 * result + cities.hashCode()
        return result
    }
}