package com.lavsystems.mycityssports.mvvm.view.adapter

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.AdsmobItemBinding
import com.lavsystems.mycityssports.databinding.ParentRowBinding
import com.lavsystems.mycityssports.databinding.TableGridBinding
import com.lavsystems.mycityssports.mvvm.model.TableItemModel
import com.lavsystems.mycityssports.mvvm.model.TableZoneModel

class TableZoneAdapter(parents: MutableList<TableZoneModel>) :
    ExpandableRecyclerViewAdapter<Any, TableZoneModel,
            TableZoneAdapter.PViewHolder,
            TableZoneAdapter.CViewHolder<Any>>(
        parents,
        ExpandingDirection.VERTICAL,
        false,
        true
    ) {

    private var adapter = TableGridItemAdapter()
    override fun onCreateParentViewHolder(parent: ViewGroup, viewType: Int): PViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ParentRowBinding.inflate(layoutInflater, parent, false)
        return PViewHolder(
            binding.root
        )
    }

    override fun onCreateChildViewHolder(child: ViewGroup, viewType: Int, position: Int): CViewHolder<Any> {
        val layoutInflater = LayoutInflater.from(child.context)
        val binding = when (viewType) {
            ITEM -> {
                TableGridBinding.inflate(layoutInflater, child, false)
            }
            else -> {
                AdsmobItemBinding.inflate(layoutInflater, child, false)
            }
        }
        return CViewHolder(
            binding.root,
            binding,
            getGroupByPosition(position).tableDataZone
        )
    }

    override fun onBindParentViewHolder(
        parentViewHolder: PViewHolder,
        expandableType: TableZoneModel,
        position: Int
    ) {
        parentViewHolder.containerView.findViewById<TextView>(R.id.tv_title).text = expandableType.tournamentZone.name
        if (expandableType.isExpanded) {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.GONE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.VISIBLE
        } else {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.VISIBLE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.GONE
        }
    }

    override fun onBindChildViewHolder(
        childViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: TableZoneModel,
        position: Int
    ) {
        when (expandedType) {
            is TableItemModel -> childViewHolder.bind()
            else -> childViewHolder.bindAdMob(expandedType)
        }
    }

    override fun onExpandedClick(
        expandableViewHolder: PViewHolder,
        expandedViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: TableZoneModel
    ) {
    }

    override fun onExpandableClick(
        expandableViewHolder: PViewHolder,
        expandableType: TableZoneModel
    ) {
    }

    class PViewHolder(v: View) : ExpandableRecyclerViewAdapter.ExpandableViewHolder(v)

    class CViewHolder<T>(
        v: View,
        private val binding: ViewDataBinding,
        private val tables: MutableList<TableItemModel>
    ) : ExpandableRecyclerViewAdapter.ExpandedViewHolder(v) {
        fun bind() {
            binding.run {
                root?.let {
                    it.findViewById<RecyclerView>(R.id.rv_container)?.apply {
                        layoutManager =
                            LinearLayoutManager(root.context, RecyclerView.VERTICAL, false)
                        adapter = TableGridItemAdapter().also{
                            it.addTables(tables)
                        }

                    }
                }
                executePendingBindings()
            }
        }

        fun bindAdMob(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }
    }
}