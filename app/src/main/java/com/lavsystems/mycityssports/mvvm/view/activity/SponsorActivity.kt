package com.lavsystems.mycityssports.mvvm.view.activity

import android.app.ActivityOptions
import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.gms.ads.AdRequest
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivitySponsorBinding
import com.lavsystems.mycityssports.mvvm.view.adapter.SponsorAdapter
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem
import com.lavsystems.mycityssports.mvvm.viewmodel.SponsorViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class SponsorActivity : BaseActivity<ActivitySponsorBinding, SponsorViewModel>(),
    OnAdapterListenerFixtureItem  {

    private val viewModel: SponsorViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_sponsor
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): SponsorViewModel = viewModel
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private val sponsorAdapter = SponsorAdapter(mutableListOf(), this).also {
        it.setExpanded(true)
       // it.setHasStableIds(true)
    }

    override fun init() {
        initToolbar()
        initAdView()
        initSwipeRefresh()
        tryAgainEmptyList()
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            getSponsorsByCity()//sponsor view model is used for sponsor details too
            sponsors.observe(this@SponsorActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            if (it.isNotEmpty())
                                hideEmptyTitle()
                            else
                                showEmptyTitle()
                            sponsorAdapter.addNewList(it as MutableList<Any>)
                            sponsorAdapter.setExpanded(true)

                            initRecyclerView()
                        } ?: run {
                            showEmptyTitle()
                        }
                        hideFriendlyError()
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(resource.message ?: getString(R.string.loading_sponsor_data))
                    }
                    StatusResponse.ERROR -> {
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                        } ?: run {
                            showFriendlyError(getString(R.string.known_error))
                        }
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun showFriendlyError(subtitle: String) {
        handleFriendlyError(true, subtitle)
    }

    private fun hideFriendlyError() {
        handleFriendlyError(false, "")
    }

    private fun handleFriendlyError(show: Boolean, subtitle: String) {
        getRootDataBinding().clErrorVisibility = show
        getRootDataBinding().title = getString(R.string.error_title_fragment)
        getRootDataBinding().subtitle = subtitle
    }

    private fun initRecyclerView() {
        getRootDataBinding().iRvSponsors.rvContainer.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = sponsorAdapter
            sponsorAdapter.setExpanded(true)
        }
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout = getRootDataBinding().iRvSponsors.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getSponsorsByCity()
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun initAdView() {
        getRootDataBinding().adViewSponsor.loadAd(AdRequest.Builder().build())
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun showEmptyTitle() {
        getRootDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getRootDataBinding().empty = false
    }

    private fun tryAgainEmptyList() {
        getRootDataBinding().iFriendlyError.btnTryAgain.setOnClickListener {
            startActivity(Intent(this, SponsorActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onItemFixture(view: View, position: Int, fixtureId: String) {

    }

    override fun onItemSponsor(view: View, position: Int, sponsorId: String) {
        val intent = Intent(this, SponsorDetailsActivity::class.java)
        intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, sponsorId)
        val vs = view.findViewById<View>(R.id.iv_sponsor)
        val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(this, pairSponsor)
        startActivity(intent, transitionActivityOptions.toBundle())
    }
}
