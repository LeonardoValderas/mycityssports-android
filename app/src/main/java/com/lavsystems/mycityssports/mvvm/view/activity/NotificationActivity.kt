package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityNotificationBinding
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import com.lavsystems.mycityssports.mvvm.view.adapter.NotificationAdapter
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.viewmodel.NotificationViewModel
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class NotificationActivity : BaseActivity<ActivityNotificationBinding, NotificationViewModel>(),
    OnAdapterListener<NotificationModel> {

    private val viewModel: NotificationViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_notification
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): NotificationViewModel = viewModel
    private val notificationAdapter = NotificationAdapter(this)
    private var clickPosition = -1

    override fun init() {
        initToolbar()
        initRecyclerView()
        initAdView()
        tryAgainEmptyList()
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            getInstitutionsNotification()//sponsor view model is used for sponsor details too
            institutions.observe(this@NotificationActivity) { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            if (it.isNotEmpty())
                                hideEmptyTitle()
                            else
                                showEmptyTitle()
                            showContainer()
                            notificationAdapter.addNotificationInstitutions(it)
                        } ?: run {
                            showEmptyTitle()
                        }
                        hideFriendlyError()
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(resource.message ?: getString(R.string.loading_institution_data))
                    }
                    StatusResponse.ERROR -> {
                        hideContainer()
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        hideContainer()
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                        } ?: run {
                            showFriendlyError(getString(R.string.known_error))
                        }
                        hideLoading()
                    }
                }
            }

            saved.observe(this@NotificationActivity) { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            notificationAdapter.setNotificationInstitution(clickPosition, it)
                        } ?: run {
                            showEmptyTitle()
                        }
                        hideFriendlyError()
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(resource.message ?: getString(R.string.loading_wait_processing))
                    }
                    StatusResponse.ERROR -> {
                        hideContainer()
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        hideContainer()
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                        } ?: run {
                            showFriendlyError(getString(R.string.known_error))
                        }
                        hideLoading()
                    }
                }
            }
        }
    }

    private fun showFriendlyError(subtitle: String) {
        handleFriendlyError(true, subtitle)
    }

    private fun hideFriendlyError() {
        handleFriendlyError(false, "")
    }

    private fun handleFriendlyError(show: Boolean, subtitle: String) {
        getRootDataBinding().clErrorVisibility = show
        getRootDataBinding().title = getString(R.string.error_title_fragment)
        getRootDataBinding().subtitle = subtitle
    }

    private fun initRecyclerView() {
        getRootDataBinding().iRvInstitutionsNotification.rvContainer.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = notificationAdapter
        }
    }

    private fun showContainer(){
        getRootDataBinding().iRvInstitutionsNotification.root.visibility = View.VISIBLE
    }

    private fun hideContainer(){
        getRootDataBinding().iRvInstitutionsNotification.root.visibility = View.INVISIBLE
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun initAdView() {
        getRootDataBinding().adViewNotification.loadAd(AdRequest.Builder().build())
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun showEmptyTitle() {
        getRootDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getRootDataBinding().empty = false
    }

    private fun tryAgainEmptyList() {
        getRootDataBinding().iFriendlyError.btnTryAgain.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onItemClick(view: View, position: Int, model: NotificationModel) {
        clickPosition = position
        if(model.selected){
            viewModel.removeNotificationInstitution(model.institution.id)
        } else {
            viewModel.addNotificationInstitution(model.institution.id)
        }
    }
}
