package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankingsModel(
    @Json(name = "_id")
    val id: String,
    val rankingType: RankingTypeModel,
    val isDescendent: Boolean,
    val rankings: MutableList<RankingItemModel>
) : ExpandableRecyclerViewAdapter.ExpandableGroup<Any>() {
    constructor() : this("", RankingTypeModel(), true, mutableListOf())

    override fun getExpandingItems(): MutableList<Any> {
        return rankings as MutableList<Any>
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RankingsModel

        if (id != other.id) return false
        if (rankingType != other.rankingType) return false
        if (isDescendent != other.isDescendent) return false
        if (rankings != other.rankings) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + rankingType.hashCode()
        result = 31 * result + isDescendent.hashCode()
        result = 31 * result + rankings.hashCode()
        return result
    }

    override fun asReversed() {
        rankings.reverse()
    }
}