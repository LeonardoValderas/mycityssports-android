package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SportModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val gender: GenderModel,
    val urlImage: String?,
    val resourceDrawable: String
) {
    constructor() : this(
        "",
        "",
        GenderModel(),
        "",
        ""
    )

    val nameGender: String
        get() = name.plus(" ").plus(gender.name)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SportModel
        if (id != other.id) return false
        if (name != other.name) return false
        if (gender != other.gender) return false
        if (urlImage != other.urlImage) return false
        if (resourceDrawable != other.resourceDrawable) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + gender.hashCode()
        result = 31 * result + urlImage.hashCode()
        result = 31 * result + resourceDrawable.hashCode()
        return result
    }
}