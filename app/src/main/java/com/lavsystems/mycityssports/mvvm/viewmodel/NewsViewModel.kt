package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.NewsRepository
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class NewsViewModel(
    private val repository: NewsRepository
) : BaseViewModel() {
    companion object {
        const val NEWS = "News"
        const val NEWS_UNKNOWN_ERROR = "Error unknown in get news data."
    }
    private val _news = MutableLiveData<DataResponse<NewsDto>>()
    val news: LiveData<DataResponse<NewsDto>>
        get() = _news

    init {
        getNews(false)
    }

    fun getNews(isRefresh: Boolean) {
        try {
            repository.getNews(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _news.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, NEWS)
                   _news.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _news.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _news.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, NEWS)
                            } ?: run {
                                simpleErrorLog(it.message ?: NEWS_UNKNOWN_ERROR,
                                    NEWS
                                )
                            }
                            _news.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, NEWS)
            _news.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}