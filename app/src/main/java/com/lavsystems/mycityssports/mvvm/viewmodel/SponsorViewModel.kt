package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.SponsorRepository
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class SponsorViewModel(private val repository: SponsorRepository) : BaseViewModel() {

    companion object{
        const val SPONSORS = "Sponsors"
        const val SPONSOR_BY_ID_UNKNOWN_ERROR = "Error unknown in get sponsor by id."
        const val SPONSORS_BY_CITY_ID_UNKNOWN_ERROR = "Error unknown in get sponsors by city id."
    }
    private val _sponsor = MutableLiveData<DataResponse<SponsorModel>>()
    val sponsor: LiveData<DataResponse<SponsorModel>>
        get() = _sponsor

    private val _sponsors = MutableLiveData<DataResponse<MutableList<SponsorDto>>>()
    val sponsors: LiveData<DataResponse<MutableList<SponsorDto>>>
        get() = _sponsors

    fun getSponsor(sponsorId: String) {
        try {
            repository.getSponsor(sponsorId)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _sponsor.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, SPONSORS)
                    _sponsor.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _sponsor.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _sponsor.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, SPONSORS)
                            } ?: run {
                                simpleErrorLog(it.message ?: SPONSOR_BY_ID_UNKNOWN_ERROR, SPONSORS)
                            }
                            _sponsor.value = DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, SPONSORS)
            _sponsor.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }

    fun getSponsorsByCity() {
        try {
            repository.getSponsorsByCity()
                .flowOn(Dispatchers.IO)
                .onStart {
                    _sponsors.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, SPONSORS)
                    _sponsors.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _sponsors.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _sponsors.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, SPONSORS)
                            } ?: run {
                                simpleErrorLog(it.message ?: SPONSORS_BY_CITY_ID_UNKNOWN_ERROR, SPONSORS)
                            }
                            _sponsors.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, SPONSORS)
            _sponsors.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}