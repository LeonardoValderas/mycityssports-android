package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.InstitutionDetailsRepository
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class InstitutionDetailsViewModel(private val repository: InstitutionDetailsRepository) : BaseViewModel() {

    companion object {
        const val INSTITUTIONS_DETAILS = "Institutions_Details"
        const val INSTITUTIONS_DETAILS_UNKNOWN_ERROR = "Error unknown in get institution data."
    }

    private val _institution = MutableLiveData<DataResponse<InstitutionModel>>()
    val institution: LiveData<DataResponse<InstitutionModel>>
        get() = _institution

    fun getInstitution(institutionId: String) {
        try {
            repository.getInstitution(institutionId)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _institution.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, INSTITUTIONS_DETAILS)
                    _institution.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _institution.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _institution.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, INSTITUTIONS_DETAILS)
                            } ?: run {
                                simpleErrorLog(it.message ?: INSTITUTIONS_DETAILS_UNKNOWN_ERROR,
                                    INSTITUTIONS_DETAILS
                                )
                            }
                            _institution.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, INSTITUTIONS_DETAILS)
            _institution.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}