package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TableItemModel(
    val team: TeamModel,
    val position: Int,
    val points: TableTotalModel,
    val pj: TableTotalModel,
    val pg: TableTotalModel,
    val pe: TableTotalModel,
    val pp: TableTotalModel,
    val gf: TableTotalModel,
    val ge: TableTotalModel,
    val dg: TableTotalModel
) {
    constructor() : this(
        TeamModel(),
        0,
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel(),
        TableTotalModel()
        )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TableItemModel

        if (team != other.team) return false
        if (position != other.position) return false
        if (points != other.points) return false
        if (pj != other.pj) return false
        if (pg != other.pg) return false
        if (pe != other.pe) return false
        if (pp != other.pp) return false
        if (gf != other.gf) return false
        if (ge != other.ge) return false
        if (dg != other.dg) return false

        return true
    }

    override fun hashCode(): Int {
        var result = team.hashCode()
        result = 31 * result + position
        result = 31 * result + points.hashCode()
        result = 31 * result + pj.hashCode()
        result = 31 * result + pg.hashCode()
        result = 31 * result + pe.hashCode()
        result = 31 * result + pp.hashCode()
        result = 31 * result + gf.hashCode()
        result = 31 * result + ge.hashCode()
        result = 31 * result + dg.hashCode()
        return result
    }


}