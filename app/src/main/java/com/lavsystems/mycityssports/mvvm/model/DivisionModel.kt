package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DivisionModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val institution: String,
    var selected: Boolean = false
) {
    constructor(): this("", "", "", false)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DivisionModel

        if (name != other.name) return false
        if (institution != other.institution) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + institution.hashCode()
        return result
    }
}