package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ScorerModel(
    val player: PlayerModel?,
    val minute: Int?,
    val against: Boolean
) {
    constructor(): this(null, null, false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScorerModel

        if (player != other.player) return false
        if (minute != other.minute) return false
        if (against != other.against) return false

        return true
    }

    override fun hashCode(): Int {
        var result = player?.hashCode() ?: 0
        result = 31 * result + (minute ?: 0)
        result = 31 * result + against.hashCode()
        return result
    }

}