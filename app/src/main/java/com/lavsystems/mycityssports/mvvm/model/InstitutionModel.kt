package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InstitutionModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val description: String,
    val address: AddressModel,
    val city: CityModel,
    val urlImage: String?,
    val hasSponsor: Boolean = true,
    val phone: String,
    val mobile: String,
    val email: String,
    val web: String,
    val facebook: String,
    val instagram: String,
    val source: String
){
    constructor() : this(
        "",
        "",
        "",
        AddressModel(),
        CityModel(),
        "",
        true,
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    )

    companion object{
        const val WITHOUT_INFO = "Sin información"
        const val SOURCE = "Fuente: "
    }

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source

    override fun toString(): String {
        return name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as InstitutionModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (address != other.address) return false
        if (city != other.city) return false
        if (urlImage != other.urlImage) return false
        if (hasSponsor != other.hasSponsor) return false
        if (phone != other.phone) return false
        if (mobile != other.mobile) return false
        if (email != other.email) return false
        if (web != other.web) return false
        if (facebook != other.facebook) return false
        if (instagram != other.instagram) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + city.hashCode()
        result = 31 * result + (urlImage?.hashCode() ?: 0)
        result = 31 * result + hasSponsor.hashCode()
        result = 31 * result + phone.hashCode()
        result = 31 * result + mobile.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + web.hashCode()
        result = 31 * result + facebook.hashCode()
        result = 31 * result + instagram.hashCode()
        return result
    }


}
