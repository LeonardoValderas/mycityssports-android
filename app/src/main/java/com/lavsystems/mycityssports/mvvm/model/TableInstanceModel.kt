package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.JsonClass

@Suppress("UNCHECKED_CAST")
@JsonClass(generateAdapter = true)
class TableInstanceModel(
    val tournamentInstance: TournamentInstanceModel,
    val tableDataInstance: MutableList<TableInstanceItemModel>
): ExpandableRecyclerViewAdapter.ExpandableGroup<Any>(){
    constructor() : this(
        TournamentInstanceModel(),
        mutableListOf()
    )
    override fun getExpandingItems(): MutableList<Any> {
        return tableDataInstance as MutableList<Any>
    }

    override fun asReversed() {
        tableDataInstance.reverse()
    }
}