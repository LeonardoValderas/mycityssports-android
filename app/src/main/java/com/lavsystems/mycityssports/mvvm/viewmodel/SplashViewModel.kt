package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.repository.SplashRepository
import com.lavsystems.mycityssports.utils.EventWrapper
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class SplashViewModel(private val repository: SplashRepository) : BaseViewModel() {

    val countryAndCityExists = repository.countryAndCityExists()
        .flowOn(Dispatchers.Main)
        .catch { e ->
            exceptionToLog(e, SPLASH)
            DataResponse.FAILURE<EventWrapper<Boolean>>(ExceptionHandleImpl(e))
        }
        .onStart {
            DataResponse.LOADING<EventWrapper<Boolean>>()
        }.map {
            delay(2000)
            DataResponse.SUCCESS(EventWrapper(it))
        }.asLiveData(viewModelScope.coroutineContext)

    companion object {
        const val SPLASH = "Splash"
    }
}