package com.lavsystems.mycityssports.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.CountryItemBinding
import com.lavsystems.mycityssports.mvvm.model.CountryModel

class CountrySpinnerAdapter (context: Context, resource: Int, var countries: MutableList<CountryModel>) :
    ArrayAdapter<CountryModel>(context, resource, countries) {

    override fun getItem(position: Int): CountryModel? {
        return countries[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return countries.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = CountryItemBinding.inflate(inflater, parent, false)
        val country = getCountryModelForPosition(position)
        bind(binding, country)
        return binding.root
    }

    fun getCountryModelForPosition(position: Int): CountryModel {
        return countries[position]
    }

    private fun bind(binding: CountryItemBinding, country: CountryModel) {
        with(binding) {
            model = country
            YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    fun setCountriesList(countries: MutableList<CountryModel>) {
        this.countries.clear()
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }
}




