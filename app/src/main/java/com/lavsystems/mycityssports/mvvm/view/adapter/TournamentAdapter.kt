package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.TournamentItemBinding
import com.lavsystems.mycityssports.mvvm.model.TournamentModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerTournament

class TournamentAdapter(private val listener: OnAdapterListenerTournament) :
    RecyclerView.Adapter<TournamentAdapter.ViewHolder>() {

    private var tournaments = mutableListOf<TournamentModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TournamentItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(tournaments[position], listener)

    override fun getItemCount(): Int = tournaments.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun addTournaments(divisions: MutableList<TournamentModel>) {
        this.tournaments.clear()
        this.tournaments.addAll(divisions)
        notifyDataSetChanged()
    }

    fun deselectAllModels() {
        tournaments.forEach { tournament -> tournament.selected = false }
        notifyDataSetChanged()
    }

    fun selectModel(position: Int) {
        tournaments[position].selected = true
        notifyDataSetChanged()
    }

    fun getItemForPosition(position: Int): TournamentModel {
        return tournaments[position]
    }

    fun selectItemIfExistFromTournamentIdOrSetFirstPosition(idTournament: String): Int {
        var indexTournament = -1
        run loop@{
            tournaments.forEachIndexed { index, tournament ->
                if (tournament.id == idTournament) {
                    indexTournament = index
                    return@loop
                }
            }
        }

        deselectAllModels()

        if (indexTournament == -1)
            selectModel(0)
        else
            selectModel(indexTournament)

        return indexTournament
    }

    class ViewHolder(private var binding: TournamentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(tournament: TournamentModel, listener: OnAdapterListenerTournament) {
            with(binding) {
                model = tournament
                YoYo.with(Techniques.FadeIn).playOn(root)
                root.setOnClickListener {
                    listener.onItemTournamentClick(it, layoutPosition, tournament)
                    YoYo.with(Techniques.Pulse).playOn(root)
                }
                executePendingBindings()
            }
        }
    }
}