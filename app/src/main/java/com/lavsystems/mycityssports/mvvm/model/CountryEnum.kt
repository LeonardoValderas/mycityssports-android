package com.lavsystems.mycityssports.mvvm.model

enum class CountryEnum(val value: Int) {
    ARGENTINA(1) ,
    BRAZIL(2)
}