package com.lavsystems.mycityssports.mvvm.view.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.databinding.FragmentRankingBinding
import com.lavsystems.mycityssports.mvvm.model.RankingModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.activity.SponsorDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.view.utils.ConcatAdapterUtils
import com.lavsystems.mycityssports.mvvm.viewmodel.RankingViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.ref.WeakReference

@ExperimentalCoroutinesApi
@Suppress("UNCHECKED_CAST")
class RankingFragment : BaseFragment<FragmentRankingBinding, RankingViewModel>(),
    OnAdapterListener<SponsorModel> {

    private val viewModel: RankingViewModel by viewModel()
    private var concatAdapter = ConcatAdapter()
    override fun initViewModel(): RankingViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_ranking
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initSwipeRefresh()
    }

    override fun initObservers() {
        with(getViewModel()) {
            ranking.observe(this@RankingFragment, { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data.let { r ->
                            r?.let {
                                 it.ranking?.let {
                                     if (it.rankings.isNotEmpty())
                                         hideEmptyTitle()
                                     else
                                         showEmptyTitle()

                                        val sponsor = getSponsorModel(r.sponsors)
                                        concatAdapter =
                                            ConcatAdapterUtils<RankingModel>(WeakReference(context), null).apply {
                                                setModel(
                                                    r.ranking!!,
                                                    AdMobAdapter(
                                                        R.layout.adsmob_item_ranking,
                                                        AdMobItem()
                                                    ),
                                                    if(sponsor == null) null else SponsorItemAdapter(R.layout.sponsor_item, sponsor, this@RankingFragment)
                                                )
                                            }.getConcatAdapter()
                                        initRecyclerView()
                                    } ?: run {
                                        showEmptyTitle()
                                    }
                            } ?: run {
                                showEmptyTitle()
                            }
                        }
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        getCommunicator()?.showLoadingUI(R.string.loading_ranking_data)
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            getCommunicator()?.showErrorFragment(it)
                        } ?: run {
                            val exceptionHandle =
                                ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                            getCommunicator()?.showErrorFragment(exceptionHandle)
                        }

                        hideLoading()
                    }
                    else -> {
                        val exceptionHandle =
                            ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                        getCommunicator()?.showErrorFragment(exceptionHandle)
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun getSponsorModel(sponsors: MutableList<SponsorModel>): SponsorModel?{
        if(sponsors.isNotEmpty()){
            return sponsors[(0 until sponsors.size).random()]
        }

        return null
    }

    override fun getFromHome() {
        getViewModel().getRanking(false)
    }

    private fun initRecyclerView() {
        getViewDataBinding().iRvRanking.rvContainer.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = concatAdapter
        }
    }

    @ExperimentalCoroutinesApi
    private fun initSwipeRefresh() {
        swipeRefreshLayout = getViewDataBinding().iRvRanking.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getRanking(true)
        }
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getCommunicator()?.hideLoadingUI()
    }

    private fun showEmptyTitle() {
        getViewDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getViewDataBinding().empty = false
    }

    private fun setFabVisibility(show: Boolean) {
        if (show)
            getCommunicator()?.showFAB()
        else
            getCommunicator()?.hideFAB()
    }

    override fun onItemClick(view: View, position: Int, sponsor: SponsorModel) {
        val intent = Intent(activity?.applicationContext, SponsorDetailsActivity::class.java)
        intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, sponsor.id)
        val vs = view.findViewById<View>(R.id.iv_sponsor)
        val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(activity, pairSponsor)
        startActivity(intent, transitionActivityOptions.toBundle())
    }
}
