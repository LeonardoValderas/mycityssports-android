package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.NotificationInstitutionItemBinding
import com.lavsystems.mycityssports.mvvm.model.NotificationModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener

class NotificationAdapter(private val listener: OnAdapterListener<NotificationModel>) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    private var notificationInstitutions = mutableListOf<NotificationModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = NotificationInstitutionItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(notificationInstitutions[position], listener)

    override fun getItemCount(): Int = notificationInstitutions.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getNotificationInstitutions(position: Int): NotificationModel {
        val institution = notificationInstitutions[position]
        return institution
    }

    fun setNotificationInstitution(position: Int, notificationModel: NotificationModel) {
        notificationInstitutions.removeAt(position)
        notificationInstitutions.add(position, notificationModel)
        notifyItemChanged(position)
    }

    fun addNotificationInstitutions(notificationInstitutions: MutableList<NotificationModel>) {
        this.notificationInstitutions.clear()
        this.notificationInstitutions.addAll(notificationInstitutions)
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: NotificationInstitutionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(institution: NotificationModel, listener: OnAdapterListener<NotificationModel>) {
            with(binding) {

                model = institution

                YoYo.with(Techniques.FadeIn).playOn(root)

                cvInstitution.setOnClickListener {
                    listener.onItemClick(it, layoutPosition, institution)
                    YoYo.with(Techniques.Pulse).playOn(root)
                }

                executePendingBindings()
            }
        }
    }
}