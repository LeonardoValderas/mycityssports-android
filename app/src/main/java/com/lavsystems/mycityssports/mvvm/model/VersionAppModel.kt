package com.lavsystems.mycityssports.mvvm.model


import androidx.room.Embedded
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VersionAppModel(
    @Json(name = "_id")
    val id: String,
    val android: String
) {
    constructor() : this(
        "",
        "",
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VersionAppModel

        if (id != other.id) return false
        if (android != other.android) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + android.hashCode()
        return result
    }

}