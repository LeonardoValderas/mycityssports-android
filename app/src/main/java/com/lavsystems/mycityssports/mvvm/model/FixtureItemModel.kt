package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.DateTimeUtils
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixtureItemModel(
    @Json(name = "_id")
    val id: String,
    val dateTime: String,
    val round: Int,
    val sport: SportModel,
    val tournament: String,
    val division: String,
    val tournamentZone: TournamentZoneModel?,
    val tournamentInstance: TournamentInstanceModel?,
    val homeTeam: TeamModel,
    val referees: MutableList<FixtureRefereeModel>?,
    val homeResult: String?,
    val homeScorers: MutableList<ScorerModel>?,
    val homeCards: MutableList<CardPlayerModel>?,
    val homeResultPenalty: String?,
    val visitorsTeam: TeamModel,
    val visitorsResult: String?,
    val visitorsScorers: MutableList<ScorerModel>?,
    val visitorsCards: MutableList<CardPlayerModel>?,
    val visitorsResultPenalty: String?,
    val playingField: PlayingFieldModel,
    val comment: String,
    val isFinish: Boolean,
    val source: String = ""
) {
    constructor() : this(
        "",
        "",
        -1,
        SportModel(),
        "",
        "",
        null,
        null,
        TeamModel(),
        mutableListOf(),
        "",
        mutableListOf(),
        mutableListOf(),
        "",
        TeamModel(),
        "",
        mutableListOf(),
        mutableListOf(),
        "",
        PlayingFieldModel(),
        "",
        false,
        ""
    )

    companion object {
        const val PLACE = "Lugar: "
        const val SOURCE = "Fuente: "
        const val DATE = "Fecha"
        const val COMMENTE_CHAR = "* "
        const val WITHOUT_DATA = "(sin dato)"
    }

//    val roundString: String
//        get() = "$DATE $round"

    val roundStg: String
        get() = "$round"

    val hasReferees: Boolean
        get() = refereeString.isNotEmpty()

    val refereeString: String
        get() {
            var string = ""
            referees?.run {
                val size = size - 1
                forEachIndexed { index, referee ->
                    string += getRefereeString(referee, size == index)
                }
            }
            return string
        }

    val hasScorers: Boolean
        get() = homeScorersString.isNotEmpty() || visitorsScorersString.isNotEmpty()

    val homeScorersString: String
        get() {
            val orderArray = homeScorers?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, scorerPlayer ->
                string += getScorerString(scorerPlayer, ((orderArray.size - 1) == index))
            }
            return string
        }

    val visitorsScorersString: String
        get() {
            val orderArray = visitorsScorers?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, scorerPlayer ->
                string += getScorerString(scorerPlayer, ((orderArray.size - 1) == index))
            }
//            var string = ""
//            visitorsScorers?.map { scorer ->
//                string += "${scorer.player?.name} (${scorer?.minute?.toString()})\n"
//            }
            return string
        }

    val hasCards: Boolean
        get() = homeYellowCardsString.isNotEmpty() || homeRedCardsString.isNotEmpty() ||
                visitorsYellowCardsString.isNotEmpty() || visitorsRedCardsString.isNotEmpty()

    val hasHomeYellowCards: Boolean
        get() = homeYellowCardsString.isNotEmpty()

    val hasHomeRedCards: Boolean
        get() = homeRedCardsString.isNotEmpty()

    val hasVisitorsYellowCards: Boolean
        get() = visitorsYellowCardsString.isNotEmpty()

    val hasVisitorsRedCards: Boolean
        get() = visitorsRedCardsString.isNotEmpty()

    val homeYellowCardsString: String
        get() {
            val yellowList = homeCards?.filter { it.card.charLetter == CardModel.CharLetter.Y }
            val orderArray = yellowList?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, cardPlayer ->
                string += getCardString(cardPlayer, ((orderArray.size - 1) == index))
            }
            return string
        }

    val homeRedCardsString: String
        get() {
            val redList = homeCards?.filter { it.card.charLetter == CardModel.CharLetter.R }
            val orderArray = redList?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, cardPlayer ->
                string += getCardString(cardPlayer, ((orderArray.size - 1) == index))
            }
            return string
        }

    val visitorsYellowCardsString: String
        get() {
            val yellowList = visitorsCards?.filter { it.card.charLetter == CardModel.CharLetter.Y}
            val orderArray = yellowList?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, cardPlayer ->
                string += getCardString(cardPlayer, ((orderArray.size - 1) == index))
            }
            return string
        }

    val visitorsRedCardsString: String
        get() {
            val redList = visitorsCards?.filter { it.card.charLetter == CardModel.CharLetter.R}
            val orderArray = redList?.sortedBy { it.minute }
            var string = ""
            orderArray?.mapIndexed { index, cardPlayer ->
                string += getCardString(cardPlayer, ((orderArray.size - 1) == index))
            }
            return string
        }

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source

    val commentFormatted: String
        get() = if(comment.isEmpty()) COMMENTE_CHAR + " - " else COMMENTE_CHAR + comment

    val playingFieldNameFormatted: String
        get() = PLACE + playingField.name

    private fun getCardString(cardPlayer: CardPlayerModel, isLast: Boolean): String {
        //return "${cardPlayer.card.name.first()} ${if (cardPlayer.player !== null) cardPlayer.player.name else "(sin dato)"} ${if(cardPlayer.minute != null) "(" + cardPlayer.minute + ")" else ""} ${if(isLast) "" else "\n"}"
        return "${if(cardPlayer.player !== null) cardPlayer.player.name else WITHOUT_DATA} ${if(cardPlayer.minute != null) "(" + cardPlayer.minute + ")" else ""} ${if(isLast) "" else "\n"}"
    }

    private fun getScorerString(scorer: ScorerModel, isLast: Boolean): String {
        return "${if (scorer.player !== null) scorer.player.name else WITHOUT_DATA} ${if(scorer.minute != null) "(" + scorer.minute + ")" else ""} ${if(scorer.against) "c" else ""} ${if(isLast) "" else "\n"}"
    }

    private fun getRefereeString(refereeModel: FixtureRefereeModel, isLast: Boolean): String {
        return "${refereeModel.referee.name } - ${refereeModel.refereeFunction.name} ${if(isLast) "" else "\n"}"
    }

    val dateFormatted: String
        get() = DateTimeUtils.dateShortFormatString(dateTime, "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy HH:mm")

    val homeResultPenaltyFormat: String
        get() = if(homeResultPenalty.isNullOrBlank()) "" else "($homeResultPenalty)"

    val visitorsResultPenaltyFormat: String
        get() = if(visitorsResultPenalty.isNullOrBlank()) "" else "($visitorsResultPenalty)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FixtureItemModel

        if (id != other.id) return false
        if (dateTime != other.dateTime) return false
        if (round != other.round) return false
        if (sport != other.sport) return false
        if (tournament != other.tournament) return false
        if (division != other.division) return false
        if (tournamentZone != other.tournamentZone) return false
        if (tournamentInstance != other.tournamentInstance) return false
        if (homeTeam != other.homeTeam) return false
        if (homeResult != other.homeResult) return false
        if (homeScorers != other.homeScorers) return false
        if (homeCards != other.homeCards) return false
        if (homeResultPenalty != other.homeResultPenalty) return false
        if (visitorsTeam != other.visitorsTeam) return false
        if (visitorsResult != other.visitorsResult) return false
        if (visitorsScorers != other.visitorsScorers) return false
        if (visitorsCards != other.visitorsCards) return false
        if (visitorsResultPenalty != other.visitorsResultPenalty) return false
        if (playingField != other.playingField) return false
        if (comment != other.comment) return false
        if (isFinish != other.isFinish) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + dateTime.hashCode()
        result = 31 * result + round
        result = 31 * result + sport.hashCode()
        result = 31 * result + tournament.hashCode()
        result = 31 * result + division.hashCode()
        result = 31 * result + (tournamentZone?.hashCode() ?: 0)
        result = 31 * result + (tournamentInstance?.hashCode() ?: 0)
        result = 31 * result + homeTeam.hashCode()
        result = 31 * result + (homeResult?.hashCode() ?: 0)
        result = 31 * result + (homeScorers?.hashCode() ?: 0)
        result = 31 * result + (homeCards?.hashCode() ?: 0)
        result = 31 * result + (homeResultPenalty?.hashCode() ?: 0)
        result = 31 * result + visitorsTeam.hashCode()
        result = 31 * result + (visitorsResult?.hashCode() ?: 0)
        result = 31 * result + (visitorsScorers?.hashCode() ?: 0)
        result = 31 * result + (visitorsCards?.hashCode() ?: 0)
        result = 31 * result + (visitorsResultPenalty?.hashCode() ?: 0)
        result = 31 * result + playingField.hashCode()
        result = 31 * result + comment.hashCode()
        result = 31 * result + isFinish.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}