package com.lavsystems.mycityssports.mvvm.view.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.databinding.FragmentNewsBinding
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.activity.SponsorDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.adapter.NewsAdapter
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.viewmodel.NewsViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.uicomponents.RecyclerViewBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class NewsFragment : BaseFragment<FragmentNewsBinding, NewsViewModel>(), OnAdapterListener<Any> {
    private val viewModel: NewsViewModel by viewModel()
    override fun initViewModel(): NewsViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_news
    private var recyclerBuilder: RecyclerViewBuilder<Any>? = null
    private val adapter = NewsAdapter(this)
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initRecyclerView()
        initSwipeRefresh()
    }

    override fun initObservers() {
        with(getViewModel()) {
            news.observe(this@NewsFragment, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            if (it.news.isNotEmpty())
                                hideEmptyTitle()
                            else
                                showEmptyTitle()

                            adapter.addSponsors(it.sponsors as MutableList<Any>)
                            @Suppress("UNCHECKED_CAST")
                            recyclerBuilder?.addItems(it.news as MutableList<Any>, true)

                        } ?: run {
                            showEmptyTitle()
                        }

                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        getCommunicator()?.showLoadingUI(R.string.loading_news_data)
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            getCommunicator()?.showErrorFragment(it)
                        } ?: run {
                            val exceptionHandle =
                                ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                            getCommunicator()?.showErrorFragment(exceptionHandle)
                        }

                        hideLoading()
                    }
                    else -> {
                        val exceptionHandle =
                            ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                        getCommunicator()?.showErrorFragment(exceptionHandle)
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun initRecyclerView() {
        val recycler = getViewDataBinding().iRvNews.rvContainer
        recyclerBuilder = RecyclerViewBuilder
            .builder<Any>(recycler)
            .setLinearLayoutManagerOrientation(isHorizontal = false)
            .setDividerItemDecoration(isHorizontal = false)
            .setHasFixedSize(true)
            //.setEndlessRecyclerOnScrollListener(Page(), 2, this)
            .setHasStableIds(true)
            .setAdapter(adapter)
            .build()

    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout = getViewDataBinding().iRvNews.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getNews(true)
        }
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getCommunicator()?.hideLoadingUI()
    }

    private fun showEmptyTitle() {
        getViewDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getViewDataBinding().empty = false
    }

    override fun getFromHome() {
        getViewModel()?.getNews(false)
    }

//    override fun scrollUp(isUp: Boolean) {
//        setFabVisibility(isUp)
//    }

//    override fun onLoadMore(
//        page: Page,
//        adapter: RecyclerView.Adapter<DataBindingViewHolder<Any>>?
//    ) {
//        getViewModel().getNews(page, true, false)
//    }

    private fun setFabVisibility(show: Boolean) {
        if (show)
            getCommunicator()?.showFAB()
        else
            getCommunicator()?.hideFAB()
    }

    override fun onItemClick(view: View, position: Int, item: Any) {
        if(item is SponsorModel){
            val intent = Intent(activity?.applicationContext, SponsorDetailsActivity::class.java)
            intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, item.id)
            val vs = view.findViewById<View>(R.id.iv_sponsor)
            val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
            val transitionActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(activity, pairSponsor)
            startActivity(intent, transitionActivityOptions.toBundle())
        }
    }
}
