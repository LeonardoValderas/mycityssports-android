package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.databinding.DividerSourceBinding

class SourceAdapter(private var source: String):RecyclerView.Adapter<SourceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DividerSourceBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun setSource(source:String){
        this.source = source
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: DividerSourceBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            with(binding) {
                tvSource.text = source
                executePendingBindings()
            }
        }
    }
}