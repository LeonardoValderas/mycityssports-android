package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankingModel(
    @Json(name = "_id")
    val id: String,
    val tournament: String,
    val sport: String,
    val division: String,
    val subDivision: String? = "",
    val rankings: MutableList<RankingsModel>
) {
    constructor() : this("", "","","", "", mutableListOf())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RankingModel

        if (id != other.id) return false
        if (tournament != other.tournament) return false
        if (sport != other.sport) return false
        if (division != other.division) return false
        if (subDivision != other.subDivision) return false
        if (rankings != other.rankings) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + tournament.hashCode()
        result = 31 * result + sport.hashCode()
        result = 31 * result + division.hashCode()
        result = 31 * result + subDivision.hashCode()
        result = 31 * result + rankings.hashCode()
        return result
    }

}


