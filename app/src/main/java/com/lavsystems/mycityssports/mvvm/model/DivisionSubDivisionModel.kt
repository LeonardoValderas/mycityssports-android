package com.lavsystems.mycityssports.mvvm.model

data class DivisionSubDivisionModel(
    val divisionId: String,
    val divisionName: String,
    val subDivisionName: String,
    var selected: Boolean = false
) {
    constructor(): this("", "", "")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DivisionSubDivisionModel

        if (divisionId != other.divisionId) return false
        if (divisionName != other.divisionName) return false
        if (subDivisionName != other.subDivisionName) return false
        if (selected != other.selected) return false

        return true
    }

    override fun hashCode(): Int {
        var result = divisionId.hashCode()
        result = 31 * result + divisionName.hashCode()
        result = 31 * result + subDivisionName.hashCode()
        result = 31 * result + selected.hashCode()
        return result
    }

    override fun toString(): String {
        return if(divisionName.compareTo(subDivisionName, true) == 0)
                 divisionName
               else
                 "$divisionName $subDivisionName"
    }
}