package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource.Status
import com.lavsystems.mycityssports.data.repository.PlacesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.utils.EventWrapper
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class PlacesViewModel(private val repository: PlacesRepository) : BaseViewModel() {
    companion object {
        const val PLACES = "Places"
        const val PLACES_UNKNOWN_ERROR = "Error unknown in get places data."
        const val PLACES_SAVE_CITY_UNKNOWN_ERROR = "Problem to save city."
        const val PLACES_SAVE_COUTRY_UNKNOWN_ERROR = "Problem to save country."
    }

    private var citySelected: CityModel? = null
    private val _country = MutableLiveData<DataResponse<EventWrapper<Boolean>>>()
    val country: LiveData<DataResponse<EventWrapper<Boolean>>>
        get() = _country
    private val _city = MutableLiveData<DataResponse<EventWrapper<Boolean>>>()
    val city: LiveData<DataResponse<EventWrapper<Boolean>>>
        get() = _city
    private val _places = MutableLiveData<DataResponse<PlaceDto>>()
    val places: LiveData<DataResponse<PlaceDto>>
        get() = _places

    val countries = repository.getCountries()
        .map {
            it
        }.asLiveData(viewModelScope.coroutineContext)

    fun getPlaces() {
        repository.getPlacesData()
            .flowOn(Dispatchers.IO)
            .onStart {
                _places.value = DataResponse.LOADING()
            }.catch {
                exceptionToLog(it, PLACES)
                _places.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
            }.onCompletion {
                if (it == null) println("Completed successfully")
            }.map {
                when (it?.status) {
                    Status.LOADING -> {
                        _places.value = DataResponse.LOADING()
                    }
                    Status.SUCCESS -> {
                        _places.value = DataResponse.SUCCESS(it.data?.data)
                    }
                    Status.ERROR -> {
                        it.e?.let { t ->
                            exceptionToLog(t, PLACES)
                        } ?: run {
                            simpleErrorLog(it.message ?: PLACES_UNKNOWN_ERROR, PLACES)
                        }
                        _places.value =
                            DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                    }
                    else -> {}
                }
            }.launchIn(viewModelScope)
    }

    fun saveCity(city: CityModel) {
        try {
            repository.saveCity(city)
                .flowOn(Dispatchers.Main)
                .catch { e ->
                    exceptionToLog(e, PLACES)
                    _city.value = (DataResponse.FAILURE(ExceptionHandleImpl(e)))
                }
                .onStart {
                    _city.value = DataResponse.LOADING()
                }.map {
                    if (it)
                        _city.value = DataResponse.SUCCESS(EventWrapper(it))
                    else {
                        simpleErrorLog(PLACES_SAVE_CITY_UNKNOWN_ERROR, PLACES)
                        _city.value = DataResponse.ERROR(null, R.string.city_not_save_places)
                    }
                }.onCompletion {
                    //_city.value = (DataResponse.LOADING(false))
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, PLACES)
            _city.value = ((DataResponse.FAILURE(ExceptionHandleImpl(e))))
        }
    }

    fun saveCountry(country: CountryModel) {
        try {
            repository.saveCountry(country)
                .flowOn(Dispatchers.Main)
                .catch { e ->
                    exceptionToLog(e, PLACES)
                    _country.value = (DataResponse.FAILURE(ExceptionHandleImpl(e)))
                }
                .onStart {
                    _country.value = DataResponse.LOADING()
                }.map {
                    if (it)
                        _country.value = DataResponse.SUCCESS(EventWrapper(it))
                    else {
                        simpleErrorLog(PLACES_SAVE_COUTRY_UNKNOWN_ERROR, PLACES)
                        _country.value = DataResponse.ERROR(null, R.string.city_not_save_places)
                    }
                }.onCompletion {
                    //_city.value = (DataResponse.LOADING(false))
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, PLACES)
            _country.value = ((DataResponse.FAILURE(ExceptionHandleImpl(e))))
        }
    }

    fun setCitySelected(citySelected: CityModel?) {
        this.citySelected = citySelected
    }

    fun getCitySelected() = citySelected
}