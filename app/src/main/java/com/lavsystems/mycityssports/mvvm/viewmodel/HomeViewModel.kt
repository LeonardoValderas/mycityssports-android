package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.BuildConfig
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.HomeDto
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.FcmRepository
import com.lavsystems.mycityssports.data.repository.HomeRepository
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.mvvm.view.adapter.DivisionsSubDivisionsFromTournamentAdapter
import com.lavsystems.mycityssports.utils.EventWrapper
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class HomeViewModel(
    private val repository: HomeRepository,
    private val fcmRepository: FcmRepository
) : BaseViewModel() {

    companion object {
        const val HOMES = "Homes"
        const val HOMES_UNKNOWN_ERROR = "Error unknown in get home data."
    }

    private var headerItems = mutableListOf<HomeDto>()

    private var institutionsAux: MutableList<InstitutionModel> = mutableListOf()

    private var tournamentsAux: MutableList<TournamentModel> = mutableListOf()

    private val _institutions = MutableLiveData<MutableList<InstitutionModel>>()
    val institutions: LiveData<MutableList<InstitutionModel>>
        get() = _institutions

    private val _sports = MutableLiveData<MutableList<SportModel>>()
    val sports: LiveData<MutableList<SportModel>>
        get() = _sports

//    private val _divisions = MutableLiveData<MutableList<DivisionModel>>()
//    val divisions: LiveData<MutableList<DivisionModel>>
//        get() = _divisions

    private val _divisionsSubDivisions = MutableLiveData<MutableList<DivisionSubDivisionModel>>()
    val divisionsSubDivisions: LiveData<MutableList<DivisionSubDivisionModel>>
        get() = _divisionsSubDivisions

    private val _tournaments = MutableLiveData<MutableList<TournamentModel>>()
    val tournaments: LiveData<MutableList<TournamentModel>>
        get() = _tournaments

    private val _sportsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val sportsEmpty: LiveData<EventWrapper<Boolean>>
        get() = _sportsIsEmpty

    private val _institutionsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val institutionsIsEmpty: LiveData<EventWrapper<Boolean>>
        get() = _institutionsIsEmpty

    private val _cityEmpty = MutableLiveData<EventWrapper<Boolean>>()

    private val _needUpdateVersion = MutableLiveData<EventWrapper<Boolean>>()
    val needUpdateVersion: LiveData<EventWrapper<Boolean>>
        get() = _needUpdateVersion

//    val cityEmpty: LiveData<EventWrapper<Boolean>>
//        get() = _cityEmpty

//    private val _sponsor = MutableLiveData<DataResponse<EventWrapper<Boolean>>>()
//    val sponsor: LiveData<DataResponse<EventWrapper<Boolean>>>
//        get() = _sponsor

//    private val _divisionsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
//    val divisionsIsEmpty: LiveData<EventWrapper<Boolean>>
//        get() = _divisionsIsEmpty

    private val _divisionsSubDivisionsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val divisionsSubDivisionsIsEmpty: LiveData<EventWrapper<Boolean>>
        get() = _divisionsSubDivisionsIsEmpty

    private val _tournamentsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val tournamentsIsEmpty: LiveData<EventWrapper<Boolean>>
        get() = _tournamentsIsEmpty

    private val _changeFragment = MutableLiveData<EventWrapper<Boolean>>()
    val changeFragment: LiveData<EventWrapper<Boolean>>
        get() = _changeFragment

    //region INSTITUTION
    //Looking for the institution between all city's institutions
    //set headerItem
    private var institution = InstitutionModel()
        set(value) {
            field = value
            setHomeDtoByInstitutionId()
        }

    private fun setHomeDtoByInstitutionId() {
        val homeHeader = headerItems.firstOrNull { item ->
            //getSponsorsOfInstitution(item.institution.id)
            institution.id == item.institution.id
        }

        homeHeader?.let {
            headerItem = it
            return
        }

        headerItem = HomeDto()
    }

    fun setInstitutionModel(institutionModel: InstitutionModel) {
        //TODO validate this flow
        //removeTournamentOnPreferences() //why?
       // removeDivisionSubDivisionOnPreferences() //why?
        saveInstitutionIdOnPreferences(institutionModel.id)
        institution = institutionModel
    }

    //endregion

    //region HEADER

    //validate if sports is empty
    //set sports and tournamnets aux
    private var headerItem = HomeDto()
        set(value) {
            field = value
            setHeaderDtoItems(value)
        }

    private fun setHeaderDtoItems(homeDto: HomeDto) {
        homeDto.let {
            if (it.sports.isNullOrEmpty()) {
                setSports(mutableListOf())
                setDivisionsSubDivisions(mutableListOf())
                //setDivisions(mutableListOf())
                setTournaments(mutableListOf())
                return@let
            }
            tournamentsAux = it.tournaments
            setSports(it.sports)
        }
    }
    //endregion

    //region SPORT
    //save sport on preference and get tournaments by id sport
    private var sport = SportModel()
        set(value) {
            field = value
            getTournamentsFromSportId(value.id)
        }

    private fun setSports(sports: MutableList<SportModel>) {
        sportsIsEmpty(sports.isEmpty())
        _sports.value = sports
    }

    private fun getTournamentsFromSportId(sportId: String) {
        val tournaments = tournamentsAux.filter { t ->
            t.sports.firstOrNull { id ->
                id == sportId
            } != null
        }.toMutableList()
        setTournaments(tournaments)
    }

    fun setSportModel(sportModel: SportModel) {
        saveSportIdOnPreferences(sportModel.id)
        sport = sportModel
    }
    //endregion

    //region TOURNAMENT
    private var tournament = TournamentModel()
        set(value) {
            field = value
            getDivisionsSubDivisionsFromTournament(value)
        }

    private fun getDivisionsSubDivisionsFromTournament(tournament: TournamentModel) {
        val adapter = DivisionsSubDivisionsFromTournamentAdapter(tournament)
        val divisionsSubDivisions = adapter.getDivisionSubDivisionModel()
        setDivisionsSubDivisions(divisionsSubDivisions)
    }

    fun setTournamentModel(tournament: TournamentModel) {
        saveTournamentIdOnPreferences(tournament.id)
        this.tournament = tournament
    }
    //endregion

    //region DIVISIONSUBDIVISION
    private var divisionSubDivision = DivisionSubDivisionModel()
        set(value) {
            field = value
            _changeFragment.value = EventWrapper(true)
        }

    fun setDivisionSubDivisionModel(divisionSubDivision: DivisionSubDivisionModel) {
        saveDivisionSubDivisionOnPreferences(divisionSubDivision)
        this.divisionSubDivision = divisionSubDivision
    }
    //endregion

    val homeData = repository.getHomeData()
        .map {
            when (it?.status) {
                Resource.Status.LOADING -> {
                    DataResponse.LOADING()
                }
                Resource.Status.SUCCESS -> {
                    val data = it.data?.data
                    //setHomeData(data)
                    DataResponse.SUCCESS(data)
                }
                Resource.Status.ERROR -> {
                    it.e?.let { t ->
                        exceptionToLog(t, HOMES)
                    } ?: run {
                        simpleErrorLog(it.message ?: HOMES_UNKNOWN_ERROR,
                            HOMES
                        )
                    }
                    DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                }

               else -> {
                   DataResponse.LOADING()
               }
            }
        }.asLiveData(viewModelScope.coroutineContext)

    fun setHomeData(homeItemDto: HomeItemDto?) {
        homeItemDto?.let { homeItem ->
            headerItems = homeItem.items
            institutionsAux = homeItem.institutions

//            if (institutionsAux.isEmpty()) {
//                setInstitutions(mutableListOf())
//                institutionsIsEmpty()
//                hideLoading()
//                return@let
//            }

            setInstitutions(institutionsAux)
        }
    }

    fun removeCityOnPreferences() {
        runBlocking {
            try {
                repository.removeCity()
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                        e.printStackTrace()
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
                e.printStackTrace()
            }
        }
    }

    fun removeCountryAndCityOnPreferences() {
        runBlocking {
            try {
                repository.removeCountryAndCity()
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                        e.printStackTrace()
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
                e.printStackTrace()
            }
        }
    }

    fun removeTournamentOnPreferences() {
        runBlocking {
            try {
                repository.removeTournamentId()
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                        e.printStackTrace()
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
                e.printStackTrace()
            }
        }
    }

    fun removeSportOnPreferences() {
        runBlocking {
            try {
                repository.removeSportId()
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
                e.printStackTrace()
            }
        }
    }

    //region EMPTY
    private fun institutionsIsEmpty(isEmpty: Boolean) {
        _institutionsIsEmpty.value = EventWrapper(isEmpty)
    }

//    private fun divisionsIsEmpty(isEmpty: Boolean) {
//        _divisionsIsEmpty.value = EventWrapper(isEmpty)
//        if (isEmpty)
//            setTournaments(mutableListOf())
//    }

    private fun divisionsSubDivisionIsEmpty(isEmpty: Boolean) {
        _divisionsSubDivisionsIsEmpty.value = EventWrapper(isEmpty)
//        if (isEmpty)
//            setTournaments(mutableListOf())
    }

    private fun sportsIsEmpty(isEmpty: Boolean) {
        _sportsIsEmpty.value = EventWrapper(isEmpty)
    }

    private fun tournamentsIsEmpty(isEmpty: Boolean) {
        _tournamentsIsEmpty.value = EventWrapper(isEmpty)
    }
    //endregion

    private fun saveInstitutionIdOnPreferences(id: String) {
        runBlocking {
            try {
                repository.saveInstitutionId(id)
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                        e.printStackTrace()
                    }.map {
                        if (!it)
                            simpleErrorLog("Institution id did not save in preferences")
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
            }
        }
    }

    private fun saveSportIdOnPreferences(id: String) {
        runBlocking {
            try {
                repository.saveSportId(id)
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                    }.map {
                        if (!it)
                            simpleErrorLog("Sport id did not save in preferences")
                    }.launchIn(this)
            } catch (e: Exception) {
                simpleErrorLog("Sport id did not save in preferences " + e.localizedMessage)
                e.printStackTrace()
            }

        }
    }

    private fun saveDivisionSubDivisionOnPreferences(divisionSubDivision: DivisionSubDivisionModel) {
        runBlocking {
            try {
                repository.saveDivisionSubDivision(divisionSubDivision)
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                    }.map {
                        if (!it)
                            simpleErrorLog("Division sub division did not save in preferences")
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
            }

        }
    }

    private fun saveTournamentIdOnPreferences(id: String) {
        runBlocking {
            try {
                repository.saveTournamentId(id)
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                    }.map {
                        if (!it)
                            simpleErrorLog("Tournament id did not save in preferences")
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
            }

        }
    }

    fun getInstitutionIdFromPreferences() = runBlocking {
//                try {
        repository.getInstitutionId()
            //.flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e)
            }.single()
    }

    fun getSportIdFromPreferences() = runBlocking {
        repository.getSportId()
            //.flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e)
            }.single()

    }

    fun getDivisionSubDivisionFromPreferences() = runBlocking {
        repository.getDivisionSubDivision()
            //.flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e)
            }.single()
    }

    fun removeDivisionSubDivisionOnPreferences() {
        runBlocking {
            try {
                repository.removeDivisionSubDivision()
                    //.flowOn(Dispatchers.Main)
                    .catch { e ->
                        exceptionToLog(e)
                    }.launchIn(this)
            } catch (e: Exception) {
                exceptionToLog(e)
                e.printStackTrace()
            }
        }
    }

    fun getTournamentIdFromPreferences() = runBlocking {
        repository.getTournamentId()
            //.flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e)
            }.single()
    }

    private fun setInstitutions(institutions: MutableList<InstitutionModel>) {
        institutionsIsEmpty(institutions.isEmpty())
        _institutions.value = institutions
    }

//    private fun setDivisions(divisions: MutableList<DivisionModel>) {
//        divisionsIsEmpty(divisions.isEmpty())
//        _divisions.value = divisions
//    }

    private fun setDivisionsSubDivisions(divisionsSubDivisions: MutableList<DivisionSubDivisionModel>) {
        divisionsSubDivisionIsEmpty(divisionsSubDivisions.isEmpty())
        _divisionsSubDivisions.value = divisionsSubDivisions
    }

    private fun setTournaments(tournaments: MutableList<TournamentModel>) {
        tournamentsIsEmpty(tournaments.isEmpty())
        _tournaments.value = tournaments
    }

    private fun isCityEmpty() {
        _cityEmpty.value = EventWrapper(true)
    }

    @ExperimentalCoroutinesApi
    val showAlert = repository.showHomeInformationAlert()
        .flowOn(Dispatchers.Main)
        .catch { e ->
            exceptionToLog(e)
        }.asLiveData(viewModelScope.coroutineContext)

    fun homeInformationAlertDisplayed() = runBlocking {
        repository.homeInformationAlertDisplayed()
            //.flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e)
            }.single()
    }

    fun needUpdateVersionApp() {
        try {
            repository.needUpdateVersionApp()
                .flowOn(Dispatchers.IO)
                .onStart {
                }.catch {
                    exceptionToLog(it, HOMES)
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                        }
                        Resource.Status.SUCCESS -> {
                            it.data?.data?.let { version ->
                                _needUpdateVersion.value =
                                    EventWrapper(BuildConfig.VERSION_NAME != version.android)
                            }
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, HOMES)
                            } ?: run {
                                simpleErrorLog(
                                    it.message ?: HOMES_UNKNOWN_ERROR,
                                    HOMES
                                )
                            }
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, FixtureViewModel.FIXTURES)
        }
    }
}