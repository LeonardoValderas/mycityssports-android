package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityPlacesBinding
import com.lavsystems.mycityssports.mvvm.view.adapter.CitySpinnerAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.CountrySpinnerAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.StateSpinnerAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.PlacesViewModel
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class PlacesActivity : BaseActivity<ActivityPlacesBinding, PlacesViewModel>() {
    private val viewModel: PlacesViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.activity_places
    override fun getBindingVariable(): Int = BR.vm

    private lateinit var spinnerCountry: Spinner
    private lateinit var spinnerState: Spinner
    private lateinit var spinnerCity: Spinner
    private lateinit var countryAdapter: CountrySpinnerAdapter
    private lateinit var stateAdapter: StateSpinnerAdapter
    private lateinit var cityAdapter: CitySpinnerAdapter

    override fun init() {
        initCitySelected()
        initSpinners()
        initSpinnerAdapter()
        tryAgainEmptyList()
    }

    private fun initCitySelected() {
        getViewModel().setCitySelected(null)
    }

    override fun initViewModel(): PlacesViewModel = viewModel

    private fun initSpinners() {
        getRootDataBinding().run {
            spinnerCountry = spCountry
            spinnerCountry.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        val country = countryAdapter.getCountryModelForPosition(position)
                        getViewModel().saveCountry(country)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
            spinnerState = spState
            spinnerState.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        val state = stateAdapter.getStateModelForPosition(position)
                        cityAdapter.setCitiesForStateId(state.id)
                        if (cityAdapter.hasCities())
                            cityAdapter.getCityModelForPosition(0).let {
                                getViewModel().setCitySelected(it)
                            }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
            spinnerCity = spCity
            spinnerCity.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        cityAdapter.getCityModelForPosition(position).let {
                            getViewModel().setCitySelected(it)
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }
                }
            }
        }
    }

    private fun initSpinnerAdapter() {
        countryAdapter = CountrySpinnerAdapter(this, R.layout.simple_spinner_item_place, arrayListOf())
        stateAdapter = StateSpinnerAdapter(this, R.layout.simple_spinner_item_place, arrayListOf())
        cityAdapter = CitySpinnerAdapter(this, R.layout.simple_spinner_item_place, arrayListOf())
        spinnerCountry.adapter = countryAdapter
        spinnerState.adapter = stateAdapter
        spinnerCity.adapter = cityAdapter
    }

    override fun initObservers() {
        showLoading(getString(R.string.loading_places_data))
        with(getViewModel()) {
            countries.observe(this@PlacesActivity, Observer { countries ->
                countryAdapter.setCountriesList(countries)
            })

            places.observe(this@PlacesActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        val states = resource?.data?.states
                        val cities = resource?.data?.cities
                        if (states.isNullOrEmpty() || cities.isNullOrEmpty())
                            showFriendlyError(getString(R.string.subtitle_places_empty))
                        else {
                            stateAdapter.setStates(states)
                            val state = stateAdapter.getStateModelForPosition(0)
                            cityAdapter.setCities(cities)
                            setCityForStateId(state.id)
                            hideFriendlyError()
                        }
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(resource.message ?: getString(R.string.loading_places_data))
                    }
                    //response without cache has only error status
                    // but within viewmodel is handle and take to Failure
                    StatusResponse.ERROR -> {
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                            hideLoading()
                            return@Observer
                        }
                        showFriendlyError(getString(R.string.known_error))
                        hideLoading()
                    }
                }
            })

            city.observe(this@PlacesActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        goToHomeActivity()
                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        showLoading(resource.message ?: getString(R.string.saving_city))
                    }
                    StatusResponse.ERROR -> {
                        showSnackbar(getRootDataBinding().clContainer, getString(R.string.city_not_save_places))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                            hideLoading()
                            return@Observer
                        }
                        showFriendlyError(getString(R.string.known_error))
                        hideLoading()
                    }
                }
            })

            country.observe(this@PlacesActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        getViewModel().getPlaces()
                    }
                    StatusResponse.LOADING -> {
                        //showLoading(resource.message ?: getString(R.string.saving_city))
                    }
                    StatusResponse.ERROR -> {
                        showSnackbar(getRootDataBinding().clContainer, getString(R.string.country_not_save_places))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                            hideLoading()
                            return@Observer
                        }
                        showFriendlyError(getString(R.string.known_error))
                        hideLoading()
                    }
                }
            })
        }
    }

    fun buttonClick(view: View) {
        getViewModel().getCitySelected()?.let {
            getViewModel().saveCity(it)
        } ?: run {
            showSnackbar(getRootDataBinding().clContainer, getString(R.string.city_null_places))
        }
    }

    private fun tryAgainEmptyList() {
        getRootDataBinding().iFriendlyError.btnTryAgain.setOnClickListener {
            startActivity(Intent(this, PlacesActivity::class.java))
            finish()
        }
    }

    private fun goToHomeActivity() {
        val intent = Intent(this@PlacesActivity, HomeActivity::class.java)
//        intent.addFlags(
//            Intent.FLAG_ACTIVITY_CLEAR_TOP and
//                    Intent.FLAG_ACTIVITY_CLEAR_TASK and
//                    Intent.FLAG_ACTIVITY_NEW_TASK
//        )
        startActivity(intent)
    }

    private fun showFriendlyError(subtitle: String) {
        handleFriendlyError(true, subtitle)
    }

    private fun handleFriendlyError(show: Boolean, subtitle: String) {
        getRootDataBinding().clErrorVisibility = show
        getRootDataBinding().title = getString(R.string.error_title_fragment)
        getRootDataBinding().subtitle = subtitle
    }

    private fun hideFriendlyError() {
        handleFriendlyError(false, "")
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun setCityForStateId(stateId: String){
        cityAdapter.setCitiesForStateId(stateId)
        if (cityAdapter.hasCities())
            cityAdapter.getCityModelForPosition(0).let {
                getViewModel().setCitySelected(it)
            }
    }
}