package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.Intent
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivitySplashBinding
import com.lavsystems.mycityssports.mvvm.viewmodel.SplashViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel


@ExperimentalCoroutinesApi
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    private val viewModel: SplashViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_splash
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): SplashViewModel = viewModel

    override fun init() {
//        Thread {
//            kotlin.run {
//                SystemClock.sleep(4000)
//            }
//        }.start()
    }

    override fun initObservers() {
        setLoading(true)
        with(getViewModel()) {
            countryAndCityExists.observe(this@SplashActivity, Observer { response ->
                when (response.status) {
                    StatusResponse.SUCCESS -> {
                        response?.data?.getContentIfNotHandled()?.let { exists ->
                            if (exists)
                                goToHomeActivity()
                            else
                                goToPlacesActivity()

                        } ?: run {
                            goToPlacesActivity()
                        }
                        setLoading(false)
                    }
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.ERROR -> {
                        //set log from base
                        goToPlacesActivity()
                    }
                    StatusResponse.FAILURE -> {
                        //set log from base
                        goToPlacesActivity()
                    }
                }
            })
        }
    }

    private fun goToPlacesActivity() {
        startActivity(Intent(this@SplashActivity, PlacesActivity::class.java))
    }

    private fun goToHomeActivity() {
        val intent = Intent(this@SplashActivity, HomeActivity::class.java)
//        intent.addFlags(
//             Intent.FLAG_ACTIVITY_CLEAR_TOP and
//                   Intent.FLAG_ACTIVITY_CLEAR_TASK and
//                   Intent.FLAG_ACTIVITY_NEW_TASK
//        )
        startActivity(intent)
    }

    private fun setLoading(loading: Boolean) {
        getRootDataBinding().loading = loading
    }
}
