package com.lavsystems.mycityssports.mvvm.model

import com.google.gson.annotations.Expose
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PlayingFieldModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val street: String,
    val number: String,
    val neighborhood: String,
    @Expose val position: PositionModel
){
    constructor() : this("", "", "", "", "", PositionModel())
}

