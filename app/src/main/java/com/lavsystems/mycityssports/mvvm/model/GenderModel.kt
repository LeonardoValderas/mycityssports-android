package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass
@JsonClass(generateAdapter = true)
data class GenderModel(
    val name: String
){
    constructor(): this("")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GenderModel

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

}