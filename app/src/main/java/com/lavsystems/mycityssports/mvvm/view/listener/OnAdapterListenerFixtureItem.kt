package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View

interface OnAdapterListenerFixtureItem {
    fun onItemFixture(view: View, position: Int, fixtureId: String)
    fun onItemSponsor(view: View, position: Int, sponsorId: String)
}