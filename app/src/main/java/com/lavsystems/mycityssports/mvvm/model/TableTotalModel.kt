package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TableTotalModel(
    val total: Int,
    val difference: Int,
    val value: Int) {
    constructor(): this(0,0,0)
}