package com.lavsystems.mycityssports.mvvm.view.activity

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.maps.GoogleMap
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityMyCitysSportsContactBinding
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntent
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.MyCitysSportsContactViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_HOME_PHONE
import com.lavsystems.mycityssports.utils.ConstantsUtils.PERMISSION_CALL_PHONE
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.permissions.Permissions
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class MyCitysSportsContactActivity :
    BaseActivity<ActivityMyCitysSportsContactBinding, MyCitysSportsContactViewModel>() {

    private val viewModel: MyCitysSportsContactViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.activity_my_citys_sports_contact
    override fun getBindingVariable(): Int = BR.vm

    override fun initViewModel(): MyCitysSportsContactViewModel = viewModel
    private lateinit var mMap: GoogleMap
    private var myCitysSportsContactModel: MyCitysSportsContactModel? = null
    lateinit var contactIntent: InformationContactIntent

    override fun init() {
        initToolbar()
        initAdView()
    }

    private fun initContactUtils(){
        myCitysSportsContactModel?.let {
            contactIntent = InformationContactIntentImpl(this@MyCitysSportsContactActivity, InstitutionModel.WITHOUT_INFO)
        }
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = getString(R.string.contact_title)
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun initAdView() {
        getRootDataBinding().adViewMcs.loadAd(AdRequest.Builder().build())
    }

    override fun initObservers() {
        with(getViewModel()) {
            contactModel.observe(this@MyCitysSportsContactActivity, { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let { c ->
                            myCitysSportsContactModel = c
                            initContactUtils()
                            contactToBinding(c)
                        } ?: run {
                            closeActivityForError()
                        }
                        hideLoading()
                    }

                    StatusResponse.LOADING -> {
                        showLoading(getString(R.string.loading_mcs_contact))
                    }

                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            closeActivityForError()
                        } ?: run {
                            closeActivityForError()
                        }

                        hideLoading()
                    }
                    else -> {
                        closeActivityForError()
                    }
                }
            })
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun contactToBinding(contact: MyCitysSportsContactModel) {
        getRootDataBinding().model = contact
    }

    private fun closeActivityForError() {
        showToast(getString(R.string.mcs_contact_error))
        finish()
    }

    private fun getBundleIntent(): Bundle? {
        return intent.extras
    }

    fun onClickHomePhone(view: View) {
        contactIntent.intentHomePhone(myCitysSportsContactModel?.phone)
    }

    fun onClickHome(view: View) {
        contactIntent.intentPhone(myCitysSportsContactModel?.mobile)
    }

    fun onClickWhatsapp(view: View) {
        contactIntent.intentWhatsapp(myCitysSportsContactModel?.mobile)
    }

    fun onClickEmail(view: View) {
        contactIntent.intentEmail(myCitysSportsContactModel?.email)
    }

    fun onClickFacebook(view: View) {
        contactIntent.intentFacebook(myCitysSportsContactModel?.facebook)
    }

    fun onClickInstagram(view: View) {
        contactIntent.intentInstagram(myCitysSportsContactModel?.instagram)
    }

    fun onClickWeb(view: View) {
        contactIntent.intentWeb(myCitysSportsContactModel?.web)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                PERMISSION_CALL_HOME_PHONE -> contactIntent.intentHomePhone(myCitysSportsContactModel?.phone)
                PERMISSION_CALL_PHONE -> contactIntent.intentPhone(myCitysSportsContactModel?.mobile)
            }
        }else {
            if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_DENIED) {
                when (requestCode) {
                    PERMISSION_CALL_PHONE,
                    PERMISSION_CALL_HOME_PHONE -> Permissions.shouldShowRequestPermissionRationale(
                        this,
                        R.id.cl_institution_details,
                        permissions.first(),
                        getString(R.string.permission_call_dialog_message))
                }
            }
        }
    }
}
