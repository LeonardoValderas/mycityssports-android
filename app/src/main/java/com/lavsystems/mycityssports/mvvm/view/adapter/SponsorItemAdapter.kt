package com.lavsystems.mycityssports.mvvm.view.adapter

import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem
import com.lavsystems.mycityssports.utils.adapter.DataBindingAdapter

class SponsorItemAdapter(private val layout: Int, private val sponsor: SponsorModel, listener: OnAdapterListener<SponsorModel>) : DataBindingAdapter<SponsorModel>(listener) {
    override fun getAnimateRoot(): YoYo.AnimationComposer? {
        return null
    }

    override fun getAnimateListener(): YoYo.AnimationComposer? {
        return null
    }

    override fun getItemModel(int: Int): SponsorModel {
        return sponsor
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return sponsor.id.toLong()
    }

    override fun addItems(items: MutableList<SponsorModel>, clear: Boolean) {
    }

    override fun getItems(): MutableList<SponsorModel> {
        return mutableListOf()
    }

    override fun getItemViewType(position: Int): Int {
        return layout
    }
}
