package com.lavsystems.mycityssports.mvvm.view.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.databinding.ActivityFixtureDetailsBinding
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.mvvm.viewmodel.FixtureDetailsViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.ConstantsUtils.FIXTURE_ID_BUDLE
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class FixtureDetailsActivity :
    BaseActivity<ActivityFixtureDetailsBinding, FixtureDetailsViewModel>(),
    OnMapReadyCallback {

    private val viewModel: FixtureDetailsViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.activity_fixture_details
    override fun getBindingVariable(): Int = BR.vm

    override fun initViewModel(): FixtureDetailsViewModel = viewModel
    private lateinit var mMap: GoogleMap
    private var fixture: FixtureItemModel? = null
    private var fixtureId = ""
    private var city: CityModel? = null
    private val roundPrefix: String by lazy {
        getString(R.string.round_prefix)
    }

    override fun init() {
        initToolbar()
        getFixtureIdFromIntentBundle()
        initMap()
        initAdView()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initToolbar() {
        val toolbar = getRootDataBinding().iToolbar as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            title = "$roundPrefix ${fixture?.roundStg}"
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    private fun initAdView() {
        getRootDataBinding().adViewFixtureDetails.loadAd(AdRequest.Builder().build())
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private fun updateMoveCamera(markerOptions: MarkerOptions) {
        mMap.addMarker(markerOptions)?.showInfoWindow()
        val cameraPosition = getCameraPosition(markerOptions.position)
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun getLatLng(longitude: Double, latitude: Double): LatLng {
        return LatLng(longitude, latitude)
    }

    private fun getMarkerOptions(latLng: LatLng): MarkerOptions {
        val title = fixture!!.playingField.name
        val address = fixture!!.playingField.street
        return MarkerOptions().position(latLng).title(title).snippet(address)
    }

    private fun getCameraPosition(latLng: LatLng): CameraPosition {
        return CameraPosition
            .builder()
            .target(latLng)
            .zoom(15.toFloat())
            .bearing(0.toFloat())
            .tilt(45.toFloat())
            .build()
    }

    private fun getFixtureIdFromIntentBundle() {
        getBundleIntent()?.let { bundle ->
            fixtureId = bundle.getString(FIXTURE_ID_BUDLE, "")
            if (fixtureId.isNullOrEmpty())
                closeActivityForError()
            getViewModel().getCity()
        } ?: kotlin.run {
            closeActivityForError()
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            cityModel.observe(this@FixtureDetailsActivity, Observer {
                when (it.status) {
                    StatusResponse.SUCCESS -> {
                        it.data?.let { city ->
                            this@FixtureDetailsActivity.city = city
                            getFixtureDetails(fixtureId)
                        }
                    }
                    StatusResponse.LOADING -> {
                        showLoading(getString(R.string.loading_fixture_data_details))
                    }
                    StatusResponse.ERROR -> {
                        //set log from base
                        closeActivityForError()
                    }
                    StatusResponse.FAILURE -> {
                        //set log from base
                        closeActivityForError()
                    }
                }
            })

            fixtureDetails.observe(this@FixtureDetailsActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let { f ->
                            fixture = f
                            fixtureToBinding(f)
                            updatePositionMapByCity()

                        } ?: run {
                            closeActivityForError()
                        }

                        hideLoading()
                    }

                    StatusResponse.LOADING -> {
                        showLoading(getString(R.string.loading_fixture_data_details))
                    }

                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            closeActivityForError()
                        } ?: run {
                            closeActivityForError()
                        }

                        hideLoading()
                    }
                    else -> {
                        closeActivityForError()
                    }
                }
            })
        }
    }

    private fun updatePositionMapByCity() {
        try {
            city?.let { c ->
                val lat = c.position.latitude.toDouble()
                val long = c.position.longitude.toDouble()
                val lanLng = getLatLng(lat, long)
                val marker = getMarkerOptions(lanLng)
                updateMoveCamera(marker)
            } ?: run {
                closeActivityForError()
            }
        } catch (e: Exception) {
            FirestoreBuilder.Builder()
                .className(ConstantsUtils.ERROR_FRAGMENT_REPOSITORY)
                .method("updatePositionMapByCity")
                .type(FirestoreBuilder.EXCEPTION)
                .message(e.stackTraceToString())
                .brand()
                .model()
                .send()
            closeActivityForError()
        }
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun fixtureToBinding(fixtureItemModel: FixtureItemModel) {
        val title = fixtureItemModel?.let {
            if (it.tournamentZone !== null)
                it.tournamentZone.name
            else if (it.tournamentInstance !== null)
                it.tournamentInstance.name
            else
                "$roundPrefix ${it.roundStg}"
        }

        supportActionBar?.title = title ?: ""
        getRootDataBinding().model = fixtureItemModel
    }

    private fun closeActivityForError() {
        showToast(getString(R.string.fixture_details_error))
        finish()
    }

    private fun getBundleIntent(): Bundle? {
        return intent?.extras
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                //finish()
                this.onBackPressed()
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }


//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        return when (item?.itemId) {
//            android.R.id.home -> {
//                //finish()
//                this.onBackPressed()
//                true
//            }
//            else ->
//                super.onOptionsItemSelected(item)
//        }
//    }
}
