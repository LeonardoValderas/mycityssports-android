package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.DateTimeUtils
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsModel(
    @Json(name = "_id")
    val id: String,
    val title: String,
    val body: String,
    val start: String,
    val expiration: String,
    val urlImage: String,
    val source: String = "") {
    constructor(): this("", "","", "", "", "")

    companion object {
        const val PUBLICATED = "Publicado: "
        const val SOURCE = "Fuente: "
    }
    val startFormatted: String
    get() = PUBLICATED + DateTimeUtils.dateShortFormatString(start, "yyyy-MM-dd'T'HH:mm:ss", "dd-MM-yyyy")

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NewsModel

        if (id != other.id) return false
        if (title != other.title) return false
        if (body != other.body) return false
        if (start != other.start) return false
        if (expiration != other.expiration) return false
        if (urlImage != other.urlImage) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + body.hashCode()
        result = 31 * result + start.hashCode()
        result = 31 * result + expiration.hashCode()
        result = 31 * result + urlImage.hashCode()
        result = 31 * result + source.hashCode()
        return result
    }
}


