package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TeamModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val sport: String,
    val urlImage: String?
) {
    constructor() : this("", "", "", "")
}
