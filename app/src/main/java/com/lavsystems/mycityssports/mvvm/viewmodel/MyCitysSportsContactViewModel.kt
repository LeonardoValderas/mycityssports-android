package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.MyCitysSportsContactRepository
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class MyCitysSportsContactViewModel(private val repository: MyCitysSportsContactRepository) : BaseViewModel() {

    companion object {
        const val CONTACTS = "Contacts"
        const val CONTACTS_UNKNOWN_ERROR = "Error unknown in get contacts data."
    }

    private val _contact = MutableLiveData<DataResponse<MyCitysSportsContactModel>>()
    val contactModel: LiveData<DataResponse<MyCitysSportsContactModel>>
        get() = _contact

    init {
        getContact()
    }
     final fun getContact() {
        try {
            repository.getContact()
                .flowOn(Dispatchers.IO)
                .onStart {
                    _contact.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, CONTACTS)
                    _contact.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _contact.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _contact.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t,CONTACTS)
                            } ?: run {
                                simpleErrorLog(it.message ?: CONTACTS_UNKNOWN_ERROR,
                                    CONTACTS
                                )
                            }
                            _contact.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, CONTACTS)
            _contact.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}