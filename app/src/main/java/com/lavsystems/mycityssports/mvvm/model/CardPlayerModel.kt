package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CardPlayerModel(
    val card: CardModel,
    val player: PlayerModel?,
    val minute: Int?
) {
    constructor(): this(CardModel(), null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CardPlayerModel

        if (card != other.card) return false
        if (player != other.player) return false
        if (minute != other.minute) return false

        return true
    }

    override fun hashCode(): Int {
        var result = card.hashCode()
        result = 31 * result + (player?.hashCode() ?: 0)
        result = 31 * result + (minute ?: 0)
        return result
    }
}