package com.lavsystems.mycityssports.mvvm.viewmodel

import android.provider.Settings
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.FcmRepository
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class FcmViewModel(
    private val repository: FcmRepository
) : BaseViewModel() {

    companion object {
        const val FCM = "Fcms"
        const val FCMS_UNKNOWN_ERROR = "Error unknown in get home data."
    }

    fun saveFcmToken(fcmModel: FcmModel) {
        repository.saveFcmToken(fcmModel)
            .flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e, FCM)
                // DataResponse.FAILURE<EventWrapper<Boolean>>(ExceptionHandleImpl(e))
            }.map {
                when (it?.status) {
                    Resource.Status.LOADING -> {
                    }
                    Resource.Status.SUCCESS -> {
                        it.data?.data?.let { model ->
                            repository.saveFcmModel(model)
                        }
                    }
                    Resource.Status.ERROR -> {
                        it.e?.let { t ->
                            exceptionToLog(t, FCM)
                        } ?: run {
                            simpleErrorLog(
                                it.message ?: FCMS_UNKNOWN_ERROR,
                                FCM
                            )
                        }
                    }
                    else -> {}
                }
            }.launchIn(viewModelScope)
    }

    fun updateFcmToken(fcmModel: FcmModel) {
        repository.updateFcmToken(fcmModel)
            .flowOn(Dispatchers.Main)
            .catch { e ->
                exceptionToLog(e, FCM)
            }.map {
                when (it?.status) {
                    Resource.Status.LOADING -> {
                    }
                    Resource.Status.SUCCESS -> {
                        it.data?.data?.let { model ->
                            repository.saveFcmModel(model)
                        }
                    }
                    Resource.Status.ERROR -> {
                        it.e?.let { t ->
                            exceptionToLog(t, FCM)
                        } ?: run {
                            simpleErrorLog(
                                it.message ?: FCMS_UNKNOWN_ERROR,
                                FCM
                            )
                        }
                    }
                    else -> {}
                }
            }.launchIn(viewModelScope)
    }

    fun getFcmModel() = runBlocking {
        repository.getFcmModel()
            .catch { e ->
                exceptionToLog(e, FCM)
            }.single()
    }
}