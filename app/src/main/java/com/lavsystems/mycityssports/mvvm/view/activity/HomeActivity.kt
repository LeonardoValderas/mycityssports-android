package com.lavsystems.mycityssports.mvvm.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseActivity
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandle
import com.lavsystems.mycityssports.databinding.ActivityHomeBinding
import com.lavsystems.mycityssports.databinding.ToolbarSpinnerRecyclerBinding
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.mvvm.view.adapter.InstitutionSpinnerAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.SportSpinnerAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.DivisionSubDivisionAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.TournamentAdapter
import com.lavsystems.mycityssports.mvvm.view.fragment.*
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerTournament
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerDivisionSubDivision
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerDivision
import com.lavsystems.mycityssports.mvvm.view.listener.CommunicatorFragmentListener
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.viewmodel.FcmViewModel
import com.lavsystems.mycityssports.mvvm.viewmodel.HomeViewModel
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.fragment.FragmentManager.currentFragmentIsErrorFragment
import com.lavsystems.mycityssports.utils.fragment.FragmentManager.replaceFragment
import com.lavsystems.mycityssports.utils.uicomponents.AlertDialogComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(),
    OnAdapterListener<InstitutionModel>,
    NavigationView.OnNavigationItemSelectedListener,
    NavigationBarView.OnItemSelectedListener,
    CommunicatorFragmentListener, OnAdapterListenerDivision, OnAdapterListenerDivisionSubDivision,
    OnAdapterListenerTournament {

    private val viewModel: HomeViewModel by viewModel()
    private val fcmViewModel: FcmViewModel by viewModel()

    override fun initViewModel(): HomeViewModel = viewModel

    ////institution -> sports -> divisions -> tournaments
    //institution -> sports -> tournaments -> divisions - subdivions
    lateinit var institutionSpinnerAdapter: InstitutionSpinnerAdapter
    lateinit var sportSpinnerAdapter: SportSpinnerAdapter

    override fun layoutRes(): Int = R.layout.activity_home
    override fun getBindingVariable(): Int = BR.vm

    private val tournamentAdapter = TournamentAdapter(this)
    private val divisionSubDivisionAdapter = DivisionSubDivisionAdapter(this)
    private lateinit var spinnerInstitution: Spinner
    private lateinit var spinnerSport: Spinner
    private lateinit var toolbarSpinnerRecyclerBinding: ToolbarSpinnerRecyclerBinding

    //private var sheetBehavior: BottomSheetBehavior<*>? = null
    //private var sheetOpened = false
    private var fragmentPosition = 2
    private var currentFragmentPosition = 2
    private var isInit = true
    private var newsFragment: NewsFragment? = null
    private var fixtureFragment: FixtureFragment? = null
    private var tableFragment: TableFragment? = null
    private var rankingFragment: RankingFragment? = null
    private var sanctionFragment: SanctionFragment? = null
    private var counterClicks = 0
    private var mInterstitialAd: InterstitialAd? = null

    override fun init() {
        initComponent()
    }

    //region INIT
    private fun initComponent() {
        initFcmToken()
        initSpinner()
        initSpinnerAdapter()
        initFragments()
        initNavigationBottom()
        /////////intiSheetBottom()
        initListener()
        initRecyclerViews()
        initInterstitial()
    }

    private fun initFcmToken() {
        if (fcmViewModel.getFcmModel() == null) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    task.result?.let { t ->
                        fcmViewModel.saveFcmToken(FcmModel("", t))
                    }
                }
            }
        }
    }

    private fun initInterstitial() {
        InterstitialAd.load(
            this,
            getString(R.string.adsmob_interstitial),
            AdRequest.Builder().build(),
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    if (mInterstitialAd == null) {
                        mInterstitialAd = interstitialAd
                        mInterstitialAd?.fullScreenContentCallback =
                            object : FullScreenContentCallback() {
                                override fun onAdDismissedFullScreenContent() {
                                    initInterstitial()
                                }

//                                override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
//                                    print(adError)
//                                }

                                override fun onAdShowedFullScreenContent() {
                                    mInterstitialAd = null
                                    initInterstitial()
                                }
                            }
                    }
                }
            })
    }

    @ExperimentalCoroutinesApi
    private fun initFragments() {
        newsFragment = NewsFragment()
        fixtureFragment = FixtureFragment()
        tableFragment = TableFragment()
        rankingFragment = RankingFragment()
        sanctionFragment = SanctionFragment()
    }

    private fun initSpinner() {
        toolbarSpinnerRecyclerBinding = getRootDataBinding().iAppBarHome.iSpinnerRecycler

        toolbarSpinnerRecyclerBinding.run {
            spinnerInstitution = spInstitution
            spinnerInstitution.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        validateIfInterstitialAdShouldBeDisplayed()
                        val institution =
                            institutionSpinnerAdapter.getInstitutionModelForPosition(position)
                        getViewModel().setInstitutionModel(institution)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
            spinnerSport = spSports
            spinnerSport.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        validateIfInterstitialAdShouldBeDisplayed()
                        val sport = sportSpinnerAdapter.getSportModelForPosition(position)
                        getViewModel().setSportModel(sport)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
        }
    }

    private fun initSpinnerAdapter() {
        institutionSpinnerAdapter =
            InstitutionSpinnerAdapter(this, R.layout.simple_spinner_item, arrayListOf())
        sportSpinnerAdapter = SportSpinnerAdapter(this, arrayListOf())
        spinnerInstitution.adapter = institutionSpinnerAdapter
        spinnerSport.adapter = sportSpinnerAdapter
    }

    private fun initNavigationBottom() {
        getRootDataBinding().iAppBarHome.navigationBottom.apply {
            setOnItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.navigation_news -> {
                        hideToolbarSpinner()
                        setFragmentPositionAndReplaceFragment(1)
                    }
                    R.id.navigation_fixture -> {
                        if (isInit) {
                            isInit = false
                        } else {
                            hideToolbarSpinner()
                            setFragmentPositionAndReplaceFragment(2)
                        }
                    }
                    R.id.navigation_table -> {
                        hideToolbarSpinner()
                        setFragmentPositionAndReplaceFragment(3)
                    }
                    R.id.navigation_rankings -> {
                        hideToolbarSpinner()
                        setFragmentPositionAndReplaceFragment(4)
                    }
                    R.id.navigation_sactions -> {
                        hideToolbarSpinner()
                        setFragmentPositionAndReplaceFragment(5)
                    }
                }
                true
            }
            setOnItemReselectedListener {

            }
            selectedItemId = R.id.navigation_fixture
        }
    }

    private fun increaseCounter() {
        counterClicks += 1
    }

    private fun resetCounter() {
        counterClicks = 0
    }

    private fun counterIsFull(value: Int) = counterClicks == value

    private fun validateIfInterstitialAdShouldBeDisplayed() {
        if (counterIsFull(5) && mInterstitialAd != null) {
            mInterstitialAd?.show(this)
            resetCounter()
        } else {
            increaseCounter()
            if (mInterstitialAd == null)
                initInterstitial()
        }

//        val appAdOpenAds = (application as MyCitysSportsApplication).getAppOpenAdsManager()
//////        if (counterIsFull(1) && (appAdOpenAds?.canDisplayAd() == true)){
//        if ((appAdOpenAds?.canDisplayAd() == true)) {
//            appAdOpenAds.showAdIfAvailable()
//            resetCounter()
//        } else {
//            increaseCounter()
//        }
    }

//    private fun intiSheetBottom() {
//        sheetBehavior = BottomSheetBehavior.from(cl_sheet)
//        sheetBehavior?.addBottomSheetCallback(object : Bot    tomSheetBehavior.BottomSheetCallback() {
//            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
//                when (newState) {
//                    BottomSheetBehavior.STATE_HIDDEN -> {
//                    }
//                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
//
//                    }
//                    BottomSheetBehavior.STATE_EXPANDED -> {
//                        // sheetOpened = true
//                    }
//                    BottomSheetBehavior.STATE_COLLAPSED -> {
//                        //updateInstitution()
//                    }
//                    BottomSheetBehavior.STATE_DRAGGING -> {
//                    }
//                    BottomSheetBehavior.STATE_SETTLING -> {
//                    }
//                }
//            }
//
//            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {
//            }
//        })
//    }

    private fun initListener() {
        getRootDataBinding().run {
            navView.setNavigationItemSelectedListener(this@HomeActivity)
//            fab.setOnClickListener {
//                //openCloseHandleSheetState()
//            }
            iFriendlyError.run {
                btnTryAgain.setOnClickListener {
                    startActivity(Intent(this@HomeActivity, HomeActivity::class.java))
                    finish()
                }
                btnOtherAction.setOnClickListener {
                    goToPlacesActivity()
                }
            }
            iAppBarHome.iSpinnerRecycler.ivHamburger.setOnClickListener {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

//        toolbar.ib_hamburger.setOnClickListener {
//            drawer_layout.openDrawer(GravityCompat.END)
//        }
    }

    private fun initRecyclerViews() {
//        rv_institution.apply {
//            setHasFixedSize(true)
//            layoutManager = GridLayoutManager(this@HomeActivity, 2)
//            addItemDecoration(ItemOffsetDecoration(context, R.dimen.item_offset))
//            adapter = institutionAdapter
//        }

        toolbarSpinnerRecyclerBinding.run {
            rvTournament.apply {
                setHasFixedSize(true)
                layoutManager =
                    LinearLayoutManager(this@HomeActivity, LinearLayoutManager.HORIZONTAL, false)
                adapter = tournamentAdapter
            }
            rvDivision.apply {
                setHasFixedSize(true)
                layoutManager =
                    LinearLayoutManager(this@HomeActivity, LinearLayoutManager.HORIZONTAL, false)
                //adapter = divisionAdapter
                adapter = divisionSubDivisionAdapter
            }
        }
    }
    //endregion

    private fun setFragmentPositionAndReplaceFragment(position: Int) {
        setFragmentPosition(position)
        replaceFragmentForPosition()
    }

    private fun setFragmentPosition(position: Int) {
        this.fragmentPosition = position
    }

    private fun replaceFragmentForPosition() {
        val isError = currentFragmentIsErrorFragment(supportFragmentManager)
        when (fragmentPosition) {
            1 -> {
                fragmentsHandle(newsFragment, isError)
            }
            2 -> {
                fragmentsHandle(fixtureFragment, isError)
            }
            3 -> {
                fragmentsHandle(tableFragment, isError)
            }
            4 -> {
                fragmentsHandle(rankingFragment, isError)
            }
            5 -> {
                fragmentsHandle(sanctionFragment, isError)
            }
        }
    }

    private fun fragmentsHandle(fragment: Fragment?, isError: Boolean) {
        if (!isError) {
            currentFragmentPosition = fragmentPosition
            fragment?.isAdded?.let {
                if (it) {
                    getFromHome(fragment)
                    return@fragmentsHandle
                }
            }
        }

        setNewFragment(fragment)
        replaceFragment(supportFragmentManager, getNewFragment(fragment))
    }

    private fun setNewFragment(fragment: Fragment?) {
        when (fragment) {
            is NewsFragment -> newsFragment = NewsFragment()
            is FixtureFragment -> fixtureFragment = FixtureFragment()
            is TableFragment -> tableFragment = TableFragment()
            is RankingFragment -> rankingFragment = RankingFragment()
            is SanctionFragment -> sanctionFragment = SanctionFragment()
        }
    }

    private fun getNewFragment(fragment: Fragment?): Fragment {
        return when (fragment) {
            is NewsFragment -> newsFragment as Fragment
            is FixtureFragment -> fixtureFragment as Fragment
            is TableFragment -> tableFragment as Fragment
            is RankingFragment -> rankingFragment as Fragment
            else -> sanctionFragment as Fragment
        }
    }

    private fun getFromHome(fragment: Fragment) {
        when (fragment) {
            is NewsFragment -> newsFragment?.getFromHome()
            is FixtureFragment -> fixtureFragment?.getFromHome()
            is TableFragment -> tableFragment?.getFromHome()
            is RankingFragment -> rankingFragment?.getFromHome()
            is SanctionFragment -> sanctionFragment?.getFromHome()
        }
    }

//    private fun openCloseHandleSheetState() {
//        if (sheetOpened)
//            closeBottomSheetBehavior()
//        else {
//            openBottomSheetBehavior()
//            //update
//        }
//        sheetOpened = !sheetOpened
//    }

//    private fun openBottomSheetBehavior() {
//        sheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
//    }
//
//    private fun closeBottomSheetBehavior() {
//        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
//    }

    override fun initObservers() {
        showLoading(getString(R.string.loading_home_data))
        with(getViewModel()) {
            needUpdateVersionApp()
            homeData.observe(this@HomeActivity, Observer { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let { homeData ->
                            getViewModel().setHomeData(homeData)
                        } ?: run {
                            showFriendlyError(
                                getString(R.string.known_error)
                            )
                        }

                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        hideFriendlyError()
                        showLoading(resource.message ?: getString(R.string.loading_home_data))
                    }
                    StatusResponse.ERROR -> {
                        showFriendlyError(resource.message ?: getString(R.string.known_error))
                        hideLoading()
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            showFriendlyError(getString(it.getMessageIntResource()))
                            return@Observer
                        } ?: run {
                            showFriendlyError(getString(R.string.known_error))
                        }
                        hideLoading()
                    }
                }
            })

            showAlert.observe(this@HomeActivity) { show ->
                if (show)
                    showPrivacyDialogAlert()
            }

            needUpdateVersion.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { needUpdate ->
                    if (needUpdate)
                        showUpdateVersionDialogAlert()
                }
            }

            institutions.observe(this@HomeActivity) { institutions ->
                institutions?.let {
                    institutionSpinnerAdapter.setInstitutions(institutions)
                    validateInstitutionIdFromPreferencesToSetSpinnerSelection()
                    if (it.isNotEmpty())
                        hideFriendlyError()
                }
            }

            sports.observe(this@HomeActivity) { sports ->
                sports?.let {
                    sportSpinnerAdapter.setSports(it)
                    spinnerSport.adapter = sportSpinnerAdapter
                    validateSportIdFromPreferencesToSetSpinnerSelection()
                }
            }

            tournaments.observe(this@HomeActivity) { tournaments ->
                tournaments?.let {
                    tournamentAdapter.addTournaments(it)
                    validateTournamentIdFromPreferencesToSelectRecyclerItemOrThrowException()
                }
            }

            divisionsSubDivisions.observe(this@HomeActivity) { divisions ->
                divisions?.let {
                    divisionSubDivisionAdapter.addDivisions(it)
                    validateDivisionIdFromPreferencesToSelectRecyclerItemOrThrowException()
                }
            }

            //EMPTY
            institutionsIsEmpty.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { empty ->
                    if (empty)
                        showFriendlyError(
                            getString(R.string.institutions_empty_subtitle_friendly)
                        )
                }
            }

            tournamentsIsEmpty.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { empty ->
                    if (empty)
                        removeTournamentOnPreferences()
                    setTournamentEmpty(empty)
                    if (empty)
                        replaceFragmentForPosition()
                }
            }

            sportsEmpty.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { empty ->
                    if (empty)
                        removeSportOnPreferences()
                    setSportEmpty(empty)
                }
            }

            divisionsSubDivisionsIsEmpty.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { empty ->
                    if (empty)
                        removeDivisionSubDivisionOnPreferences()
                    setDivisionEmpty(empty)
                }
            }

            changeFragment.observe(this@HomeActivity) { event ->
                event?.getContentIfNotHandled()?.let { _ ->
                    replaceFragmentForPosition()
                }
            }
        }
    }

    private fun validateInstitutionIdFromPreferencesToSetSpinnerSelection() {
        val institutionId = getViewModel().getInstitutionIdFromPreferences()
        if (institutionId.isNullOrEmpty())
            return

        val index = institutionSpinnerAdapter.getIndexForInstitutionId(institutionId)
        if (index >= 0) {
            spinnerInstitution.setSelection(index)
        } else {
            getViewModel().run {
                removeTournamentOnPreferences()
                removeDivisionSubDivisionOnPreferences()
            }
        }
    }

    private fun validateSportIdFromPreferencesToSetSpinnerSelection() {
        val sportId = getViewModel().getSportIdFromPreferences()

        if (sportId.isNullOrEmpty())
            return

        val index = sportSpinnerAdapter.getIndexForSportId(sportId)
        if (index >= 0) {
            spinnerSport?.setSelection(index)
        }
    }

    private fun validateDivisionIdFromPreferencesToSelectRecyclerItemOrThrowException() {
        if (divisionSubDivisionAdapter.itemCount > 0) {
            val index = validateDivisionSubDivisionFromPreferencesToSelectRecyclerItem()
            val divisionSubDivision =
                divisionSubDivisionAdapter.getItemForPosition(if (index >= 0) index else 0)
            getViewModel().setDivisionSubDivisionModel(divisionSubDivision)
        }
    }

    private fun validateDivisionSubDivisionFromPreferencesToSelectRecyclerItem(): Int {
        val divisionSubDivision = getViewModel().getDivisionSubDivisionFromPreferences()
        if (divisionSubDivision === null || divisionSubDivision.divisionId.isEmpty()) {
            divisionSubDivisionAdapter.deselectAllModels()
            divisionSubDivisionAdapter.selectModel(0)
            return 0
        }

        val index = divisionSubDivisionAdapter.selectItemIfExistFromDivisionIdOrSetFirstPosition(
            divisionSubDivision
        )
        return index
    }

    private fun validateTournamentIdFromPreferencesToSelectRecyclerItemOrThrowException() {
        if (tournamentAdapter.itemCount > 0) {
            val index = validateTournamentIdFromPreferencesToSelectRecyclerItem()
            val tournament = tournamentAdapter.getItemForPosition(if (index >= 0) index else 0)
            getViewModel().setTournamentModel(tournament)
        }
        //TODO do i need refresh the fragment?
    }

    private fun validateTournamentIdFromPreferencesToSelectRecyclerItem(): Int {
        val idTournament = getViewModel().getTournamentIdFromPreferences()
        if (idTournament.isNullOrEmpty()) {
            tournamentAdapter.deselectAllModels()
            tournamentAdapter.selectModel(0)
            return 0
        }

        val index =
            tournamentAdapter.selectItemIfExistFromTournamentIdOrSetFirstPosition(idTournament)
        return index
    }

    override fun onItemClick(view: View, position: Int, t: InstitutionModel) {
        //setIdInstitutionFromPreferences(t.id)
        //openCloseHandleSheetState()
    }

    override fun onBackPressed() {
        getRootDataBinding().run {
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                drawerLayout.closeDrawer(GravityCompat.END)
            }
            //else {
            //  super.onBackPressed()
            //}
        }
    }

    private fun goToPlacesActivity() {
        getViewModel().removeCountryAndCityOnPreferences()
        startActivity(Intent(this, PlacesActivity::class.java))
        finish()
    }

    override fun showFAB() {
        //  fab?.show()
    }

    override fun hideFAB() {
        //fab?.hide()
    }

    override fun showLoadingUI(textInfoResource: Int) {
        showLoadingFragment(textInfoResource)
    }

    override fun hideLoadingUI() {
        hideLoadingFragment()
    }

    override fun showFriendlyError(subtitle: String?) {
        handleFriendlyError(true, subtitle ?: "")
    }

    override fun showErrorFragment(exceptionHandle: ExceptionHandle) {
        replaceFragment(
            supportFragmentManager,
            ErrorFragment.newInstance(currentFragmentPosition, exceptionHandle)
        )
    }

    override fun hideErrorFragment(position: Int) {
        setFragmentPositionAndReplaceFragment(position)
    }

    override fun showToolbarSpinner() {
        //sp_division?.visibility = View.VISIBLE
    }

    private fun hideToolbarSpinner() {
        //sp_division?.visibility = View.INVISIBLE
    }

    private fun handleFriendlyError(show: Boolean, subtitle: String) {
        getRootDataBinding().clErrorVisibility = show
        getRootDataBinding().title = getString(R.string.error_title_fragment)
        getRootDataBinding().subtitle = subtitle
        getRootDataBinding().btnOtherVisibility = true
        getRootDataBinding().btnOtherText = getString(R.string.go_to_places_text_button)
        hideLoading()
    }

    private fun hideFriendlyError() {
        handleFriendlyError(false, "")
    }

    private fun showLoading(message: String) {
        getRootDataBinding().progress = DataProgressbar(true, message)
    }

    private fun hideLoading() {
        getRootDataBinding().progress = DataProgressbar(false, "")
    }

    private fun showLoadingFragment(resource: Int) {
        getRootDataBinding().progressFragment = DataProgressbar(true, getString(resource))
    }

    private fun hideLoadingFragment() {
        getRootDataBinding().progressFragment = DataProgressbar(false, "")
    }

    override fun onItemDivisionClick(view: View, position: Int, division: DivisionModel) {

        //TODO delete?
        //   divisionAdapter.deselectAllModels()
        //   divisionAdapter.selectModel(position)
        // getViewModel().setDivisionModel(division)

    }

    override fun onItemDivisionClick(
        view: View,
        position: Int,
        division: DivisionSubDivisionModel
    ) {
        validateIfInterstitialAdShouldBeDisplayed()
        divisionSubDivisionAdapter.deselectAllModels()
        divisionSubDivisionAdapter.selectModel(position)
        getViewModel().setDivisionSubDivisionModel(division)
        replaceFragmentForPosition()
    }

    override fun onItemTournamentClick(view: View, position: Int, tournament: TournamentModel) {
        validateIfInterstitialAdShouldBeDisplayed()
        tournamentAdapter.deselectAllModels()
        tournamentAdapter.selectModel(position)
        getViewModel().setTournamentModel(tournament)
    }

    private fun setDivisionEmpty(empty: Boolean) {
        getRootDataBinding().divisionEmpty = empty
    }

    private fun setTournamentEmpty(empty: Boolean) {
        getRootDataBinding().tournamentEmpty = empty
    }

    private fun setSportEmpty(empty: Boolean) {
        getRootDataBinding().sportEmpty = empty
    }

    private fun showPrivacyDialogAlert() {
        AlertDialogComponent.showAlert(
            this,
            getString(R.string.alert_info_legal_title),
            getString(R.string.alert_info_legal),
            getString(R.string.alert_button_understand),
            ::homeInformationAlertDisplayed
        )
    }

    private fun showUpdateVersionDialogAlert() {
        AlertDialogComponent.showAlert(
            this,
            getString(R.string.alert_attention_title),
            getString(R.string.alert_update_version_message),
            getString(R.string.alert_button_understand),
            ::homeInformationAlertDisplayed
        )

    }

    private fun homeInformationAlertDisplayed() {
        getViewModel().homeInformationAlertDisplayed()
    }

    //navigation drawer
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.institutions_menu -> {
                startActivity(Intent(this, InstitutionActivity::class.java))
            }
            R.id.sponsors_menu -> {
                startActivity(Intent(this, SponsorActivity::class.java))
            }
            R.id.change_place_menu -> {
                goToPlacesActivity()
            }
            R.id.mcs_contact -> {
                startActivity(Intent(this, MyCitysSportsContactActivity::class.java))
            }
            R.id.notification -> {
                startActivity(Intent(this, NotificationActivity::class.java))
            }
        }
        validateIfInterstitialAdShouldBeDisplayed()
        //drawer_layout.closeDrawer(GravityCompat.END)
        getRootDataBinding().drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    //    override fun onNavigationItemReselected(p0: MenuItem) {
    //}
}
