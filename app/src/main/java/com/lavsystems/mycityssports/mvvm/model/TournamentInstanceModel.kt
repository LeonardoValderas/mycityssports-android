package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TournamentInstanceModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val countTeam: Int
) {
    constructor() : this("", "", 0)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TournamentInstanceModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (countTeam != other.countTeam) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + countTeam.hashCode()
        return result
    }
}