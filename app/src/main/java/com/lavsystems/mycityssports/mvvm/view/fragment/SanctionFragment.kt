package com.lavsystems.mycityssports.mvvm.view.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.databinding.FragmentSanctionBinding
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.activity.SponsorDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.adapter.SanctionAdapter
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.mvvm.viewmodel.SanctionViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.uicomponents.RecyclerViewBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
class SanctionFragment : BaseFragment<FragmentSanctionBinding, SanctionViewModel>(), OnAdapterListener<Any> {
    private val viewModel: SanctionViewModel by viewModel()
    override fun initViewModel(): SanctionViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_sanction
    private var recyclerBuilder: RecyclerViewBuilder<Any>? = null
    private val adapter = SanctionAdapter(this)
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initRecyclerView()
        initSwipeRefresh()
    }

    override fun initObservers() {
        with(getViewModel()) {
            sanctions.observe(this@SanctionFragment, { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data?.let {
                            if (it.sanctions.isNotEmpty())
                                hideEmptyTitle()
                            else
                                showEmptyTitle()

                            adapter.addSponsors(it.sponsors as MutableList<Any>)
                            @Suppress("UNCHECKED_CAST")
                            recyclerBuilder?.addItems(it.sanctions as MutableList<Any>, true)
                        } ?: run {
                            showEmptyTitle()
                        }

                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        getCommunicator()?.showLoadingUI(R.string.loading_sanction_data)
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            getCommunicator()?.showErrorFragment(it)
                        } ?: run {
                            val exceptionHandle =
                                ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                            getCommunicator()?.showErrorFragment(exceptionHandle)
                        }

                        hideLoading()
                    }
                    else -> {
                        val exceptionHandle =
                            ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                        getCommunicator()?.showErrorFragment(exceptionHandle)
                        hideLoading()
                    }
                }
            })
        }
    }

    private fun initRecyclerView() {
        val recycler = getViewDataBinding().iRvSanction.rvContainer
        recyclerBuilder = RecyclerViewBuilder
            .builder<Any>(recycler)
            .setLinearLayoutManagerOrientation(isHorizontal = false)
            .setDividerItemDecoration(isHorizontal = false)
            .setHasFixedSize(true)
            //.setEndlessRecyclerOnScrollListener(Page(), 2, this)
            .setHasStableIds(true)
            .setAdapter(adapter)
            .build()
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout = getViewDataBinding().iRvSanction.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getSanctions(true)
        }
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getCommunicator()?.hideLoadingUI()
    }

    private fun showEmptyTitle() {
        getViewDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getViewDataBinding().empty = false
    }

    override fun getFromHome() {
        getViewModel().getSanctions(false)
    }

    private fun setFabVisibility(show: Boolean) {
        if (show)
            getCommunicator()?.showFAB()
        else
            getCommunicator()?.hideFAB()
    }

    override fun onItemClick(view: View, position: Int, item: Any) {
        if(item is SponsorModel){
            val intent = Intent(activity?.applicationContext, SponsorDetailsActivity::class.java)
            intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, item.id)
            val vs = view.findViewById<View>(R.id.iv_sponsor)
            val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
            val transitionActivityOptions =
                ActivityOptions.makeSceneTransitionAnimation(activity, pairSponsor)
            startActivity(intent, transitionActivityOptions.toBundle())
        }
    }
}
