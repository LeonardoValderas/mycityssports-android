package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.JsonClass

@Suppress("UNCHECKED_CAST")
@JsonClass(generateAdapter = true)
data class FixtureInstanceItemModel(
    val tournamentInstance: TournamentInstanceModel,
    val fixtures: MutableList<FixtureItemModel>,
    val sponsors: MutableList<SponsorModel>
): ExpandableRecyclerViewAdapter.ExpandableGroup<Any>(){
    constructor(): this(TournamentInstanceModel(), mutableListOf(), mutableListOf())

    override fun getExpandingItems(): MutableList<Any> {
        return getFixturesWithSponsors(fixtures as MutableList<Any>)
    }

    private fun getFixturesWithSponsors(fixtures: MutableList<Any>): MutableList<Any>{
        if (fixtures.filterIsInstance(SponsorModel::class.java)
                .isEmpty() && sponsors.isNotEmpty()
        ) {
            when (val size = fixtures.size) {
                1 -> {
                    fixtures.add(fixtures.size, sponsors.random())
                }
                2 -> {
                    fixtures.add(size, sponsors.random())
                }
                else -> {
                    for (i in 2..size step 3) {
                        fixtures.add(i, sponsors.random())
                    }
                }
            }
        }

        return fixtures
    }

//    private fun getIndexAdMob(): Int {
//        return when(fixtures.size){
//            in 1..3 -> {
//                // add on last index
//                fixtures.size
//            } else -> {
//                val range = 0..fixtures.size
//                range.random()
//            }
//        }
//    }

    private fun getIndexSponsor(): Int {
        return when(fixtures.size){
            in 1..3 -> {
                // add on last index
                fixtures.size
            } else -> {
                val range = 0..fixtures.size
                range.random()
            }
        }
    }

    override fun asReversed() {
        fixtures.reverse()
    }
}