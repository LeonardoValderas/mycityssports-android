package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.TableRepository
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class TableViewModel(
    private val repository: TableRepository
) : BaseViewModel() {
    companion object {
        const val TABLES = "Tables"
        const val TABLES_UNKNOWN_ERROR = "Error unknown in get table data."
    }

    private val _table = MutableLiveData<DataResponse<TableDto>>()
    val table: LiveData<DataResponse<TableDto>>
        get() = _table

    init {
        getTable(false)
    }

    final fun getTable(isRefresh: Boolean) {
        try {
            repository.getTable(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _table.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, TABLES)
                    _table.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _table.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _table.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, TABLES)
                            } ?: run {
                                simpleErrorLog(it.message ?: TABLES_UNKNOWN_ERROR,
                                    TABLES
                                )
                            }
                            _table.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, TABLES)
            _table.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}
