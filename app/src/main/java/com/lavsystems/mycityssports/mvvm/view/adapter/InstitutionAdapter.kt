package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.InstitutionItemBinding
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener

class InstitutionAdapter(private val listener: OnAdapterListener<InstitutionModel>) :
    RecyclerView.Adapter<InstitutionAdapter.ViewHolder>() {

    private var institutions = mutableListOf<InstitutionModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = InstitutionItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(institutions[position], listener)

    override fun getItemCount(): Int = institutions.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getInstitution(position: Int): InstitutionModel {
        val institution = institutions[position]
        return institution
    }

    fun addInstitutions(institutions: MutableList<InstitutionModel>) {
        this.institutions.clear()
        this.institutions.addAll(institutions)
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: InstitutionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(institution: InstitutionModel, listener: OnAdapterListener<InstitutionModel>) {
            with(binding) {

                model = institution

                YoYo.with(Techniques.FadeIn).playOn(root)

                root.setOnClickListener {
                    listener.onItemClick(it, layoutPosition, institution)
                    YoYo.with(Techniques.Pulse).playOn(root)
                }

                executePendingBindings()
            }
        }
    }
}