package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel

interface OnAdapterListenerDivisionSubDivision {
    fun onItemDivisionClick(view: View, position: Int, division: DivisionSubDivisionModel)
}