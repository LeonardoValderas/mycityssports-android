package com.lavsystems.mycityssports.mvvm.view.adapter

import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.TournamentModel
import com.lavsystems.mycityssports.utils.listener.DivisionsSubDivisionsListener

class DivisionsSubDivisionsFromTournamentAdapter constructor(private val tournamentModel: TournamentModel):
    DivisionsSubDivisionsListener {
    private val divisionSubDivisionModelList: MutableList<DivisionSubDivisionModel>
        get() = getDivisionsSubDivisionsModelList()

    override fun getDivisionSubDivisionModel(): MutableList<DivisionSubDivisionModel> {
        return divisionSubDivisionModelList

    }
    override fun hasSubDivisions(): Boolean {
        return tournamentModel.subDivisions.isNotEmpty()
    }

    private fun getDivisionsSubDivisionsModelList(): MutableList<DivisionSubDivisionModel>{
        return  if (hasSubDivisions()) {
            tournamentModel.divisions.flatMap { d ->
                tournamentModel.subDivisions.map { sb ->
                    DivisionSubDivisionModel(d.id, d.name, sb)
                }
            }.toMutableList()
        } else {
            tournamentModel.divisions.map { d ->
                DivisionSubDivisionModel(d.id, d.name, "")
            }.toMutableList()
        }
    }
}