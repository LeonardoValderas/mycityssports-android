package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CityModel(
    @Json(name = "_id")
    val id: String,
    val name: String,
    val state: StateModel,
    val position: PositionModel
) {
    constructor() : this("", "", StateModel(), PositionModel("", ""))

    override fun toString(): String {
        return name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CityModel

        if (id != other.id) return false
        if (name != other.name) return false
        if (state != other.state) return false
        if (position != other.position) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + state.hashCode()
        result = 31 * result + position.hashCode()
        return result
    }

}