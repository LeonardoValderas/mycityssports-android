package com.lavsystems.mycityssports.mvvm.model

import android.util.Log
import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.JsonClass

@Suppress("UNCHECKED_CAST")
@JsonClass(generateAdapter = true)
data class FixtureZoneItemModel(
    val tournamentZone: TournamentZoneModel,
    val fixtures: MutableList<FixtureItemModel>,
    val sponsors: MutableList<SponsorModel>
) : ExpandableRecyclerViewAdapter.ExpandableGroup<Any>() {
    constructor() : this(TournamentZoneModel(), mutableListOf(), mutableListOf())

    override fun getExpandingItems(): MutableList<Any> {
        return getFixturesWithSponsors(fixtures as MutableList<Any>)
    }

    override fun asReversed() {
        fixtures.reverse()
    }

    private fun getFixturesWithSponsors(fixtures: MutableList<Any>): MutableList<Any>{
        if (fixtures.filterIsInstance(SponsorModel::class.java)
                .isEmpty() && sponsors.isNotEmpty()
        ) {
            when (val size = fixtures.size) {
                1 -> {
                    fixtures.add(fixtures.size, sponsors.random())
                }
                2 -> {
                    fixtures.add(size, sponsors.random())
                }
                else -> {
                    for (i in 2..size step 3) {
                        fixtures.add(i, sponsors.random())
                    }
                }
            }
        }

        return fixtures
    }

//    private fun getIndexAdMob(): Int {
//        return when (fixtures.size) {
//            in 1..3 -> {
//                fixtures.size
//            }
//            else -> {
//                val range = 0..fixtures.size
//                range.random()
//            }
//        }
//    }

    private fun getIndexSponsor(): Int {
        return when(fixtures.size){
            in 1..3 -> {
                // add on last index
                fixtures.size
            } else -> {
                val range = 0..fixtures.size
                range.random()
            }
        }
    }
}