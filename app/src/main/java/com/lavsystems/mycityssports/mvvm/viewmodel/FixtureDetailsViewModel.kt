package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.*
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.repository.FixtureDetailsRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class FixtureDetailsViewModel(
    private val repository: FixtureDetailsRepository
) : BaseViewModel() {

    companion object {
        const val FIXTURES_DETAILS = "Fixtures_Details"
        const val FIXTURES_DETAILS_UNKNOWN_ERROR = "Error unknown in get fixture details data."
        const val FIXTURES_DETAILS_CITY_UNKNOWN_ERROR = "Error unknown in get city data."
    }

    private val _fixtureDetails = MutableLiveData<DataResponse<FixtureItemModel>>()
    val fixtureDetails: LiveData<DataResponse<FixtureItemModel>>
        get() = _fixtureDetails

    private val _city = MutableLiveData<DataResponse<CityModel>>()
    val cityModel: LiveData<DataResponse<CityModel>>
        get() = _city

    fun getCity() {
        try {
            repository.getCity()
                .flowOn(Dispatchers.Main)
                .onStart {
                    _city.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, FIXTURES_DETAILS)
                    _city.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    it?.let {
                        _city.value = DataResponse.SUCCESS(it)
                    } ?: run {
                        simpleErrorLog(
                            FIXTURES_DETAILS_CITY_UNKNOWN_ERROR,
                            FIXTURES_DETAILS
                        )
                        _city.value =
                            DataResponse.FAILURE(ExceptionHandleImpl(Exception()))
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, FIXTURES_DETAILS)
            _city.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }

    fun getFixtureDetails(fixtureId: String) {
        try {
            repository.getFixtureDetails(fixtureId)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _fixtureDetails.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, FIXTURES_DETAILS)
                    _fixtureDetails.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _fixtureDetails.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _fixtureDetails.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, FIXTURES_DETAILS)
                            } ?: run {
                                simpleErrorLog(it.message ?: FIXTURES_DETAILS_UNKNOWN_ERROR,
                                    FIXTURES_DETAILS
                                )
                            }
                            _fixtureDetails.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, FIXTURES_DETAILS)
            _fixtureDetails.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}