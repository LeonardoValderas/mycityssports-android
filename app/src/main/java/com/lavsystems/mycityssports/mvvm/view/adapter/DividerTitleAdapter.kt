package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.databinding.FixtureDividerTitleBinding

class DividerTitleAdapter(private val title: String):
    RecyclerView.Adapter<DividerTitleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = FixtureDividerTitleBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(title)
    }

    inner class ViewHolder(private val binding: FixtureDividerTitleBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(title: String) {
            with(binding) {
                tvDividerTitle.text = title
                executePendingBindings()
            }
        }
    }
}