package com.lavsystems.mycityssports.mvvm.view.adapter

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.*
import com.lavsystems.mycityssports.mvvm.model.FixtureInstanceItemModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem

class FixtureInstanceAdapter(
    parents: MutableList<FixtureInstanceItemModel>,
    private val listener: OnAdapterListenerFixtureItem
) : ExpandableRecyclerViewAdapter<
        Any,
        FixtureInstanceItemModel,
        FixtureInstanceAdapter.PViewHolder,
        FixtureInstanceAdapter.CViewHolder<Any>>(
    parents,
    ExpandingDirection.HORIZONTAL,
    true,
    false
) {

    override fun onCreateParentViewHolder(parent: ViewGroup, viewType: Int): PViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ParentRowBinding.inflate(layoutInflater, parent, false)
        return PViewHolder(
            binding.root
        )
    }

    override fun onCreateChildViewHolder(child: ViewGroup, viewType: Int, position: Int): CViewHolder<Any> {
        val layoutInflater = LayoutInflater.from(child.context)
        val binding = when (viewType) {
            ITEM -> {
                FixtureItemBinding.inflate(layoutInflater, child, false)
            }
            ADMOB_ITEM -> {
                AdsmobItemBinding.inflate(layoutInflater, child, false)
            }
            SPONSOR_ITEM -> {
                SponsorFixtureItemBinding.inflate(layoutInflater, child, false)
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
        return CViewHolder(
            binding.root, binding
        )
    }

    override fun onBindParentViewHolder(
        parentViewHolder: PViewHolder,
        expandableType: FixtureInstanceItemModel,
        position: Int
    ) {
        parentViewHolder.containerView.findViewById<TextView>(R.id.tv_title).text = expandableType.tournamentInstance.name
        if (expandableType.isExpanded) {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.GONE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.VISIBLE
        } else {
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_down_float).visibility = View.VISIBLE
            parentViewHolder.containerView.findViewById<ImageView>(R.id.iv_up_float).visibility = View.GONE
        }
    }

    override fun onBindChildViewHolder(
        childViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: FixtureInstanceItemModel,
        position: Int
    ) {
        when (expandedType) {
            is FixtureItemModel -> childViewHolder.bind(expandedType)
            is SponsorModel -> childViewHolder.bindSponsor(expandedType)
            else -> childViewHolder.bindAdMob(expandedType)
        }
    }

    override fun onExpandedClick(
        expandableViewHolder: PViewHolder,
        expandedViewHolder: CViewHolder<Any>,
        expandedType: Any,
        expandableType: FixtureInstanceItemModel
    ) {
        when (expandedType) {
            is FixtureItemModel -> {
                listener.onItemFixture(
                    expandableViewHolder.containerView,
                    expandedViewHolder.bindingAdapterPosition,
                    expandedType.id
                )
            }
            is SponsorModel -> {
                listener.onItemSponsor(
                    expandableViewHolder.containerView,
                    expandedViewHolder.bindingAdapterPosition,
                    expandedType.id
                )
            }
        }
    }

    override fun onExpandableClick(
        expandableViewHolder: PViewHolder,
        expandableType: FixtureInstanceItemModel
    ) {
    }

    class PViewHolder(v: View) : ExpandableRecyclerViewAdapter.ExpandableViewHolder(v)

    class CViewHolder<T>(v: View, private val binding: ViewDataBinding) :
        ExpandableRecyclerViewAdapter.ExpandedViewHolder(v) {
        fun bind(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }

        fun bindSponsor(item: SponsorModel) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }

        fun bindAdMob(item: T) {
            binding.run {
                setVariable(BR.model, item)
                executePendingBindings()
            }
        }
    }
}