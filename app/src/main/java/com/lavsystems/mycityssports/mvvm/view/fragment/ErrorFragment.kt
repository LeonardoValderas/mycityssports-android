package com.lavsystems.mycityssports.mvvm.view.fragment

import android.os.Bundle
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandle
import com.lavsystems.mycityssports.databinding.FragmentErrorBinding
import com.lavsystems.mycityssports.mvvm.viewmodel.ErrorFragmentViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE
import com.lavsystems.mycityssports.utils.ConstantsUtils.FRAGMENT_POSITION_BUNDLE
import org.koin.androidx.viewmodel.ext.android.viewModel

class ErrorFragment : BaseFragment<FragmentErrorBinding, ErrorFragmentViewModel>() {

    private val viewModel: ErrorFragmentViewModel by viewModel()
    override fun initViewModel(): ErrorFragmentViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_error
    //private var position: Int = 0

    companion object { //type errro?
        @JvmStatic
        fun newInstance(fragmentPosition: Int, exceptionHandle: ExceptionHandle) =
            ErrorFragment().apply {
                arguments = Bundle().apply {
                    putInt(FRAGMENT_POSITION_BUNDLE, fragmentPosition)
                    putSerializable(FRAGMENT_EXCEPTION_HANDLE_BUNDLE, exceptionHandle)
                }
            }
    }

    override fun init() {
        initListeners()
    }

    private fun initListeners() {
        getViewDataBinding().btnTryAgain.setOnClickListener {
            getCommunicator()?.hideErrorFragment(getViewModel().getFragmentPosition())
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            setBundle(this@ErrorFragment.getBundle())
            sendErrorToAnalytic()
            setResourceStringIdFromBundle()

            resourceStringId.observe(this@ErrorFragment, { resource ->
                resource?.let {
                    getViewDataBinding().tvSubtitleError.text = getString(it)
                }
            })
        }
    }

    override fun getFromHome() {

    }
}
