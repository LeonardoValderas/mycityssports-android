package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.NotificationRepository
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class NotificationViewModel(
    private val repository: NotificationRepository
) : BaseViewModel() {

    companion object {
        const val NOTIFICATIONS = "Notifications"
        const val NOTIFICATIONS_UNKNOWN_ERROR = "Error unknown in get notifications data."
    }

    private val _institutions = MutableLiveData<DataResponse<MutableList<NotificationModel>>>()
    val institutions: LiveData<DataResponse<MutableList<NotificationModel>>>
        get() = _institutions

    private val _saved = MutableLiveData<DataResponse<NotificationModel>>()
    val saved: LiveData<DataResponse<NotificationModel>>
        get() = _saved

    init {
        getInstitutionsNotification()
    }

    fun getInstitutionsNotification() {
        try {
            repository.getNotificationInstitutions()
                .flowOn(Dispatchers.IO)
                .onStart {
                    _institutions.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, NOTIFICATIONS)
                    _institutions.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it.status) {
                        Resource.Status.LOADING -> {
                            _institutions.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _institutions.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, NOTIFICATIONS)
                            } ?: run {
                                simpleErrorLog(
                                    it.message ?: NOTIFICATIONS_UNKNOWN_ERROR,
                                    NOTIFICATIONS
                                )
                            }
                            _institutions.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, NOTIFICATIONS)
            _institutions.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }

    fun addNotificationInstitution(institutionId: String) {
        try {
            repository.addNotificationInstitution(institutionId)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _saved.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, NOTIFICATIONS)
                    _saved.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it.status) {
                        Resource.Status.LOADING -> {
                            _saved.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _saved.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, NOTIFICATIONS)
                            } ?: run {
                                simpleErrorLog(
                                    it.message ?: NOTIFICATIONS_UNKNOWN_ERROR,
                                    NOTIFICATIONS
                                )
                            }
                            _saved.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, NOTIFICATIONS)
            _saved.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }

    fun removeNotificationInstitution(institutionId: String) {
        try {
            repository.removeNotificationInstitution(institutionId)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _saved.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, NOTIFICATIONS)
                    _saved.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it.status) {
                        Resource.Status.LOADING -> {
                            _saved.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _saved.value = DataResponse.SUCCESS(it.data?.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, NOTIFICATIONS)
                            } ?: run {
                                simpleErrorLog(
                                    it.message ?: NOTIFICATIONS_UNKNOWN_ERROR,
                                    NOTIFICATIONS
                                )
                            }
                            _saved.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, NOTIFICATIONS)
            _saved.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}