package com.lavsystems.mycityssports.mvvm.view.adapter

import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import com.lavsystems.mycityssports.utils.adapter.DataBindingAdapter
import com.lavsystems.mycityssports.utils.adapter.ProgressbarItem

class FixtureItemAdapterOld(listener: OnAdapterListener<Any>) : DataBindingAdapter<Any>(listener) {

    private var listFixture = mutableListOf<Any>()

    override fun getAnimateRoot(): YoYo.AnimationComposer? {
        return null
        //return YoYo.with(Techniques.FadeIn)
    }

    override fun getAnimateListener(): YoYo.AnimationComposer? {
        return YoYo.with(Techniques.Pulse)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItemModel(position)) {
            is AdMobItem -> R.layout.adsmob_item
            is ProgressbarItem -> R.layout.left_load_more_progress_bar
            else -> R.layout.fixture_item
        }
    }

    override fun getItemModel(int: Int): Any {
        return listFixture[int]
    }

    override fun getItemCount(): Int {
        return listFixture.size
    }

    override fun getItemId(position: Int): Long {
        val fixture = getItemModel(position)
        return when(fixture){
            is FixtureModel -> fixture.id.toLong()
            else -> (fixture as AdMobItem).id.toLong()
        }
    }

    override fun addItems(items: MutableList<Any>, clear: Boolean) {
        if(clear){
            listFixture.clear()
            notifyDataSetChanged()
        }

        listFixture.addAll(items)
        notifyDataSetChanged()
    }

    fun getNumberMatch(): Int {
        try {
            return when (listFixture.isEmpty()) {
                true -> -1
                else -> listFixture.filterIsInstance<FixtureItemModel>().first().round
            }
        } catch (e: Exception) {
            return -1
        }
    }

    override fun getItems(): MutableList<Any> {
        return listFixture
    }
}





/*class FixtureAdapter(private val listener: OnAdapterListenerFullFixture) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listFixture = mutableListOf<Any>()
    private var adsPosition = -1
    private var page = Page(1, 10, true)
    private var numberMatch = -1

    companion object {
        const val FIXTURE_ITEM = 0
        const val ADS_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        if (viewType == ADS_ITEM) {
            val binding = AdsmobItemBinding.inflate(layoutInflater, parent, false)
            return ViewHolderAds(binding)
        } else {
            val binding = FixtureItemBinding.inflate(layoutInflater, parent, false)
            return ViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ADS_ITEM -> (holder as ViewHolderAds).bind()
            else -> (holder as ViewHolder).bind(listFixture[position] as FixtureModel, listener)
        }
    }

    override fun getItemCount(): Int = listFixture.size

    override fun getItemViewType(position: Int): Int {
        return when (position == adsPosition) {
            true -> ADS_ITEM
            else -> FIXTURE_ITEM
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun addFixtures(fixtures: MutableList<Any>) {
        if(fixtures.first() is FixtureModel) {
            numberMatch = (fixtures.first() as FixtureModel).numberMatch
        }

        listFixture.clear()
        listFixture.addAll(fixtures)
        notifyDataSetChanged()
    }

    fun addMoreFixtures(fixtures: MutableList<Any>) {
        if(fixtures.isEmpty()){
            page.hasMore = false
            return
        }

        listFixture.addAll(fixtures)
        notifyDataSetChanged()
    }

    fun getPage(): Page {
        page.page++
        return page
    }

    fun hasMore(): Boolean{
        return page.hasMore
    }

    fun getNumberMatch(): Int {
        return numberMatch
    }

    fun addAdsPosition(adsPosition: Int) {
        this.adsPosition = adsPosition
    }

    class ViewHolder(private var binding: FixtureItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(fixture: FixtureModel, listener: OnAdapterListenerFullFixture) {
            with(binding) {

                model = fixture

                YoYo.with(Techniques.FadeIn).playOn(root)

                root.setOnClickListener {
                    listener.onItemFixture(it, layoutPosition, fixture)
                    // YoYo.with(Techniques.Pulse).playOn(root)
                }

                executePendingBindings()
            }
        }
    }

    class ViewHolderAds(private var binding: AdsmobItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            with(binding) {
                YoYo.with(Techniques.FadeIn).playOn(root)
                (root.findViewById(R.id.adView_fixture) as AdView).apply {
                    loadAd(AdRequest.Builder().build())
                }
                executePendingBindings()
            }
        }

    }
}*/
