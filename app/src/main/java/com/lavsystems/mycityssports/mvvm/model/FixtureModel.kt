package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class FixtureModel(
    @Json(name = "_id")
    val id: String = UUID.randomUUID().toString(),
    val tournament: String,
    val sport: SportModel,
    val division: String,
    val tournamentInstances: MutableList<FixtureInstanceItemModel>,
    val tournamentZones: MutableList<FixtureZoneItemModel>,
    val tournamentPoints: MutableList<FixturePointItemModel>
) {
    constructor() : this(
        "",
        "",
        SportModel(),
        "",
        mutableListOf(),
        mutableListOf(),
        mutableListOf()
    )
}