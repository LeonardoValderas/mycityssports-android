package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.databinding.TableGridItemBinding
import com.lavsystems.mycityssports.mvvm.model.TableItemModel

class TableGridItemAdapter() :
    RecyclerView.Adapter<TableGridItemAdapter.ViewHolder>() {

    private var tables = mutableListOf<TableItemModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableGridItemAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TableGridItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(tables[position])

    override fun getItemCount(): Int = tables.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun addTables(tables: MutableList<TableItemModel>) {
        this.tables.clear()
        this.tables.addAll(tables)
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: TableGridItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(table: TableItemModel) {
            with(binding) {
                model = table
                executePendingBindings()
            }
        }
    }
}