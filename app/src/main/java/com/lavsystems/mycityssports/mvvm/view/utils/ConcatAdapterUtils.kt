package com.lavsystems.mycityssports.mvvm.view.utils

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.ConcatAdapter
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.model.RankingModel
import com.lavsystems.mycityssports.mvvm.model.TableModel
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem
import java.lang.ref.WeakReference

class ConcatAdapterUtils<T>(private val context: WeakReference<Context>, private val listener: OnAdapterListenerFixtureItem?) :
    OnAdapterListenerFixtureItem {

    private var concatAdapter: ConcatAdapter = ConcatAdapter()

    companion object {
        private val FINAL_INSTANCES = "Instancias Finales"
        private val ZONES = "Zonas"
        private val POINTS = "Puntos"
        private val ROUND = "Fecha"
    }

    fun getConcatAdapter() = concatAdapter

    fun setModel(model: T, adMobAdapter: AdMobAdapter?, sponsorItemAdapter: SponsorItemAdapter?) {
        when (model) {
            is TableModel -> {
                //1 instances
                if (model.tournamentInstances.isNotEmpty()) {
                    concatAdapter.addAdapter(DividerTitleAdapter(context.get()?.getString(R.string.final_instances_title) ?: FINAL_INSTANCES))
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        TableInstanceAdapter(model.tournamentInstances, this).apply {
                            setExpanded(false)
                            setHasStableIds(true)
                        })
                }
                //2 zones
                if (model.tournamentZones.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        DividerTitleAdapter(context.get()?.getString(R.string.zones_title) ?: ZONES)
                    )
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        TableZoneAdapter(model.tournamentZones)
//                            .apply {
//                            setHasStableIds(true)
//                        }
                    )
                }
                // 3 points
                if (model.tournamentPoints.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        DividerTitleAdapter(context.get()?.getString(R.string.points_title) ?: POINTS)
                    )
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        TableGridAdapter(arrayListOf()).apply {
                            setHasStableIds(true)
                            addTables(model.tournamentPoints)
                        }
                    )
                }
                // comment
                if (model.comment.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        CommentAdapter(model.commentFormatted)
                    )
                }
                // source
                if (model.source.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        SourceAdapter(model.sourceFormatted)
                    )
                }

                addSponsorAdapter(sponsorItemAdapter)
                addAdsAdapter(adMobAdapter)
            }
            is FixtureModel -> {

                //1 instances
                if (model.tournamentInstances.isNotEmpty()) {
                    concatAdapter.addAdapter(DividerTitleAdapter(context.get()?.getString(R.string.final_instances_title) ?: FINAL_INSTANCES))
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        FixtureInstanceAdapter(model.tournamentInstances, this).apply {
                            setExpanded(false)
                            setHasStableIds(true)
                        })
                }
                //2 zones
                if (model.tournamentZones.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        DividerTitleAdapter(context.get()?.getString(R.string.zones_title) ?: ZONES)
                    )
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        FixtureZoneAdapter(model.tournamentZones, this).apply {
                            setExpanded(false)
                            setHasStableIds(true)
                        })
                }
                // 3 points
                if (model.tournamentPoints.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        DividerTitleAdapter(context.get()?.getString(R.string.points_title) ?: POINTS)
                    )
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        FixturePointAdapter(
                            model.tournamentPoints,
                            this,
                            context.get()?.getString(R.string.round_prefix) ?: ROUND
                        ).apply {
                            setExpanded(false)
                            setHasStableIds(true)
                        })

                }
                addAdsAdapter(adMobAdapter)
            }
            is RankingModel -> {
                //1 ranking
                if (model.rankings.isNotEmpty()) {
                    concatAdapter.addAdapter(
                        concatAdapter.adapters.size,
                        RankingAdapter(model.rankings).apply {
                            setExpanded(false)
                            setHasStableIds(true)
                        })
                    addSponsorAdapter(sponsorItemAdapter)
                    addAdsAdapter(adMobAdapter)
                }
            }
        }
    }

    private fun addSponsorAdapter(sponsorItemAdapter: SponsorItemAdapter?) {
        if (!concatAdapter.adapters.contains(sponsorItemAdapter)) {
            sponsorItemAdapter?.run {
                concatAdapter.addAdapter(concatAdapter.adapters.size, this)
//                val size = concatAdapter.adapters.size
//                if(size > 2){
//                    val array = (1 until concatAdapter.adapters.size)
//                    val filterOdd = (1 until concatAdapter.adapters.size).filter{ f -> f % 2 == 0 }
//                    val index = filterOdd.random()
//                    concatAdapter.addAdapter(index, this)
//                } else {
//                    concatAdapter.addAdapter(size, this)
//                }

            }
        }
    }

    private fun addAdsAdapter(adMobAdapter: AdMobAdapter?) {
        if (!concatAdapter.adapters.contains(adMobAdapter)) {
            adMobAdapter?.run {
                concatAdapter.addAdapter(concatAdapter.adapters.size, this)
            }
        }
    }

    override fun onItemFixture(view: View, position: Int, fixtureId: String) {
        listener?.onItemFixture(view, position, fixtureId)
    }

    override fun onItemSponsor(view: View, position: Int, sponsorId: String) {
        listener?.onItemSponsor(view, position, sponsorId)
    }
}