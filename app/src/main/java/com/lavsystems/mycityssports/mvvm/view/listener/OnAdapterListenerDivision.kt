package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View
import com.lavsystems.mycityssports.mvvm.model.DivisionModel

interface OnAdapterListenerDivision {
    fun onItemDivisionClick(view: View, position: Int, division: DivisionModel)
}