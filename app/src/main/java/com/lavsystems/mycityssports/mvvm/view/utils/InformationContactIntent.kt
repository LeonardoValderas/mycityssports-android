package com.lavsystems.mycityssports.mvvm.view.utils

import com.lavsystems.mycityssports.mvvm.model.PositionModel

interface InformationContactIntent {
    fun intentHomePhone(phone: String?)
    fun intentPhone(phone: String?)
    fun intentWhatsapp(whatsapp: String?)
    fun intentMap(position: PositionModel?, name: String?)
    fun intentWeb(web: String?)
    fun intentFacebook(facebook: String?)
    fun intentInstagram(instagram: String?)
    fun intentEmail(email: String?)
}