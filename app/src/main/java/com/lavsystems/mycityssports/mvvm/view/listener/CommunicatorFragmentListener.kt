package com.lavsystems.mycityssports.mvvm.view.listener

import com.lavsystems.mycityssports.data.network.exception.ExceptionHandle

interface CommunicatorFragmentListener {
    fun showFAB()
    fun hideFAB()
    fun showLoadingUI(textInfoResource: Int)
    fun hideLoadingUI()
    fun showFriendlyError(subtitle: String?)
    fun showErrorFragment(exceptionHandle: ExceptionHandle)
    fun hideErrorFragment(position: Int)
    fun showToolbarSpinner()
}