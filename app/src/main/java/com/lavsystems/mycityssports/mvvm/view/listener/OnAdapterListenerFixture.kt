package com.lavsystems.mycityssports.mvvm.view.listener

import android.view.View
import com.lavsystems.mycityssports.data.dto.LoadMoreFixtureDto
import com.lavsystems.mycityssports.mvvm.model.FixtureModel

interface OnAdapterListenerFixture {
    fun onItemFixture(view: View, position: Int, fixture: FixtureModel)
    fun onLoadMore(loadMoreDto: LoadMoreFixtureDto)
}