package com.lavsystems.mycityssports.mvvm.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TableModel(
    @Json(name = "_id")
    val id: String,
    val tournament: String,
    val sport: String,
    val division: String,
    val tournamentZones: MutableList<TableZoneModel>,
    val tournamentInstances: MutableList<TableInstanceModel>,
    val tournamentPoints: MutableList<TableItemModel>,
    val comment: String,
    val institution: String,
    val dateCreate: String,
    val dateUpdate: String?,
    val source: String = ""
) {
    constructor() : this(
        "",
        "",
        "",
        "",
        mutableListOf(),
        mutableListOf(),
        mutableListOf(),
        "",
        "",
        "",
        "",
        ""
    )

    companion object {
        const val SOURCE = "Fuente: "
        const val COMMENTE_CHAR = "* "
    }

    val sourceFormatted: String
        get() = if(source.isEmpty()) source else SOURCE + source
    val commentFormatted: String
        get() = if(comment.isEmpty()) comment else COMMENTE_CHAR + comment

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TableModel

        if (id != other.id) return false
        if (tournament != other.tournament) return false
        if (sport != other.sport) return false
        if (division != other.division) return false
        if (tournamentZones != other.tournamentZones) return false
        if (tournamentInstances != other.tournamentInstances) return false
        if (tournamentPoints != other.tournamentPoints) return false
        if (comment != other.comment) return false
        if (institution != other.institution) return false
        if (dateCreate != other.dateCreate) return false
        if (dateUpdate != other.dateUpdate) return false
        if (source != other.source) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + tournament.hashCode()
        result = 31 * result + sport.hashCode()
        result = 31 * result + division.hashCode()
        result = 31 * result + tournamentZones.hashCode()
        result = 31 * result + tournamentInstances.hashCode()
        result = 31 * result + tournamentPoints.hashCode()
        result = 31 * result + comment.hashCode()
        result = 31 * result + institution.hashCode()
        result = 31 * result + dateCreate.hashCode()
        result = 31 * result + (dateUpdate?.hashCode() ?: 0)
        result = 31 * result + source.hashCode()
        return result
    }


}