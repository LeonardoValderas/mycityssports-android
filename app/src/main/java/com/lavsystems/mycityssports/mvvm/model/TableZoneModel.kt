package com.lavsystems.mycityssports.mvvm.model

import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.squareup.moshi.JsonClass

@Suppress("UNCHECKED_CAST")
@JsonClass(generateAdapter = true)
class TableZoneModel(
    val tournamentZone: TournamentZoneModel,
    val tableDataZone: MutableList<TableItemModel>
): ExpandableRecyclerViewAdapter.ExpandableGroup<Any>(){
    constructor() : this(
        TournamentZoneModel(),
        mutableListOf()
    )

    override fun getExpandingItems(): MutableList<Any> {
        return tableDataZone as MutableList<Any>
    }

    override fun asReversed() {
        tableDataZone.reverse()
    }
}