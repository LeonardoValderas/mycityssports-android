package com.lavsystems.mycityssports.mvvm.view.adapter

import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import com.lavsystems.mycityssports.utils.adapter.DataBindingAdapter

class AdMobAdapter(private val layout: Int, private val adMobItem: AdMobItem) : DataBindingAdapter<AdMobItem>(null) {
    override fun getAnimateRoot(): YoYo.AnimationComposer? {
        return null
    }

    override fun getAnimateListener(): YoYo.AnimationComposer? {
        return null
    }

    override fun getItemModel(int: Int): AdMobItem {
        return adMobItem
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun getItemId(position: Int): Long {
        return adMobItem.id.toLong()
    }

    override fun addItems(items: MutableList<AdMobItem>, clear: Boolean) {
    }

    override fun getItems(): MutableList<AdMobItem> {
        return mutableListOf()
    }

    override fun getItemViewType(position: Int): Int {
        return layout
    }
}
