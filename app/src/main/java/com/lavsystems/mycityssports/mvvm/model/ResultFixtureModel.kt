package com.lavsystems.mycityssports.mvvm.model

import androidx.databinding.BaseObservable
import com.google.gson.annotations.Expose
import java.io.Serializable

class ResultFixtureModel(@Expose
val value: Int?,
@Expose
val label: String?
): BaseObservable(), Serializable {
    constructor(): this(null, null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResultFixtureModel

        if (value != other.value) return false
        if (label != other.label) return false

        return true
    }

    override fun hashCode(): Int {
        var result = value ?: 0
        result = 31 * result + (label?.hashCode() ?: 0)
        return result
    }

}