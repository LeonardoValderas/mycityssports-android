package com.lavsystems.mycityssports.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.databinding.TableGridBinding
import com.lavsystems.mycityssports.mvvm.model.TableItemModel

class TableGridAdapter(private var tables: MutableList<TableItemModel>): RecyclerView.Adapter<TableGridAdapter.ViewHolder>() {
    private var adapter = TableGridItemAdapter()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TableGridBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, adapter)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(tables)

    override fun getItemCount(): Int = 1

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getTableGridItemAdapter(): TableGridItemAdapter {
        return adapter
    }

    fun addTables(tables: MutableList<TableItemModel>) {
        this.tables.clear()
        this.tables.addAll(tables)
        notifyDataSetChanged()
    }

    class ViewHolder(
        private var binding: TableGridBinding,
        private val pointAdapter: TableGridItemAdapter
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(tables: MutableList<TableItemModel>) {
            with(binding) {
                root?.let {
                    it.findViewById<RecyclerView>(R.id.rv_container)?.apply {
                        layoutManager = LinearLayoutManager(root.context, RecyclerView.VERTICAL, false)
                        adapter = pointAdapter
                        pointAdapter.addTables(tables)
                    }
                }
                executePendingBindings()
            }
        }
    }
}