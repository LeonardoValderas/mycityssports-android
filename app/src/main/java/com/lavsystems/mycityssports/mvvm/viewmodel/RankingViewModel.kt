package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.mycityssports.base.BaseViewModel
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.RankingRepository
import com.lavsystems.mycityssports.utils.OpenForTesting
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@OpenForTesting
class RankingViewModel(
    private val repository: RankingRepository
) : BaseViewModel() {

    companion object {
        const val RANKINGS = "Rankings"
        const val RANKINGS_UNKNOWN_ERROR = "Error unknown in get ranking data."
    }

    private val _ranking = MutableLiveData<DataResponse<RankingDto>>()
    val ranking: LiveData<DataResponse<RankingDto>>
        get() = _ranking

    init {
        getRanking(false)
    }

    final fun getRanking(isRefresh: Boolean) {
        try {
            repository.getRanking(isRefresh)
                .flowOn(Dispatchers.IO)
                .onStart {
                    _ranking.value = DataResponse.LOADING()
                }.catch {
                    exceptionToLog(it, RANKINGS)
                    _ranking.value = DataResponse.FAILURE(ExceptionHandleImpl(it))
                }.onCompletion {
                    if (it == null) println("Completed successfully")
                }.map {
                    when (it?.status) {
                        Resource.Status.LOADING -> {
                            _ranking.value = DataResponse.LOADING()
                        }
                        Resource.Status.SUCCESS -> {
                            _ranking.value = DataResponse.SUCCESS(it.data)
                        }
                        Resource.Status.ERROR -> {
                            it.e?.let { t ->
                                exceptionToLog(t, RANKINGS)
                            } ?: run {
                                simpleErrorLog(it.message ?: RANKINGS_UNKNOWN_ERROR,
                                    RANKINGS
                                )
                            }
                            _ranking.value =
                                DataResponse.FAILURE(ExceptionHandleImpl(Exception(it.message)))
                        }
                        else -> {}
                    }
                }.launchIn(viewModelScope)
        } catch (e: Exception) {
            exceptionToLog(e, RANKINGS)
            _ranking.value = DataResponse.FAILURE(ExceptionHandleImpl(e))
        }
    }
}
