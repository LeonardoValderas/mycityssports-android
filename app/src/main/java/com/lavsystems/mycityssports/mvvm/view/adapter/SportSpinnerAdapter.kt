package com.lavsystems.mycityssports.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.SimpleSpinnerItemSportBinding
import com.lavsystems.mycityssports.databinding.SportSpinnerItemBinding
import com.lavsystems.mycityssports.mvvm.model.SportModel

class SportSpinnerAdapter (context: Context, var sportsList: MutableList<SportModel>) :
    ArrayAdapter<SportModel>(context, 0, sportsList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = SimpleSpinnerItemSportBinding.inflate(inflater, parent, false)
        val country = getSportModelForPosition(position)
        bindView(binding, country)
        return binding.root
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = SportSpinnerItemBinding.inflate(inflater, parent, false)
        val country = getSportModelForPosition(position)
        bindDropDown(binding, country)
        return binding.root
    }

    override fun getItem(position: Int): SportModel? {
        return sportsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return sportsList.size
    }

    fun getSportModelForPosition(position: Int): SportModel {
        return sportsList[position]
    }

    fun getIndexForSportId(idSport: String): Int {
        sportsList.forEachIndexed { index, sport ->
            if (sport.id == idSport)
                return index
        }

        return -2
    }

    private fun bindDropDown(binding: SportSpinnerItemBinding, sport: SportModel) {
        with(binding) {
            model = sport
            //YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    private fun bindView(binding: SimpleSpinnerItemSportBinding, sport: SportModel) {
        with(binding) {
            model = sport
            //YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    fun setSports(sports: MutableList<SportModel>) {
        this.sportsList = sports
        notifyDataSetChanged()
    }
}




