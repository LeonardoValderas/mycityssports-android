package com.lavsystems.mycityssports.mvvm.model


import androidx.room.Embedded
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AddressModel(
    val street: String,
    val number: String,
    val neighborhood: String,
   // @Embedded
    val position: PositionModel,
    val complement: String
) {
    constructor() : this(
        "",
        "",
        "",
        PositionModel(),
        ""
    )

    private fun addressEmpty() = street.isEmpty()

    val streetAndNumber: String
       get() = if(addressEmpty()) "" else "$street $number"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AddressModel

        if (street != other.street) return false
        if (number != other.number) return false
        if (neighborhood != other.neighborhood) return false
        if (position != other.position) return false
        if (complement != other.complement) return false

        return true
    }

    override fun hashCode(): Int {
        var result = street.hashCode()
        result = 31 * result + number.hashCode()
        result = 31 * result + neighborhood.hashCode()
        result = 31 * result + position.hashCode()
        result = 31 * result + complement.hashCode()
        return result
    }
}