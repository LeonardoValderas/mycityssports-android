package com.lavsystems.mycityssports.mvvm.view.fragment

import android.app.ActivityOptions
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lavsystems.mycityssports.BR
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseFragment
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.databinding.FragmentFixtureBinding
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.model.TableModel
import com.lavsystems.mycityssports.mvvm.view.activity.FixtureDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.activity.SponsorDetailsActivity
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListenerFixtureItem
import com.lavsystems.mycityssports.mvvm.view.utils.ConcatAdapterUtils
import com.lavsystems.mycityssports.mvvm.viewmodel.FixtureViewModel
import com.lavsystems.mycityssports.utils.ConstantsUtils.FIXTURE_ID_BUDLE
import com.lavsystems.mycityssports.utils.ConstantsUtils.SPONSOR_ID_BUDLE
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.ref.WeakReference

@ExperimentalCoroutinesApi
@Suppress("UNCHECKED_CAST")
class FixtureFragment : BaseFragment<FragmentFixtureBinding, FixtureViewModel>(),
    OnAdapterListenerFixtureItem {

    private val viewModel: FixtureViewModel by viewModel()
    private var concatAdapter = ConcatAdapter()
    override fun initViewModel(): FixtureViewModel = viewModel
    override fun getBindingVariable() = BR.vm
    override fun layoutRes(): Int = R.layout.fragment_fixture
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun init() {
        initSwipeRefresh()
    }

    override fun initObservers() {
        with(getViewModel()) {
            fixture.observe(this@FixtureFragment) { resource ->
                when (resource.status) {
                    StatusResponse.SUCCESS -> {
                        resource.data.let { f ->
                            f?.let { fixtureModel ->
                                getViewDataBinding().sport = "${fixtureModel.sport.name} ${fixtureModel.sport.gender.name}"
                                if (isModelEmpty(fixtureModel))
                                    showEmptyTitle()
                                else
                                    hideEmptyTitle()

                                concatAdapter =
                                    ConcatAdapterUtils<FixtureModel>(
                                        WeakReference(context),
                                        this@FixtureFragment
                                    ).apply {
                                        setModel(
                                            fixtureModel, AdMobAdapter(R.layout.adsmob_item_fixture, AdMobItem()),
                                            null
                                        )
                                    }.getConcatAdapter()

                                initRecyclerView()
                            } ?: run {
                                showEmptyTitle()
                            }
                        }

                        hideLoading()
                    }
                    StatusResponse.LOADING -> {
                        getCommunicator()?.showLoadingUI(R.string.loading_fixture_data)
                    }
                    StatusResponse.FAILURE -> {
                        resource.exceptionHandle?.let {
                            getCommunicator()?.showErrorFragment(it)
                        } ?: run {
                            val exceptionHandle =
                                ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                            getCommunicator()?.showErrorFragment(exceptionHandle)
                        }

                        hideLoading()
                    }
                    else -> {
                        val exceptionHandle =
                            ExceptionHandleImpl(Throwable(getString(R.string.known_error)))
                        getCommunicator()?.showErrorFragment(exceptionHandle)
                        hideLoading()
                    }
                }
            }
        }
    }

    override fun getFromHome() {
        getViewModel().getFixture(false)
    }

    private fun initRecyclerView() {
        getViewDataBinding().iRvFixture.rvContainer.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = concatAdapter
        }
    }

    private fun initSwipeRefresh() {
        swipeRefreshLayout = getViewDataBinding().iRvFixture.srlContainer
        swipeRefreshLayout.setOnRefreshListener {
            getViewModel().getFixture(true)
        }
    }

    private fun hideLoading() {
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false

        getCommunicator()?.hideLoadingUI()
    }

    private fun showEmptyTitle() {
        getViewDataBinding().empty = true
    }

    private fun hideEmptyTitle() {
        getViewDataBinding().empty = false
    }

    private fun setFabVisibility(show: Boolean) {
        if (show)
            getCommunicator()?.showFAB()
        else
            getCommunicator()?.hideFAB()
    }

    private fun isModelEmpty(fixtureModel: FixtureModel): Boolean{
        return fixtureModel.tournamentInstances.isEmpty() &&
                fixtureModel.tournamentZones.isEmpty() &&
                fixtureModel.tournamentPoints.isEmpty()
    }

    override fun onItemFixture(view: View, position: Int, fixtureId: String) {
        val intent = Intent(activity?.applicationContext, FixtureDetailsActivity::class.java)
        intent.putExtra(FIXTURE_ID_BUDLE, fixtureId)
        val vl = view.findViewById<View>(R.id.iv_local_team)
        val vv = view.findViewById<View>(R.id.iv_visit_team)
        val pairHome = android.util.Pair(vl, getString(R.string.fixture_details_transition_local))
        val pairVisitors =
            android.util.Pair(vv, getString(R.string.fixture_details_transition_visita))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(activity, pairHome, pairVisitors)
        startActivity(intent, transitionActivityOptions.toBundle())
    }

    override fun onItemSponsor(view: View, position: Int, sponsorId: String) {
        val intent = Intent(activity?.applicationContext, SponsorDetailsActivity::class.java)
        intent.putExtra(SPONSOR_ID_BUDLE, sponsorId)
        val vs = view.findViewById<View>(R.id.iv_sponsor)
        val pairSponsor = android.util.Pair(vs, getString(R.string.sponsor_transition))
        val transitionActivityOptions =
            ActivityOptions.makeSceneTransitionAnimation(activity, pairSponsor)
        startActivity(intent, transitionActivityOptions.toBundle())
    }
}
