package com.lavsystems.mycityssports.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.databinding.StateItemBinding
import com.lavsystems.mycityssports.mvvm.model.StateModel

class StateSpinnerAdapter (context: Context, resource: Int, var statesList: MutableList<StateModel>) :
    ArrayAdapter<StateModel>(context, resource, statesList) {

    override fun getItem(position: Int): StateModel? {
        return statesList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return statesList.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = StateItemBinding.inflate(inflater, parent, false)
        val country = getStateModelForPosition(position)
        bind(binding, country)
        return binding.root
    }

    fun getStateModelForPosition(position: Int): StateModel {
        return statesList[position]
    }

    private fun bind(binding: StateItemBinding, state: StateModel) {
        with(binding) {
            model = state
            YoYo.with(Techniques.FadeIn).playOn(root)
            executePendingBindings()
        }
    }

    fun setStates(states: MutableList<StateModel>) {
        this.statesList.clear()
        this.statesList.addAll(states)
        notifyDataSetChanged()
    }
}




