package com.lavsystems.mycityssports.utils.binding

import androidx.databinding.BindingAdapter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.lavsystems.mycityssports.utils.adapter.AdMobItem

object AdmobAdapter {
    @BindingAdapter("buildAdView")
    @JvmStatic
    fun AdView.buildAdView(n: Int) {
        loadAd(AdRequest.Builder().build())
    }

    @BindingAdapter("buildAdViewModel")
    @JvmStatic
    fun AdView.buildAdViewModel(adMobItem: AdMobItem) {
        this.id = adMobItem.id
        //this.adSize = adMobItem.size
        //this.adUnitId = adMobItem.adUnitId
        loadAd(AdRequest.Builder().build())
    }
}