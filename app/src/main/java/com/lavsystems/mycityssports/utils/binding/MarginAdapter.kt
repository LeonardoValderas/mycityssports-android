package com.lavsystems.mycityssports.utils.binding

import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter

object MarginAdapter {
    @BindingAdapter("layoutMarginTop")
    @JvmStatic
    fun setMarginTop(view: View, topMargin: Float) {
        val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.topMargin = topMargin.toInt()
        view.layoutParams = layoutParams
    }
}
