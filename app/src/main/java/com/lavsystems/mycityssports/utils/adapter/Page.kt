package com.lavsystems.mycityssports.utils.adapter

data class Page(var page: Int, var size: Int, var hasMore: Boolean){
    constructor(): this(0, 10, true)
}