package com.lavsystems.mycityssports.utils.firebase

import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.crashlytics.ktx.KeyValueBuilder
import com.google.firebase.crashlytics.ktx.setCustomKeys

object CrashlyticsUtils {

// EXAMPLE
//    CrashlyticsUtils.setEvent {
//        key("Hola", "hello")
//    }
    fun setEvent(customKeysAndValues: KeyValueBuilder.() -> Unit) {
        FirebaseCrashlytics.getInstance().setCustomKeys(customKeysAndValues)
//        {
//            key("str_key", "hello")
//            key("bool_key", true)
//            key("int_key", 1)
//            key("long_key", 1L)
//            key("float_key", 1.0f)
//            key("double_key", 1.0)
//        }
    }
}