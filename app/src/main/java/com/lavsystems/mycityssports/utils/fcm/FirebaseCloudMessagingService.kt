package com.lavsystems.mycityssports.utils.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.mvvm.model.FcmModel
import com.lavsystems.mycityssports.mvvm.view.activity.HomeActivity
import com.lavsystems.mycityssports.mvvm.viewmodel.FcmViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.java.KoinJavaComponent

@OptIn(ExperimentalCoroutinesApi::class)
class FirebaseCloudMessagingService() : FirebaseMessagingService() {
    private val viewModel = KoinJavaComponent.get(FcmViewModel::class.java)

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage.data.let {
            if (it.isNotEmpty()) {
                if (it[TITLE] != null && it[BODY] != null)
                    showNotification(it[TITLE].toString(), it[BODY].toString())
            }
        }

        remoteMessage.notification?.let {
            if (it.title != null && it.body != null)
                showNotification(it.title!!, it.body!!)
        }
    }

    override fun onNewToken(token: String) {
        viewModel.getFcmModel()?.let {
            if (it.token != token) {
                it.token = token
                viewModel.updateFcmToken(it)
            }
        } ?: kotlin.run {
            viewModel.saveFcmToken(FcmModel("", token))
        }
    }

    private fun showNotification(title: String, message: String) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this,
            INTENT_REQUEST_CODE,
            intent,
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_mcs_notification)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(NOTIFICATION_MANAGER_ID, notificationBuilder.build())
    }

    companion object {
        private const val INTENT_REQUEST_CODE = 120
        private const val NOTIFICATION_MANAGER_ID = 1999
        private const val CHANNEL_NAME = "mcs_channel_name_notification"
        private const val TAG = "MyFirebaseMsgService"
        private const val TITLE = "title"
        private const val BODY = "body"
    }
}