package com.lavsystems.mycityssports.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {
    fun dateShortFormatString(date: String, formatIn: String, formatOut: String): String {

        val parserToDate = SimpleDateFormat(formatIn, Locale.getDefault())
        //val parserToDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatterString = SimpleDateFormat(formatOut, Locale.getDefault())
        //val formatterString = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        try {
            val date = parserToDate.parse(date)
            //val threeHours = (60 * 60 * 3000)
            date?.let {
                    return formatterString.format(it)
            }
        } catch (e: ParseException) {
            return ""
        }
        return ""
    }

    private fun dateTimeFormat(dateTime: String): String {
        val parserToDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val formatterString = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
        try {
            val dateTimeLong = parserToDate.parse(dateTime)?.time
            val threeHours = (60 * 60 * 3000)
            dateTimeLong?.let {
                val newDateTimeLong = it - threeHours
                val date = Date(newDateTimeLong)
                date.let {
                    return formatterString.format(it)
                }
            }
        } catch (e: ParseException) {
            return ""
        }
        return ""
    }

    fun getCurrentStringDateTime(): String {
         SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).run {
            return format(Date())
        }
    }
}