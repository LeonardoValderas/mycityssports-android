package com.lavsystems.mycityssports.utils.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.mvvm.view.fragment.ErrorFragment

object FragmentManager {

    fun replaceFragment(supportFragmentManager: FragmentManager, fragment: Fragment?) {
        fragment?.let {
            replaceFragment(supportFragmentManager, R.id.fl_container, it)
        }
    }

    fun replaceFragment(supportFragmentManager: FragmentManager, layout: Int, fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(layout, fragment)
            .commit()
    }

    fun replaceFragment(supportFragmentManager: FragmentManager, tag: String) {
        getCurrentFragmentByTag(supportFragmentManager, tag)?.let {
            supportFragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fl_container, it)
                .commit()
        }
    }

    fun addFragment(supportFragmentManager: FragmentManager, layout: Int, fragment: Fragment, tag: String, hide: Boolean) {
        val fragmentTransaction = supportFragmentManager.beginTransaction().add(layout, fragment, tag)

        if(hide){
           fragmentTransaction.hide(fragment)
        }

        fragmentTransaction.commit()
    }

    fun showFragment(supportFragmentManager: FragmentManager, fragmentHide: Fragment, fragmentShow: Fragment) {
        supportFragmentManager.beginTransaction().hide(fragmentHide).show(fragmentShow).commit()
    }

    fun showFragment(supportFragmentManager: FragmentManager, showTag: String) {
        val currentFragment = getCurrentFragment(supportFragmentManager)
        val fragment = getCurrentFragmentByTag(supportFragmentManager, showTag)
        if(currentFragment != null && fragment != null){
            supportFragmentManager.beginTransaction().hide(currentFragment).show(fragment).commit()
        }
    }

    fun getCurrentFragment(supportFragmentManager: FragmentManager): Fragment? {
        return supportFragmentManager
            .findFragmentById(R.id.fl_container)
    }

    fun getCurrentFragmentByTag(supportFragmentManager: FragmentManager, tag: String): Fragment? {
        return supportFragmentManager
            .findFragmentByTag(tag)
    }

    fun currentFragmentIsErrorFragment(supportFragmentManager: FragmentManager): Boolean{
        val fragment = supportFragmentManager
            .findFragmentById(R.id.fl_container)
        fragment?.let {
            return it is ErrorFragment
        }

        return false
    }

}
