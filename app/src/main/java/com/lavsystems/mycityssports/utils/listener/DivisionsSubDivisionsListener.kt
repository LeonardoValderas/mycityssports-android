package com.lavsystems.mycityssports.utils.listener

import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel

interface DivisionsSubDivisionsListener {
    fun getDivisionSubDivisionModel(): MutableList<DivisionSubDivisionModel>
    fun hasSubDivisions(): Boolean
}