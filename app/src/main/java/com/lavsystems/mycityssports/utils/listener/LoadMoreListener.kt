package com.lavsystems.mycityssports.utils.listener

interface LoadMoreListener {
    fun notifyNoMoreItems()
    fun show()
    fun hide()
}