package com.lavsystems.mycityssports.utils.uicomponents

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.lavsystems.mycityssports.R

object AlertDialogComponent {
    fun showAlert(context: Context, title: String, message: String, positiveButtonText: String, function: () -> Unit){
        val alertDialog: AlertDialog? = context.let {
            val builder = AlertDialog.Builder(it, R.style.DialogTheme)
            builder.apply {
                setTitle(title)
                setMessage(message)
                setCancelable(false)
                setPositiveButton(positiveButtonText
                ) { dialog, id ->
                    function()
                    dialog.cancel()
                }
            }
            builder.create()
        }

        alertDialog?.show()
    }
}