package com.lavsystems.mycityssports.utils.preferences

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_ALERT_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_CITY_ID_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_CITY_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_COUNTRY_ID_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_DIVISION__SUB_DIVISION_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_FCM_MODEL_KEY_ARG
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_FCM_MODEL_KEY_BR
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_INSTITUTION_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_MCS
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_SPORT_KEY
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl.KEYS.SHARED_PREFERENCES_TOURNAMENT_KEY

class PreferencesImpl(private val context: Context) :
    Preferences {

    object KEYS {
        const val SHARED_PREFERENCES_MCS = "mcs_shared_preferences"
        const val SHARED_PREFERENCES_CITY_ID_KEY = "city_id_shared_preferences_key"
        const val SHARED_PREFERENCES_COUNTRY_ID_KEY = "country_id_shared_preferences_key"
        const val SHARED_PREFERENCES_CITY_KEY = "city_shared_preferences_key"
        const val SHARED_PREFERENCES_INSTITUTION_KEY = "institution_shared_preferences_key"
        const val SHARED_PREFERENCES_SPORT_KEY = "sport_shared_preferences_key"
        const val SHARED_PREFERENCES_DIVISION__SUB_DIVISION_KEY =
            "division_sub_division_shared_preferences_key"
        const val SHARED_PREFERENCES_TOURNAMENT_KEY = "tournament_shared_preferences_key"
        const val SHARED_PREFERENCES_ALERT_KEY = "tournament_shared_alert_key"
        const val SHARED_PREFERENCES_FCM_MODEL_KEY_ARG = "fcm_model_shared_key_arg"
        const val SHARED_PREFERENCES_FCM_MODEL_KEY_BR = "fcm_model_shared_key_br"
    }

    override fun saveCity(city: CityModel): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_CITY_KEY, Gson().toJson(city))
        }.commit()
    }

    override fun getCity(): CityModel? {
        val preferences = getSharedPreferences()
        val cityJson = preferences.getString(SHARED_PREFERENCES_CITY_KEY, "")
        try {
            cityJson?.let {
                val city = Gson().fromJson(cityJson, CityModel::class.java)
                city?.let {
                    return it
                }
            }
        } catch (e: Exception) {
            sendLog("getCity", e)
            return null
        } catch (e: JsonSyntaxException) {
            sendLog("getCity", e)
            return null
        }

        return null
    }

    override fun removeCity(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_CITY_KEY, "")
        }.commit()
    }

    override fun removeCountryAndCity(): Boolean {
        val cityRemoved = removeCity()
        val countryRemoved = removeCountry()
        return cityRemoved && countryRemoved
    }

    override fun saveCountry(country: CountryModel): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_COUNTRY_ID_KEY, Gson().toJson(country))
        }.commit()
    }

    override fun getCountry(): CountryModel? {
        val preferences = getSharedPreferences()
        val countryJson = preferences.getString(SHARED_PREFERENCES_COUNTRY_ID_KEY, "")
        try {
            countryJson?.let {
                Gson().fromJson(it, CountryModel::class.java)?.let {
                    return it
                }
            }
        } catch (e: Exception) {
            sendLog("getCountry", e)
            return null
        } catch (e: JsonSyntaxException) {
            sendLog("getCountry", e)
            return null
        }

        return null
    }

    override fun removeCountry(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_COUNTRY_ID_KEY, "")
        }.commit()
    }

    override fun saveCityId(cityId: String): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_CITY_ID_KEY, cityId)
        }.commit()
    }

    override fun getCityId(): String? {
        val preferences = getSharedPreferences()
        val cityId = preferences.getString(SHARED_PREFERENCES_CITY_ID_KEY, "")
        return cityId
    }

    override fun removeCityId(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_CITY_ID_KEY, "")
        }.commit()
    }

    override fun saveInstitutionId(institutionId: String): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_INSTITUTION_KEY, institutionId)
        }.commit()
    }

    override fun getInstitutionId(): String? {
        val preferences = getSharedPreferences()
        val institutionId = preferences.getString(SHARED_PREFERENCES_INSTITUTION_KEY, "")
        return institutionId
    }

    override fun removeInstitutionId(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_INSTITUTION_KEY, "")
        }.commit()
    }

    override fun saveSportId(sportId: String): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_SPORT_KEY, sportId)
        }.commit()
    }

    override fun getSportId(): String? {
        val preferences = getSharedPreferences()
        val sportId = preferences.getString(SHARED_PREFERENCES_SPORT_KEY, "")
        return sportId
    }

    override fun removeSportId(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_SPORT_KEY, "")
        }.commit()
    }

    override fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(
                SHARED_PREFERENCES_DIVISION__SUB_DIVISION_KEY,
                Gson().toJson(divisionSubDivision)
            )
        }.commit()
    }

    override fun getDivisionSubDivision(): DivisionSubDivisionModel? {
        val preferences = getSharedPreferences()
        val divisionSubDivisionJson =
            preferences.getString(SHARED_PREFERENCES_DIVISION__SUB_DIVISION_KEY, "")
        try {
            divisionSubDivisionJson?.let {
                val divisionSubDivision = Gson().fromJson(it, DivisionSubDivisionModel::class.java)
                divisionSubDivision?.let {
                    return it
                }
            }
        } catch (e: Exception) {
            sendLog("getDivisionSubDivision", e)
            return null
        } catch (e: JsonSyntaxException) {
            sendLog("getDivisionSubDivision", e)
            return null
        }

        return null
    }

    override fun removeDivisionSubDivision(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_DIVISION__SUB_DIVISION_KEY, "")
        }.commit()
    }

    override fun saveTournamentId(tournamentId: String): Boolean {
        val preferences = getSharedPreferences(SHARED_PREFERENCES_MCS)
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_TOURNAMENT_KEY, tournamentId)
        }.commit()
    }

    override fun getTournamentId(): String? {
        val preferences = getSharedPreferences()
        val tournamentId = preferences.getString(SHARED_PREFERENCES_TOURNAMENT_KEY, "")
        return tournamentId
    }

    override fun removeTournamentId(): Boolean {
        val preferences = getSharedPreferences()
        return preferences.edit().apply {
            putString(SHARED_PREFERENCES_TOURNAMENT_KEY, "")
        }.commit()
    }

    override fun showHomeInformationAlert(): Boolean {
        val preferences = getSharedPreferences()
        val show = preferences.getBoolean(SHARED_PREFERENCES_ALERT_KEY, true)
        return show
    }

    override fun homeInformationAlertDisplayed(): Boolean {
        return getSharedPreferences().edit().apply {
            putBoolean(SHARED_PREFERENCES_ALERT_KEY, false)
        }.commit()
    }

    override fun getFcmModel(): FcmModel? {
        val preferences = getSharedPreferences()
        getCountry()?.let {
            val fcmJson = preferences.getString(
                if (it.code == CountryEnum.ARGENTINA.value) SHARED_PREFERENCES_FCM_MODEL_KEY_ARG else SHARED_PREFERENCES_FCM_MODEL_KEY_BR,
                ""
            )
            return try {
                Gson().fromJson(fcmJson, FcmModel::class.java)
            } catch (e: Exception) {
                sendLog("getFcmModel", e)
                null
            } catch (e: JsonSyntaxException) {
                sendLog("getFcmModel", e)
                null
            }
        }

        return null
    }

    override fun getFcmId(): String? {
        val fcmModel = getFcmModel()
        return fcmModel?.id
    }

    override fun saveFcmModel(fcmModel: FcmModel): Boolean {
        val preferences = getSharedPreferences()
        getCountry()?.let {
            return preferences.edit().apply {
                putString(
                    if (it.code == CountryEnum.ARGENTINA.value) SHARED_PREFERENCES_FCM_MODEL_KEY_ARG else SHARED_PREFERENCES_FCM_MODEL_KEY_BR,
                    Gson().toJson(fcmModel)
                )
            }.commit()
        }

        return false
    }

    private fun getSharedPreferences(key: String = SHARED_PREFERENCES_MCS): SharedPreferences {
        return context.getSharedPreferences(key, MODE_PRIVATE)
    }

    private fun sendLog(method: String, e: Exception) {
        FirestoreBuilder.Builder()
            .className(ConstantsUtils.PREFERENCES)
            .method(method)
            .type(FirestoreBuilder.EXCEPTION)
            .message(e.stackTraceToString())
            .brand()
            .model()
            .send()
    }
}