package com.lavsystems.mycityssports.utils.permissions

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.utils.uicomponents.ViewComponents

object Permissions {
    /**
     * individual permision
     */

    fun needPermissionForAction(activity: Activity, permission: String, codePermission: Int): Boolean {
        if (versionNeedPermission())
            if (hasNotPermission(activity, permission)) {
                requestPermission(activity, arrayOf(permission), codePermission)
                return true
            }

        return false
    }

    private fun versionNeedPermission(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    private fun hasNotPermission(activity: Activity, permission: String) =
        ContextCompat.checkSelfPermission(
            activity,
            permission
        ) != PackageManager.PERMISSION_GRANTED


    private fun requestPermission(activity: Activity, hasNotPermissions: Array<String>, codePermission: Int) {
        ActivityCompat.requestPermissions(activity, hasNotPermissions, codePermission)
    }

    fun shouldShowRequestPermissionRationale(activity: Activity, viewId: Int, permission: String, msg: String) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            ViewComponents.showCustomPermissionDialog(activity, msg)
        } else {
            ViewComponents.showSnackbarWithAction(activity, activity.findViewById(viewId), activity.getString(
                R.string.permission_call_snack_message), activity.getString(R.string.allow_dialog_text))
        }
    }
}