package com.lavsystems.mycityssports.utils.recycler

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.annotation.NonNull
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ItemOffsetDecoration(private val mItemOffset: Int) : RecyclerView.ItemDecoration() {

    constructor(@NonNull context: Context, @DimenRes itemOffsetId: Int) : this(context.resources.getDimensionPixelSize(itemOffsetId)
    ) {

    }

    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView,
        state: RecyclerView.State
    ) {
//        outRect.left = mItemOffset
//        outRect.right = mItemOffset
//        outRect.bottom = mItemOffset
        super.getItemOffsets(outRect, view, parent, state)


//        if (parent.getChildLayoutPosition(view) == 0) {
//            outRect.top = mItemOffset;
//        } else {
//            outRect.top = 0;
//        }

//        val position: Int = parent.getChildLayoutPosition(view)
//
//        if (position < (parent.layoutManager as GridLayoutManager).spanCount) outRect.top = mItemOffset
//
//        if (position % 2 != 0) {
//            outRect.right = mItemOffset
//        }
//
//        outRect.left = mItemOffset
//        outRect.bottom = mItemOffset



       outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset)
    }
}