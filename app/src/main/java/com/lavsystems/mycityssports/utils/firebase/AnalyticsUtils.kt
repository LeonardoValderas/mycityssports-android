package com.lavsystems.mycityssports.utils.firebase

import android.os.Bundle
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase

object AnalyticsUtils {
    const val CLASS_NAME = "class_name"
    const val METHOD_NAME = "method_name"
    const val EXCEPTION = "exception"
    fun setEvent(bundle: Bundle){
        Firebase.analytics.run {
            setDefaultEventParameters(bundle)
        }
    }

// EXAMPLE
//    AnalyticsUtils.setEvent(Bundle().apply {
//        putString(AnalyticsUtils.CLASS_NAME, ConstantsUtils.BASE_VIEW_MODEL)
//        putString(AnalyticsUtils.METHOD_NAME, "HOLAAAA")
//        //putString(AnalyticsUtils.EXCEPTION, e.stackTraceToString())
//    })
}