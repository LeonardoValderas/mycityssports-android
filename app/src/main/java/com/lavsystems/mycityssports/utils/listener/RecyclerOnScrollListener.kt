package com.lavsystems.mycityssports.utils.listener

import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.utils.adapter.DataBindingViewHolder
import com.lavsystems.mycityssports.utils.adapter.Page

interface RecyclerOnScrollListener<T> {
    fun scrollUp(isUp: Boolean)
    fun onLoadMore(page: Page, adapter: RecyclerView.Adapter<DataBindingViewHolder<T>>?)
}