package com.lavsystems.mycityssports.utils.binding

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import com.lavsystems.mycityssports.R

object ImageLoad {


    @BindingAdapter("imageNewsUrl", "imagePlaceholder", "withoutImage")
    @JvmStatic
    fun loadImageNews(view: ImageView, url: String?, placeHolder: Int, withoutImage: Int) {
        val image = if(url.isNullOrEmpty()) withoutImage else url
        val context = view.context
        Glide.with(context)
            .load(image)
            .placeholder(placeHolder)
            // .override(50, 50)
            //.dontAnimate()
            .error(R.drawable.ic_image_error)
            .into(view)
    }



    @BindingAdapter("imageUrl", "imageResource")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?, placeHolder: Int) {
        url?.let {image ->
            if (image.isNotEmpty()) {
                val context = view.context
                Glide.with(context)
                    .load(image)
                    .placeholder(placeHolder)
                    //.override(50, 50)
                    //.dontAnimate()
                    .error(R.drawable.ic_image_error)
                    .into(view)
            } else {
                val context = view.context
                Glide.with(context)
                    .load(placeHolder)
                    //.placeholder(placeHolder)
                    //.override(100, 100)
                    //.dontAnimate()
                    .error(R.drawable.ic_image_error)
                    .into(view)
            }
        }
    }


    @BindingAdapter("imageUrl", "imagePlaceholder", "withoutImage")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?, placeHolder: Int, withoutImage: Int) {
        val image = if(url.isNullOrEmpty()) withoutImage else url
        val context = view.context
        Glide.with(context)
            .load(image)
            .placeholder(placeHolder)
            //.override(50, 50)
            //.dontAnimate()
            .error(R.drawable.ic_image_error)
            .into(view)
//        Glide
//                    .with(context)
//                    .load(image)
//                    .apply(RequestOptions.noTransformation()
//                        .placeholder(placeHolder)
//                        //.override(40, 40)
//                        //.dontAnimate()
//                        .error(R.drawable.ic_image_error)
//                        .priority(Priority.HIGH))
//                    .into(view)
    }



    @BindingAdapter("iconResource", "imageResource")
    @JvmStatic
    fun loadImageFromResource(view: ImageView, resource: String, placeHolder: Int) {
        val context = view.context
        val id = context.resources.getIdentifier(resource, "drawable", context.packageName)
        Glide.with(context)
            .load(id)
            .placeholder(placeHolder)
            .override(50, 50)
            .dontAnimate()
            .error(R.drawable.ic_image_error)
            .into(view)
    }

    @BindingAdapter("iconResource")
    @JvmStatic
    fun loadImageFromDrawable(view: TextView, resource: String) {
        val context = view.context
        val id = context.resources.getIdentifier(resource, "drawable", context.packageName)
        view.setCompoundDrawables(null, null, context.getDrawable(id), null)
//        Glide.with(context)
//            .load(id)
//            .placeholder(placeHolder)
//            .override(50, 50)
//            .dontAnimate()
//            .error(R.drawable.ic_image_error)
//            .into(view)
    }
}