package com.lavsystems.mycityssports.utils

object ConstantsUtils {
    const val FRAGMENT_INSTITUTION_ID = "fragment_institution_id"

    //FRAGMENT BUNGLE
    const val FRAGMENT_POSITION_BUNDLE = "bundle_position_fragment"
    const val FRAGMENT_EXCEPTION_HANDLE_BUNDLE = "bundle_handle_exception_fragment"
    const val FIXTURE_ID_BUDLE = "fixture_id"
    const val SPONSOR_ID_BUDLE = "sponsor_id"
    const val INSTITUTION_ID_BUDLE = "institution_id"


    //RESULT ACTIVITY
    const val PERMISSION_CALL_PHONE = 100
    const val PERMISSION_CALL_HOME_PHONE = 101
    const val PERMISSION_CALL_LOCATION_FINE = 102
    const val REQUEST_APP_DETAILS = 500


    //CLASSES
    const val BASE_REPOSITORY = "Base_Repository"
    const val ERROR_FRAGMENT_REPOSITORY = "Error_Fragment_Repository"
    const val BASE_VIEW_MODEL = "Base_View_Model"
    const val INFORMATION_CONTACT_INTENT = "Information Contact Intent"
    const val PREFERENCES = "Preferences"

}