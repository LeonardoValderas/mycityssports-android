package com.lavsystems.mycityssports.utils.uicomponents

import android.util.Log
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.utils.adapter.DataBindingAdapter
import com.lavsystems.mycityssports.utils.adapter.ExpandableRecyclerViewAdapter
import com.lavsystems.mycityssports.utils.adapter.Page
import com.lavsystems.mycityssports.utils.adapter.ProgressbarItem
import com.lavsystems.mycityssports.utils.listener.LoadMoreListener
import com.lavsystems.mycityssports.utils.listener.RecyclerOnScrollListener

class RecyclerViewBuilder<T> private constructor(private val recycler: RecyclerView) :
    LoadMoreListener {

    private var page: Page? = null
    private var scrollListener: RecyclerOnScrollListener<T>? = null
    private var context = recycler.context
    private var adapter: DataBindingAdapter<T>? = null
    private var progressAdded = false

    companion object {
        fun <T> builder(recycler: RecyclerView): RecyclerViewBuilder<T> {
            return RecyclerViewBuilder<T>(recycler)
        }
    }

    fun setLinearLayoutManagerOrientation(isHorizontal: Boolean): RecyclerViewBuilder<T> {
        val horizontal = when (isHorizontal) {
            true -> LinearLayoutManager.HORIZONTAL
            else -> LinearLayoutManager.VERTICAL
        }
        recycler.layoutManager = LinearLayoutManager(context, horizontal, false)
        return this
    }

    fun setDividerItemDecoration(isHorizontal: Boolean): RecyclerViewBuilder<T> {
        val horizontal = when (isHorizontal) {
            true -> DividerItemDecoration.HORIZONTAL
            else -> DividerItemDecoration.VERTICAL
        }
        val itemDecoration = DividerItemDecoration(context, horizontal)

        val divider = if(isHorizontal) AppCompatResources.getDrawable(
            context,
            R.drawable.horizontal_divider
        ) else AppCompatResources.getDrawable(context, R.drawable.vertical_divider)
        divider?.let {
            itemDecoration.setDrawable(it)
        }
        recycler.addItemDecoration(itemDecoration)
        return this
    }

    fun setEndlessRecyclerOnScrollListener(page: Page, visibleThreshold: Int, scrollListener: RecyclerOnScrollListener<T>): RecyclerViewBuilder<T> {
        this.scrollListener = scrollListener
        this.page = page

        recycler.addOnScrollListener(object : EndlessRecyclerOnScrollListener(visibleThreshold) {
            override fun setLoadingAdded(): Boolean {
                return progressAdded
            }

            override fun scrollUp(isUp: Boolean) {
                this@RecyclerViewBuilder.scrollListener?.scrollUp(isUp)
            }

            override fun onLoadMore() {
                if (hasMore()) {
                    Log.i("LOADING", "Loadmore")
                    getPageIncremented()?.let {
                        this@RecyclerViewBuilder.scrollListener?.onLoadMore(it, this@RecyclerViewBuilder.adapter)
                    }
                }
            }
        })

        return this
    }

    private fun hasMore(): Boolean {
        page?.let {
            return it.hasMore
        }

        return false
    }

    fun setPage(page: Page): RecyclerViewBuilder<T> {
        this.page = page
        return this
    }


    fun setHasFixedSize(fixed: Boolean): RecyclerViewBuilder<T> {
        recycler?.setHasFixedSize(fixed)

        return this
    }

    fun setHasStableIds(stable: Boolean): RecyclerViewBuilder<T> {
        adapter?.setHasStableIds(stable)

        return this
    }

    fun setAdapter(adapter: DataBindingAdapter<T>): RecyclerViewBuilder<T> {
        this.adapter = adapter
        this.adapter?.setHasntMoreListenter(this)
        recycler?.adapter = this.adapter
        return this
    }

    fun hasNotMoreItems() {
        page?.hasMore = false
    }

    fun addItems(items: MutableList<T>, clear: Boolean) {
        adapter?.addItems(items, clear)
    }

//    fun addItems(items: MutableList<T>) {
//        adapter?.addItems(items, fa)
//    }

    override fun notifyNoMoreItems() {
        hasNotMoreItems()
    }

    fun getPage(): Page? {
        return page
    }

    fun getPageIncremented(): Page? {
        return page?.apply {
            page++
        }
    }

    fun showLoading() {
        progressAdded = true
        recycler.post(Runnable {
            adapter?.let {
                if (it.itemCount > 0)
                    it.addByIndex(it.itemCount, ProgressbarItem())
            }
        })
    }

    fun hideLoading() {
        // recycler.post(Runnable {
        adapter?.let {
            if (it.itemCount > 0)
            //it.removeLoadingIfExists((it.itemCount.minus(1)))
                it.removeLoadingIfExists()
        }
        //})
        progressAdded = false
    }

    override fun show() {
        progressAdded = true
    }

    override fun hide() {
        progressAdded = false
    }

    fun build(): RecyclerViewBuilder<T> {
        return this
    }
}