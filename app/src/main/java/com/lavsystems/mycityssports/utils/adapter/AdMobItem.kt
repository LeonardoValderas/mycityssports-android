package com.lavsystems.mycityssports.utils.adapter

data class AdMobItem(
    val id: Int = 1222123
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AdMobItem

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}
