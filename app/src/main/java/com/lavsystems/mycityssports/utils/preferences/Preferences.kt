package com.lavsystems.mycityssports.utils.preferences

import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.CountryModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.FcmModel

interface Preferences {
    fun saveCity(city: CityModel): Boolean
    fun getCity(): CityModel?
    fun removeCity(): Boolean
    fun removeCountryAndCity(): Boolean
    fun saveCountry(country: CountryModel): Boolean
    fun getCountry(): CountryModel?
    fun removeCountry(): Boolean
    fun saveCityId(cityId: String): Boolean
    fun getCityId(): String?
    fun removeCityId(): Boolean
    fun saveInstitutionId(institutionId: String): Boolean
    fun getInstitutionId(): String?
    fun removeInstitutionId(): Boolean
    fun saveSportId(sportId: String): Boolean
    fun getSportId(): String?
    fun removeSportId(): Boolean
    fun saveDivisionSubDivision(divisionSubDivision: DivisionSubDivisionModel): Boolean
    fun getDivisionSubDivision(): DivisionSubDivisionModel?
    fun removeDivisionSubDivision(): Boolean
    fun saveTournamentId(tournamentId: String): Boolean
    fun getTournamentId(): String?
    fun removeTournamentId(): Boolean
    fun showHomeInformationAlert(): Boolean
    fun homeInformationAlertDisplayed(): Boolean
    fun getFcmModel(): FcmModel?
    fun getFcmId(): String?
    fun saveFcmModel(fcmModel: FcmModel): Boolean
}