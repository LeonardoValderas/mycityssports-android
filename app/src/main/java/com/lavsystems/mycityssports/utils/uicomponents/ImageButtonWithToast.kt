package com.lavsystems.mycityssports.utils.uicomponents

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageButton


class ImageButtonWithToast @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0):
    AppCompatImageButton(context, attrs, defStyleAttr), View.OnClickListener {

    private var listener: OnClickListener? = null

    override fun setOnClickListener(listener: OnClickListener?) {
        if(listener == this) {
            super.setOnClickListener(listener)
            return
        }

        this.listener = listener
    }

    override fun onClick(view: View?){
        listener?.run {
            //if(!onClick(view)){
                handleClick()
           // }
        } //?: run {
         //   handleClick()
       // }
    }

    private fun handleClick(){
        val description = contentDescription.toString()
        if(description.isNotEmpty()){
            val pos = IntArray(2)
            getLocationInWindow(pos)
            Toast.makeText(context, description, Toast.LENGTH_LONG).apply {
                setGravity(Gravity.TOP or Gravity.START, pos[0] - ((description.length / 2) * 12), pos[1] - 128)
            }.show()
        }
    }
}