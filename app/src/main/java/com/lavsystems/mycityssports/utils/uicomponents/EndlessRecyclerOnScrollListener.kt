package com.lavsystems.mycityssports.utils.uicomponents

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListener(val visibleThreshold: Int) : RecyclerView.OnScrollListener() {

    /**
     * The total number of items in the dataset after the last load
     */
    private var mPreviousTotal = 0
    /**
     * True if we are still waiting for the last set of data to load.
     */
    private var mLoading = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if(dy > 0 || dx > 0) {

            val visibleItemCount = recyclerView.childCount
            val totalItemCount = recyclerView.layoutManager!!.itemCount
            val firstVisibleItem = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            //val firstVisibleItem = (recyclerView.layoutManager as GridLayoutManager).findFirstVisibleItemPosition()

            if (mLoading && !setLoadingAdded()) {
                if (totalItemCount > mPreviousTotal) {
                    mLoading = false
                    mPreviousTotal = totalItemCount
                }
            }
            synchronized(this) {
                if (!mLoading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    onLoadMore()
                    mLoading = true
                }
            }
        }
        scrollState(dy)
    }

    private fun scrollState(dy: Int){
        if (dy >0) {
            // Scroll Down
            scrollUp(false)
        }
        else if (dy <0) {
            // Scroll Up
            scrollUp(true)
        }
    }

    fun setLoadind(){

    }

    abstract fun onLoadMore()
    abstract fun scrollUp(isUp: Boolean)
    abstract fun setLoadingAdded(): Boolean
}