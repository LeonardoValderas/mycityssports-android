package com.lavsystems.mycityssports.utils.uicomponents

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.utils.ConstantsUtils.REQUEST_APP_DETAILS

object ViewComponents {
    fun showCustomPermissionDialog(activity: Activity, msg: String) {
        val builder = AlertDialog.Builder(activity, R.style.DialogTheme)
        builder.setMessage(msg)
        builder.setCancelable(false)
        builder.setPositiveButton(activity.getString(R.string.allow_dialog_text)){ _, _ ->
            goToApplicationSettings(activity)
        }

        builder.setNegativeButton(activity.getString(R.string.cancel_dialog_text)){_, _ -> }

        val alert: AlertDialog = builder.create()
        alert.show()
    }

    private fun goToApplicationSettings(activity: Activity){
        val i = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        i.data = Uri.parse("package:"+ activity.packageName)
        activity.startActivityForResult(i, REQUEST_APP_DETAILS)
    }

    fun showToast(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun showSnackbar(view: View, message: String) {
//        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
//        val view = snackbar.view
//        val tv = view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
//        tv.setTextColor(ContextCompat.getColor(view.context, android.R.color.white))
        getSnackbar(view, message).show()
    }

    fun showSnackbarWithAction(activity: Activity, view: View, msg: String, buttonTitle: String) =
        getSnackbar(view, msg).setAction(buttonTitle) {
            //Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).setAction(buttonTitle) {
            goToApplicationSettings(activity)
        }.show()

    private fun getSnackbar(v: View, message: String): Snackbar {
        return Snackbar.make(v, message, Snackbar.LENGTH_LONG).apply {
            with(view) {
                findViewById<TextView>(com.google.android.material.R.id.snackbar_text).apply {
                    setTextColor(ContextCompat.getColor(this.context, android.R.color.white))
                }
            }
        }
    }
}