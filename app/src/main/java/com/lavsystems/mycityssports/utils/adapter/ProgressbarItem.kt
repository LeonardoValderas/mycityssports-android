package com.lavsystems.mycityssports.utils.adapter

data class ProgressbarItem(val id: Int = 0)