package com.lavsystems.mycityssports.utils.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.BR

class DataBindingViewHolder<T>(private val binding: ViewDataBinding,
                               private val listener: OnAdapterListener<T>?) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: T, animationComposer: YoYo.AnimationComposer?, animationComposerListener: YoYo.AnimationComposer?) {
        binding.run {
            setVariable(BR.model, item)
            listener?.let {
                animationComposer?.playOn(root)
                root.setOnClickListener {
                    animationComposerListener?.playOn(root)
                    listener.onItemClick(it, layoutPosition, item)
                }
            }
            executePendingBindings()
        }
    }
}