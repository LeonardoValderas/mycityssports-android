package com.lavsystems.mycityssports.utils.firebase

import android.os.Build
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lavsystems.mycityssports.utils.DateTimeUtils

class FirestoreBuilder private constructor() {
    companion object {
        const val LOGS = "logs"
        const val FIRESTONE_TAG = "FIRESTONE_TAG"
        const val EXCEPTION = "exception"
        const val ERROR = "error"
        const val INFO = "info"

    }
    data class Builder(
        private var logHashMap: HashMap<String, Any> = hashMapOf(
            Pair(
                KeyLog.DATE.value,
                DateTimeUtils.getCurrentStringDateTime()
            )
        )
    ) {
        fun className(className: String): Builder {
            logHashMap.put(KeyLog.CLASS_NAME.value, className)
            return this
        }

        fun method(method: String): Builder {
            logHashMap.put(KeyLog.METHOD_NAME.value, method)
            return this
        }

        fun type(type: String): Builder {
            logHashMap.put(KeyLog.TYPE_LOG.value, type)
            return this
        }

        fun message(message: String): Builder {
            logHashMap.put(KeyLog.MESSAGE.value, message)
            return this
        }

        fun brand(): Builder {
            logHashMap.put(KeyLog.MOBILE_BRAND.value, Build.BRAND)
            return this
        }

        fun model(): Builder {
            logHashMap.put(KeyLog.MOBILE_MODEL.value, Build.MODEL)
            return this
        }

        fun send() {
            Firebase.firestore.run {
                collection(LOGS)
                    .add(logHashMap)
                    .addOnSuccessListener {
                        Log.d(FIRESTONE_TAG, "Logs added with success")
                    }
                    .addOnFailureListener { e ->
                        Log.w(FIRESTONE_TAG, "Error adding log", e)
                    }
            }
        }
    }

    enum class KeyLog(val value: String) {
        DATE("date"),
        CLASS_NAME("class_name"),
        METHOD_NAME("method_name"),
        TYPE_LOG("type_log"),
        MESSAGE("message"),
        MOBILE_BRAND("mobile_brand"),
        MOBILE_MODEL("mobile_model")
    }
}