package com.lavsystems.mycityssports.utils.binding

import androidx.databinding.BaseObservable
import java.io.Serializable

data class DataProgressbar(
    val show: Boolean,
    val info: String
): BaseObservable(), Serializable {
    constructor():this(false, "")
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataProgressbar

        if (show != other.show) return false
        if (info != other.info) return false

        return true
    }

    override fun hashCode(): Int {
        var result = show.hashCode()
        result = 31 * result + info.hashCode()
        return result
    }
}