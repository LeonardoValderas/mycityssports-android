package com.lavsystems.mycityssports.utils.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.YoYo
import com.lavsystems.mycityssports.mvvm.view.listener.OnAdapterListener
import com.lavsystems.mycityssports.utils.listener.LoadMoreListener
import java.lang.IndexOutOfBoundsException
import java.util.*

abstract class DataBindingAdapter<T>(private val listener: OnAdapterListener<T>?) :
    RecyclerView.Adapter<DataBindingViewHolder<T>>() {

    abstract fun getItemModel(int: Int): T
    abstract fun getItems(): MutableList<T>
    abstract fun addItems(items: MutableList<T>, clear: Boolean)

    abstract fun getAnimateRoot(): YoYo.AnimationComposer?
    abstract fun getAnimateListener(): YoYo.AnimationComposer?
    val id = UUID.randomUUID().toString()
    private var moreListener: LoadMoreListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return DataBindingViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: DataBindingViewHolder<T>, position: Int) {
        holder.bind(getItemModel(position), getAnimateRoot(), getAnimateListener())
    }

    fun setHasntMoreListenter(listener: LoadMoreListener) {
        moreListener = listener
    }

    fun notifyHasntMoreItems() {
        moreListener?.notifyNoMoreItems()
    }

    fun showLoading(){
        moreListener?.show()
    }

    fun hideLoading(){
        moreListener?.hide()
    }

    fun removeByIndex(index: Int) {
        try {
            getItems().removeAt(index)
            notifyItemRemoved(index)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    fun addByIndex(index: Int, t: Any) {
        try {
            getItems().add(index, t as T)
            notifyItemInserted(index)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    fun addFooter(t: Any) {
        try {
            getItems().add(itemCount, t as T)
            notifyItemInserted(itemCount)
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    fun removeLoadingIfExists() {
        try {
            val deleted = getItems().removeAll { t -> t is ProgressbarItem }
            if (deleted)
                notifyDataSetChanged()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }
}