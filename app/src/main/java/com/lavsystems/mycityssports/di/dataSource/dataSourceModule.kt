package com.lavsystems.mycityssports.di.dataSource

import com.lavsystems.mycityssports.data.dataSource.*
import org.koin.dsl.module

val dataSourceModule = module {
    single<SplashDataSource> {
        SplashDataSourceImpl(preferences = get())
    }
    single<PlacesDataSource> {
        //PlacesDataSourceImpl(service = get(), preferences = get())
        PlacesDataSourceImpl(preferences = get())
    }
    single<HomeDataSource> {
        //HomeDataSourceImpl(service = get(), preferences = get())
        HomeDataSourceImpl(preferences = get())
    }
    single<NewsDataSource> {
        NewsDataSourceImpl(service = get(), preferences = get())
    }
    single<FixtureDataSource> {
//        FixtureDataSourceImpl(service = get(), preferences = get())
        FixtureDataSourceImpl(preferences = get())
    }
    single<TableDataSource> {
        TableDataSourceImpl(service = get(), preferences = get())
    }
    single<FixtureDetailsDataSource> {
        FixtureDetailsDataSourceImpl(service = get(), preferences = get())
    }
    single<SponsorDataSource> {
        SponsorDataSourceImpl(service = get(), preferences = get())
    }
    single<SanctionDataSource> {
        SanctionDataSourceImpl(service = get(), preferences = get())
    }
    single<RankingDataSource> {
        RankingDataSourceImpl(service = get(), preferences = get())
    }
    single<InstitutionDataSource> {
        InstitutionDataSourceImpl(service = get(), preferences = get())
    }
    single<MyCitysSportsContactDataSource> {
        MyCitysSportsContactDataSourceImpl(service = get())
    }
    single<InstitutionDetailsDataSource> {
        InstitutionDetailsDataSourceImpl(service = get())
    }
    single<FcmDataSource> {
        FcmDataSourceImpl(preferences = get())
    }

    single<NotificationDataSource> {
        NotificationDataSourceImpl(service = get(), preferences = get())
    }
}