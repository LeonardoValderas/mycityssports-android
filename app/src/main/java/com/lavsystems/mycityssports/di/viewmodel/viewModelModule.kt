package com.lavsystems.mycityssports.di.viewmodel

import com.lavsystems.mycityssports.mvvm.viewmodel.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

@ExperimentalCoroutinesApi
val viewModelModule = module {
    viewModel {
        SplashViewModel(repository = get())
    }
    viewModel {
        PlacesViewModel(repository = get())
    }
    viewModel {
        HomeViewModel(repository = get(), fcmRepository = get())
    }
    viewModel {
        NewsViewModel(repository = get())
    }
    viewModel {
        FixtureViewModel(repository = get())
    }
    viewModel {
        FixtureDetailsViewModel(repository = get())
    }
    viewModel {
        SponsorViewModel(repository = get())
    }
    viewModel {
        TableViewModel(repository = get())
    }
    viewModel {
        SanctionViewModel(repository = get())
    }
    viewModel {
        RankingViewModel(repository = get())
    }
    viewModel {
        InstitutionViewModel(repository = get())
    }
    viewModel {
        InstitutionDetailsViewModel(repository = get())
    }
    viewModel {
        MyCitysSportsContactViewModel(repository = get())
    }
    viewModel {
        ErrorFragmentViewModel(repository = get())
    }
    viewModel {
        FcmViewModel(repository = get())
    }
    viewModel {
        NotificationViewModel(repository = get())
    }
}