package com.lavsystems.mycityssports.di.utils

import com.lavsystems.mycityssports.utils.preferences.Preferences
import com.lavsystems.mycityssports.utils.preferences.PreferencesImpl
import org.koin.dsl.module

val utilsModule = module {
    single<Preferences> {
        PreferencesImpl(
            context = get()
        )
    }
}