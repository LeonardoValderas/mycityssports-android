package com.lavsystems.mycityssports.di.repository

import com.lavsystems.mycityssports.data.repository.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module

@ExperimentalCoroutinesApi
val repositoryModule = module {
    single<SplashRepository> { SplashRepositoryImpl(dataSource = get()) }
    single<PlacesRepository> { PlacesRepositoryImpl(dataSource = get()) }
    single<PreferencesRepository> { PreferencesRepositoryImpl(preferences = get()) }
    single<HomeRepository> { HomeRepositoryImpl(dataSource = get()) }
    single<NewsRepository> { NewsRepositoryImpl(dataSource = get()) }
    single<FixtureRepository> { FixtureRepositoryImpl(dataSource = get()) }
    single<FixtureDetailsRepository> { FixtureDetailsRepositoryImpl(dataSource = get()) }
    single<SponsorRepository> { SponsorRepositoryImpl(dataSource = get()) }
    single<TableRepository> { TableRepositoryImpl(dataSource = get()) }
    single<SanctionRepository> { SanctionRepositoryImpl(dataSource = get()) }
    single<RankingRepository> { RankingRepositoryImpl(dataSource = get()) }
    single<InstitutionRepository> { InstitutionRepositoryImpl(dataSource = get()) }
    single<InstitutionDetailsRepository> { InstitutionDetailsRepositoryImpl(dataSource = get()) }
    single<MyCitysSportsContactRepository> { MyCitysSportsContactRepositoryImpl(dataSource = get()) }
    single<ErrorFragmentRepository> { ErrorFragmentRepositoryImpl(service = get(), preferences = get()) }
    single<FcmRepository> { FcmRepositoryImpl(dataSource = get()) }
    single<NotificationRepository> { NotificationRepositoryImpl(dataSource = get()) }
}