package com.lavsystems.mycityssports.di.network

import com.lavsystems.mycityssports.data.network.api.service.*
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrl
import com.lavsystems.mycityssports.data.network.api.utils.BaseUrlImpl
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptor
import com.lavsystems.mycityssports.data.network.api.utils.ConnectivityInterceptorImpl
import org.koin.dsl.module

val networkModule = module {
    single<ConnectivityInterceptor> { ConnectivityInterceptorImpl(context = get()) }
    factory<BaseUrl> { BaseUrlImpl(preferences = get()) }
//    factory { params -> PlaceService(connectivityInterceptor = get(), baseUrl = params.get()) }
    factory {
        PlaceService(connectivityInterceptor = get(), baseUrl = get())
    }
    factory {
        HomeService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        NewsService(connectivityInterceptor = get(), baseUrl = get())
    }
    factory {
        FixtureService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        TableService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        SponsorService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        SanctionService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        RankingService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        InstitutionService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        MyCitysSportsContactService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        ErrorFragmentService(connectivityInterceptor = get(), baseUrl = get())
    }
    factory {
        FcmService(connectivityInterceptor = get(), baseUrl = get())
    }
    single {
        NotificationService(connectivityInterceptor = get(), baseUrl = get())
    }
}