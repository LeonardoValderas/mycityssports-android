package com.lavsystems.mycityssports.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.exception.TypeErrorEnum
import com.lavsystems.mycityssports.mvvm.view.listener.CommunicatorFragmentListener
import com.lavsystems.mycityssports.utils.ConstantsUtils.FRAGMENT_INSTITUTION_ID

abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    private var viewDataBinding: T? = null
    private var viewModel: V? = null
    private var communicator: CommunicatorFragmentListener? = null
    private var bundle: Bundle? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CommunicatorFragmentListener)
            communicator = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewDataBinding = DataBindingUtil.inflate(inflater, layoutRes(), container, false)
        viewDataBinding!!.setVariable(getBindingVariable(), getViewModel())
        viewDataBinding!!.executePendingBindings()
        return getViewDataBinding().root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBundle(arguments)
        init()
        initObservers()
        initViewModel()
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onViewCreated(savedInstanceState)
//        setBundle(arguments)
//        init()
//        initObservers()
//        initViewModel()
//    }

    @LayoutRes
    protected abstract fun layoutRes(): Int

    protected abstract fun getBindingVariable(): Int

    protected abstract fun initViewModel(): V

    protected abstract fun initObservers()

    protected abstract fun init()
    abstract fun getFromHome()

    fun getViewDataBinding(): T {
        return viewDataBinding!!
    }

    fun getCommunicator(): CommunicatorFragmentListener? {
        return communicator
    }

    fun getViewModel() = this.viewModel.let {
        when (it) {
            null -> initViewModel()
            else -> it
        }
    }

    private fun setBundle(bundle: Bundle?){
        this.bundle = bundle
    }

    fun getBundle(): Bundle?{
        return bundle
    }

    fun getIdInstitution(): String {
        val institutionId = getBundle()?.getString(FRAGMENT_INSTITUTION_ID)
        return when(institutionId){
            null -> ""
            else -> institutionId
        }
    }
}