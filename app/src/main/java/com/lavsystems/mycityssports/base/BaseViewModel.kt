package com.lavsystems.mycityssports.base

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Debug
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lavsystems.mycityssports.BuildConfig
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.utils.ConstantsUtils
import com.lavsystems.mycityssports.utils.EventWrapper
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder

abstract class BaseViewModel() : ViewModel() {

    private val _showLoading = MutableLiveData<Boolean>()
    val showLoading: LiveData<Boolean>
        get() = _showLoading

    protected fun showLoading(resource: Int) {
        _resourceInfoLoading.value = resource
        _showLoading.value = true
    }

    protected fun hideLoading() {
        _showLoading.value = false
        _showLoadingMore.value = false
    }

    private val _resourceInfoLoading = MutableLiveData<Int>()
    val resourceInfoLoading: LiveData<Int>
        get() = _resourceInfoLoading

    private val _showLoadingMore = MutableLiveData<Boolean>()
    val showLoadingMore: LiveData<Boolean>
        get() = _showLoadingMore

    protected fun showLoadingMore() {
        _showLoadingMore.value = true
    }

    private val _exceptionHandle = MutableLiveData<ExceptionHandleImpl>()
    val exceptionHandle: LiveData<ExceptionHandleImpl>
        get() = _exceptionHandle

    protected fun setHttpException(exceptionHandle: ExceptionHandleImpl) {
        _exceptionHandle.value = exceptionHandle
    }

    private val _preferencesEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val preferencesEmpty: LiveData<EventWrapper<Boolean>>
        get() = _preferencesEmpty

    private var bundle: Bundle? = null
    fun setBundle(bundle: Bundle?) {
        this.bundle = bundle
    }

    protected fun getBundle(): Bundle? {
        return bundle
    }

    protected fun preferencesEmpty() {
        _preferencesEmpty.value = EventWrapper(true)
    }

    private val _modelsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    val modelsIsEmpty: LiveData<EventWrapper<Boolean>>
        get() = _modelsIsEmpty

    protected fun setModelsIsEmpty(isEmpty: Boolean) {
        _modelsIsEmpty.value = EventWrapper(isEmpty)
    }


    protected fun hideLoadingMore() {
        _showLoadingMore.value = false
    }

    protected fun exceptionToLog(e: Throwable, className: String = "") {
        if (!BuildConfig.DEBUG)
            FirestoreBuilder.Builder()
                .className("${ConstantsUtils.BASE_VIEW_MODEL} - $className")
                .method("exceptionToLog")
                .type(FirestoreBuilder.EXCEPTION)
                .message(e.stackTraceToString())
                .brand()
                .model()
                .send()

    }

    protected fun simpleErrorLog(error: String, className: String = "") {
        if (!BuildConfig.DEBUG)
            FirestoreBuilder.Builder()
                .className("${ConstantsUtils.BASE_VIEW_MODEL} - $className")
                .method("simpleErrorLog")
                .type(FirestoreBuilder.ERROR)
                .message(error)
                .brand()
                .model()
                .send()
    }

    protected fun infoLog(info: String, className: String = "") {
        if (!BuildConfig.DEBUG)
            FirestoreBuilder.Builder()
                .className("${ConstantsUtils.BASE_VIEW_MODEL} - $className")
                .method("infoLog")
                .type(FirestoreBuilder.INFO)
                .message(info)
                .brand()
                .model()
                .send()
    }

    private fun getInstitutionId(): String {
        return getBundle()?.getString(ConstantsUtils.FRAGMENT_INSTITUTION_ID) ?: ""
    }
}