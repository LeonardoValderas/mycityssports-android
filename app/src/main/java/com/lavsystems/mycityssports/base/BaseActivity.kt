package com.lavsystems.mycityssports.base

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.utils.binding.DataProgressbar
import com.lavsystems.mycityssports.utils.uicomponents.ViewComponents

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {
    private var viewDataBinding: T? = null
    private var viewModel: V? = null

    @LayoutRes
    protected abstract fun layoutRes(): Int

    protected abstract fun getBindingVariable(): Int
    protected abstract fun initViewModel(): V
    protected abstract fun initObservers()
    protected abstract fun init()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        init()
        initViewModel()
        initObservers()
    }

    fun getRootDataBinding(): T {
        return viewDataBinding!!
    }

    fun getViewModel() = this.viewModel.let {
        when (it) {
            null -> initViewModel()
            else -> it
        }
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutRes())
        viewDataBinding!!.setVariable(getBindingVariable(), getViewModel())
        viewDataBinding!!.executePendingBindings()
    }

    protected fun showMainLayout(idLayout: Int) {
        val view = findViewById<View>(idLayout)
        view?.visibility = View.VISIBLE
    }

    protected fun hideMainLayout(idLayout: Int) {
        val view = findViewById<View>(idLayout)
        view?.visibility = View.GONE
    }

    fun showSnackbar(view: View, message: String) {
       ViewComponents.showSnackbar(view, message)
    }

    fun showToast (message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}