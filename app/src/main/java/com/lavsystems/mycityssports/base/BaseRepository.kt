package com.lavsystems.mycityssports.base

import com.lavsystems.mycityssports.BuildConfig
import com.lavsystems.mycityssports.data.network.api.utils.CacheImpl
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.utils.ConstantsUtils.BASE_REPOSITORY
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder
import com.lavsystems.mycityssports.utils.firebase.FirestoreBuilder.Companion.EXCEPTION
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking

abstract class BaseRepository(private val preferencesRepository: PreferencesRepository) {
    companion object {
        val NEWS = "news"
        val FIXTURE = "fixture"
        val TABLE = "table"
        val SANCTION = "sanction"
        val INSTITUTION = "institution"
        val RANKING = "ranking"
    }

    private var cache = CacheImpl()

    protected fun setCache(any: Any) = cache.set(getKeyToCache(), any)
    protected fun setCache(key: String, any: Any) = cache.set("$key${getKeyToCache()}", any)
    protected fun getCache(key: String) = cache.get("$key${getKeyToCache()}")
    protected fun getCache() = cache.get(getKeyToCache())
    protected fun removeCache(key: String) = cache.remove("$key${getKeyToCache()}")
    protected fun removeCache() = cache.remove(getKeyToCache())

    private fun getKeyToCache(): String {
        val preferenceData = try {
            getPreferencesData()
        } catch (e: Exception){
            if(!BuildConfig.DEBUG)
                FirestoreBuilder.Builder()
                    .className(BASE_REPOSITORY)
                    .method("getKeyToCache")
                    .type(EXCEPTION)
                    .message(e.stackTraceToString())
                    .brand()
                    .model()
                    .send()
            return ""
        }
        return preferenceData.toString()
    }

    fun preferencesIsEmpty(): Boolean {
        return runBlocking {
            try {
                val preference = getPreferencesData()
                preference.isAnyValueEmpty
            } catch (e: Exception) {
                FirestoreBuilder.Builder()
                    .className(BASE_REPOSITORY)
                    .method("preferencesIsEmpty")
                    .type(EXCEPTION)
                    .message(e.stackTraceToString())
                    .brand()
                    .model()
                    .send()
                true
            }
        }
    }

    fun isInstitutionOrCityEmpty(): Boolean {
        return runBlocking {
            try {
                val preference = getPreferencesData()
                preference.isInstitutionOrCityEmpty
            } catch (e: Exception) {
                FirestoreBuilder.Builder()
                    .className(BASE_REPOSITORY)
                    .method("isInstitutionOrCityEmpty")
                    .type(EXCEPTION)
                    .message(e.stackTraceToString())
                    .brand()
                    .model()
                    .send()
                true
            }
        }
    }

    fun getPreferencesData() = runBlocking {
        preferencesRepository.getAllPreferenceIds().single()
    }

    fun getPreferencesDataOrThrowException() = runBlocking {
        preferencesRepository.getAllPreferenceIdsOrThrowException().single()
    }

    fun getCurrentCountry() = runBlocking {
        preferencesRepository.getCountry().single()
    }
}