package com.lavsystems.mycityssports

import com.lavsystems.mycityssports.data.dataSource.*
import com.lavsystems.mycityssports.data.dataSource.SplashDataSourceImplTest
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImplTest
import com.lavsystems.mycityssports.data.repository.*
import com.lavsystems.mycityssports.data.repository.SplashRepositoryImplTest
import com.lavsystems.mycityssports.mvvm.viewmodel.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite


@ExperimentalCoroutinesApi
@RunWith(Suite::class)
@Suite.SuiteClasses(
    InstitutionDataSourceImplTest::class,
    PlacesDataSourceImplTest::class,
    SplashDataSourceImplTest::class,
    SponsorDataSourceImplTest::class,
    FixtureDetailsDataSourceImplTest::class,
    InstitutionDetailsDataSourceImplTest::class,
    MyCitysSportsContactDataSourceImplTest::class,
    NewsDataSourceImplTest::class,
    RankingDataSourceImplTest::class,
    SanctionDataSourceImplTest::class,
    TableDataSourceImplTest::class,
    FixtureDataSourceImplTest::class,
    HomeDataSourceImplTest::class,

    InstitutionRepositoryImplTest::class,
    PlacesRepositoryImplTest::class,
    SplashRepositoryImplTest::class,
    SponsorRepositoryImplTest::class,
    FixtureDetailsRepositoryImplTest::class,
    InstitutionDetailsRepositoryImplTest::class,
    MyCitysSportsContactRepositoryImplTest::class,
    NewsRepositoryImplTest::class,
    RankingRepositoryImplTest::class,
    SanctionRepositoryImplTest::class,
    TableRepositoryImplTest::class,
    FixtureRepositoryImplTest::class,
    HomeRepositoryImplTest::class,

    InstitutionViewModelTest::class,
    PlacesViewModelTest::class,
    SplashViewModelTest::class,
    SponsorViewModelTest::class,
    ErrorFragmentViewModelTest::class,
    FixtureDetailsViewModelTest::class,
    InstitutionDetailsViewModelTest::class,
    MyCitysSportsContactViewModelTest::class,
    NewsViewModelTest::class,
    RankingViewModelTest::class,
    SanctionViewModelTest::class,
    TableViewModelTest::class,
    FixtureViewModelTest::class,
    HomeViewModelTest::class,

    ExceptionHandleImplTest::class,
)
class RunAllTest {
}