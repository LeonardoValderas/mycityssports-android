package com.lavsystems.mycityssports.base

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.firebase.FirebaseApp
import org.junit.Before
import org.mockito.MockitoAnnotations

abstract class BaseTest {

    @Before
    open fun setUp(){
        MockitoAnnotations.openMocks(this)
//        val context = ApplicationProvider.getApplicationContext<Context>()
//        FirebaseApp.initializeApp(context)
    }
}