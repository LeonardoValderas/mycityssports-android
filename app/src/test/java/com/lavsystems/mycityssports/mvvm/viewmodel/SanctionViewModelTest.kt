package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getSanctions
import ProvidesTestAndroidModels.getSponsors
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.SanctionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class SanctionViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: SanctionRepository
    lateinit var viewModel: SanctionViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var sanctionsObserver: Observer<DataResponse<SanctionDto>>

    @Captor
    private lateinit var sanctionsArgumentCaptor: ArgumentCaptor<DataResponse<SanctionDto>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region SANCTION
    @Test
    fun testSanctionDtoWhenIsSuccess_ShouldReturnDto() = testDispatcher.runBlockingTest {
        val sanctionsDto = SanctionDto(
            getSanctions(1),
            getSponsors(1)
        )
        Mockito.`when`(repository.getSanctions(false)).thenReturn(
            flowOf(
                Resource.success(
                    sanctionsDto
                )
            )
        )

        viewModel = SanctionViewModel(repository)

        viewModel.sanctions.apply {
            observeForever(sanctionsObserver)
        }

        Mockito.verify(sanctionsObserver, Mockito.times(1)).onChanged(
            sanctionsArgumentCaptor.capture()
        )

        val values = sanctionsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(sanctionsDto, values.first()?.data)
    }

    @Test
    fun testSanctionDtoWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getSanctions(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = SanctionViewModel(repository)

        viewModel.sanctions.apply {
            observeForever(sanctionsObserver)
        }

        Mockito.verify(sanctionsObserver, Mockito.times(1)).onChanged(
            sanctionsArgumentCaptor.capture()
        )

        val values = sanctionsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}