package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.MyCitysSportsContactRepository
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class MyCitysSportsContactViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: MyCitysSportsContactRepository
    lateinit var viewModel: MyCitysSportsContactViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var contactModelObserver: Observer<DataResponse<MyCitysSportsContactModel>>

    @Captor
    private lateinit var contactModelArgumentCaptor: ArgumentCaptor<DataResponse<MyCitysSportsContactModel>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region CONTACT
    @Test
    fun testContactWhenIsSuccess_ShouldReturnContactModel() =
        testDispatcher.runBlockingTest {
            val contactModel = MyCitysSportsContactModel()
            Mockito.`when`(repository.getContact()).thenReturn(
                flowOf(
                    Resource.success(ApiResponseData(contactModel))
                )
            )

            viewModel = MyCitysSportsContactViewModel(repository)

            viewModel.contactModel.apply {
                observeForever(contactModelObserver)
            }

            Mockito.verify(contactModelObserver, Mockito.times(1)).onChanged(
                contactModelArgumentCaptor.capture()
            )

            val values = contactModelArgumentCaptor.allValues
            assertEquals(1, values.size)
            assertEquals(StatusResponse.SUCCESS, values.first().status)
            assertEquals(contactModel, values.first()?.data)
        }

    @Test
    fun testContactWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getContact()).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = MyCitysSportsContactViewModel(repository)

        viewModel.contactModel.apply {
            observeForever(contactModelObserver)
        }

        Mockito.verify(contactModelObserver, Mockito.times(1)).onChanged(
            contactModelArgumentCaptor.capture()
        )

        val values = contactModelArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}