package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getSponsors
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.SponsorRepository
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class SponsorViewModelTest : BaseTest() {
    @Mock
    private lateinit var repository: SponsorRepository
    lateinit var viewModel: SponsorViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var sponsorObserver: Observer<DataResponse<SponsorModel>>

    @Mock
    private lateinit var sponsorsObserver: Observer<DataResponse<MutableList<SponsorDto>>>

    @Captor
    private lateinit var sponsorArgumentCaptor: ArgumentCaptor<DataResponse<SponsorModel>>

    @Captor
    private lateinit var sponsorsArgumentCaptor: ArgumentCaptor<DataResponse<MutableList<SponsorDto>>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region SPONSORS
    @Test
    fun testSponsorsWhenIsSuccess_ShouldReturnSponsorDtoList() = testDispatcher.runBlockingTest {
        val sponsorDtoList = mutableListOf(
            SponsorDto(
                "id",
                getSponsors(2)
            )
        )
        Mockito.`when`(repository.getSponsorsByCity()).thenReturn(
            flowOf(
                Resource.success(ApiResponseData(sponsorDtoList))
            )
        )

        viewModel = SponsorViewModel(repository)

        viewModel.sponsors.apply {
            observeForever(sponsorsObserver)
        }

        viewModel.getSponsorsByCity()

        Mockito.verify(sponsorsObserver, Mockito.times(2)).onChanged(
            sponsorsArgumentCaptor.capture()
        )

        val values = sponsorsArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.SUCCESS, values[1].status)
        assertEquals(sponsorDtoList, values[1]?.data)
    }

    @Test
    fun testSponsorsWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getSponsorsByCity()).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = SponsorViewModel(repository)

        viewModel.sponsors.apply {
            observeForever(sponsorsObserver)
        }

        viewModel.getSponsorsByCity()

        Mockito.verify(sponsorsObserver, Mockito.times(2)).onChanged(
            sponsorsArgumentCaptor.capture()
        )

        val values = sponsorsArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.FAILURE, values[1].status)
    }

    //endregion

    //region SPONSOR
    @Test
    fun testSponsorWhenIsSuccess_ShouldReturnSponsor() = testDispatcher.runBlockingTest {
        val sponsor = getSponsors(1)[0]

        Mockito.`when`(repository.getSponsor("id")).thenReturn(
            flowOf(
                Resource.success(ApiResponseData(sponsor))
            )
        )

        viewModel = SponsorViewModel(repository)

        viewModel.sponsor.apply {
            observeForever(sponsorObserver)
        }

        viewModel.getSponsor("id")

        Mockito.verify(sponsorObserver, Mockito.times(2)).onChanged(
            sponsorArgumentCaptor.capture()
        )

        val values = sponsorArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.SUCCESS, values[1].status)
        assertEquals(sponsor, values[1]?.data)
    }

    @Test
    fun testSponsorWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getSponsor("id")).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = SponsorViewModel(repository)

        viewModel.sponsor.apply {
            observeForever(sponsorObserver)
        }

        viewModel.getSponsor("id")

        Mockito.verify(sponsorObserver, Mockito.times(2)).onChanged(
            sponsorArgumentCaptor.capture()
        )

        val values = sponsorArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.FAILURE, values[1].status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}