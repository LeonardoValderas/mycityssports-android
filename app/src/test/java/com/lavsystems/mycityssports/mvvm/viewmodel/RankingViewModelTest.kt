package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getRanking
import ProvidesTestAndroidModels.getSponsors
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.RankingRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class RankingViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: RankingRepository
    lateinit var viewModel: RankingViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var rankingsObserver: Observer<DataResponse<RankingDto>>

    @Captor
    private lateinit var rankingsArgumentCaptor: ArgumentCaptor<DataResponse<RankingDto>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region SANCTION
    @Test
    fun testRankingModelWhenIsSuccess_ShouldReturnModel() = testDispatcher.runBlockingTest {
        val rankingsModel = RankingDto(
            getRanking(1, 1),
            getSponsors(1)
        )
        Mockito.`when`(repository.getRanking(false)).thenReturn(
            flowOf(
                Resource.success(
                    rankingsModel
                )
            )
        )

        viewModel = RankingViewModel(repository)

        viewModel.ranking.apply {
            observeForever(rankingsObserver)
        }

        Mockito.verify(rankingsObserver, Mockito.times(1)).onChanged(
            rankingsArgumentCaptor.capture()
        )

        val values = rankingsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(rankingsModel, values.first()?.data)
    }

    @Test
    fun testRankingModelWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getRanking(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = RankingViewModel(repository)

        viewModel.ranking.apply {
            observeForever(rankingsObserver)
        }

        Mockito.verify(rankingsObserver, Mockito.times(1)).onChanged(
            rankingsArgumentCaptor.capture()
        )

        val values = rankingsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}