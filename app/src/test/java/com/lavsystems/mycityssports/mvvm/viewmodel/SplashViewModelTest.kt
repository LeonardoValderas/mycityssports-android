package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandle
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.repository.SplashRepository
import com.lavsystems.mycityssports.utils.EventWrapper
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.BDDMockito.given
import org.mockito.Mockito.verify
import java.lang.Exception

@ExperimentalCoroutinesApi
class SplashViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: SplashRepository
    lateinit var viewModel: SplashViewModel
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var observer: Observer<DataResponse<EventWrapper<Boolean>>>
    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<DataResponse<EventWrapper<Boolean>>>
    private val testDispatcher = TestCoroutineDispatcher()

    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
    }

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun testCityExistsWhenIsSuccessTrue_ShouldBeSuccessTrue() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.cityExists()).thenReturn(flowOf(IS_TRUE))
        viewModel = SplashViewModel(repository)
        viewModel.cityExists.apply {
            observeForever(observer)
        }

        delay(3000)

        verify(observer, Mockito.times(1)).onChanged(
            argumentCaptor.capture())
        val values = argumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(IS_TRUE, values.first()?.data?.getContentIfNotHandled())
    }

    @Test
    fun testCityExistsWhenIsSuccessFalse_ShouldBeSuccessFalse() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.cityExists()).thenReturn(flowOf(IS_FALSE))
        viewModel = SplashViewModel(repository)
        viewModel.cityExists.apply {
            observeForever(observer)
        }

        delay(3000)

        verify(observer, Mockito.times(1)).onChanged(
            argumentCaptor.capture())
        val values = argumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(IS_FALSE, values.first()?.data?.getContentIfNotHandled())
    }

    // this method no work. I dont know how to throw a flow exception
//    @Test
//    fun testCityExistsWhenIsError_ShouldBeException() = testDispatcher.runBlockingTest {
//       val e = Exception()
//        val exceptionHandle = ExceptionHandleImpl(e)
//        given(repository.cityExists()).willAnswer {
//            flow {
//                emit(e)
//            }
//        }
//        viewModel = SplashViewModel(repository)
//        viewModel.cityExists.apply {
//             observeForever(observer)
//        }
//
//        delay(3000)
//
//        verify(observer, Mockito.times(1)).onChanged(
//            argumentCaptor.capture())
//        val values = argumentCaptor.allValues
//        assertEquals(1, values.size)
//        assertEquals(StatusResponse.FAILURE, values.first().status)
//        assertEquals(exceptionHandle.getMessageIntResource(), values.first()?.exceptionHandle?.getMessageIntResource())
//    }


    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}