package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getFixture
import ProvidesTestAndroidModels.getFixtureInstanceItemModel
import ProvidesTestAndroidModels.getFixtureZoneItemModel
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.FixtureRepository
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class FixtureViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: FixtureRepository
    lateinit var viewModel: FixtureViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var fixtureObserver: Observer<DataResponse<FixtureModel>>

    @Captor
    private lateinit var fixtureArgumentCaptor: ArgumentCaptor<DataResponse<FixtureModel>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region FIXTURE
    @Test
    fun testFixtureModelWhenIsSuccess_ShouldReturnDto() = testDispatcher.runBlockingTest {
        val fixtureModel = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )
        Mockito.`when`(repository.getFixtures(false)).thenReturn(
            flowOf(
                Resource.success(
                    fixtureModel
                )
            )
        )

        viewModel = FixtureViewModel(repository)

        viewModel.fixture.apply {
            observeForever(fixtureObserver)
        }

        Mockito.verify(fixtureObserver, Mockito.times(1)).onChanged(
            fixtureArgumentCaptor.capture()
        )

        val values = fixtureArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(fixtureModel, values.first()?.data)
    }

    @Test
    fun testFixtureModelWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getFixtures(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = FixtureViewModel(repository)

        viewModel.fixture.apply {
            observeForever(fixtureObserver)
        }

        Mockito.verify(fixtureObserver, Mockito.times(1)).onChanged(
            fixtureArgumentCaptor.capture()
        )

        val values = fixtureArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}