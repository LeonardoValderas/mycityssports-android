package com.lavsystems.mycityssports.mvvm.viewmodel

import android.os.Bundle
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.data.network.exception.NoConnectivityException
import com.lavsystems.mycityssports.data.repository.ErrorFragmentRepository
import com.lavsystems.mycityssports.utils.ConstantsUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class ErrorFragmentViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: ErrorFragmentRepository
    lateinit var viewModel: ErrorFragmentViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<Int>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<Int>

    private val testDispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var bundle: Bundle

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun testResourceStringIdWhenIsCalled_ShouldReturnResIdException() = testDispatcher.runBlockingTest {
        viewModel = ErrorFragmentViewModel(repository)
        Mockito.`when`(bundle.getSerializable(ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE)).thenReturn(
            ExceptionHandleImpl(NoConnectivityException())
        )
        viewModel.setBundle(bundle)
        viewModel.resourceStringId.apply {
            observeForever(observer)
        }

        viewModel.setResourceStringIdFromBundle()
        Mockito.verify(observer, Mockito.times(1)).onChanged(
            argumentCaptor.capture()
        )

        val values = argumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(R.string.without_internet, values.first())
    }

    @Test
    fun testFragmentPositionWhenIsCalled_ShouldReturnPosition() = testDispatcher.runBlockingTest {
        viewModel = ErrorFragmentViewModel(repository)
        Mockito.`when`(bundle.getInt(ConstantsUtils.FRAGMENT_POSITION_BUNDLE)).thenReturn(2)
        viewModel.setBundle(bundle)

        assertEquals(2,viewModel.getFragmentPosition())
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}