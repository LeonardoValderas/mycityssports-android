package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getSponsors
import ProvidesTestAndroidModels.getTable
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.TableRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class TableViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: TableRepository
    lateinit var viewModel: TableViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var tableObserver: Observer<DataResponse<TableDto>>

    @Captor
    private lateinit var tableArgumentCaptor: ArgumentCaptor<DataResponse<TableDto>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region TABLE
    @Test
    fun testTableDtoWhenIsSuccess_ShouldReturnDto() = testDispatcher.runBlockingTest {
        val tableDto = TableDto(
            getTable(1,1,0),
            getSponsors(1)
        )
        Mockito.`when`(repository.getTable(false)).thenReturn(
            flowOf(
                Resource.success(
                    tableDto
                )
            )
        )

        viewModel = TableViewModel(repository)

        viewModel.table.apply {
            observeForever(tableObserver)
        }

        Mockito.verify(tableObserver, Mockito.times(1)).onChanged(
            tableArgumentCaptor.capture()
        )

        val values = tableArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(tableDto, values.first()?.data)
    }

    @Test
    fun testTableDtoWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getTable(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = TableViewModel(repository)

        viewModel.table.apply {
            observeForever(tableObserver)
        }

        Mockito.verify(tableObserver, Mockito.times(1)).onChanged(
            tableArgumentCaptor.capture()
        )

        val values = tableArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}