package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getInstitutions
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.InstitutionRepository
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class InstitutionViewModelTest : BaseTest() {
    @Mock
    private lateinit var repository: InstitutionRepository
    lateinit var viewModel: InstitutionViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var institutionsObserver: Observer<DataResponse<MutableList<InstitutionModel>>>

    @Captor
    private lateinit var institutionsArgumentCaptor: ArgumentCaptor<DataResponse<MutableList<InstitutionModel>>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region INSTITUTIONS
    @Test
    fun testInstitutionsWhenIsSuccess_ShouldReturnInstitutionsList() = testDispatcher.runBlockingTest {
        val institutions = getInstitutions(2)
        Mockito.`when`(repository.getInstitutions(false)).thenReturn(
            flowOf(
                Resource.success(institutions)
            )
        )

        viewModel = InstitutionViewModel(repository)

        viewModel.institutions.apply {
            observeForever(institutionsObserver)
        }

        Mockito.verify(institutionsObserver, Mockito.times(1)).onChanged(
            institutionsArgumentCaptor.capture()
        )

        val values = institutionsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(institutions, values.first()?.data)
    }

    @Test
    fun testInstitutionsWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getInstitutions(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = InstitutionViewModel(repository)

        viewModel.institutions.apply {
            observeForever(institutionsObserver)
        }

        Mockito.verify(institutionsObserver, Mockito.times(1)).onChanged(
            institutionsArgumentCaptor.capture()
        )

        val values = institutionsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}