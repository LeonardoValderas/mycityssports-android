package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.PlacesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.utils.EventWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class PlacesViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: PlacesRepository
    lateinit var viewModel: PlacesViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var cityObserver: Observer<DataResponse<EventWrapper<Boolean>>>
    @Mock
    private lateinit var placesObserver: Observer<DataResponse<PlaceDto>>

    @Captor
    private lateinit var cityArgumentCaptor: ArgumentCaptor<DataResponse<EventWrapper<Boolean>>>
    @Captor
    private lateinit var placesArgumentCaptor: ArgumentCaptor<DataResponse<PlaceDto>>

    private val testDispatcher = TestCoroutineDispatcher()

    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
    }

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }


    //region PLACES
    @Test
    fun testPlacesWhenIsSuccess_ShouldReturnPlaceDto() = testDispatcher.runBlockingTest {
        val placeDto = PlaceDto(mutableListOf(), mutableListOf())
        Mockito.`when`(repository.getPlacesData()).thenReturn(flowOf(
            Resource.success(ApiResponseData(placeDto)))
        )
        viewModel = PlacesViewModel(repository)

        viewModel.places.apply {
            observeForever(placesObserver)
        }

        Mockito.verify(placesObserver, Mockito.times(1)).onChanged(
            placesArgumentCaptor.capture()
        )

        val values = placesArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(placeDto, values.first()?.data)
    }
    @Test
    fun testPlacesWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        val placeDto = PlaceDto(mutableListOf(), mutableListOf())
        Mockito.`when`(repository.getPlacesData()).thenReturn(flowOf(
            Resource.error("Error", null, null, 400)
        ))
        viewModel = PlacesViewModel(repository)

        viewModel.places.apply {
            observeForever(placesObserver)
        }

        Mockito.verify(placesObserver, Mockito.times(1)).onChanged(
            placesArgumentCaptor.capture()
        )

        val values = placesArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
        //assertEquals(placeDto, values.first()?.data)
    }
    //endregion

    //region CITY
    @Test
    fun testSaveCityWhenIsSuccess_ShouldBeSuccessTrue() = testDispatcher.runBlockingTest {
        val city = CityModel()
        Mockito.`when`(repository.saveCity(city)).thenReturn(flowOf(IS_TRUE))
        viewModel = PlacesViewModel(repository)

        viewModel.city.apply {
            observeForever(cityObserver)
        }

        viewModel.saveCity(city)

        Mockito.verify(cityObserver, Mockito.times(2)).onChanged(
            cityArgumentCaptor.capture()
        )

        val values = cityArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.SUCCESS, values[1].status)
        assertEquals(IS_TRUE, values[1]?.data?.getContentIfNotHandled())
    }

    @Test
    fun testSaveCityWhenIsSuccessFalse_ShouldReturnError() = testDispatcher.runBlockingTest {
        val city = CityModel()
        Mockito.`when`(repository.saveCity(city)).thenReturn(flowOf(IS_FALSE))
        viewModel = PlacesViewModel(repository)

        viewModel.city.apply {
            observeForever(cityObserver)
        }

        viewModel.saveCity(city)

        Mockito.verify(cityObserver, Mockito.times(2)).onChanged(
            cityArgumentCaptor.capture()
        )

        val values = cityArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.ERROR, values[1].status)
        //assertEquals(IS_FALSE, values[1]?.data?.getContentIfNotHandled())
    }

//    @Test
//    fun testSaveCityWhenIsException_ShouldReturnFailure() = testDispatcher.runBlockingTest {
//        val city = CityModel()
//        Mockito.`when`(repository.saveCity(city)).thenThrow(Exception())
//        viewModel = PlacesViewModel(repository)
//
//        viewModel.city.apply {
//            observeForever(cityObserver)
//        }
//
//        viewModel.saveCity(city)
//
//        Mockito.verify(cityObserver, Mockito.times(2)).onChanged(
//            argumentCaptor.capture()
//        )
//
//        val values = argumentCaptor.allValues
//        assertEquals(2, values.size)
//        assertEquals(StatusResponse.LOADING, values.first().status)
//        assertEquals(StatusResponse.FAILURE, values[1].status)
//    }
    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}