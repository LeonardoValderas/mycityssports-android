package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getHomeItemDto
import ProvidesTestAndroidModels.getTournaments
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.HomeRepository
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SportModel
import com.lavsystems.mycityssports.mvvm.model.TournamentModel
import com.lavsystems.mycityssports.mvvm.view.adapter.DivisionsSubDivisionsFromTournamentAdapter
import com.lavsystems.mycityssports.utils.EventWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class HomeViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: HomeRepository
    lateinit var viewModel: HomeViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var homeItemObserver: Observer<DataResponse<HomeItemDto>>

    @Captor
    private lateinit var homeItemArgumentCaptor: ArgumentCaptor<DataResponse<HomeItemDto>>

    @Mock
    private lateinit var institutionsObserver: Observer<MutableList<InstitutionModel>>

    @Captor
    private lateinit var institutionsArgumentCaptor: ArgumentCaptor<MutableList<InstitutionModel>>

    @Mock
    private lateinit var institutionsEmptyObserver: Observer<EventWrapper<Boolean>>

    @Captor
    private lateinit var institutionsEmptyArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>

    @Mock
    private lateinit var sportsObserver: Observer<MutableList<SportModel>>

    @Captor
    private lateinit var sportsArgumentCaptor: ArgumentCaptor<MutableList<SportModel>>

    @Mock
    private lateinit var sportsEmptyObserver: Observer<EventWrapper<Boolean>>

    @Captor
    private lateinit var sportsEmptyArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>

    @Mock
    private lateinit var tournamentsObserver: Observer<MutableList<TournamentModel>>

    @Captor
    private lateinit var tournamentsArgumentCaptor: ArgumentCaptor<MutableList<TournamentModel>>

    @Mock
    private lateinit var tournamentsEmptyObserver: Observer<EventWrapper<Boolean>>

    @Captor
    private lateinit var tournamentsEmptyArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>

    @Mock
    private lateinit var divisionSubDivisionObserver: Observer<MutableList<DivisionSubDivisionModel>>

    @Captor
    private lateinit var divisionSubDivisionArgumentCaptor: ArgumentCaptor<MutableList<DivisionSubDivisionModel>>

    @Mock
    private lateinit var divisionSubDivisionEmptyObserver: Observer<EventWrapper<Boolean>>

    @Captor
    private lateinit var divisionSubDivisionEmptyArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>

    @Mock
    private lateinit var changeFragmentObserver: Observer<EventWrapper<Boolean>>

    @Captor
    private lateinit var changeFragmentArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>


    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region HOME DATA
    @Test
    fun testHomeModelWhenIsSuccess_ShouldReturnDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val divisionSubdivision = DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()
        
        Mockito.`when`(repository.getHomeData()).thenReturn(
            flowOf(
                Resource.success(
                    ApiResponseData(dto)
                )
            )
        )

        viewModel = HomeViewModel(repository)

        viewModel.homeData.apply {
            observeForever(homeItemObserver)
        }

        Mockito.verify(homeItemObserver, Mockito.times(1)).onChanged(
            homeItemArgumentCaptor.capture()
        )

        val values = homeItemArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(dto, values.first()?.data)
    }

    @Test
    fun testHomeModelWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getHomeData()).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = HomeViewModel(repository)

        viewModel.homeData.apply {
            observeForever(homeItemObserver)
        }

        Mockito.verify(homeItemObserver, Mockito.times(1)).onChanged(
            homeItemArgumentCaptor.capture()
        )

        val values = homeItemArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    //region INSTITUTIONS
    @Test
    fun testInstitutionsWhenIsSuccess_ShouldBeEqualsToDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.institutions.apply {
            observeForever(institutionsObserver)
        }

        Mockito.verify(institutionsObserver, Mockito.times(1)).onChanged(
            institutionsArgumentCaptor.capture()
        )

        val values = institutionsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(dto.institutions, values.first())
    }

    @Test
    fun testInstitutionsWhenIsEmpty_ShouldBeTrue() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(HomeItemDto())
        viewModel.institutionsIsEmpty.apply {
            observeForever(institutionsEmptyObserver)
        }

        Mockito.verify(institutionsEmptyObserver, Mockito.times(1)).onChanged(
            institutionsEmptyArgumentCaptor.capture()
        )

        val values = institutionsEmptyArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(true, values.first().getContentIfNotHandled())
    }


    //endregion

    //region SPORTS
    @Test
    fun testSportsWhenIsSuccess_ShouldBeEqualsToDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )
        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.institutions[0])
        viewModel.sports.apply {
            observeForever(sportsObserver)
        }

        Mockito.verify(sportsObserver, Mockito.times(1)).onChanged(
            sportsArgumentCaptor.capture()
        )

        val values = sportsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(dto.items[0].sports, values.first())
    }

    @Test
    fun testSportsWhenIsEmpty_ShouldBeTrue() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, -1)
        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )
        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.institutions[0])
        viewModel.sportsEmpty.apply {
            observeForever(sportsEmptyObserver)
        }

        Mockito.verify(sportsEmptyObserver, Mockito.times(1)).onChanged(
            sportsEmptyArgumentCaptor.capture()
        )

        val values = sportsEmptyArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(true, values.first().getContentIfNotHandled())
    }

    //endregion

    //region TOURNAMENT
    @Test
    fun testTournamentsWhenIsSuccess_ShouldBeEqualsToDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        dto.items[0] = dto.items[0].copy(tournaments = tournaments)
        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )
        Mockito.`when`(repository.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.items[0].institution)
        viewModel.setSportModel(dto.items[0].sports[0])
        viewModel.tournaments.apply {
            observeForever(tournamentsObserver)
        }

        Mockito.verify(tournamentsObserver, Mockito.times(1)).onChanged(
            tournamentsArgumentCaptor.capture()
        )

        val values = tournamentsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(dto.items[0].tournaments, values.first())
    }

    @Test
    fun testTournamentsWhenIsEmpty_ShouldBeTrue() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        //val tournaments = getTournaments(2, 1, 0, 1)
       // dto.items[0] = dto.items[0].copy(tournaments = tournaments)
        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )
        Mockito.`when`(repository.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.items[0].institution)
        viewModel.setSportModel(dto.items[0].sports[0])
        viewModel.tournamentsIsEmpty.apply {
            observeForever(tournamentsEmptyObserver)
        }

        Mockito.verify(tournamentsEmptyObserver, Mockito.times(1)).onChanged(
            tournamentsEmptyArgumentCaptor.capture()
        )

        val values = tournamentsEmptyArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(true, values.first().getContentIfNotHandled())
    }
    //endregion

    //region DIVISION SUBDIVISION
    @Test
    fun testDivisionSubDivisionWhenIsSuccess_ShouldBeEqualsToDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        dto.items[0] = dto.items[0].copy(tournaments = tournaments)
        val divisionSubdivision = DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.items[0].institution)
        viewModel.setTournamentModel(tournaments[0])

        viewModel.divisionsSubDivisions.apply {
            observeForever(divisionSubDivisionObserver)
        }

        Mockito.verify(divisionSubDivisionObserver, Mockito.times(1)).onChanged(
            divisionSubDivisionArgumentCaptor.capture()
        )

        val values = divisionSubDivisionArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(divisionSubdivision, values.first())
    }

    @Test
    fun testDivisionSubDivisionWhenIsEmpty_ShouldBeEqualsToDto() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 0, 0, 1)
        dto.items[0] = dto.items[0].copy(tournaments = tournaments)
        //val divisionSubdivision = DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )

        viewModel = HomeViewModel(repository)
        viewModel.setHomeData(dto)
        viewModel.setInstitutionModel(dto.items[0].institution)
        viewModel.setTournamentModel(tournaments[0].copy(divisions = mutableListOf()))

        viewModel.divisionsSubDivisionsIsEmpty.apply {
            observeForever(divisionSubDivisionEmptyObserver)
        }

        Mockito.verify(divisionSubDivisionEmptyObserver, Mockito.times(1)).onChanged(
            divisionSubDivisionEmptyArgumentCaptor.capture()
        )

        val values = divisionSubDivisionEmptyArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(true, values.first().getContentIfNotHandled())
    }
    //endregion

    //region CHANGEFRAGMENT
    @Test
    fun testChangeFragmentWhenSetDivisionSubDivision_ShouldBeCalled() = testDispatcher.runBlockingTest {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        dto.items[0] = dto.items[0].copy(tournaments = tournaments)
        val divisionSubdivision = DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        Mockito.`when`(repository.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(true)
        )

        viewModel = HomeViewModel(repository)
        viewModel.setDivisionSubDivisionModel(divisionSubdivision[0])

        viewModel.changeFragment.apply {
            observeForever(changeFragmentObserver)
        }

        Mockito.verify(changeFragmentObserver, Mockito.times(1)).onChanged(
            changeFragmentArgumentCaptor.capture()
        )

        val values = changeFragmentArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(true, values.first().getContentIfNotHandled())
    }
    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}