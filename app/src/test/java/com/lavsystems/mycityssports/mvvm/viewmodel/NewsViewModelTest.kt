package com.lavsystems.mycityssports.mvvm.viewmodel

import ProvidesTestAndroidModels.getNews
import ProvidesTestAndroidModels.getSponsors
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class NewsViewModelTest : BaseTest() {
    @Mock
    private lateinit var repository: NewsRepository
    lateinit var viewModel: NewsViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var newsObserver: Observer<DataResponse<NewsDto>>

    @Captor
    private lateinit var newsArgumentCaptor: ArgumentCaptor<DataResponse<NewsDto>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region NEWS
    @Test
    fun testNewsDtoWhenIsSuccess_ShouldReturnDto() = testDispatcher.runBlockingTest {
        val newsDto = NewsDto(
            getNews(1),
            getSponsors(1)
        )
        Mockito.`when`(repository.getNews(false)).thenReturn(
            flowOf(
                Resource.success(
                    newsDto
                )
            )
        )

        viewModel = NewsViewModel(repository)

        viewModel.news.apply {
            observeForever(newsObserver)
        }

        Mockito.verify(newsObserver, Mockito.times(1)).onChanged(
            newsArgumentCaptor.capture()
        )

        val values = newsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.SUCCESS, values.first().status)
        assertEquals(newsDto, values.first()?.data)
    }

    @Test
    fun testNewsDtoWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getNews(false)).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = NewsViewModel(repository)

        viewModel.news.apply {
            observeForever(newsObserver)
        }

        Mockito.verify(newsObserver, Mockito.times(1)).onChanged(
            newsArgumentCaptor.capture()
        )

        val values = newsArgumentCaptor.allValues
        assertEquals(1, values.size)
        assertEquals(StatusResponse.FAILURE, values.first().status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}