package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.InstitutionDetailsRepository
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class InstitutionDetailsViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: InstitutionDetailsRepository
    lateinit var viewModel: InstitutionDetailsViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<DataResponse<InstitutionModel>>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<DataResponse<InstitutionModel>>

    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region INSTITUTION
    @Test
    fun testInstitutionWhenIsSuccess_ShouldReturnInstitutionModel() =
        testDispatcher.runBlockingTest {
            val model = InstitutionModel()
            Mockito.`when`(repository.getInstitution("id")).thenReturn(
                flowOf(
                    Resource.success(ApiResponseData(model))
                )
            )

            viewModel = InstitutionDetailsViewModel(repository)

            viewModel.institution.apply {
                observeForever(observer)
            }

            viewModel.getInstitution("id")

            Mockito.verify(observer, Mockito.times(2)).onChanged(
                argumentCaptor.capture()
            )

            val values = argumentCaptor.allValues
            assertEquals(2, values.size)
            assertEquals(StatusResponse.LOADING, values.first().status)
            assertEquals(StatusResponse.SUCCESS, values[1].status)
            assertEquals(model, values[1]?.data)
        }

    @Test
    fun testInstitutionWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getInstitution("id")).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = InstitutionDetailsViewModel(repository)

        viewModel.institution.apply {
            observeForever(observer)
        }
        viewModel.getInstitution("id")

        Mockito.verify(observer, Mockito.times(2)).onChanged(
            argumentCaptor.capture()
        )

        val values = argumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.FAILURE, values[1].status)
    }

    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}