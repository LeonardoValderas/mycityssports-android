package com.lavsystems.mycityssports.mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.repository.FixtureDetailsRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class FixtureDetailsViewModelTest: BaseTest() {
    @Mock
    private lateinit var repository: FixtureDetailsRepository
    lateinit var viewModel: FixtureDetailsViewModel

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var cityObserver: Observer<DataResponse<CityModel>>

    @Captor
    private lateinit var cityArgumentCaptor: ArgumentCaptor<DataResponse<CityModel>>

    @Mock
    private lateinit var observer: Observer<DataResponse<FixtureItemModel>>

    @Captor
    private lateinit var argumentCaptor: ArgumentCaptor<DataResponse<FixtureItemModel>>
    private val testDispatcher = TestCoroutineDispatcher()

    override fun setUp() {
        super.setUp()
        Dispatchers.setMain(testDispatcher)
    }

    //region CITY
    @Test
    fun testCityWhenIsSuccess_ShouldReturnCityModel() =
        testDispatcher.runBlockingTest {
            val model = CityModel()
            Mockito.`when`(repository.getCity()).thenReturn(
                flowOf(
                    model
                )
            )
            viewModel = FixtureDetailsViewModel(repository)
            viewModel.cityModel.apply {
                observeForever(cityObserver)
            }
            viewModel.getCity()
            Mockito.verify(cityObserver, Mockito.times(2)).onChanged(
                cityArgumentCaptor.capture()
            )

            val values = cityArgumentCaptor.allValues
            assertEquals(2, values.size)
            assertEquals(StatusResponse.LOADING, values.first().status)
            assertEquals(StatusResponse.SUCCESS, values[1].status)
            assertEquals(model, values[1]?.data)
        }

    @Test
    fun testCityWhenIsNull_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getCity()).thenReturn(
            flowOf(
                null
            )
        )
        viewModel = FixtureDetailsViewModel(repository)
        viewModel.cityModel.apply {
            observeForever(cityObserver)
        }
        viewModel.getCity()
        Mockito.verify(cityObserver, Mockito.times(2)).onChanged(
            cityArgumentCaptor.capture()
        )

        val values = cityArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.FAILURE, values[1].status)
    }
    //endregion

    //region FIXTURE
    @Test
    fun testFixtureWhenIsSuccess_ShouldReturnFixtureModel() =
        testDispatcher.runBlockingTest {
            val model = FixtureItemModel()
            Mockito.`when`(repository.getFixtureDetails("id")).thenReturn(
                flowOf(
                    Resource.success(ApiResponseData(model))
                )
            )
            viewModel = FixtureDetailsViewModel(repository)

            viewModel.fixtureDetails.apply {
                observeForever(observer)
            }

            viewModel.getFixtureDetails("id")

            Mockito.verify(observer, Mockito.times(2)).onChanged(
                argumentCaptor.capture()
            )

            val values = argumentCaptor.allValues
            assertEquals(2, values.size)
            assertEquals(StatusResponse.LOADING, values.first().status)
            assertEquals(StatusResponse.SUCCESS, values[1].status)
            assertEquals(model, values[1]?.data)
        }

    @Test
    fun testFixtureWhenIsError_ShouldReturnFailure() = testDispatcher.runBlockingTest {
        Mockito.`when`(repository.getFixtureDetails("id")).thenReturn(
            flowOf(
                Resource.error("Error", null, null, 400)
            )
        )
        viewModel = FixtureDetailsViewModel(repository)
        viewModel.fixtureDetails.apply {
            observeForever(observer)
        }
        viewModel.getFixtureDetails("id")
        Mockito.verify(observer, Mockito.times(2)).onChanged(
            argumentCaptor.capture()
        )

        val values = argumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(StatusResponse.LOADING, values.first().status)
        assertEquals(StatusResponse.FAILURE, values[1].status)
    }
    //endregion

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}