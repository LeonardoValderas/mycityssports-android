package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.network.api.service.FixtureService
import com.lavsystems.mycityssports.data.network.api.service.InstitutionService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class FixtureDetailsDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: FixtureService
    private lateinit var dataSource: FixtureDetailsDataSource

    override fun setUp() {
        super.setUp()
    }

    //region CITY
    @Test
    fun testCityWhenIsSuccess_ShouldReturnCityModel() = runBlocking {
        val model = CityModel()
        Mockito.`when`(preferences.getCity()).thenReturn(
            flowOf(
                model
            )
        )
        dataSource = FixtureDetailsDataSourceImpl(service, preferences)
        val result: CityModel? = dataSource.getCity().single()

        assertEquals(model, result)
    }

    @Test
    fun testCityWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getCity()).thenReturn(
            flowOf(
                null
            )
        )
        dataSource = FixtureDetailsDataSourceImpl(service, preferences)
        val result: CityModel? = dataSource.getCity().single()

        assertNull(result)
    }
    //endregion

    //region FIXTURE
    @Test
    fun testFixtureWhenIsSuccess_ShouldReturnFixtureModel() = runBlocking {
        val model = FixtureItemModel()
        Mockito.`when`(service.getFixture("id")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(model)
                    )
                )
            )
        )
        dataSource = FixtureDetailsDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<FixtureItemModel>> = dataSource.getFromApi("id").single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(model, body?.data)
    }

    @Test
    fun testFixtureWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(service.getFixture("id")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = FixtureDetailsDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<FixtureItemModel>> = dataSource.getFromApi("id").single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testFixtureWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(service.getFixture("id")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = FixtureDetailsDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<FixtureItemModel>> = dataSource.getFromApi("id").single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion
}