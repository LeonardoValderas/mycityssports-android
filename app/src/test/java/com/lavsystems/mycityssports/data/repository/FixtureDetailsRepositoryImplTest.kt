package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.FixtureDetailsDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class FixtureDetailsRepositoryImplTest : BaseTest() {

    @Mock
    private lateinit var dataSource: FixtureDetailsDataSource
    private lateinit var repository: FixtureDetailsRepository

    override fun setUp() {
        super.setUp()
    }

    //region CITY
    @Test
    fun testCityWhenIsSuccess_ShouldReturnCityModel() = runBlocking {
        val model = CityModel()
        Mockito.`when`(dataSource.getCity()).thenReturn(
            flowOf(
                model
            )
        )
        repository = FixtureDetailsRepositoryImpl(dataSource)
        val result = repository.getCity().single()

       // assertEquals(Resource.Status.LOADING, result[0].status)
        //assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(model, result)
    }

    @Test
    fun testCityWhenIsNull_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.getCity()).thenReturn(
            flowOf(
                null
            )
        )
        repository = FixtureDetailsRepositoryImpl(dataSource)
        val result = repository.getCity().single()
        assertNull(result)
//        assertEquals(Resource.Status.LOADING, result[0].status)
//        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    //endregion

    //region FIXTURE
    @Test
    fun testFixtureWhenIsSuccess_ShouldReturnFixtureModel() = runBlocking {
        val model = FixtureItemModel()
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(model)))
            )
        )

        repository = FixtureDetailsRepositoryImpl(dataSource)

        val result = repository.getFixtureDetails("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(model, result[1].data?.data)
    }

    @Test
    fun testFixtureWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = FixtureDetailsRepositoryImpl(dataSource)

        val result = repository.getFixtureDetails("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testFixtureWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = FixtureDetailsRepositoryImpl(dataSource)

        val result = repository.getFixtureDetails("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion

}