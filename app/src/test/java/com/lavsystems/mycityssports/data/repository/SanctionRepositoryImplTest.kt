package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getSanctions
import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.SanctionDataSource
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class SanctionRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: SanctionDataSource
    private lateinit var repository: SanctionRepository

    override fun setUp() {
        super.setUp()
    }

    //region NEWS
    @Test
    fun testSanctionWhenIsInstitutionOrCityEmpty_ShouldReturnSanctionDtoEmpty() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            true
        )
        repository = SanctionRepositoryImpl(dataSource)

        val result = repository.getSanctions(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(SanctionDto(), result.first().data)
    }

    @Test
    fun testSanctionWhenIsRefreshSuccess_ShouldReturnSanctionDto() = runBlocking {
        val newsDto = SanctionDto(
            getSanctions(2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(newsDto)))
            )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = SanctionRepositoryImpl(dataSource)

        val result = repository.getSanctions(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }

    @Test
    fun testSanctionWhenIsNotRefreshSuccess_ShouldReturnNewDtoCache() = runBlocking {
        val newsDto = SanctionDto(
            getSanctions(2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = SanctionRepositoryImpl(dataSource)

        val result = repository.getSanctions(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }


    @Test
    fun testSanctionWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = SanctionRepositoryImpl(dataSource)

        val result = repository.getSanctions(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testSanctionWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = SanctionRepositoryImpl(dataSource)

        val result = repository.getSanctions(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}