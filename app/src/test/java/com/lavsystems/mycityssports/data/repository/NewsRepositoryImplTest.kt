package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getNews
import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.NewsDataSource
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class NewsRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: NewsDataSource
    private lateinit var repository: NewsRepository

    override fun setUp() {
        super.setUp()
    }

    //region NEWS
    @Test
    fun testNewsWhenIsInstitutionOrCityEmpty_ShouldReturnNewsDtoEmpty() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            true
        )
        repository = NewsRepositoryImpl(dataSource)

        val result = repository.getNews(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(NewsDto(), result.first().data)
    }

    @Test
    fun testNewsWhenIsRefreshSuccess_ShouldReturnNewDto() = runBlocking {
        val newsDto = NewsDto(
            getNews(2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(newsDto)))
            )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = NewsRepositoryImpl(dataSource)

        val result = repository.getNews(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }

    @Test
    fun testNewsWhenIsNotRefreshSuccess_ShouldReturnNewDtoCache() = runBlocking {
        val newsDto = NewsDto(
            getNews(2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = NewsRepositoryImpl(dataSource)

        val result = repository.getNews(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }


    @Test
    fun testNewsWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = NewsRepositoryImpl(dataSource)

        val result = repository.getNews(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testNewsWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = NewsRepositoryImpl(dataSource)

        val result = repository.getNews(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}