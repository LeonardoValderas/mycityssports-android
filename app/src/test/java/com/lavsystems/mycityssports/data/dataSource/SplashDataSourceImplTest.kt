package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito


internal class SplashDataSourceImplTest: BaseTest(){
    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    private lateinit var dataSource: SplashDataSource

    override fun setUp() {
        super.setUp()
    }



    //region CITY
    @Test
    fun testCityExistsWhenIsSuccessTrue_ShouldBeSuccessTrue() = runBlocking {
        Mockito.`when`(preferences.cityExists()).thenReturn(flowOf(IS_TRUE))
        dataSource = SplashDataSourceImpl(preferences)
        val result = preferences.cityExists().single()
        Assert.assertEquals(
            IS_TRUE,
            result
        )
    }

    @Test
    fun testCityExistsWhenIsSuccessFalse_ShouldBeSuccessFalse() = runBlocking {
        Mockito.`when`(preferences.cityExists()).thenReturn(flowOf(IS_FALSE))
        dataSource = SplashDataSourceImpl(preferences)
        val result = preferences.cityExists().single()
        Assert.assertEquals(
            IS_FALSE,
            result
        )
    }
    //endregion
}