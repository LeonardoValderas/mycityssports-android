package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.InstitutionDetailsDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class InstitutionDetailsRepositoryImplTest : BaseTest() {

    @Mock
    private lateinit var dataSource: InstitutionDetailsDataSource
    private lateinit var repository: InstitutionDetailsRepository

    override fun setUp() {
        super.setUp()
    }

    //region INSTITUTION
    @Test
    fun testInstitutionWhenIsSuccess_ShouldReturnInstitutionModel() = runBlocking {
        val model = InstitutionModel()
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(model)))
            )
        )

        repository = InstitutionDetailsRepositoryImpl(dataSource)

        val result = repository.getInstitution("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(model, result[1].data?.data)
    }

    @Test
    fun testInstitutionWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = InstitutionDetailsRepositoryImpl(dataSource)

        val result = repository.getInstitution("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testInstitutionWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = InstitutionDetailsRepositoryImpl(dataSource)

        val result = repository.getInstitution("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion

}