package com.lavsystems.mycityssports.data.dataSource

import ProvidesTestAndroidModels.getFixture
import ProvidesTestAndroidModels.getFixtureInstanceItemModel
import ProvidesTestAndroidModels.getFixtureZoneItemModel
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.network.api.service.FixtureService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class FixtureDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: FixtureService
    private lateinit var dataSource: FixtureDataSource

    override fun setUp() {
        super.setUp()
    }

    //region FIXTURE API
    @Test
    fun testInstitutionsWhenIsSuccess_ShouldReturnInstitutionsList() = runBlocking {
        val fixtureModel = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getFixturesData(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )
        ).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(fixtureModel)
                    )
                )
            )
        )
        dataSource = FixtureDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<FixtureModel>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(fixtureModel, body?.data)
    }

    @Test
    fun testFixtureWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getFixturesData(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = FixtureDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<FixtureModel>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testFixtureWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getFixturesData(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = FixtureDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<FixtureModel>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region CACHE
    @Test
    fun testFixtureCacheWhenSaveLocal_ShouldReturnSavedList() = runBlocking {
        val fixtureModel = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = FixtureDataSourceImpl(service, preferences)
        dataSource.saveLocal(fixtureModel)

        val cacheList = dataSource.getFromLocal().single()
        assertEquals(fixtureModel, cacheList)
    }

    @Test
    fun testFixtureCacheWhenRemoveLocal_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = FixtureDataSourceImpl(service, preferences)
        dataSource.removeLocal()

        val cacheList = dataSource.getFromLocal().single()
        assertNull(cacheList)
    }

    //endregion
}