package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.HomeDataSource
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class HomeRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: HomeDataSource
    private lateinit var repository: HomeRepository

    companion object{
        const val ID_STRING = "ID"
    }
    override fun setUp() {
        super.setUp()
    }

    //region HOME
    @Test
    fun testHomeWhenIsSuccess_ShouldReturnHomeModel() = runBlocking {
        val model = HomeItemDto()
        Mockito.`when`(dataSource.getHomeData()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(model)))
            )
        )

        repository = HomeRepositoryImpl(dataSource)

        val result = repository.getHomeData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(model, result[1].data?.data)
    }

    @Test
    fun testHomeWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.getHomeData()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = HomeRepositoryImpl(dataSource)

        val result = repository.getHomeData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testHomeWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getHomeData()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = HomeRepositoryImpl(dataSource)

        val result = repository.getHomeData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion

    //region CITY
    @Test
    fun testCityWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.removeCity()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeCity().single()

        assertEquals(true, result)
    }

    @Test
    fun testCityWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.removeCity()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeCity().single()

        assertEquals(false, result)
    }

    //endregion

    //region INSTITUTION
    @Test
    fun testInstitutionWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveInstitutionId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testInstitutionWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveInstitutionId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testInstitutionWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(dataSource.getInstitutionId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getInstitutionId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testInstitutionWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.getInstitutionId()).thenReturn(
            flowOf(
                null
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getInstitutionId().single()

        assertEquals(null, result)
    }

    //endregion

    //region SPORT
    @Test
    fun testSportWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveSportId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testSportWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveSportId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testSportWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(dataSource.getSportId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getSportId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testSportWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.getSportId()).thenReturn(
            flowOf(
                null
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getSportId().single()

        assertEquals(null, result)
    }

    @Test
    fun testSportWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.removeSportId()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeSportId().single()

        assertEquals(true, result)
    }

    @Test
    fun testSportWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.removeSportId()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeSportId().single()

        assertEquals(false, result)
    }

    //endregion

    //region TOURNAMENT
    @Test
    fun testTournamentWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.saveTournamentId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveTournamentId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testTournamentWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.saveTournamentId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveTournamentId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testTournamentWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(dataSource.getTournamentId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getTournamentId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testTournamentWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.getTournamentId()).thenReturn(
            flowOf(
                null
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getTournamentId().single()

        assertEquals(null, result)
    }

    @Test
    fun testTournamentWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.removeTournamentId()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeTournamentId().single()

        assertEquals(true, result)
    }

    @Test
    fun testTournamentWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.removeTournamentId()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeTournamentId().single()

        assertEquals(false, result)
    }

    //endregion

    //region DIVISION SUBDIVISION
    @Test
    fun testDivisionSubDivisionWhenIsSave_ShouldReturnTrue() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(dataSource.saveDivisionSubDivision(model)).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveDivisionSubDivision(model).single()

        assertEquals(true, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsSave_ShouldReturnFalse() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(dataSource.saveDivisionSubDivision(model)).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.saveDivisionSubDivision(model).single()

        assertEquals(false, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsGetId_ShouldReturnId() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(dataSource.getDivisionSubDivision()).thenReturn(
            flowOf(
                model
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getDivisionSubDivision().single()

        assertEquals(model, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.getDivisionSubDivision()).thenReturn(
            flowOf(
                null
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.getDivisionSubDivision().single()

        assertEquals(null, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.removeDivisionSubDivision()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeDivisionSubDivision().single()

        assertEquals(true, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.removeDivisionSubDivision()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.removeDivisionSubDivision().single()

        assertEquals(false, result)
    }

    //endregion

    //region ALERT
    @Test
    fun testAlertWhenShow_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.showHomeInformationAlert()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.showHomeInformationAlert().single()

        assertEquals(true, result)
    }

    @Test
    fun testAlertWhenShow_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.showHomeInformationAlert()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.showHomeInformationAlert().single()

        assertEquals(false, result)
    }

    @Test
    fun testAlertWhenShowed_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(dataSource.homeInformationAlertDisplayed()).thenReturn(
            flowOf(
                true
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.homeInformationAlertDisplayed().single()

        assertEquals(true, result)
    }

    @Test
    fun testAlertWhenShowed_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(dataSource.homeInformationAlertDisplayed()).thenReturn(
            flowOf(
                false
            )
        )
        repository = HomeRepositoryImpl(dataSource)
        val result = repository.homeInformationAlertDisplayed().single()

        assertEquals(false, result)
    }

    //endregion
}