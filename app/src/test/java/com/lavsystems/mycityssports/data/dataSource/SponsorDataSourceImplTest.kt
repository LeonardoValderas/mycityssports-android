package com.lavsystems.mycityssports.data.dataSource

import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.service.SponsorService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class SponsorDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: SponsorService
    private lateinit var dataSource: SponsorDataSource

    override fun setUp() {
        super.setUp()
    }

    //region SPONSORS
    @Test
    fun testSponsorsWhenIsSuccess_ShouldReturnSponsorDtoList() = runBlocking {
        val sponsorDtoList = mutableListOf(
            SponsorDto(
                "id",
                getSponsors(2)
            )
        )
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getSponsorsByCity("")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(sponsorDtoList)
                    )
                )
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<MutableList<SponsorDto>>> = dataSource.getSponsorsByCity().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(sponsorDtoList, body?.data)
    }

    @Test
    fun testSponsorsWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getSponsorsByCity("")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<MutableList<SponsorDto>>> = dataSource.getSponsorsByCity().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testSponsorsWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getSponsorsByCity("")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<MutableList<SponsorDto>>> = dataSource.getSponsorsByCity().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region SPONSOR
    @Test
    fun testSponsorWhenIsSuccess_ShouldReturnSponsor() = runBlocking {
        val sponsor = SponsorModel()
        Mockito.`when`(service.getSponsor("id")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(sponsor)
                    )
                )
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<SponsorModel>> = dataSource.getFromApi("id").single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(sponsor, body?.data)
    }

    @Test
    fun testSponsorWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(service.getSponsor("id")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<SponsorModel>> = dataSource.getFromApi("id").single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testSponsorWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(service.getSponsor("id")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = SponsorDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<SponsorModel>> = dataSource.getFromApi("id").single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion
}