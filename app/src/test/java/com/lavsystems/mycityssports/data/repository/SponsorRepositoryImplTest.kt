package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.SponsorDataSource
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class SponsorRepositoryImplTest : BaseTest() {

    @Mock
    private lateinit var dataSource: SponsorDataSource
    private lateinit var repository: SponsorRepository

    override fun setUp() {
        super.setUp()

    }

    //region SPONSORS
    @Test
    fun testSponsorsWhenIsSuccess_ShouldReturnSponsorsDtoList() = runBlocking {
        val sponsorDtoList = mutableListOf(
            SponsorDto(
                "id",
                getSponsors(2)
            )
        )
        Mockito.`when`(dataSource.getSponsorsByCity()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(sponsorDtoList)))
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsorsByCity().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(sponsorDtoList, result[1].data?.data)
    }

    @Test
    fun testSponsorsWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.getSponsorsByCity()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsorsByCity().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testSponsorsWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getSponsorsByCity()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsorsByCity().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion

    //region SPONSOR
    @Test
    fun testSponsorWhenIsSuccess_ShouldReturnSponsor() = runBlocking {
        val sponsor = SponsorModel()
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(sponsor)))
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsor("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(sponsor, result[1].data?.data)
    }

    @Test
    fun testSponsorWhenIsError_ShouldReturnError() = runBlocking {
        val sponsor = SponsorModel()
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsor("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testSponsorWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getFromApi("id")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = SponsorRepositoryImpl(dataSource)

        val result = repository.getSponsor("id").toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}