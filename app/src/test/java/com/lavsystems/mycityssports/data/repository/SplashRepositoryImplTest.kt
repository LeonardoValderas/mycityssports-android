package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.SplashDataSource
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

internal class SplashRepositoryImplTest: BaseTest(){
    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
    }

    @Mock
    private lateinit var dataSource: SplashDataSource
    private lateinit var splashRepository: SplashRepository

    override fun setUp() {
        super.setUp()
    }

    @Test
    fun testCityExistsWhenIsSuccessTrue_ShouldBeSuccessTrue() = runBlocking {
        Mockito.`when`(dataSource.cityExists()).thenReturn(flowOf(IS_TRUE))
        splashRepository = SplashRepositoryImpl(dataSource)
        val result = splashRepository.cityExists().single()
        Assert.assertEquals(
            IS_TRUE,
           result
        )
    }

    @Test
    fun testCityExistsWhenIsSuccessFalse_ShouldBeSuccessFalse() = runBlocking {
        Mockito.`when`(dataSource.cityExists()).thenReturn(flowOf(IS_FALSE))
        splashRepository = SplashRepositoryImpl(dataSource)
        val result = splashRepository.cityExists().single()
        Assert.assertEquals(
            IS_FALSE,
            result
        )
    }

}