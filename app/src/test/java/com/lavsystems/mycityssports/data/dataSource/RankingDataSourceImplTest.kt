package com.lavsystems.mycityssports.data.dataSource

import ProvidesTestAndroidModels.getRanking
import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.service.RankingService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class RankingDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: RankingService
    private lateinit var dataSource: RankingDataSource

    override fun setUp() {
        super.setUp()
    }

    //region RANKING API
    @Test
    fun testRankingWhenIsSuccess_ShouldReturnRankingList() = runBlocking {
        val news = RankingDto(
            getRanking(2, 2),
            getSponsors(3)
        )
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getRanking(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )
        ).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(news)
                    )
                )
            )
        )
        dataSource = RankingDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<RankingDto>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(news, body?.data)
    }

    @Test
    fun testRankingWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getRanking(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = RankingDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<RankingDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testRankingWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getRanking(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = RankingDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<RankingDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region CACHE
    @Test
    fun testRankingCacheWhenSaveLocal_ShouldReturnSavedList() = runBlocking {
        val news = RankingDto(
            getRanking(2, 2),
            getSponsors(3)
        )
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = RankingDataSourceImpl(service, preferences)
        dataSource.saveLocal(news)

        val cacheList = dataSource.getFromLocal().single()
        assertEquals(news, cacheList)
    }

    @Test
    fun testRankingCacheWhenRemoveLocal_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = RankingDataSourceImpl(service, preferences)
        dataSource.removeLocal()

        val cacheList = dataSource.getFromLocal().single()
        assertNull(cacheList)
    }

    //endregion
}