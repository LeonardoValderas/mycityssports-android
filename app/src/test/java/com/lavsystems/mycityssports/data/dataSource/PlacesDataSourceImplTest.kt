package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.service.PlaceService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class PlacesDataSourceImplTest : BaseTest() {
    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
        const val IS_ERROR = "Error"
        const val CODE = 500
    }

    @Mock
    private lateinit var preferences: PreferencesRepository

    @Mock
    private lateinit var service: PlaceService
    private lateinit var dataSource: PlacesDataSource

    override fun setUp() {
        super.setUp()
    }

    //region PLACES
    @Test
    fun testPlacesWhenIsSuccess_ShouldReturnPlaceDto() = runBlocking {
        val placeDto = PlaceDto(mutableListOf(), mutableListOf())
        Mockito.`when`(service.getPlacesData()).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(placeDto)
                    )
                )
            )
        )
        dataSource = PlacesDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<PlaceDto>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(placeDto, body?.data)
    }

    @Test
    fun testPlacesWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(service.getPlacesData()).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(CODE, ResponseBody.create(null, IS_ERROR))
                )
            )
        )
        dataSource = PlacesDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<PlaceDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testPlacesWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(service.getPlacesData()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = PlacesDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<PlaceDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region CITY
    @Test
    fun testSaveCityWhenIsSuccess_ShouldReturnTrue() = runBlocking {
        val city = CityModel()
        Mockito.`when`(preferences.saveCity(city)).thenReturn(flowOf(IS_TRUE))
        dataSource = PlacesDataSourceImpl(service, preferences)
        val result = dataSource.saveCity(city).single()
        assertEquals(
            IS_TRUE,
            result
        )
    }

    @Test
    fun testSaveCityWhenIsError_ShouldReturnFalse() = runBlocking {
        val city = CityModel()
        Mockito.`when`(preferences.saveCity(city)).thenReturn(flowOf(IS_FALSE))
        dataSource = PlacesDataSourceImpl(service, preferences)
        val result = dataSource.saveCity(city).single()
        assertEquals(
            IS_FALSE,
            result
        )
    }
    //endregion
}