package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getFixture
import ProvidesTestAndroidModels.getFixtureInstanceItemModel
import ProvidesTestAndroidModels.getFixtureZoneItemModel
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.FixtureDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class FixtureRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: FixtureDataSource
    private lateinit var repository: FixtureRepository

    override fun setUp() {
        super.setUp()
    }

    //region TABLE
    @Test
    fun testFixtureWhenIsInstitutionOrCityEmpty_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            true
        )
        repository = FixtureRepositoryImpl(dataSource)

        val result = repository.getFixtures(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(null, result.first().data)
    }

    @Test
    fun testFixtureWhenIsRefreshSuccess_ShouldReturnFixtureDto() = runBlocking {
        val fixtureModel = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(fixtureModel)))
            )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                fixtureModel
            )
        )

        repository = FixtureRepositoryImpl(dataSource)

        val result = repository.getFixtures(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(fixtureModel, result[1].data)
    }

    @Test
    fun testFixtureWhenIsNotRefreshSuccess_ShouldReturnNewDtoCache() = runBlocking {
        val fixtureModel = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                fixtureModel
            )
        )

        repository = FixtureRepositoryImpl(dataSource)

        val result = repository.getFixtures(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(fixtureModel, result[1].data)
    }

    @Test
    fun testFixtureWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = FixtureRepositoryImpl(dataSource)

        val result = repository.getFixtures(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testFixtureWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = FixtureRepositoryImpl(dataSource)

        val result = repository.getFixtures(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}