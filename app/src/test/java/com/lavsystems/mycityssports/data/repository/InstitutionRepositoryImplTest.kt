package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getInstitutions
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.InstitutionDataSource
import com.lavsystems.mycityssports.data.dataSource.SponsorDataSource
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class InstitutionRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: InstitutionDataSource
    private lateinit var repository: InstitutionRepository

    override fun setUp() {
        super.setUp()
    }

    //region INSTITUTIONS
    @Test
    fun testInstitutionsWhenIsInstitutionOrCityEmpty_ShouldReturnInstitutionsListEmpty() = runBlocking {
        Mockito.`when`(dataSource.isInstitutionOrCityEmpty()).thenReturn(
            true
        )
        repository = InstitutionRepositoryImpl(dataSource)

        val result = repository.getInstitutions(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(mutableListOf<InstitutionModel>(), result.first().data)
    }

    @Test
    fun testInstitutionsWhenIsRefreshSuccess_ShouldReturnInstitutionsList() = runBlocking {
        val institutionsApi = getInstitutions(2)

        Mockito.`when`(dataSource.isInstitutionOrCityEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(institutionsApi)))
                )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                institutionsApi
            )
        )

        repository = InstitutionRepositoryImpl(dataSource)

        val result = repository.getInstitutions(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(institutionsApi, result[1].data)
    }

    @Test
    fun testInstitutionsWhenIsNotRefreshSuccess_ShouldReturnInstitutionsList() = runBlocking {
        val institutionsCache = getInstitutions(2)

        Mockito.`when`(dataSource.isInstitutionOrCityEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                institutionsCache
            )
        )

        repository = InstitutionRepositoryImpl(dataSource)

        val result = repository.getInstitutions(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(institutionsCache, result[1].data)
    }


    @Test
    fun testInstitutionsWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.isInstitutionOrCityEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = InstitutionRepositoryImpl(dataSource)

        val result = repository.getInstitutions(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testInstitutionsWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.isInstitutionOrCityEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = InstitutionRepositoryImpl(dataSource)

        val result = repository.getInstitutions(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}