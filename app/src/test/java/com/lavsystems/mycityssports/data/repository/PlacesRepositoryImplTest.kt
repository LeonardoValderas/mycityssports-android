package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.PlacesDataSource
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.CityModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class PlacesRepositoryImplTest : BaseTest() {
    companion object {
        const val IS_TRUE = true
        const val IS_FALSE = false
    }

    @Mock
    private lateinit var dataSource: PlacesDataSource
    private lateinit var repository: PlacesRepository

    override fun setUp() {
        super.setUp()

    }

    //region PLACES
    @Test
    fun testPlacesWhenIsSuccess_ShouldReturnPlaceDto() = runBlocking {
        val placeDto = PlaceDto(mutableListOf(), mutableListOf())
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(placeDto)))
            )
        )
        repository = PlacesRepositoryImpl(dataSource)

        val result = repository.getPlacesData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(placeDto, result[1].data?.data)
    }

    @Test
    fun testPlacesWhenIsError_ShouldReturnError() = runBlocking {
        //val placeDto = PlaceDto(mutableListOf(), mutableListOf())
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = PlacesRepositoryImpl(dataSource)

        val result = repository.getPlacesData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testPlacesWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = PlacesRepositoryImpl(dataSource)

        val result = repository.getPlacesData().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
        //assertEquals(placeDto, result[1].data?.data)
    }
    //endregion

    //region CITY
    @Test
    fun testSaveCityWhenIsSuccess_ShouldBeSuccessTrue() = runBlocking {
        val city = CityModel()
        Mockito.`when`(dataSource.saveCity(city)).thenReturn(
            flowOf(IS_TRUE)
        )
        repository = PlacesRepositoryImpl(dataSource)

        val result = repository.saveCity(city).single()

        assertEquals(IS_TRUE, result)
    }

    @Test
    fun testSaveCityWhenIsError_ShouldBeSuccessFalse() = runBlocking {
        val city = CityModel()
        Mockito.`when`(dataSource.saveCity(city)).thenReturn(
            flowOf(IS_FALSE)
        )
        repository = PlacesRepositoryImpl(dataSource)

        val result = repository.saveCity(city).single()

        assertEquals(IS_FALSE, result)
    }
    //endregion
}