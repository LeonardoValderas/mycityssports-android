package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getSponsors
import ProvidesTestAndroidModels.getTable
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.TableDataSource
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class TableRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: TableDataSource
    private lateinit var repository: TableRepository

    override fun setUp() {
        super.setUp()
    }

    //region TABLE
    @Test
    fun testTableWhenIsInstitutionOrCityEmpty_ShouldReturnNull() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            true
        )
        repository = TableRepositoryImpl(dataSource)

        val result = repository.getTable(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(null, result.first().data)
    }

    @Test
    fun testTableWhenIsRefreshSuccess_ShouldReturnTableDto() = runBlocking {
        val tableDto = TableDto(
            getTable(2,2,0),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(tableDto)))
            )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                tableDto
            )
        )

        repository = TableRepositoryImpl(dataSource)

        val result = repository.getTable(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(tableDto, result[1].data)
    }

    @Test
    fun testTableWhenIsNotRefreshSuccess_ShouldReturnNewDtoCache() = runBlocking {
        val newsDto = TableDto(
            getTable(0,2, 1),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = TableRepositoryImpl(dataSource)

        val result = repository.getTable(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }

    @Test
    fun testTableWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = TableRepositoryImpl(dataSource)

        val result = repository.getTable(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testTableWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = TableRepositoryImpl(dataSource)

        val result = repository.getTable(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}