package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.network.api.service.MyCitysSportsContactService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class MyCitysSportsContactDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: MyCitysSportsContactService
    private lateinit var dataSource: MyCitysSportsContactDataSource

    override fun setUp() {
        super.setUp()
    }

    //region CONTACT
    @Test
    fun testContactWhenIsSuccess_ShouldReturnContactModel() = runBlocking {
        val contactModel = MyCitysSportsContactModel()
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getContact()).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(contactModel)
                    )
                )
            )
        )
        dataSource = MyCitysSportsContactDataSourceImpl(service)

        val result: ApiResponse<ApiResponseData<MyCitysSportsContactModel>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(contactModel, body?.data)
    }

    @Test
    fun testContactWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(service.getContact()).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = MyCitysSportsContactDataSourceImpl(service)
        val result: ApiResponse<ApiResponseData<MyCitysSportsContactModel>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testContactWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(service.getContact()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = MyCitysSportsContactDataSourceImpl(service)
        val result: ApiResponse<ApiResponseData<MyCitysSportsContactModel>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion
}