package com.lavsystems.mycityssports.data.dataSource

import ProvidesTestAndroidModels.getSponsors
import ProvidesTestAndroidModels.getTable
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.service.TableService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class TableDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: TableService
    private lateinit var dataSource: TableDataSource

    override fun setUp() {
        super.setUp()
    }

    //region TABLE API
    @Test
    fun testInstitutionsWhenIsSuccess_ShouldReturnInstitutionsList() = runBlocking {
        val table = TableDto(
            getTable(2, 2, 0),
            getSponsors(3)
        )
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getTable(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )
        ).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(table)
                    )
                )
            )
        )
        dataSource = TableDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<TableDto>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(table, body?.data)
    }

    @Test
    fun testTableWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getTable(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = TableDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<TableDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testTableWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIdsOrThrowException()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getTable(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString(),
            ArgumentMatchers.anyString()
        )).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = TableDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<TableDto>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region CACHE
    @Test
    fun testTableCacheWhenSaveLocal_ShouldReturnSavedList() = runBlocking {
        val table = TableDto(
            getTable(0, 2, 1),
            getSponsors(3)
        )
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = TableDataSourceImpl(service, preferences)
        dataSource.saveLocal(table)

        val cacheList = dataSource.getFromLocal().single()
        assertEquals(table, cacheList)
    }

    @Test
    fun testTableCacheWhenRemoveLocal_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = TableDataSourceImpl(service, preferences)
        dataSource.removeLocal()

        val cacheList = dataSource.getFromLocal().single()
        assertNull(cacheList)
    }

    //endregion
}