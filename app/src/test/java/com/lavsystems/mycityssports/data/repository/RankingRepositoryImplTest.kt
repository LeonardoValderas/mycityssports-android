package com.lavsystems.mycityssports.data.repository

import ProvidesTestAndroidModels.getRanking
import ProvidesTestAndroidModels.getSponsors
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.RankingDataSource
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class RankingRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: RankingDataSource
    private lateinit var repository: RankingRepository

    override fun setUp() {
        super.setUp()
    }

    //region NEWS
    @Test
    fun testRankingWhenIsInstitutionOrCityEmpty_ShouldReturnRankingDtoEmpty() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            true
        )
        repository = RankingRepositoryImpl(dataSource)

        val result = repository.getRanking(true).toList()

        assertEquals(Resource.Status.SUCCESS, result.first().status)
        assertEquals(null, result.first().data)
    }

    @Test
    fun testRankingWhenIsRefreshSuccess_ShouldReturnNewDto() = runBlocking {
        val newsDto = RankingDto(
            getRanking(2, 2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(newsDto)))
            )
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = RankingRepositoryImpl(dataSource)

        val result = repository.getRanking(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }

    @Test
    fun testRankingWhenIsNotRefreshSuccess_ShouldReturnNewDtoCache() = runBlocking {
        val newsDto = RankingDto(
            getRanking(2, 2),
            getSponsors(2)
        )

        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                newsDto
            )
        )

        repository = RankingRepositoryImpl(dataSource)

        val result = repository.getRanking(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(newsDto, result[1].data)
    }


    @Test
    fun testRankingWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )

        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = RankingRepositoryImpl(dataSource)

        val result = repository.getRanking(true).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testRankingWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.preferencesIsEmpty()).thenReturn(
            false
        )
        Mockito.`when`(dataSource.getFromLocal()).thenReturn(
            flowOf(
                null
            )
        )
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = RankingRepositoryImpl(dataSource)

        val result = repository.getRanking(false).toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
    //endregion
}