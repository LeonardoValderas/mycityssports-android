package com.lavsystems.mycityssports.data.dataSource

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.network.api.service.HomeService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.DivisionSubDivisionModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class HomeDataSourceImplTest : BaseTest() {

    companion object {
        const val IS_ERROR = "Error"
        const val ID_STRING = "ID"
        const val CODE = 500
    }

    @Mock
    private lateinit var preferences: PreferencesRepository

    @Mock
    private lateinit var service: HomeService
    private lateinit var dataSource: HomeDataSource

    override fun setUp() {
        super.setUp()
    }

    //region HOME
    @Test
    fun testHomeWhenIsSuccess_ShouldReturnHomeModel() = runBlocking {
        val model = HomeItemDto()
        Mockito.`when`(preferences.getCity()).thenReturn(
            flowOf(
                CityModel().copy(id = "cityId")
            )
        )
        Mockito.`when`(service.getHomeData("cityId")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(model)
                    )
                )
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<HomeItemDto>> = dataSource.getHomeData().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(model, body?.data)
    }

    @Test
    fun testHomeWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getCity()).thenReturn(
            flowOf(
                CityModel().copy(id = "cityId")
            )
        )
        Mockito.`when`(service.getHomeData("cityId")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        InstitutionDataSourceImplTest.CODE, ResponseBody.create(
                            null,
                            InstitutionDataSourceImplTest.IS_ERROR
                        )
                    )
                )
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<HomeItemDto>> = dataSource.getHomeData().single()
        val error = (result as ApiErrorResponse)

        assertEquals(InstitutionDataSourceImplTest.IS_ERROR, error.errorMessage)
        assertEquals(InstitutionDataSourceImplTest.CODE, error.statusCode)
    }

    @Test
    fun testHomeWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getCity()).thenReturn(
            flowOf(
                CityModel().copy(id = "cityId")
            )
        )
        Mockito.`when`(service.getHomeData("cityId")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(InstitutionDataSourceImplTest.IS_ERROR))
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<HomeItemDto>> = dataSource.getHomeData().single()
        val error = (result as ApiErrorResponse)

        assertEquals(InstitutionDataSourceImplTest.IS_ERROR, error.errorMessage)
        assertEquals(InstitutionDataSourceImplTest.CODE, error.statusCode)
    }
//endregion

    //region CITY
    @Test
    fun testCityWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.removeCity()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeCity().single()

        assertEquals(true, result)
    }

    @Test
    fun testCityWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.removeCity()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeCity().single()

        assertEquals(false, result)
    }

    //endregion

    //region INSTITUTION
    @Test
    fun testInstitutionWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveInstitutionId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testInstitutionWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.saveInstitutionId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveInstitutionId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testInstitutionWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(preferences.getInstitutionId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getInstitutionId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testInstitutionWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getInstitutionId()).thenReturn(
            flowOf(
                null
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getInstitutionId().single()

        assertEquals(null, result)
    }

    //endregion

    //region SPORT
    @Test
    fun testSportWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveSportId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testSportWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.saveSportId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveSportId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testSportWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(preferences.getSportId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getSportId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testSportWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getSportId()).thenReturn(
            flowOf(
                null
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getSportId().single()

        assertEquals(null, result)
    }

    @Test
    fun testSportWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.removeSportId()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeSportId().single()

        assertEquals(true, result)
    }

    @Test
    fun testSportWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.removeSportId()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeSportId().single()

        assertEquals(false, result)
    }

    //endregion

    //region TOURNAMENT
    @Test
    fun testTournamentWhenIsSave_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.saveTournamentId(Mockito.anyString())).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveTournamentId(Mockito.anyString()).single()

        assertEquals(true, result)
    }

    @Test
    fun testTournamentWhenIsSave_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.saveTournamentId(Mockito.anyString())).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveTournamentId(Mockito.anyString()).single()

        assertEquals(false, result)
    }

    @Test
    fun testTournamentWhenIsGetId_ShouldReturnId() = runBlocking {
        Mockito.`when`(preferences.getTournamentId()).thenReturn(
            flowOf(
                ID_STRING
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getTournamentId().single()

        assertEquals(ID_STRING, result)
    }

    @Test
    fun testTournamentWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getTournamentId()).thenReturn(
            flowOf(
                null
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getTournamentId().single()

        assertEquals(null, result)
    }

    @Test
    fun testTournamentWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.removeTournamentId()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeTournamentId().single()

        assertEquals(true, result)
    }

    @Test
    fun testTournamentWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.removeTournamentId()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeTournamentId().single()

        assertEquals(false, result)
    }

    //endregion

    //region DIVISION SUBDIVISION
    @Test
    fun testDivisionSubDivisionWhenIsSave_ShouldReturnTrue() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(preferences.saveDivisionSubDivision(model)).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveDivisionSubDivision(model).single()

        assertEquals(true, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsSave_ShouldReturnFalse() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(preferences.saveDivisionSubDivision(model)).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.saveDivisionSubDivision(model).single()

        assertEquals(false, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsGetId_ShouldReturnId() = runBlocking {
        val model = DivisionSubDivisionModel()
        Mockito.`when`(preferences.getDivisionSubDivision()).thenReturn(
            flowOf(
                model
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getDivisionSubDivision().single()

        assertEquals(model, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsGetId_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getDivisionSubDivision()).thenReturn(
            flowOf(
                null
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.getDivisionSubDivision().single()

        assertEquals(null, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsRemove_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.removeDivisionSubDivision()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeDivisionSubDivision().single()

        assertEquals(true, result)
    }

    @Test
    fun testDivisionSubDivisionWhenIsRemove_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.removeDivisionSubDivision()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.removeDivisionSubDivision().single()

        assertEquals(false, result)
    }

    //endregion

    //region ALERT
    @Test
    fun testAlertWhenShow_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.showHomeInformationAlert()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.showHomeInformationAlert().single()

        assertEquals(true, result)
    }

    @Test
    fun testAlertWhenShow_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.showHomeInformationAlert()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.showHomeInformationAlert().single()

        assertEquals(false, result)
    }

    @Test
    fun testAlertWhenShowed_ShouldReturnTrue() = runBlocking {
        Mockito.`when`(preferences.homeInformationAlertDisplayed()).thenReturn(
            flowOf(
                true
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.homeInformationAlertDisplayed().single()

        assertEquals(true, result)
    }

    @Test
    fun testAlertWhenShowed_ShouldReturnFalse() = runBlocking {
        Mockito.`when`(preferences.homeInformationAlertDisplayed()).thenReturn(
            flowOf(
                false
            )
        )
        dataSource = HomeDataSourceImpl(service, preferences)
        val result = dataSource.homeInformationAlertDisplayed().single()

        assertEquals(false, result)
    }

    //endregion
}