package com.lavsystems.mycityssports.data.repository

import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dataSource.MyCitysSportsContactDataSource
import com.lavsystems.mycityssports.data.network.resource.Resource
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

@ExperimentalCoroutinesApi
class MyCitysSportsContactRepositoryImplTest: BaseTest() {

    @Mock
    private lateinit var dataSource: MyCitysSportsContactDataSource
    private lateinit var repository: MyCitysSportsContactRepository

    override fun setUp() {
        super.setUp()
    }

    //region CONTACT

    @Test
    fun testContactWhenIsRefreshSuccess_ShouldReturnContactModel() = runBlocking {
        val model = MyCitysSportsContactModel().copy(description = "leis")
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.success(ApiResponseData(model)))
            )
        )

        repository = MyCitysSportsContactRepositoryImpl(dataSource)

        val result = repository.getContact().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.SUCCESS, result[1].status)
        assertEquals(model, result[1].data?.data)
    }

    @Test
    fun testContactWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Response.error(404, ResponseBody.create(null, "Error")))
            )
        )
        repository = MyCitysSportsContactRepositoryImpl(dataSource)

        val result = repository.getContact().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }

    @Test
    fun testContactWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(dataSource.getFromApi()).thenReturn(
            flowOf(
                ApiResponse.create(Throwable())
            )
        )
        repository = MyCitysSportsContactRepositoryImpl(dataSource)

        val result = repository.getContact().toList()

        assertEquals(Resource.Status.LOADING, result[0].status)
        assertEquals(Resource.Status.ERROR, result[1].status)
    }
//endregion
}