package com.lavsystems.mycityssports.data.dataSource

import ProvidesTestAndroidModels.getInstitutions
import com.lavsystems.mycityssports.base.BaseTest
import com.lavsystems.mycityssports.data.dto.PreferenceDataDto
import com.lavsystems.mycityssports.data.network.api.service.InstitutionService
import com.lavsystems.mycityssports.data.network.response.ApiErrorResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponse
import com.lavsystems.mycityssports.data.network.response.ApiResponseData
import com.lavsystems.mycityssports.data.network.response.ApiSuccessResponse
import com.lavsystems.mycityssports.data.repository.PreferencesRepository
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import retrofit2.Response

class InstitutionDataSourceImplTest: BaseTest() {
    companion object {
        const val IS_ERROR = "Error"
        const val CODE = 500
    }
    @Mock
    private lateinit var preferences: PreferencesRepository
    @Mock
    private lateinit var service: InstitutionService
    private lateinit var dataSource: InstitutionDataSource

    override fun setUp() {
        super.setUp()
    }

    //region INSTITUTIONS API
    @Test
    fun testInstitutionsWhenIsSuccess_ShouldReturnInstitutionsList() = runBlocking {
        val institutions = getInstitutions(2)
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getInstitutions("")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.success(
                        ApiResponseData(institutions)
                    )
                )
            )
        )
        dataSource = InstitutionDataSourceImpl(service, preferences)

        val result: ApiResponse<ApiResponseData<MutableList<InstitutionModel>>> = dataSource.getFromApi().single()
        val body = (result as ApiSuccessResponse).body

        assertEquals(true, body?.success)
        assertEquals(institutions, body?.data)
    }

    @Test
    fun testInstitutionsWhenIsError_ShouldReturnError() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getInstitutions("")).thenReturn(
            flowOf(
                ApiResponse.create(
                    Response.error(
                        CODE, ResponseBody.create(null,
                            IS_ERROR
                        ))
                )
            )
        )
        dataSource = InstitutionDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<MutableList<InstitutionModel>>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }

    @Test
    fun testInstitutionsWhenIsFailure_ShouldReturnFailure() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        Mockito.`when`(service.getInstitutions("")).thenReturn(
            flowOf(
                ApiResponse.create(Throwable(IS_ERROR))
            )
        )
        dataSource = InstitutionDataSourceImpl(service, preferences)
        val result: ApiResponse<ApiResponseData<MutableList<InstitutionModel>>> = dataSource.getFromApi().single()
        val error = (result as ApiErrorResponse)

        assertEquals(IS_ERROR, error.errorMessage)
        assertEquals(CODE,  error.statusCode)
    }
    //endregion

    //region CACHE
    @Test
    fun testInstitutionsCacheWhenSaveLocal_ShouldReturnSavedList() = runBlocking {
        val institutions = getInstitutions(2)
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = InstitutionDataSourceImpl(service, preferences)
        dataSource.saveLocal(institutions)

        val cacheList = dataSource.getFromLocal().single()
        assertEquals(institutions, cacheList)
    }

    @Test
    fun testInstitutionsCacheWhenRemoveLocal_ShouldReturnNull() = runBlocking {
        Mockito.`when`(preferences.getAllPreferenceIds()).thenReturn(
            flowOf(
                PreferenceDataDto()
            )
        )
        dataSource = InstitutionDataSourceImpl(service, preferences)
        dataSource.removeLocal()

        val cacheList = dataSource.getFromLocal().single()
        assertNull(cacheList)
    }

    //endregion
}