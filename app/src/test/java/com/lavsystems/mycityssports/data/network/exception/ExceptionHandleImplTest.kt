package com.lavsystems.mycityssports.data.network.exception

import android.database.sqlite.SQLiteConstraintException
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.base.BaseTest
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownServiceException

class ExceptionHandleImplTest : BaseTest() {

    private lateinit var exceptionHandle: ExceptionHandle

    companion object {
        const val ERROR_TEXT = "error_text"
        const val CODE_500 = 500
        const val CODE_400 = 400
    }

    override fun setUp() {
        super.setUp()
    }

    @Test
    fun testMessageIntResourceWhenIsNoConnectivityException_ShouldReturnInternetResource() =
        runBlocking {
            exceptionHandle = ExceptionHandleImpl(NoConnectivityException())
            assertEquals(R.string.without_internet, exceptionHandle.getMessageIntResource())
        }

    @Test
    fun testMessageIntResourceWhenIsSocketTimeoutException_ShouldReturnInternetResource() =
        runBlocking {
            exceptionHandle = ExceptionHandleImpl(SocketTimeoutException())
            assertEquals(R.string.without_internet, exceptionHandle.getMessageIntResource())
        }

    @Test
    fun testMessageIntResourceWhenIsSQLiteConstraintException_ShouldReturnDefaultResource() =
        runBlocking {
            exceptionHandle = ExceptionHandleImpl(SQLiteConstraintException())
            assertEquals(R.string.known_error, exceptionHandle.getMessageIntResource())
        }

    @Test
    fun testMessageIntResourceWhenIsIllegalStateException_ShouldReturnDefaultResource() =
        runBlocking {
            exceptionHandle = ExceptionHandleImpl(IllegalStateException(ERROR_TEXT))
            assertEquals(R.string.known_error, exceptionHandle.getMessageIntResource())
        }

    @Test
    fun testMessageIntResourceWhenIsConnectException_ShouldReturnDefaultResource() = runBlocking {
        exceptionHandle = ExceptionHandleImpl(ConnectException())
        assertEquals(R.string.known_error, exceptionHandle.getMessageIntResource())
    }

    @Test
    fun testMessageIntResourceWhenIsUnknownServiceException_ShouldReturnDefaultResource() =
        runBlocking {
            exceptionHandle = ExceptionHandleImpl(UnknownServiceException())
            assertEquals(R.string.known_error, exceptionHandle.getMessageIntResource())
        }

    @Test
    fun testMessageIntResourceWhenIsHttpException_ShouldReturn500Resource() = runBlocking {
        val response = Response.error<Int>(
            CODE_500, ResponseBody.create(
                null,
                ERROR_TEXT
            )
        )
        exceptionHandle = ExceptionHandleImpl(HttpException(response))
        assertEquals (R.string.response_500, exceptionHandle.getMessageIntResource())
    }

    @Test
    fun testMessageIntResourceWhenIsHttpException_ShouldReturn400Resource() = runBlocking {
        val response = Response.error<Int>(
            CODE_400, ResponseBody.create(
                null,
                ERROR_TEXT
            )
        )
        exceptionHandle = ExceptionHandleImpl(HttpException(response))
        assertEquals (R.string.response_400, exceptionHandle.getMessageIntResource())
    }

//    @Test
//    fun testMessageIntResourceWhenIsTimeoutCancellationException_ShouldReturnDefaultResource() = runBlocking {
//        exceptionHandle = ExceptionHandleImpl(TimeoutCancellationException())
//        assertEquals(R.string.known_error, exceptionHandle.getMessageIntResource())
//    }

    @Test
    fun testExceptionStackTraceWhenIsException_ShouldContainsErrorText() = runBlocking {
        exceptionHandle = ExceptionHandleImpl(IllegalStateException(ERROR_TEXT))
        assertTrue(exceptionHandle.getExceptionStackTrace().contains(ERROR_TEXT))
    }

}