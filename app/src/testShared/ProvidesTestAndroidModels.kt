import com.lavsystems.mycityssports.data.dto.HomeDto
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.mvvm.model.*
import java.util.*

object ProvidesTestAndroidModels {

    fun getStates(): MutableList<StateModel> {
        return mutableListOf(
            StateModel("1", "Cordoba"),
            StateModel("2", "Rio Negro"),
            StateModel("3", "Buenos Aires"),
            StateModel("4", "Rosario")
        )
    }

    fun getCities(): MutableList<CityModel> {
        return mutableListOf(
            CityModel("1", "Cordoba", getStates()[0], PositionModel("-30", "-40")),
            CityModel("1", "Bariloche", getStates()[1], PositionModel("-35", "-45"))
        )
    }

    fun getSponsors(count: Int): MutableList<SponsorModel> {
        val list = mutableListOf<SponsorModel>()
        for (i in 1..count) {
            list.add(
                SponsorModel(
                    "$i",
                    "nameSponsor$i",
                    AddressModel(
                        "street$i",
                        "number$i",
                        "neigh$i",
                        PositionModel(
                            "lat$i",
                            "long$i"
                        ),
                        "comple$i"
                    ),
                    "phone$i",
                    "mobile$i",
                    "email$i",
                    "web$i",
                    "facebook$i",
                    "instagram$i",
                    "supportMessage$i",
                    "urlImage$i",
                    getInstitutions(1)[0]
                )
            )
        }

        return list
    }

    fun getInstitutions(count: Int): MutableList<InstitutionModel> {
        val list = mutableListOf<InstitutionModel>()
        for (i in 1..count) {
            list.add(
                InstitutionModel(
                    "$i",
                    "nameInstitution$i",
                    "description$i",
                    AddressModel(
                        "street$i",
                        "number$i",
                        "neigh$i",
                        PositionModel(
                            "lat$i",
                            "long$i"
                        ),
                        "comple$i"
                    ),
                    CityModel(
                        "$i",
                        "nameCity$i",
                        StateModel(
                            "$i",
                            "nameState$i"
                        ),
                        PositionModel(
                            "lat$i",
                            "long$i"
                        )
                    ),
                    "url$i",
                    true,
                    "phone$i",
                    "mobile$i",
                    "email$i",
                    "web$i",
                    "facebook$i",
                    "instagram$i",
                    "source$i"
                )
            )
        }

        return list
    }

    fun getMyCitysSportsContactModel(): MyCitysSportsContactModel {
        return MyCitysSportsContactModel(
            "mcsId",
            "mcsName",
            "mcsDescription",
            AddressModel(
                "mcsAddressStreet", "mcsAddressNumber", "mcsAddressNeigh", PositionModel(
                    "mcsPositionLat",
                    "mcsPositionLong"
                ), "mcsAddressComplement"
            ),
            "mcsPhone",
            "mcsMobile",
            "mcsEmail",
            "mcsWeb",
            "mcsFacebook",
            "mcsInstagram"
        )
    }

    fun getFixtureItemModel(): FixtureItemModel {
        return FixtureItemModel(
            "fixtureItemId",
            "fixtureItemDateTime",
            1,
            "fixtureItemSport",
            "fixtureItemTournament",
            "fixtureItemDivision",
            TournamentZoneModel(
                "fixtureItemZoneId",
                "fixtureItemZoneName",
                "fixtureItemZoneInstitution"
            ),
            TournamentInstanceModel(
                "fixtureItemInstanceId",
                "fixtureItemInstanceName",
                1
            ),
            TeamModel(
                "fixtureItemHomeTeamId",
                "fixtureItemHomeTeamName",
                "fixtureItemHomeTeamSport",
                "fixtureItemHomeTeamUrl",
            ),
            mutableListOf(
                FixtureRefereeModel(
                    RefereeModel(
                        "fixtureItemRefereeModelId",
                        "fixtureItemRefereeModelName"
                    ),
                    RefereeFunctionModel(
                        "fixtureItemRefereeFunctionModelId",
                        "fixtureItemRefereeFunctionModelName"
                    )
                )
            ),
            "2",
            mutableListOf(
                ScorerModel(
                    PlayerModel(
                        "fixtureItemHomeScorerPlayerModelId",
                        "fixtureItemHomeScorerPlayerModelName",
                        "fixtureItemHomeScorerPlayerModelImage"
                    ),
                    10,
                    false
                )
            ),
            mutableListOf(
                CardPlayerModel(
                    CardModel(
                        "fixtureItemHomeCardModelId",
                        "fixtureItemHomeCardModelName",
                        "fixtureItemHomeCardModelRes",
                        CardModel.CharLetter.Y
                    ),
                    PlayerModel(
                        "fixtureItemHomePlayerModelId",
                        "fixtureItemHomePlayerModelName",
                        "fixtureItemHomePlayerModelUrl"
                    ),
                    9
                )
            ),
            "1",
            TeamModel(
                "fixtureItemVisitorsTeamId",
                "fixtureItemVisitorsTeamName",
                "fixtureItemVisitorsTeamSport",
                "fixtureItemVisitorsTeamUrl",
            ),
            "2",
            mutableListOf(
                ScorerModel(
                    PlayerModel(
                        "fixtureItemVisitorsScorerPlayerModelId",
                        "fixtureItemVisitorsScorerPlayerModelName",
                        "fixtureItemVisitorsScorerPlayerModelImage"
                    ),
                    10,
                    false
                )
            ),
            mutableListOf(
                CardPlayerModel(
                    CardModel(
                        "fixtureItemVisitorsCardModelId",
                        "fixtureItemVisitorsCardModelName",
                        "fixtureItemVisitorsCardModelRes",
                        CardModel.CharLetter.R
                    ),
                    PlayerModel(
                        "fixtureItemVisitorsPlayerModelId",
                        "fixtureItemVisitorsPlayerModelName",
                        "fixtureItemVisitorsPlayerModelUrl"
                    ),
                    9
                )
            ),
            "2",
            PlayingFieldModel(
                "fixtureItemPlayingFieldModelId",
                "fixtureItemPlayingFieldModelName",
                "fixtureItemPlayingFieldModelStreet",
                "fixtureItemPlayingFieldModelNumber",
                "fixtureItemPlayingFieldModelNeigh",
                PositionModel(
                    "-20",
                    "-40"
                )
            ),
            "fixtureItemComment",
            true,
            "fixtureItemSource"
        )
    }

    fun getCity(): CityModel {
        return CityModel(
            "cityModelId",
            "cityModelName",
            StateModel(
                "stateModelId",
                "stateModelName",
            ),
            PositionModel(
                "-35",
                "-31"
            )
        )
    }

    fun getNews(count: Int): MutableList<NewsModel> {
        val list = mutableListOf<NewsModel>()
        for (i in 1..count) {
            list.add(
                NewsModel(
                    "newsId$i",
                    "newsTitle$i",
                    "newsBody$i",
                    "newsStart$i",
                    "newsExpiration$i",
                    "newsUrlImage$i",
                    "newsSource$i"
                )
            )
        }

        return list
    }

    fun getSanctions(count: Int): MutableList<SanctionModel> {
        val list = mutableListOf<SanctionModel>()
        for (i in 1..count) {
            list.add(
                SanctionModel(
                    "sanctionId$i",
                    getSport(),
                    getTeam(),
                    getDivision(),
                    getPlayer(),
                    "sanctionMotive$i",
                    "sanctionSanction$i",
                    "sanctionExpiration$i",
                    "sanctionSource$i"
                )
            )
        }

        return list
    }

    private fun getSport(): SportModel {
        return SportModel(
            "sportId",
            "sportName",
            GenderModel(
                "genderName"
            ),
            "sportUrl",
            "sportResource"
        )
    }

    private fun getSports(count: Int): MutableList<SportModel> {
        val list = mutableListOf<SportModel>()
        for (i in 1..count) {
            list.add(
                SportModel(
                    "sportId$i",
                    "sportName$i",
                    GenderModel(
                        "genderName$i"
                    ),
                    "sportUrl$i",
                    "sportResource$i"
                )
            )
        }

        return list
    }

    private fun getTeam(): TeamModel {
        return TeamModel(
            "teamId",
            "teamName",
            "teamSport",
            "teamResource"
        )
    }

    private fun getDivision(): DivisionModel {
        return DivisionModel(
            "divisionId",
            "divisionName",
            "divisionInstitution",
            false
        )
    }

    private fun getPlayer(): PlayerModel {
        return PlayerModel(
            "playerId",
            "playerName",
            "playerUrl"
        )
    }

    fun getRanking(count: Int, countItem: Int): RankingModel {
        return RankingModel(
            "rankingId",
            "rankingTournament",
            "rankingSport",
            "rankingDivision",
            "rankingSubDivision",
            getRankings(count, countItem)
        )
    }

    fun getRankings(count: Int, countItem: Int): MutableList<RankingsModel> {
        val list = mutableListOf<RankingsModel>()
        for (i in 1..count) {
            list.add(
                RankingsModel(
                    "rankingId$i",
                    RankingTypeModel(
                        "rankingTypeId$i",
                        "rankingTypeName$i",
                        "rankingTypeInstitution$i"
                    ),
                    true,
                    getRankingItemModel(countItem)
                )
            )
        }

        return list
    }

    private fun getRankingItemModel(count: Int): MutableList<RankingItemModel> {
        val list = mutableListOf<RankingItemModel>()
        for (i in 1..count) {
            list.add(
                RankingItemModel(
                    "rankingItemId$i",
                    i,
                    getTeam(),
                    getPlayer(),
                    i,
                    "rankingItemComment$i",
                    "rankingItemSource$i"
                )
            )
        }

        return list
    }

    fun getTable(countZone: Int, countInstance: Int, countItem: Int): TableModel {
        return TableModel(
            "tableId",
            "tableTournament",
            "tableSport",
            "tableDivision",
            getTableZoneModel(countZone),
            getTableInstanceModel(countInstance),
            getTableItemModel(countItem),
            "tableComment",
            "tableInstitution",
            "tableDateCreate",
            "tableDateUpdate",
            "tableSource"
        )
    }

    private fun getTableZoneModel(count: Int): MutableList<TableZoneModel> {
        val list = mutableListOf<TableZoneModel>()
        for (i in 1..count) {
            list.add(
                TableZoneModel(
                    TournamentZoneModel(
                        "tournamentZoneId$i",
                        "tournamentZoneName$i",
                        "tournamentZoneInstitution$i",
                    ),
                    getTableItemModel(i)
                )
            )
        }

        return list
    }

    private fun getTableInstanceModel(count: Int): MutableList<TableInstanceModel> {
        val list = mutableListOf<TableInstanceModel>()
        for (i in 1..count) {
            list.add(
                TableInstanceModel(
                    TournamentInstanceModel(
                        "tournamentInstanceId$i",
                        "tournamentInstanceName$i",
                        i
                    ),
                    getTableInstanceItemModel(i)
                )
            )
        }

        return list
    }

    private fun getTableInstanceItemModel(count: Int): MutableList<TableInstanceItemModel> {
        val list = mutableListOf<TableInstanceItemModel>()
        for (i in 1..count) {
            list.add(
                TableInstanceItemModel(
                    "tableInstanceFixture$i",
                    getTeam(),
                    "$i",
                    "$i",
                    getTeam(),
                    "$i",
                    "$i",
                )
            )
        }

        return list
    }

    private fun getTableItemModel(count: Int): MutableList<TableItemModel> {
        val list = mutableListOf<TableItemModel>()
        for (i in 1..count) {
            list.add(
                TableItemModel(
                    getTeam(),
                    i,
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i),
                    getTableTotalModel(1 + i)
                )
            )
        }

        return list
    }

    private fun getTableTotalModel(count: Int): TableTotalModel {
        return TableTotalModel(
            count,
            0,
            count
        )
    }

    fun getFixture(): FixtureModel {
        return FixtureModel(
            UUID.randomUUID().toString(),
            "fixtureTournament",
            "fixtureSport",
            "fixtureDivision",
            mutableListOf(),
            mutableListOf(),
            mutableListOf()
        )
    }

    fun getFixtureInstanceItemModel(
        countInstance: Int,
        countFixture: Int,
        countSponsor: Int
    ): MutableList<FixtureInstanceItemModel> {
        val list = mutableListOf<FixtureInstanceItemModel>()
        for (i in 1..countInstance) {
            list.add(
                FixtureInstanceItemModel(
                    TournamentInstanceModel(
                        "tournamentInstanceId$i",
                        "tournamentInstanceName$i",
                        i
                    ),
                    getFixtureItemModel(countFixture),
                    getSponsors(countSponsor)
                )
            )
        }

        return list
    }

    private fun getFixtureItemModel(count: Int): MutableList<FixtureItemModel> {
        val list = mutableListOf<FixtureItemModel>()
        for (i in 1..count) {
            list.add(
                FixtureItemModel(
                    "fixtureItemId",
                    "fixtureItemDateTime",
                    i,
                    "fixtureItemSport",
                    "fixtureItemTournament",
                    "fixtureItemDivision",
                    TournamentZoneModel(
                        "tournamentZoneId",
                        "tournamentZoneName",
                        "tournamentZoneInstitution"
                    ),
                    TournamentInstanceModel(
                        "tournamentInstanceId",
                        "tournamentInstanceName",
                        i
                    ),
                    getTeam(),
                    mutableListOf(
                        FixtureRefereeModel(
                            RefereeModel(
                                "refereeId",
                                "refereeName"
                            ),
                            RefereeFunctionModel(
                                "refereeFunctionId",
                                "refereeFunctionName"
                            )
                        )
                    ),
                    "fixtureItemHomeResult",
                    mutableListOf(
                        ScorerModel(
                            getPlayer(),
                            10 + i,
                            false
                        )
                    ),
                    mutableListOf(
                        CardPlayerModel(
                            CardModel(
                                "cardId",
                                "cardName",
                                "cardResourceDrawable",
                                CardModel.CharLetter.Y
                            ),
                            getPlayer(),
                            5 + i
                        )
                    ),
                    "fixtureItemHomeResultPenalty",
                    getTeam(),
                    "fixtureItemVisitorsResult",
                    mutableListOf(
                        ScorerModel(
                            getPlayer(),
                            8 + i,
                            false
                        )
                    ),
                    mutableListOf(
                        CardPlayerModel(
                            CardModel(
                                "cardId",
                                "cardName",
                                "cardResourceDrawable",
                                CardModel.CharLetter.Y
                            ),
                            getPlayer(),
                            6 + i
                        )
                    ),
                    "fixtureItemvVisitorsResultPenalty",
                    PlayingFieldModel(

                    ),
                    "fixtureItemComment",
                    true,
                    "fixtureItemSource"
                )
            )
        }

        return list
    }

    fun getFixtureZoneItemModel(
        countZone: Int,
        countFixture: Int,
        countSponsor: Int
    ): MutableList<FixtureZoneItemModel> {
        val list = mutableListOf<FixtureZoneItemModel>()
        for (i in 1..countZone) {
            list.add(
                FixtureZoneItemModel(
                    TournamentZoneModel(
                        "tournamentZoneId$i",
                        "tournamentZoneName$i",
                        "tournamentZoneInstitution$i"
                    ),
                    getFixtureItemModel(countFixture),
                    getSponsors(countSponsor)
                )
            )
        }

        return list
    }

    fun getFixturePointItemModel(
        count: Int,
        countFixture: Int,
        countSponsor: Int
    ): MutableList<FixturePointItemModel> {
        val list = mutableListOf<FixturePointItemModel>()
        for (i in 1..count) {
            list.add(
                FixturePointItemModel(
                    i,
                    getFixtureItemModel(countFixture),
                    getSponsors(countSponsor)
                )
            )
        }

        return list
    }

    fun getHomeItemDto(count: Int, countSport: Int): HomeItemDto {
        val institutions = getInstitutions(count)
        return HomeItemDto(
            institutions,
            getHomeDto(institutions, countSport)
        )
    }

    fun getHomeDto(
        institutions: MutableList<InstitutionModel>,
        countSport: Int
    ): MutableList<HomeDto> {
        val list = mutableListOf<HomeDto>()
        var count = 0
        for (institution in institutions) {
            count++
            list.add(HomeDto(
                institution,
                getSports(countSport + count),
                mutableListOf()
            ))
        }

        return list
    }

    fun getTournaments(
        count: Int,
        countSubDivision: Int,
        countZone: Int,
        countInstance: Int
    ): MutableList<TournamentModel> {
        val list = mutableListOf<TournamentModel>()
        for (i in 1..count) {
            list.add(
                TournamentModel(
                    "tournamentId$i",
                    "tournamentName$i",
                    "tournamentYear$i",
                    i,
                    TournamentTypeModel(
                        "tournamentTypeId$i",
                        "tournamentTypeName$i",
                        "tournamentTypeCode$i"
                    ),
                    getSportString(i),
                    getDivisions(i),
                    getSubDivisions(countSubDivision),
                    getTournamentZones(countZone),
                    getTournamentInstances(countInstance),
                    "institution$i",
                    false

                )
            )
        }

        return list
    }

    private fun getSportString(count: Int): MutableList<String> {
        val list = mutableListOf<String>()
        for (i in 1..count) {
            list.add(
                "sportId$i"
            )
        }
        return list
    }

    private fun getDivisions(count: Int): MutableList<DivisionModel> {
        val list = mutableListOf<DivisionModel>()
        for (i in 1..count) {
            list.add(
                DivisionModel(
                    "divisionId$i",
                    "divisionName$i",
                    "divisionInstitution$i",
                    false
                )
            )
        }
        return list
    }

    private fun getSubDivisions(count: Int): MutableList<String> {
        val list = mutableListOf<String>()
        for (i in 1..count) {
            list.add(
                "subDivision$i"
            )
        }
        return list
    }

    private fun getTournamentZones(count: Int): MutableList<TournamentZoneModel> {
        val list = mutableListOf<TournamentZoneModel>()
        for (i in 1..count) {
            list.add(
                TournamentZoneModel(
                    "tournamentZoneId$i",
                    "tournamentZoneName$i",
                    "tournamentZoneInstitution$i"
                )
            )
        }
        return list
    }

    private fun getTournamentInstances(count: Int): MutableList<TournamentInstanceModel> {
        val list = mutableListOf<TournamentInstanceModel>()
        for (i in 1..count) {
            list.add(
                TournamentInstanceModel(
                    "tournamentInstanceId$i",
                    "tournamentInstanceName$i",
                    i
                )
            )
        }
        return list
    }
}