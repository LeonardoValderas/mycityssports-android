package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getHomeItemDto
import ProvidesTestAndroidModels.getTournaments
import android.content.Context
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.rule.GrantPermissionRule
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.HomeItemDto
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.data.network.api.utils.StatusResponse
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.mvvm.view.adapter.DivisionSubDivisionAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.DivisionsSubDivisionsFromTournamentAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.SportSpinnerAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.TournamentAdapter
import com.lavsystems.mycityssports.mvvm.view.fragment.*
import com.lavsystems.mycityssports.mvvm.viewmodel.HomeViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.EventWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.*
import org.mockito.Mockito.never
import org.mockito.Mockito.times

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class HomeActivityTest : KoinTest {

    object MockitoHelper {
        fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()
    }

    @Mock
    lateinit var viewModel: HomeViewModel

    private var homeDataModel = MutableLiveData<DataResponse<HomeItemDto>>()
    private var institutionsModel = MutableLiveData<MutableList<InstitutionModel>>()
    private var sportsModel = MutableLiveData<MutableList<SportModel>>()

    //    private var divisionsModel = MutableLiveData<MutableList<DivisionModel>>()
    private var divisionsSubDivisionsModel =
        MutableLiveData<MutableList<DivisionSubDivisionModel>>()
    private var tournamentsModel = MutableLiveData<MutableList<TournamentModel>>()

    private var institutionsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    private var sportsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    private var divisionsSubDivisionsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    private var tournamentsIsEmpty = MutableLiveData<EventWrapper<Boolean>>()
    private var changeFragment = MutableLiveData<EventWrapper<Boolean>>()
    private var showAlert = MutableLiveData<Boolean>()

    @Captor
    private lateinit var institutionArgumentCaptor: ArgumentCaptor<InstitutionModel>
    @Captor
    private lateinit var sportArgumentCaptor: ArgumentCaptor<SportModel>
    @Captor
    private lateinit var tournamentArgumentCaptor: ArgumentCaptor<TournamentModel>
    @Captor
    private lateinit var divisionSubDivisionArgumentCaptor: ArgumentCaptor<DivisionSubDivisionModel>
    @Captor
    private lateinit var changeFragmentArgumentCaptor: ArgumentCaptor<EventWrapper<Boolean>>

//    private val intent = Intent(
//        ApplicationProvider.getApplicationContext(),
//        HomeActivity::class.java)

    @get:Rule
    var permissionRule = GrantPermissionRule.grant(
        android.Manifest.permission.ACCESS_FINE_LOCATION,
        android.Manifest.permission.CALL_PHONE
    )

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        //intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, "id")
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region DIALOG
    @Test
    fun testDialogWhenIsTrue_ShouldShow() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            showAlert.value = true
        }
        AndroidTestRobot.init()
            .checkViewText(getContext().getString(R.string.alert_info_legal_title))
    }

    @Test
    fun testDialogWhenIsFalse_ShouldNotShow() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            showAlert.value = false
        }
        AndroidTestRobot.init()
            .checkNotViewText(getContext().getString(R.string.alert_info_legal_title))
    }
    //endregion

    //region DRAWER
    @Test
    fun testDrawerWhenActivityStart_ShouldBeNotDisplayed() {
        launchActivity<HomeActivity>()
        AndroidTestRobot.init()
            .drawerIsClosed(R.id.drawer_layout)
    }

    @Test
    fun testDrawerWhenIsOpen_ShouldBeDisplayed() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .openDrawer(R.id.drawer_layout)
            .drawerIsOpen(R.id.drawer_layout)
    }

    @Test
    fun testDrawerItemWhenIsClickInstitutions_ShouldOpenActivity() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .openDrawer(R.id.drawer_layout)
            .drawerIsOpen(R.id.drawer_layout)
            .viewOnClick(R.id.institutions_menu)

        Intents.intended(IntentMatchers.hasComponent(InstitutionActivity::class.java.name))
    }

    @Test
    fun testDrawerItemWhenIsClickSponsor_ShouldOpenActivity() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .openDrawer(R.id.drawer_layout)
            .drawerIsOpen(R.id.drawer_layout)
            .viewOnClick(R.id.sponsors_menu)

        Intents.intended(IntentMatchers.hasComponent(SponsorActivity::class.java.name))
    }

    @Test
    fun testDrawerItemWhenIsClickChangePlace_ShouldOpenActivity() {
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .openDrawer(R.id.drawer_layout)
            .drawerIsOpen(R.id.drawer_layout)
            .viewOnClick(R.id.change_place_menu)

        Mockito.verify(viewModel, times(1)).removeCityOnPreferences()
        Intents.intended(IntentMatchers.hasComponent(PlacesActivity::class.java.name))
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testDrawerItemWhenIsClickMCSContacts_ShouldOpenActivity() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .openDrawer(R.id.drawer_layout)
            .drawerIsOpen(R.id.drawer_layout)
            .viewOnClick(R.id.mcs_contact)

        Intents.intended(IntentMatchers.hasComponent(MyCitysSportsContactActivity::class.java.name))
    }
    //endregion

    //region ERROR
    @Test
    fun testFriendlyErrorWhenIsEmpty_ShouldShow() {
        val dto = HomeItemDto()
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsIsEmpty.value = EventWrapper(dto.institutions.isEmpty())
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsNull_ShouldShow() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                null
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsError_ShouldShow() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsFailure_ShouldShow() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsSuccess_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_friendly_error)
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_home)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                HomeItemDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_home)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccessNull_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                null
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_home)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_home)
    }

    @Test
    fun testProgressbarVisibilityWhenError_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_home)
    }

    @Test
    fun testProgressbarVisibilityWhenFailure_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_home)
    }
    //endregion

    //region COORDINATOR
    @Test
    fun testCoordinatorWhenIsSuccess_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                getHomeItemDto(1, 1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.i_app_bar_home)
    }

    @Test
    fun testCoordinatorWhenIsEmpty_ShouldBeGone() {
        val dto = HomeItemDto()
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            institutionsIsEmpty.value = EventWrapper(dto.institutions.isEmpty())
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_app_bar_home)
    }

    @Test
    fun testCoordinatorWhenIsNull_ShouldShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                null
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_app_bar_home)
    }

    @Test
    fun testCoordinatorWhenIsError_ShouldShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_app_bar_home)
    }

    @Test
    fun testCoordinatorWhenIsFailure_ShouldShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_app_bar_home)
    }

    @Test
    fun testCoordinatorWhenIsLoading_ShouldShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_app_bar_home)
    }
    //endregion

    //region TOOLBAR SPINNER INSTITUTION
    @Test
    fun testSpInstitutionsCountWhenIsSuccess_ShouldReturnTwo() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
        }

        var count = 0
        scenario.onActivity {
            count = it.findViewById<Spinner>(R.id.sp_institution).count
        }

        assertEquals(dto.institutions.size, count)
    }

    @Test
    fun testSpInstitutionsSelectedWhenIdIsNullOrEmpty_ShouldReturnZero() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            institutionsModel.value = dto.institutions
        }

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_institution).selectedItemPosition
        }
        assertEquals(0, position)
    }

    @Test
    fun testSpInstitutionsSelectedWhenIdDoNotExists_ShouldReturnZero() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)

        Mockito.`when`(viewModel.getInstitutionIdFromPreferences()).thenReturn("IdFalse")
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            institutionsModel.value = dto.institutions
        }

        Mockito.verify(viewModel, times(1)).removeTournamentOnPreferences()
        Mockito.verify(viewModel, times(1)).removeDivisionSubDivisionOnPreferences()

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_institution).selectedItemPosition
        }
        assertEquals(0, position)
    }

    @Test
    fun testSpInstitutionsSelectedWhenIdExists_ShouldReturnOne() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        Mockito.`when`(viewModel.getInstitutionIdFromPreferences()).thenReturn("2")
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
        }

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_institution).selectedItemPosition
        }

        assertEquals(1, position)
    }

    @Test
    fun testMvSetInstitutionWhenSelectedItem_ShouldItemPosition() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
        }

        scenario.onActivity {
            it.findViewById<Spinner>(R.id.sp_institution).setSelection(1)
        }
        institutionArgumentCaptor =  ArgumentCaptor.forClass(InstitutionModel::class.java)
        Thread.sleep(2000)

        Mockito.verify(viewModel, times(2)).setInstitutionModel(MockitoHelper.capture(institutionArgumentCaptor))

        val values = institutionArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(dto.institutions[0], values.first())
        assertEquals(dto.institutions[1], values[1])
    }
    //endregion

    //region TOOLBAR IMAGE SPORTS
    @Test
    fun testImageEmptySportsWhenIsEmpty_ShouldBeVisible() {
        val dto = getHomeItemDto(2, 1)
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            sportsIsEmpty.value = EventWrapper(true)
        }
        Mockito.verify(viewModel, times(1)).removeSportOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.iv_empty_sports)
    }

    @Test
    fun testImageEmptySportsWhenIsNotEmpty_ShouldBeDone() {
        val dto = getHomeItemDto(2, 1)
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            sportsModel.value = dto.items[0].sports
            sportsIsEmpty.value = EventWrapper(false)
        }
        Mockito.verify(viewModel, never()).removeSportOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.iv_empty_sports)
    }
    //endregion

    //region TOOLBAR SPINNER SPORTS
    @Test
    fun testSpSportsSelectedWhenIdIsNullOrEmpty_ShouldReturnZero() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val sports = dto.items[0].sports
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            sportsModel.value = sports
        }

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_sports).selectedItemPosition
        }
        assertEquals(0, position)
    }

    @Test
    fun testSpSportsSelectedWhenIdDoNotExists_ShouldReturnZero() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val sports = dto.items[0].sports
        Mockito.`when`(viewModel.getSportIdFromPreferences()).thenReturn("idFalse")
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            sportsModel.value = sports
        }

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_sports).selectedItemPosition
        }
        assertEquals(0, position)
    }

    @Test
    fun testSpSportsSelectedWhenIdExists_ShouldReturnOne() {
        val scenario = launchActivity<HomeActivity>()
        Mockito.`when`(viewModel.getSportIdFromPreferences()).thenReturn("sportId2")
        val dto = getHomeItemDto(2, 2)
        val sports = dto.items[0].sports
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            institutionsModel.value = dto.institutions
            sportsModel.value = sports
        }

        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_sports).selectedItemPosition
        }

        assertEquals(1, position)
    }

    @Test
    fun testSpSportsItemsWhenInstitutionIndexIsZero_ShouldReturnItemsIndexZero() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val sports = dto.items[0].sports
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            sportsModel.value = sports
        }

        var sportList = mutableListOf<SportModel>()
        scenario.onActivity {
            val spinner = it.findViewById<Spinner>(R.id.sp_sports)
            sportList = (spinner.adapter as SportSpinnerAdapter).sportsList
        }

        assertEquals(sports.size, sportList.size)
        assertEquals(sports, sportList)
    }

    @Test
    fun testSpSportsItemsWhenInstitutionIndexIsOne_ShouldReturnItemsIndexOne() {
        val scenario = launchActivity<HomeActivity>()
        Mockito.`when`(viewModel.getInstitutionIdFromPreferences()).thenReturn("2")
        Mockito.`when`(viewModel.getSportIdFromPreferences()).thenReturn("sportId2")
        val dto = getHomeItemDto(2, 2)
        val sports = dto.items[1].sports
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            institutionsModel.value = dto.institutions
            sportsModel.value = sports
        }

        var sportList = mutableListOf<SportModel>()
        scenario.onActivity {
            val spinner = it.findViewById<Spinner>(R.id.sp_sports)
            sportList = (spinner.adapter as SportSpinnerAdapter).sportsList
        }

        assertEquals(sports.size, sportList.size)
        assertEquals(sports, sportList)
    }

    @Test
    fun testSpSportsPreferencesWhenSelectedItem_ShouldItemPosition() {
        val dto = getHomeItemDto(2, 2)
        val sports = dto.items[1].sports
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            institutionsModel.value = dto.institutions
            sportsModel.value = sports
        }

        scenario.onActivity {
            it.findViewById<Spinner>(R.id.sp_sports).setSelection(1)
        }

        scenario.recreate()
        var position = 0
        scenario.onActivity {
            position = it.findViewById<Spinner>(R.id.sp_sports).selectedItemPosition
        }

        assertEquals(1, position)
    }

    @Test
    fun testMvSetSportWhenSelectedItem_ShouldItemPosition() {
        val dto = getHomeItemDto(2, 1)
        val sports = dto.items[1].sports
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = sports
        }

        scenario.onActivity {
            it.findViewById<Spinner>(R.id.sp_sports).setSelection(1)
        }

        sportArgumentCaptor =  ArgumentCaptor.forClass(SportModel::class.java)
        Thread.sleep(2000)

        Mockito.verify(viewModel, times(2)).setSportModel(MockitoHelper.capture(sportArgumentCaptor))
        Mockito.verify(viewModel, Mockito.never()).removeSportOnPreferences()

        val values = sportArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(sports[0], values.first())
        assertEquals(sports[1], values[1])
    }

    //endregion

    //region TOOLBAR RECYCLER TOURNAMENT
    @Test
    fun testRvTournamentVisibilityWhenIsSuccess_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }
        Mockito.verify(viewModel, never()).removeTournamentOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.rv_tournament)
    }

    @Test
    fun testRvTournamentVisibilityWhenIsEmpty_ShouldBeGone() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsIsEmpty.value = EventWrapper(true)
        }
        Mockito.verify(viewModel, times(1)).removeTournamentOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_tournament)
    }

    @Test
    fun testRvTournamentCountWhenIsSuccess_ShouldReturnTwo() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }

        var count: Int? = 0
        scenario.onActivity {
            count = it.findViewById<RecyclerView>(R.id.rv_tournament).adapter?.itemCount
        }

        assertEquals(tournaments.size, count)
    }

    @Test
    fun testRvTournamentSelectedWhenIdDoNotExists_ShouldReturnZero() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }

        var isSelected = false
        scenario.onActivity {
            isSelected =
                (it.findViewById<RecyclerView>(R.id.rv_tournament).adapter as TournamentAdapter).getItemForPosition(
                    0
                ).selected
        }

        assertTrue(isSelected)
    }

    @Test
    fun testRvTournamentSelectedWhenIdExists_ShouldReturnOne() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()
        Mockito.`when`(viewModel.getTournamentIdFromPreferences()).thenReturn("tournamentId2")
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }

        var isSelected = false
        scenario.onActivity {
            isSelected =
                (it.findViewById<RecyclerView>(R.id.rv_tournament).adapter as TournamentAdapter).getItemForPosition(
                    1
                ).selected
        }

        assertTrue(isSelected)
    }

    @Test
    fun testMvSetTournamentWhenSelectedItem_ShouldReturnItemPosition() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }

        tournamentArgumentCaptor =  ArgumentCaptor.forClass(TournamentModel::class.java)
        AndroidTestRobot.init().clickItemList(R.id.rv_tournament, 1)
        Thread.sleep(2000)

        Mockito.verify(viewModel, times(2)).setTournamentModel(MockitoHelper.capture(tournamentArgumentCaptor))
        Mockito.verify(viewModel, Mockito.never()).removeTournamentOnPreferences()

        val values = tournamentArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(tournaments[0], values.first())
        assertEquals(tournaments[1], values[1])
    }

    //endregion

    //region TOOLBAR TEXTVIEW TOURNAMENT
    @Test
    fun testTvTournamentVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsModel.value = tournaments
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_tournament_empty)
    }

    @Test
    fun testTvTournamentVisibilityWhenIsEmpty_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsIsEmpty.value = EventWrapper(true)
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_tournament_empty)
    }

    //endregion

    //region TOOLBAR RECYCLER DIVISION SUB DIVISION
    @Test
    fun testRvDivisionSubDivisionVisibilityWhenIsSuccess_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            val adapter =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments.get(0))
            divisionsSubDivisionsModel.value = adapter.getDivisionSubDivisionModel()
        }
        Mockito.verify(viewModel, never()).removeDivisionSubDivisionOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.rv_division)
    }

    @Test
    fun testRvDivisionSubDivisionVisibilityWhenIsEmpty_ShouldBeGone() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            divisionsSubDivisionsIsEmpty.value = EventWrapper(true)
        }
        Mockito.verify(viewModel, times(1)).removeDivisionSubDivisionOnPreferences()
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_division)
    }

    @Test
    fun testRvDivisionSubDivisionVisibilityWhenIsTournamentEmpty_ShouldBeGone() {
        launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            tournamentsIsEmpty.value = EventWrapper(true)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_division)
    }

    @Test
    fun testRvDivisionSubDivisionCountWhenIsSuccess_ShouldReturnTwo() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 2, 0, 1)

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            val adapter =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0])
            divisionsSubDivisionsModel.value = adapter.getDivisionSubDivisionModel()
        }

        var count: Int? = 0
        scenario.onActivity {
            count = it.findViewById<RecyclerView>(R.id.rv_division).adapter?.itemCount
        }

        assertEquals(tournaments[0].subDivisions.size, count)
    }

    @Test
    fun testRvDivisionSubDivisionSelectedWhenIdDoNotExists_ShouldReturnZero() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            val adapter =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0])
            divisionsSubDivisionsModel.value = adapter.getDivisionSubDivisionModel()
        }

        var isSelected = false
        scenario.onActivity {
            isSelected =
                (it.findViewById<RecyclerView>(R.id.rv_division).adapter as DivisionSubDivisionAdapter).getItemForPosition(
                    0
                ).selected
        }

        assertTrue(isSelected)
    }

    @Test
    fun testRvDivisionSubDivisionSelectedWhenIdExists_ShouldReturnOne() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 2, 0, 1)
        val model =
            DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        val scenario = launchActivity<HomeActivity>()
        Mockito.`when`(viewModel.getDivisionSubDivisionFromPreferences()).thenReturn(model[1])
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            divisionsSubDivisionsModel.value = model
        }

        var isSelected = false
        scenario.onActivity {
            isSelected =
                (it.findViewById<RecyclerView>(R.id.rv_division).adapter as DivisionSubDivisionAdapter).getItemForPosition(
                    1
                ).selected
        }

        assertTrue(isSelected)
    }

    @Test
    fun testMvDivisionSubDivisionWhenSelectedItem_ShouldReturnItemPosition() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 2, 0, 1)
        val model =
            DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            divisionsSubDivisionsModel.value = model
        }

        divisionSubDivisionArgumentCaptor =  ArgumentCaptor.forClass(DivisionSubDivisionModel::class.java)
        AndroidTestRobot.init().clickItemList(R.id.rv_division, 1)
        //Thread.sleep(2000)

        Mockito.verify(viewModel, times(2)).setDivisionSubDivisionModel(MockitoHelper.capture(divisionSubDivisionArgumentCaptor))
        Mockito.verify(viewModel, Mockito.never()).removeDivisionSubDivisionOnPreferences()

        val values = divisionSubDivisionArgumentCaptor.allValues
        assertEquals(2, values.size)
        assertEquals(model[0], values.first())
        assertEquals(model[1], values[1])
    }
    //endregion

    //region CHANGE FRAGMENT
    @Test
    fun testChangeFragmentWhenCurrentFragmentOne_ShouldShowNews() {
        val scenario = launchActivity<HomeActivity>()
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val model =
            DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )

            divisionsSubDivisionsModel.value = model
        }

        Mockito.verify(viewModel, times(1)).setDivisionSubDivisionModel(model[0])
    }
    //endregion

    //region FRAGMENT LAYOUT
    @Test
    fun testFragmentLayoutVisibilityWhenIsLoading_ShouldBeGone() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.fl_container)
    }

    @Test
    fun testFragmentLayoutVisibilityWhenIsSuccess_ShouldBeVisible() {
        launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                HomeItemDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.fl_container)
    }

    @Test
    fun testFragmentLayoutWhenIsSuccessNull_ShouldBeNull() {
        val scenario = launchActivity<HomeActivity>()
        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                null
            )
        }

        var fragment: Fragment? = null
        scenario.onActivity {
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }

        assertNull(fragment)
    }

    @Test
    fun testFragmentLayoutWhenIsSuccess_ShouldShowFixture() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = dto.items[0].sports
            tournamentsModel.value = tournaments
            divisionsSubDivisionsModel.value =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        }
        AndroidTestRobot.init().clickItemList(R.id.rv_division, 0)

        var fragment: Fragment? = null
        scenario.onActivity {
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }

        assertTrue(fragment is FixtureFragment)
    }

    @Test
    fun testNvBottomWhenClickNewsItem_ShouldShowNews() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = dto.items[0].sports
            tournamentsModel.value = tournaments
            divisionsSubDivisionsModel.value =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        }

        AndroidTestRobot.init().viewOnClick(R.id.navigation_news)
        var fragment: Fragment? = null
        scenario.onActivity {
            //it.findViewById<BottomNavigationView>(R.id.navigationBottom).selectedItemId = R.id.navigation_news
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }
        assertTrue(fragment is NewsFragment)
    }

    @Test
    fun testNvBottomWhenClickTableItem_ShouldShowTable() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = dto.items[0].sports
            tournamentsModel.value = tournaments
            divisionsSubDivisionsModel.value =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        }

        AndroidTestRobot.init().viewOnClick(R.id.navigation_table)
        var fragment: Fragment? = null
        scenario.onActivity {
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }
        assertTrue(fragment is TableFragment)
    }

    @Test
    fun testNvBottomWhenClickSanctionItem_ShouldShowSanction() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = dto.items[0].sports
            tournamentsModel.value = tournaments
            divisionsSubDivisionsModel.value =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        }

        AndroidTestRobot.init().viewOnClick(R.id.navigation_sactions)
        var fragment: Fragment? = null
        scenario.onActivity {
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }
        assertTrue(fragment is SanctionFragment)
    }

    @Test
    fun testFragmentLayoutWhenError_ShouldShowError() {
        val dto = getHomeItemDto(2, 1)
        val tournaments = getTournaments(2, 1, 0, 1)
        val scenario = launchActivity<HomeActivity>()

        UiThreadStatement.runOnUiThread {
            homeDataModel.value = DataResponse.SUCCESS(
                dto
            )
            institutionsModel.value = dto.institutions
            sportsModel.value = dto.items[0].sports
            tournamentsModel.value = tournaments
            divisionsSubDivisionsModel.value =
                DivisionsSubDivisionsFromTournamentAdapter(tournamentModel = tournaments[0]).getDivisionSubDivisionModel()

        }
        AndroidTestRobot.init().viewOnClick(R.id.navigation_sactions)
        var fragment: Fragment? = null
        scenario.onActivity {
            it.run {
                showErrorFragment(ExceptionHandleImpl(Exception()))
            }
        }

        scenario.onActivity {
            fragment = it.supportFragmentManager.findFragmentById(R.id.fl_container)
        }

        assertTrue(fragment is ErrorFragment)
    }

    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.homeData).thenReturn(homeDataModel)
        Mockito.`when`(viewModel.institutions).thenReturn(institutionsModel)
        Mockito.`when`(viewModel.sports).thenReturn(sportsModel)
        //Mockito.`when`(viewModel.divisions).thenReturn(divisionsModel)
        Mockito.`when`(viewModel.divisionsSubDivisions).thenReturn(divisionsSubDivisionsModel)
        Mockito.`when`(viewModel.tournaments).thenReturn(tournamentsModel)

        Mockito.`when`(viewModel.institutionsIsEmpty).thenReturn(institutionsIsEmpty)
        Mockito.`when`(viewModel.sportsEmpty).thenReturn(sportsIsEmpty)
        Mockito.`when`(viewModel.divisionsSubDivisionsIsEmpty)
            .thenReturn(divisionsSubDivisionsIsEmpty)
        Mockito.`when`(viewModel.tournamentsIsEmpty).thenReturn(tournamentsIsEmpty)
        Mockito.`when`(viewModel.changeFragment).thenReturn(changeFragment)
        Mockito.`when`(viewModel.showAlert).thenReturn(showAlert)

    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

    private fun getContext(): Context {
        return ApplicationProvider.getApplicationContext() as Context
    }

//endregion
}