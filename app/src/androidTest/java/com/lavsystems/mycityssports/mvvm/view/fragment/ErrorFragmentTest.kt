package com.lavsystems.mycityssports.mvvm.view.fragment

import android.content.Context
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.exception.ExceptionHandleImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.ErrorFragmentViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.ConstantsUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class ErrorFragmentTest: KoinTest {

    @Mock
    lateinit var viewModel: ErrorFragmentViewModel
    private var resource = MutableLiveData<Int>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region VIEW
    @Test
    fun testViewsVisibilityWhenActivityFragment_ShouldBeVisibles() {
        val fragmentArgs = bundleOf(ConstantsUtils.FRAGMENT_POSITION_BUNDLE to 1,
            ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE to ExceptionHandleImpl(IllegalStateException()))
        launchFragmentInContainer<ErrorFragment>(fragmentArgs)
        runOnUiThread {
            resource.value = R.string.known_error
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_error)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_title_error)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_subtitle_error)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.btn_try_again)
    }

    @Test
    fun testTitleTextWhenActivityFragment_ShouldBeTextTitle() {
        val fragmentArgs = bundleOf(ConstantsUtils.FRAGMENT_POSITION_BUNDLE to 1,
            ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE to ExceptionHandleImpl(IllegalStateException()))
        val scenario = launchFragmentInContainer<ErrorFragment>(fragmentArgs)
        runOnUiThread {
            resource.value = R.string.known_error
        }
        var textView: TextView? = null
        scenario.onFragment {
            textView = it.view?.findViewById(R.id.tv_title_error)
        }
        val context = ApplicationProvider.getApplicationContext() as Context

        assertEquals(context.getString(R.string.error_title_fragment), textView?.text.toString())
    }

    @Test
    fun testSubTitleTextWhenIsException_ShouldBeSubTextTitleException() {
        val fragmentArgs = bundleOf(ConstantsUtils.FRAGMENT_POSITION_BUNDLE to 1,
            ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE to ExceptionHandleImpl(IllegalStateException()))
        val scenario = launchFragmentInContainer<ErrorFragment>(fragmentArgs)
        var textView: TextView? = null
        scenario.onFragment {
            textView = it.view?.findViewById(R.id.tv_subtitle_error)
        }
        val context = ApplicationProvider.getApplicationContext() as Context
        runOnUiThread {
            resource.value = R.string.known_error
        }
        var res = viewModel.resourceStringId.value!!
        assertEquals(res, R.string.known_error)
        assertEquals(context.getString(res), textView?.text.toString())
        runOnUiThread {
            resource.value = R.string.without_internet
        }
        res = viewModel.resourceStringId.value!!
        assertEquals(res, R.string.without_internet)
        assertEquals(context.getString(res), textView?.text.toString())
    }

//    @Test
//    fun testButtonWhenIsClick_ShouldDestroy() {
//        val fragmentArgs = bundleOf(ConstantsUtils.FRAGMENT_POSITION_BUNDLE to 1,
//            ConstantsUtils.FRAGMENT_EXCEPTION_HANDLE_BUNDLE to ExceptionHandleImpl(
//                NoConnectivityException()
//            ))
//        val scenario = launchFragmentInContainer<ErrorFragment>(fragmentArgs)
//        runOnUiThread {
//            resource.value = R.string.without_internet
//        }
//        AndroidTestRobot.init().viewOnClick(R.id.btn_try_again)
//        scenario.onFragment { frag ->
//            try {
//                frag.arguments
//                fail("should throw if accessed when not set")
//            } catch (e: NullPointerException) {
//            }
//        }
//    }
   //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.resourceStringId).thenReturn(resource)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}