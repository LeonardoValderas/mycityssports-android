package com.lavsystems.mycityssports.mvvm.view.activity

import android.widget.Spinner
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.PlaceDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.CityModel
import com.lavsystems.mycityssports.mvvm.model.StateModel
import com.lavsystems.mycityssports.mvvm.viewmodel.PlacesViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
//import com.lavsystems.mycityssports.testUtils.ProvidesTestAndroidModels
import com.lavsystems.mycityssports.utils.EventWrapper
import org.junit.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
internal class PlacesActivityTest : KoinTest {

    @Mock
    lateinit var viewModel: PlacesViewModel
    private var places = MutableLiveData<DataResponse<PlaceDto>>()
    private val city = MutableLiveData<DataResponse<EventWrapper<Boolean>>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<PlacesActivity>()
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenCityIsSelectedAndClickBtnGo_ShouldBeVisible() {
        launchActivity<PlacesActivity>()
        Mockito.`when`(viewModel.getCitySelected()).thenReturn(CityModel())
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel()), mutableListOf(
                        CityModel()
                    )
                )
            )
        }

        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_go)

        runOnUiThread {
            city.value = DataResponse.LOADING()
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenPlacesIsSuccess_ShouldBeGone() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel()), mutableListOf(
                        CityModel()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenPlacesIsError_ShouldBeGone() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.ERROR("", null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenCityIsError_ShouldBeGone() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            city.value = DataResponse.ERROR("", null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenPlacesIsFailure_ShouldBeGone() {
        val scenario = launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_places)
    }

    @Test
    fun testProgressbarVisibilityWhenCityIsFailure_ShouldBeGone() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            city.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_places)
    }

    //endregion

    //region PLACES
    @Test
    fun testPlacesWhenStatesIsEmpty_ShouldShowFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value =
                DataResponse.SUCCESS(PlaceDto(mutableListOf(), mutableListOf(CityModel())))
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testPlacesWhenCitiesIsEmpty_ShouldShowFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel(), StateModel()),
                    mutableListOf()
                )
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testPlacesWhenIsNotEmpty_ShouldHideFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel(), StateModel()),
                    mutableListOf(CityModel())
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_friendly_error)
    }

    @Test
    fun testPlacesWhenIsError_ShouldShowFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testPlacesWhenIsFailure_ShouldShowFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }
    //endregion

    //region SPINNERS
    @Test
    fun testSpinnerStatesWhenStatesIsNotEmpty_ShouldBeVisible() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel(), StateModel()),
                    mutableListOf(CityModel())
                )
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.sp_state)
    }

    @Test
    fun testSpinnerCitiesWhenStatesIsNotEmpty_ShouldBeVisible() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel(), StateModel()),
                    mutableListOf(CityModel())
                )
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.sp_city)
    }

    @Test
    fun testSpinnerStatesWhenSelectFirstItem_ShouldBeFirstItem() {
        val scenario = launchActivity<PlacesActivity>()
        val states = ProvidesTestAndroidModels.getStates()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    states,
                    ProvidesTestAndroidModels.getCities()
                )
            )
        }
        var stateSelected: StateModel? = null
        scenario.onActivity {
            stateSelected = it.findViewById<Spinner>(R.id.sp_state).selectedItem as StateModel
        }

        assertEquals(states[0], stateSelected)
    }

    @Test
    fun testSpinnerCitiesWhenSelectFirstItem_ShouldBeFirstItem() {
        val scenario = launchActivity<PlacesActivity>()
        val cities = ProvidesTestAndroidModels.getCities()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    ProvidesTestAndroidModels.getStates(),
                    cities
                )
            )
        }
        var citySelected: CityModel? = null
        scenario.onActivity {
            citySelected = it.findViewById<Spinner>(R.id.sp_city).selectedItem as CityModel
        }

        assertEquals(cities[0], citySelected)
    }

    @Test
    fun testSpinnerStatesWhenGetCount_ShouldBeEqualsToListSize() {
        val scenario = launchActivity<PlacesActivity>()
        val states = ProvidesTestAndroidModels.getStates()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    states,
                    ProvidesTestAndroidModels.getCities()
                )
            )
        }
        var count = 0
        scenario.onActivity {
            count = it.findViewById<Spinner>(R.id.sp_state).count
        }

        assertEquals(states.size, count)
    }

    @Test
    fun testSpinnerCitiesWhenGetCount_ShouldBeEqualsToListSize() {
        val scenario = launchActivity<PlacesActivity>()
        val states = ProvidesTestAndroidModels.getStates()
        val cities = ProvidesTestAndroidModels.getCities()
        val citiesFilter = cities.filter {
            it.state.id === states[0].id
        }
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    ProvidesTestAndroidModels.getStates(),
                    cities
                )
            )
        }
        var count = 0
        scenario.onActivity {
            count = it.findViewById<Spinner>(R.id.sp_city).count
        }

        assertEquals(citiesFilter.size, count)
    }
    //endregion

    //region CITY
    @Test
    fun testCityWhenSaveSuccess_ShouldGoToHomeActivity() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            city.value = DataResponse.SUCCESS(EventWrapper(true))
        }
        intended(IntentMatchers.hasComponent(HomeActivity::class.java.name))
    }

    @Test
    fun testCityWhenSaveIsError_ShouldShowSnackBar() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            city.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .checkSnackBarDisplayed(R.string.city_not_save_places)
    }

    @Test
    fun testCityWhenSaveIsFailure_ShouldShowFriendlyError() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            city.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }
    //endregion

    //region CLICK BUTTON
    @Test
    fun testCitySelectedWhenClickBtnGo_ShouldBeNotNull() {
        launchActivity<PlacesActivity>()
        Mockito.`when`(viewModel.getCitySelected()).thenReturn(CityModel())
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel()), mutableListOf(
                        CityModel()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_go)
            .checkSnackBarIsNotDisplayed(R.string.city_null_places)
    }

    @Test
    fun testCitySelectedWhenClickBtnGo_ShouldBeNullAndShowSnackBar() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value = DataResponse.SUCCESS(
                PlaceDto(
                    mutableListOf(StateModel()), mutableListOf(
                        CityModel()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_go)
            .checkSnackBarDisplayed(R.string.city_null_places)
    }
    //endregion


    @Test
    fun testFriendlyErrorWhenClickBtnAgain_ShouldThrowIntent() {
        launchActivity<PlacesActivity>()
        runOnUiThread {
            places.value =
                DataResponse.FAILURE(null)
        }

        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_try_again)

        intended(IntentMatchers.hasComponent(PlacesActivity::class.java.name))

    }


    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.places).thenReturn(places)
        Mockito.`when`(viewModel.city).thenReturn(city)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

    //endregion
}