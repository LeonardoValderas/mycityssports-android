package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getSponsors
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.SponsorDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.adapter.SponsorAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.SponsorViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class SponsorActivityTest : KoinTest {

    @Mock
    lateinit var viewModel: SponsorViewModel
    private var sponsors = MutableLiveData<DataResponse<MutableList<SponsorDto>>>()
    private var sponsor = MutableLiveData<DataResponse<SponsorModel>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindItem() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("nameSponsor1")
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldHasOneGroupItem() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }
        var adapterSponsor: SponsorAdapter? = null
        scenario.onActivity {
            it.findViewById<RecyclerView>(R.id.rv_container)?.run {
                adapterSponsor = adapter as SponsorAdapter
            }
        }

        assertNotNull(adapterSponsor)
        assertEquals(1, adapterSponsor?.itemCount)
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldHasTwoChildItems() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }
        var adapterSponsor: SponsorAdapter? = null
        scenario.onActivity {
            it.findViewById<RecyclerView>(R.id.rv_container)?.run {
                adapterSponsor = adapter as SponsorAdapter
            }
        }

        assertNotNull(adapterSponsor)
        assertEquals(2, adapterSponsor?.getGroupByPosition(0)?.sponsors?.size)
    }

    @Test
    fun testRecyclerWhenClickItem_ShouldGoToSponsorDetails() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }
        AndroidTestRobot.init().clickItemList(R.id.rv_container, 0)
        Intents.intended(IntentMatchers.hasComponent(SponsorDetailsActivity::class.java.name))
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchActivity<SponsorActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "1",
                        getSponsors(2)
                    )
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchActivity<SponsorActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            sponsors.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchActivity<SponsorActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            sponsors.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.LOADING()
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_sponsors)
    }

    @Test
    fun testProgressbarVisibilityWhenSponsorsIsSuccess_ShouldBeGone() {
        val scenario = launchActivity<SponsorActivity>()
        UiThreadStatement.runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "id",
                        mutableListOf()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_sponsors)
    }

    @Test
    fun testProgressbarVisibilityWhenSponsorsIsError_ShouldBeGone() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.ERROR("", null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_sponsors)
    }

    @Test
    fun testProgressbarVisibilityWhenPlacesIsFailure_ShouldBeGone() {
        val scenario = launchActivity<SponsorActivity>()
        UiThreadStatement.runOnUiThread {
            sponsors.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_sponsors)
    }
    //endregion

    //region EMPTY TEXT
    @Test
    fun testTextEmptyWhenSponsorsIsSuccessEmpty_ShouldBeVisible() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.cl_empty_text)
    }

    @Test
    fun testTextEmptyWhenSponsorsIsSuccess_ShouldBeGone() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "id",
                        mutableListOf()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_empty_text)
    }
    //endregion

    //region FRIENDLY ERROR
    @Test
    fun testFriendlyErrorWhenIsSuccess_ShouldBeGone() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.SUCCESS(
                mutableListOf(
                    SponsorDto(
                        "id",
                        mutableListOf()
                    )
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsError_ShouldBeVisible() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsFailure_ShouldBeVisible() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenClickBtnAgain_ShouldThrowIntent() {
        val scenario = launchActivity<SponsorActivity>()
        runOnUiThread {
            sponsors.value = DataResponse.FAILURE(null)
        }

        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_try_again)

        Intents.intended(IntentMatchers.hasComponent(SponsorActivity::class.java.name))
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.sponsors).thenReturn(sponsors)
        Mockito.`when`(viewModel.sponsor).thenReturn(sponsor)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}