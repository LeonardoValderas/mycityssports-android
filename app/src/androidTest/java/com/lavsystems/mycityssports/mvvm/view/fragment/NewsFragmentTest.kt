package com.lavsystems.mycityssports.mvvm.view.fragment

import ProvidesTestAndroidModels.getNews
import ProvidesTestAndroidModels.getSponsors
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.NewsDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.NewsModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.adapter.NewsAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.NewsViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class NewsFragmentTest : KoinTest {

    @Mock
    lateinit var viewModel: NewsViewModel
    private var news = MutableLiveData<DataResponse<NewsDto>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_container)
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindItem() {
        launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(2),
                    getSponsors(1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("newsTitle1")
    }

    // is error
//    @Test
//    fun testRecyclerLayoutWhenIsSuccess_ShouldBeVertical() {
//        val scenario = launchFragmentInContainer<NewsFragment>()
//        UiThreadStatement.runOnUiThread {
//            news.value = DataResponse.SUCCESS(
//                NewsDto(
//                    getNews(1),
//                    getSponsors(2)
//                )
//            )
//        }
//        var recycler: RecyclerView? = null
//        scenario.onFragment {
//             recycler = it.view?.findViewById(R.id.rv_container)
//        }
//        assertEquals(LinearLayoutManager.VERTICAL, recycler?.layoutManager?.layoutDirection?)
//    }

    //endregion

    //region ADAPTER
    //Use Robolectric ?????
    @Test
    fun testAdapterCountWhenIsTwoItem_ShouldReturnFour() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertEquals(4, newsAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterCountWhenIsFiveItem_ShouldReturnEight() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(5),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertEquals(8, newsAdapter?.itemCount)
        }
    }

    // Sponsor
    @Test
    fun testAdapterSponsorWhenIsEmptySponsors_ShouldHasNotSponsor() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(2),
                    mutableListOf()
                    )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertTrue(newsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).isEmpty())
            assertEquals(3, newsAdapter.itemCount)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsOneItem_ShouldBeAnteUltimate() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(1),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            val count = newsAdapter?.itemCount!!
            val index = count - 2
            assertTrue(newsAdapter.getItemModel(index) is SponsorModel)
            assertEquals(3, count)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsTwoItems_ShouldHasOneItemSecondPos() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertEquals(1, newsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).size)
            assertTrue(newsAdapter?.getItemModel(2) is SponsorModel)
            assertEquals(4, newsAdapter.itemCount)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsFourItems_ShouldHasTwoItems() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(4),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertEquals(2, newsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).size)
            assertTrue(newsAdapter.getItemModel(2) is SponsorModel)
            assertTrue(newsAdapter.getItemModel(6) is SponsorModel)
            assertEquals(7, newsAdapter.itemCount)
        }
    }

    // Ads
    @Test
    fun testAdapterAdsPositionWhenIsLessToThreeItems_ShouldBeLastPos() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            val count = newsAdapter?.itemCount!!
            assertNotNull(newsAdapter)
            assertTrue(newsAdapter.getItemModel(count - 1) is AdMobItem)
        }
    }

    @Test
    fun testAdapterAdsPositionWhenIsHigherToThreeItems_ShouldBeInLastPos() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(4),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val newsAdapter: NewsAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as NewsAdapter
                }

            assertNotNull(newsAdapter)
            assertTrue(newsAdapter?.getItemModel(3) is AdMobItem)
        }
    }
    //endregion

    //region EMPTY TEXT
    @Test
    fun testEmptyTextVisibilityWhenIsSuccessEmpty_ShouldBeVisible() {
        launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_empty)
    }

    @Test
    fun testEmptyTextVisibilityWhenIsSuccessNoEmpty_ShouldBeGone() {
        launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    mutableListOf(
                        NewsModel()
                    ),
                    mutableListOf()
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_empty)
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(10),
                    getSponsors(5)
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.SUCCESS(
                NewsDto(
                    getNews(10),
                    getSponsors(5)
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchFragmentInContainer<NewsFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            news.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.news).thenReturn(news)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}