package com.lavsystems.mycityssports.mvvm.view.activity

import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.viewmodel.SplashViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.EventWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class SplashActivityTest: KoinTest {
    @Mock
    lateinit var splashViewModel: SplashViewModel
    private var observerExists = MutableLiveData<DataResponse<EventWrapper<Boolean>>>()

    @Before
    fun setUp(){
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear(){
        Intents.release()
    }

    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        val scenario = launchActivity<SplashActivity>()
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.pb_splash)
    }

    @Test
    fun testIntentWhenCityExists_ShouldGoToHomeActivity() {
         runOnUiThread {
            observerExists.value = DataResponse.SUCCESS(EventWrapper(true))
        }
        launchActivity<SplashActivity>()
        intended(hasComponent(HomeActivity::class.java.name))
    }

    @Test
    fun testIntentWhenCityNotExists_ShouldGoToPlacesActivity() {
        runOnUiThread {
            observerExists.value = DataResponse.SUCCESS(EventWrapper(false))
        }

        val scenario = launchActivity<SplashActivity>()
        intended(hasComponent(PlacesActivity::class.java.name))
    }

    @Test
    fun testIntentWhenIsSuccessDataNull_ShouldGoToPlacesActivity() {
        runOnUiThread {
            observerExists.value = DataResponse.SUCCESS(null)
        }

        val scenario = launchActivity<SplashActivity>()
        intended(hasComponent(PlacesActivity::class.java.name))
    }

    @Test
    fun testIntentWhenIsError_ShouldGoToPlacesActivity() {
        runOnUiThread {
            observerExists.value = DataResponse.ERROR("message", 123)
        }

        val scenario = launchActivity<SplashActivity>()
        intended(hasComponent(PlacesActivity::class.java.name))
    }

    @Test
    fun testIntentWhenIsFailure_ShouldGoToPlacesActivity() {
        runOnUiThread {
            observerExists.value = DataResponse.FAILURE(null)
        }

        val scenario = launchActivity<SplashActivity>()
        intended(hasComponent(PlacesActivity::class.java.name))
    }

    //region PRIVATE
    private fun initViewModel() {
        //splashViewModel = mock(SplashViewModel::class.java)
        `when`(splashViewModel.cityExists).thenReturn(observerExists)
    }

    private fun initModule(){
        val mockedModule = module {
            single(override = true) { splashViewModel }
        }
        loadKoinModules(mockedModule)
    }

    //endregion
}