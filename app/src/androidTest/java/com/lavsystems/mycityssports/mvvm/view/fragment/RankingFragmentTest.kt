package com.lavsystems.mycityssports.mvvm.view.fragment

import ProvidesTestAndroidModels.getRanking
import ProvidesTestAndroidModels.getSponsors
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.RankingDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.view.adapter.AdMobAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.RankingAdapter
import com.lavsystems.mycityssports.mvvm.view.adapter.SponsorItemAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.RankingViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class RankingFragmentTest: KoinTest {

    @Mock
    lateinit var viewModel: RankingViewModel
    private var rankings = MutableLiveData<DataResponse<RankingDto>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_container)
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindTitleItem() {
        launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 3),
                    getSponsors(1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("rankingTypeName1")
    }

    //endregion

    //region ADAPTER
    //Use Robolectric ?????
    @Test
    fun testAdapterCountWhenHasSponsor_ShouldReturnThreeAdapters() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(3, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterCountWhenHasNotSponsor_ShouldReturnTwoAdapters() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(2, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterItemCountWhenIsSuccess_ShouldReturnTwoItem() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val rankingAdapter = concatAdapter?.adapters?.filterIsInstance(RankingAdapter::class.java)?.first()
            assertNotNull(rankingAdapter)
            assertEquals(2, rankingAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterWhenHasSponsors_ShouldHasSponsorAdapter() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val sponsorAdapter = concatAdapter?.adapters?.filterIsInstance(SponsorItemAdapter::class.java)?.first()
            assertNotNull(sponsorAdapter)
            assertEquals(1, sponsorAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldHasAdAdapter() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val sponsorAdapter = concatAdapter?.adapters?.filterIsInstance(AdMobAdapter::class.java)?.first()
            assertNotNull(sponsorAdapter)
            assertEquals(1, sponsorAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterGroupItemsCountWhenIsSuccess_ShouldReturnTwoItem() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val rankingAdapter = concatAdapter?.adapters?.filterIsInstance(RankingAdapter::class.java)?.first()
            assertNotNull(rankingAdapter)
            assertEquals(2, rankingAdapter?.getGroupByPosition(0)?.rankings?.size)
            assertEquals(2, rankingAdapter?.getGroupByPosition(1)?.rankings?.size)
//            assertNull( rankingAdapter?.getGroupByPosition(2))
        }
    }

//    @Test
//    fun testAdapterGroupItemsWhenIsSuccess_ShouldFindItem() {
//        val scenario = launchFragmentInContainer<RankingFragment>()
//        UiThreadStatement.runOnUiThread {
//            rankings.value = DataResponse.SUCCESS(
//                RankingDto(
//                    getRanking(2, 2),
//                    getSponsors(3)
//                )
//            )
//        }
//
//        scenario.onFragment {
//            it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
//
//                val adapterC = adapter as ConcatAdapter
//
//                val rankingAdapter = (adapter as ConcatAdapter).adapters?.filterIsInstance(RankingAdapter::class.java)?.first()
//                rankingAdapter?.setExpanded(true)
//                adapterC.addAdapter(rankingAdapter)
//                adapter = adapterC
//            }
//            val concatAdapter: ConcatAdapter? =
//                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
//                    adapter as ConcatAdapter
//                }
//
//            val rankingAdapter = concatAdapter?.adapters?.filterIsInstance(RankingAdapter::class.java)?.first()
//            rankingAdapter?.setExpanded(true)
//            rankingAdapter?.getGroupByPosition(0)?.isExpanded = true
//            assertNotNull(rankingAdapter)
//            AndroidTestRobot.init()
//                .itemIsDisplayedWithText("rankingId1")
//           // assertEquals(2, rankingAdapter?.getGroupByPosition(0)?.rankings?.size)
//            //assertEquals(2, rankingAdapter?.getGroupByPosition(1)?.rankings?.size)
////            assertNull( rankingAdapter?.getGroupByPosition(2))
//        }
//    }

    //endregion

    //region EMPTY TEXT
    @Test
    fun testEmptyTextVisibilityWhenIsSuccessEmpty_ShouldBeVisible() {
        launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_empty)
    }

    @Test
    fun testEmptyTextVisibilityWhenIsSuccessNoEmpty_ShouldBeGone() {
        launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(1, 1),
                    mutableListOf()
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_empty)
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(2, 3),
                    getSponsors(5)
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.SUCCESS(
                RankingDto(
                    getRanking(3, 3),
                    getSponsors(5)
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchFragmentInContainer<RankingFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            rankings.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.ranking).thenReturn(rankings)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}