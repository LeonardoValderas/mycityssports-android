package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getSponsors
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import androidx.test.rule.GrantPermissionRule
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.AddressModel
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.PositionModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.SponsorViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.ConstantsUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class SponsorDetailsActivityTest: KoinTest {

    @Mock
    lateinit var viewModel: SponsorViewModel
    private var sponsor = MutableLiveData<DataResponse<SponsorModel>>()
    private val intent = Intent(
        ApplicationProvider.getApplicationContext(),
        SponsorDetailsActivity::class.java)
    @get:Rule
    var permissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        intent.putExtra(ConstantsUtils.SPONSOR_ID_BUDLE, "id")
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region TOOLBAR
    @Test
    fun testToolbarWhenActivityStart_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_toolbar)
    }

    @Test
    fun testToolbarWhenActivityStart_ShouldHasTitle() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        AndroidTestRobot.init()
            .toolbarTitleText(R.id.i_toolbar, (ApplicationProvider.getApplicationContext() as Context).getString(R.string.sponsor_title))
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_sponsor)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_sponsor)
    }
    //endregion

    //region ERROR
    @Test
    fun testActivityStateVisibilityWhenIsBundleNull_ShouldBeDestroy() {
        val scenario = launchActivity<SponsorDetailsActivity>()
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenIsError_ShouldBeDestroy() {
        val scenario = launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.ERROR("", null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenIsFailure_ShouldBeDestroy() {
        val scenario = launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.FAILURE(null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }
    // Verify toast test when activity finish
//    @Test
//    fun testToastBarVisibilityWhenIsFailure_ShouldBeVisible() {
//        val scenario = launchActivity<SponsorDetailsActivity>()
//        var activity: SponsorDetailsActivity? = null
//        scenario.onActivity {
//            activity = it
//        }
//
//        runOnUiThread {
//            sponsor.value = DataResponse.FAILURE(null)
//        }
//
//        AndroidTestRobot.init()
//            .verifyToastDisplayed(activity!!, R.string.sponsor_error)
//    }
    //endregion

    //region SPONSOR VIEWS
    //NAME
    @Test
    fun testNameWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                getSponsors(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_name)
    }
    @Test
    fun testNameWhenIsNotEmpty_ShouldShowName() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_name, model.name)
    }

    @Test
    fun testImageWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                getSponsors(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_sponsor_image)
    }
    //ADDRESS
    @Test
    fun testAddressWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_address)
    }
    @Test
    fun testAddressWhenAddressIsNotEmpty_ShouldShowAddress() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_address, model.address.streetAndNumber)
    }
    @Test
    fun testAddressWhenAddressIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_address, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testAddressWhenClickAddress_ShouldGoToCallMap() {
        val model = getSponsors(1)[0].copy(address = AddressModel(
            "street",
            "number",
            "neigh",
            PositionModel("-30", "-45"),
            "")
        )
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_address)

        val url = String.format(
            Locale.ENGLISH,
            InformationContactIntentImpl.GOOGLE_MAPS,
            model.address.position.latitude.toDouble(),
            model.address.position.longitude.toDouble(),
            model.name
        )

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(url))
            )
        )
    }
    @Test
    fun testAddressWhenClickEmptyAddress_ShouldNotGoToCallMap() {
        val model = SponsorModel()
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_address)
            .viewIsDisplayed(R.id.tv_sponsor_address)
    }
    //PHONE
    @Test
    fun testPhoneWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_home_phone)
    }
    @Test
    fun testPhoneWhenPhoneIsNotEmpty_ShouldShowPhone() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_home_phone, model.phone)
    }
    @Test
    fun testPhoneWhenPhoneIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_home_phone, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testPhoneWhenClickPhone_ShouldGoToCallPanel() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_home_phone)

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_DIAL),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.TEL}${model.phone}"))
            )
        )
    }
    @Test
    fun testPhoneWhenClickEmptyPhone_ShouldNotGoToCallPanel() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_home_phone)
            .viewIsDisplayed(R.id.tv_sponsor_home_phone)
    }
    //MOBILE
    @Test
    fun testMobileWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_phone)
    }
    @Test
    fun testMobileWhenMobileIsNotEmpty_ShouldShowMobile() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_phone, model.mobile)
    }
    @Test
    fun testMobileWhenMobileIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_phone, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testMobileWhenClickMobile_ShouldGoToCallPanel() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_phone)

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_DIAL),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.TEL}${model.mobile}"))
            )
        )
    }
    @Test
    fun testMobileWhenClickEmptyMobile_ShouldNotGoToCallPanel() {
        val model = getSponsors(1)[0].copy(mobile = "")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_phone)
            .viewIsDisplayed(R.id.tv_sponsor_phone)
    }
    //EMAIL
    @Test
    fun testEmailWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_email)
    }
    @Test
    fun testEmailWhenEmailIsNotEmpty_ShouldShowEmail() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_email, model.email)
    }
    @Test
    fun testEmailWhenEmailIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_email, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testEmailWhenClickEmail_ShouldGoToEmailOptions() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_email)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_SENDTO),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.MAIL_TO}${model.email}"))
            )
        )
    }
    @Test
    fun testEmailWhenClickEmptyEmail_ShouldNotGoToEmailOptions() {
        val model = getSponsors(1)[0].copy(email = "")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_email)
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_email)
    }
    //WEB
    @Test
    fun testWebWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_sponsor_web)
    }
    @Test
    fun testWebWhenWebIsNotEmpty_ShouldShowWeb() {
        val model = getSponsors(1)[0]
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_web, model.web)
    }
    @Test
    fun testWebWhenWebIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_web, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testWebWhenClickValidWeb_ShouldGoToBrowser() {
        val model = getSponsors(1)[0].copy(web = "https://www.google.com/")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_web)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.web))
            )
        )
    }
    @Test
    fun testWebWhenClickNotValidWeb_ShouldShowToast() {
        val model = getSponsors(1)[0]
        val scenario = launchActivity<SponsorDetailsActivity>(intent = intent)
        var activity: SponsorDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_web)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    @Test
    fun testWebWhenClickEmptyWeb_ShouldNotGoToBrowser() {
        val model = getSponsors(1)[0].copy(web = "")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_web)
        Thread.sleep(1000)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_web)
    }
    //FACEBOOK
    @Test
    fun testFacebookWhenIsSuccess_ShouldBeVisible() {
        val model = getSponsors(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_facebook)
    }
    @Test
    fun testFacebookWhenFacebookIsNotEmpty_ShouldBeVisible() {
        val model = getSponsors(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_facebook)
    }
    @Test
    fun testFacebookWhenFacebookIsEmpty_ShouldBeNotVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_sponsor_facebook)
    }
    @Test
    fun testFacebookWhenClickFacebook_ShouldGoToFacebook() {
        val model = getSponsors(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_facebook)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.facebook))
            )
        )
    }
    @Test
    fun testFacebookWhenClickNotValidFacebook_ShouldShowToast() {
        val model = getSponsors(1)[0]
        val scenario = launchActivity<SponsorDetailsActivity>(intent = intent)
        var activity: SponsorDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_facebook)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    //INSTAGRAM
    @Test
    fun testInstagramWhenIsSuccess_ShouldBeVisible() { // Error performing 'scroll to' on view 'with id is <com.lavsystems.mycityssports:id/tv_sponsor_instagram>'.
        val model = getSponsors(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_instagram)
    }
    @Test
    fun testInstagramWhenInstagramIsNotEmpty_ShouldBeVisible() { // Error performing 'scroll to' on view 'with id is <com.lavsystems.mycityssports:id/tv_sponsor_instagram>'.
        val model = getSponsors(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_sponsor_instagram)
    }
    @Test
    fun testInstagramWhenInstagramIsEmpty_ShouldBeNotVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                SponsorModel()
            )
        }

        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_sponsor_instagram)
    }
    @Test
    fun testInstagramWhenClickInstagram_ShouldGoToInstagram() { // Error performing 'scroll to' on view 'with id is <com.lavsystems.mycityssports:id/tv_sponsor_instagram>'.
        val model = getSponsors(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_instagram)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.instagram))
            )
        )
    }
    @Test
    fun testInstagramWhenClickNotValidInstagram_ShouldShowToast() { // Error performing 'scroll to' on view 'with id is <com.lavsystems.mycityssports:id/tv_sponsor_instagram>'.
        val model = getSponsors(1)[0]
        val scenario = launchActivity<SponsorDetailsActivity>(intent = intent)
        var activity: SponsorDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_sponsor_instagram)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }

    //DESCRIPTION
    @Test
    fun testDescriptionWhenIsSuccess_ShouldBeVisible() {
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                getSponsors(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_sponsor_description)
    }
    @Test
    fun testDescriptionWhenIsNotEmpty_ShouldShowDescription() {
        val model = getSponsors(1)[0].copy(supportMessage = "Support")
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_description, model.supportMessage)
    }
    @Test
    fun testDescriptionWhenIsEmpty_ShouldBeEmpty() {
        val model = SponsorModel()
        launchActivity<SponsorDetailsActivity>(intent = intent)
        runOnUiThread {
            sponsor.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_sponsor_description, "")
    }
    //endregion


    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.sponsor).thenReturn(sponsor)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }
//endregion
}