package com.lavsystems.mycityssports.mvvm.view.fragment

import ProvidesTestAndroidModels.getFixture
import ProvidesTestAndroidModels.getFixtureInstanceItemModel
import ProvidesTestAndroidModels.getFixturePointItemModel
import ProvidesTestAndroidModels.getFixtureZoneItemModel
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.FixtureItemModel
import com.lavsystems.mycityssports.mvvm.model.FixtureModel
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.viewmodel.FixtureViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class FixtureFragmentTest : KoinTest {

    @Mock
    lateinit var viewModel: FixtureViewModel
    private var fixtures = MutableLiveData<DataResponse<FixtureModel>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                FixtureModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_container)
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindZoneTitleItem() {
        val model = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                model
            )
        }
        Thread.sleep(1000)
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("tournamentZoneName1")
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindInstanceItem() {
        val model = getFixture().copy(
            tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
            tournamentZones = getFixtureZoneItemModel(1, 1, 1)
        )
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                model
            )
        }
        Thread.sleep(1000)
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("tournamentInstanceName1")
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindPointItem() {
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentPoints = getFixturePointItemModel(1, 1, 1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("Fecha 1")
    }

    //endregion

    //region ADAPTER
    @Test
    fun testAdapterCountWhenIsSuccessZone_ShouldReturnFiveAdapters() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(5, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterCountWhenIsSuccessPoint_ShouldReturnFiveAdapters() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentPoints = getFixturePointItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(5, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterCountWhenHasNotInstance_ShouldReturnThreeAdapters() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(3, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterInstanceWhenHasSponsor_ShouldReturnThreeItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(3, 2, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val instanceAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixtureInstanceAdapter::class.java)
                    ?.first()
            assertNotNull(instanceAdapter)
            val fixture = instanceAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(3, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
            assertTrue(fixture?.get(1) is FixtureItemModel)
            assertTrue(fixture?.get(2) is SponsorModel)
        }
    }

    @Test
    fun testAdapterInstanceWhenHasNotSponsor_ShouldReturnTwoItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(3, 2, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val instanceAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixtureInstanceAdapter::class.java)
                    ?.first()
            assertNotNull(instanceAdapter)
            val fixture = instanceAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(2, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
            assertTrue(fixture?.get(1) is FixtureItemModel)
        }
    }

    @Test
    fun testAdapterZoneWhenHasSponsor_ShouldReturnTwoItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val zoneAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixtureZoneAdapter::class.java)?.first()
            assertNotNull(zoneAdapter)
            val fixture = zoneAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(2, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
            assertTrue(fixture?.get(1) is SponsorModel)
        }
    }

    @Test
    fun testAdapterZoneWhenHasNotSponsor_ShouldReturnOneItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 0)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val zoneAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixtureZoneAdapter::class.java)?.first()
            assertNotNull(zoneAdapter)
            val fixture = zoneAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(1, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
        }
    }

    @Test
    fun testAdapterPointWhenHasSponsor_ShouldReturnTwoItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentPoints = getFixturePointItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val pointAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixturePointAdapter::class.java)?.first()
            assertNotNull(pointAdapter)
            val fixture = pointAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(2, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
            assertTrue(fixture?.get(1) is SponsorModel)
        }
    }

    @Test
    fun testAdapterPointWhenHasNotSponsor_ShouldReturnOneItem() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentPoints = getFixturePointItemModel(1, 1, 0)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val pointAdapter =
                concatAdapter?.adapters?.filterIsInstance(FixturePointAdapter::class.java)?.first()
            assertNotNull(pointAdapter)
            val fixture = pointAdapter?.getGroupByPosition(0)?.getExpandingItems()
            assertEquals(1, fixture?.size)
            assertTrue(fixture?.get(0) is FixtureItemModel)
        }
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldHasAdAdapter() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 0)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val adMobAdapter =
                concatAdapter?.adapters?.filterIsInstance(AdMobAdapter::class.java)?.first()
            assertNotNull(adMobAdapter)
            assertEquals(1, adMobAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterSortWhenIsPoint_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentPoints = getFixturePointItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(5, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is FixtureInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is FixturePointAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenIsPointWithoutInstance_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentPoints = getFixturePointItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(3, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is FixturePointAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenIsZone_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(5, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is FixtureInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is FixtureZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenIsZoneWithoutInstance_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(0, 0, 0),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(3, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is FixtureZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenIsInstance_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentPoints = getFixturePointItemModel(0, 0, 0)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(3, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is FixtureInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is AdMobAdapter)
        }
    }

    //endregion

    //region EMPTY TEXT
    @Test
    fun testEmptyTextVisibilityWhenIsSuccessEmpty_ShouldBeVisible() {
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                FixtureModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_empty)
    }

    @Test
    fun testEmptyTextVisibilityWhenIsSuccessNoEmpty_ShouldBeGone() {
        launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_empty)
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.SUCCESS(
                getFixture().copy(
                    tournamentInstances = getFixtureInstanceItemModel(2, 1, 1),
                    tournamentZones = getFixtureZoneItemModel(1, 1, 1)
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchFragmentInContainer<FixtureFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            fixtures.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.fixture).thenReturn(fixtures)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}