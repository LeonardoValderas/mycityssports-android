package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getMyCitysSportsContactModel
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import androidx.test.rule.GrantPermissionRule
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel
import com.lavsystems.mycityssports.mvvm.model.MyCitysSportsContactModel.Companion.WITHOUT_INFO
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.MyCitysSportsContactViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.hamcrest.core.AllOf.allOf
import org.junit.*
import org.junit.Assert.assertEquals
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class MyCitysSportsContactActivityTest : KoinTest {

    @Mock
    lateinit var viewModel: MyCitysSportsContactViewModel
    private var contact = MutableLiveData<DataResponse<MyCitysSportsContactModel>>()
    @get:Rule
    var permissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region TOOLBAR
    @Test
    fun testToolbarWhenActivityStart_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_toolbar)
    }

    @Test
    fun testToolbarWhenActivityStart_ShouldHasTitle() {
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        var context: Context? = null
        scenario.onActivity {
            context = it.applicationContext
        }
        AndroidTestRobot.init()
            .toolbarTitleText(R.id.i_toolbar, context!!.getString(R.string.contact_title))
    }
    //endregion

    //region ADVIEW
    @Test
    fun testAdViewWhenActivityStart_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        Thread.sleep(8000)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.adView_mcs)
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.LOADING()
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_mcs)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                MyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_mcs)
    }
    //endregion

    //region ERROR
    @Test
    fun testActivityStateVisibilityWhenIsError_ShouldBeDestroy() {
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.ERROR("", null)
        }
        Thread.sleep(500)
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenIsFailure_ShouldBeDestroy() {
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.FAILURE(null)
        }
        Thread.sleep(500)
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }
    // Verify toast test when activity finish
//    @Test
//    fun testToastBarVisibilityWhenIsFailure_ShouldBeVisible() {
//        val scenario = launchActivity<MyCitysSportsContactActivity>()
//        var activity: MyCitysSportsContactActivity? = null
//
//        scenario.onActivity {
//            activity = it
//        }
//        runOnUiThread {
//            contact.value = DataResponse.FAILURE(null)
//        }
//
//
//        AndroidTestRobot.init()
//            .verifyToastDisplayed(activity!!, R.string.mcs_contact_error)
//    }


    //endregion

    //region CONTACT VIEWS
    @Test
    fun testContactImageWhenContactIsSuccess_ShouldBeVisible() {
       launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                getMyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_mcs_image)
    }
    //PHONE
    @Test
    fun testContactPhoneWhenContactIsSuccess_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                getMyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_home_phone)
    }
    @Test
    fun testContactPhoneWhenContactPhoneIsNotEmpty_ShouldShowPhone() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_home_phone, contactModel.phone)
    }
    @Test
    fun testContactPhoneWhenContactPhoneIsEmpty_ShouldBeWithoutInfo() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_home_phone, WITHOUT_INFO)
    }
    @Test
    fun testContactPhoneWhenClickContactPhone_ShouldGoToCallPanel() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_home_phone)

        intended(allOf(
            hasAction(Intent.ACTION_DIAL),
            hasData(Uri.parse("${InformationContactIntentImpl.TEL}${contactModel.phone}")))
        )
    }
    @Test
    fun testContactPhoneWhenClickEmptyContactPhone_ShouldNotGoToCallPanel() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_home_phone)
            .viewIsDisplayed(R.id.tv_mcs_home_phone)
    }
    //MOBILE
    @Test
    fun testContactMobileWhenContactIsSuccess_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                getMyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_mobile)
    }
    @Test
    fun testContactMobileWhenContactMobileIsNotEmpty_ShouldShowMobile() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_mobile, contactModel.mobile)
    }
    @Test
    fun testContactMobileWhenContactMobileIsEmpty_ShouldBeWithoutInfo() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_mobile, WITHOUT_INFO)
    }
    @Test
    fun testContactMobileWhenClickContactMobile_ShouldGoToCallPanel() {
        val contactModel = getMyCitysSportsContactModel()
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_mobile)

        intended(allOf(
            hasAction(Intent.ACTION_DIAL),
            hasData(Uri.parse("${InformationContactIntentImpl.TEL}${contactModel.mobile}")))
        )
    }
    @Test
    fun testContactMobileWhenClickEmptyContactMobile_ShouldNotGoToCallPanel() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_mobile)
            .viewIsDisplayed(R.id.tv_mcs_mobile)
    }
    //EMAIL
    @Test
    fun testContactEmailWhenContactIsSuccess_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                getMyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_email)
    }
    @Test
    fun testContactEmailWhenContactEmailIsNotEmpty_ShouldShowEmail() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_email, contactModel.email)
    }
    @Test
    fun testContactEmailWhenContactEmailIsEmpty_ShouldBeWithoutInfo() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_email, WITHOUT_INFO)
    }
    @Test
    fun testContactEmailWhenClickContactEmail_ShouldGoToEmailOptions() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_email)
        intended(allOf(
            hasAction(Intent.ACTION_SENDTO),
            hasData(Uri.parse("${InformationContactIntentImpl.MAIL_TO}${contactModel.email}")))
        )
    }
    @Test
    fun testContactEmailWhenClickEmptyContactEmail_ShouldNotGoToEmailOptions() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_email)
            .viewIsDisplayed(R.id.tv_mcs_email)
    }
    //WEB
    @Test
    fun testContactWebWhenContactIsSuccess_ShouldBeVisible() {
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                getMyCitysSportsContactModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_web)
    }
    @Test
    fun testContactWebWhenContactWebIsNotEmpty_ShouldShowWeb() {
        val contactModel = getMyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_web, contactModel.web)
    }
    @Test
    fun testContactWebWhenContactWebIsEmpty_ShouldBeWithoutInfo() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_mcs_web, WITHOUT_INFO)
    }
    @Test
    fun testContactWebWhenClickContactValidWeb_ShouldGoToBrowser() {
        val contactModel = getMyCitysSportsContactModel().copy(web = "https://www.google.com/")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_web)
        intended(allOf(
            hasAction(Intent.ACTION_VIEW),
            hasData(Uri.parse(contactModel.web)))
        )
    }
    @Test
    fun testContactWebWhenClickContactNotValidWeb_ShouldShowToast() {
        val contactModel = getMyCitysSportsContactModel()
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        var activity: MyCitysSportsContactActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }

        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_web)
        Thread.sleep(100)
        AndroidTestRobot.init()
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    @Test
    fun testContactWebWhenClickEmptyContactWeb_ShouldNotGoToBrowser() {
        val contactModel = MyCitysSportsContactModel()
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_web)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_web)
    }
    //FACEBOOK
    @Test
    fun testContactFacebookWhenContactIsSuccess_ShouldBeVisible() {
        val contactModel = getMyCitysSportsContactModel().copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_facebook)
    }
    @Test
    fun testContactFacebookWhenContactFacebookIsNotEmpty_ShouldBeVisible() {
        val contactModel = getMyCitysSportsContactModel().copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_facebook)
    }
    @Test
    fun testContactFacebookWhenContactFacebookIsEmpty_ShouldBeNotVisible() {
        val contactModel = MyCitysSportsContactModel().copy(facebook = "")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_mcs_facebook)
    }
    @Test
    fun testContactFacebookWhenClickContactFacebook_ShouldGoToFacebook() {
        val contactModel = getMyCitysSportsContactModel().copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_facebook)
        intended(allOf(
            hasAction(Intent.ACTION_VIEW),
            hasData(Uri.parse(contactModel.facebook)))
        )
    }
    @Test
    fun testContactFacebookWhenClickContactNotValidFacebook_ShouldShowToast() {
        val contactModel = getMyCitysSportsContactModel()
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        var activity: MyCitysSportsContactActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }

        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_facebook)
        Thread.sleep(100)
        AndroidTestRobot.init()
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    //INSTAGRAM
    @Test
    fun testContactInstagramWhenContactIsSuccess_ShouldBeVisible() {
        val contactModel = getMyCitysSportsContactModel().copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_instagram)
    }
    @Test
    fun testContactInstagramWhenContactInstagramIsNotEmpty_ShouldBeVisible() {
        val contactModel = getMyCitysSportsContactModel().copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_mcs_instagram)
    }
    @Test
    fun testContactInstagramWhenContactInstagramIsEmpty_ShouldBeNotVisible() {
        val contactModel = MyCitysSportsContactModel().copy(instagram = "")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_mcs_instagram)
    }
    @Test
    fun testContactInstagramWhenClickContactInstagram_ShouldGoToInstagram() {
        val contactModel = getMyCitysSportsContactModel().copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<MyCitysSportsContactActivity>()
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_instagram)
        intended(allOf(
            hasAction(Intent.ACTION_VIEW),
            hasData(Uri.parse(contactModel.instagram)))
        )
    }
    @Test
    fun testContactInstagramWhenClickContactNotValidInstagram_ShouldShowToast() {
        val contactModel = getMyCitysSportsContactModel()
        val scenario = launchActivity<MyCitysSportsContactActivity>()
        var activity: MyCitysSportsContactActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            contact.value = DataResponse.SUCCESS(
                contactModel
            )
        }

        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_mcs_instagram)
        Thread.sleep(100)
        AndroidTestRobot.init()
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        `when`(viewModel.contactModel).thenReturn(contact)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}