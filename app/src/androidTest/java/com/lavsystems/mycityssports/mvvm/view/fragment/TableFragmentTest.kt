package com.lavsystems.mycityssports.mvvm.view.fragment

import ProvidesTestAndroidModels.getSponsors
import ProvidesTestAndroidModels.getTable
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.TableDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.view.adapter.*
import com.lavsystems.mycityssports.mvvm.viewmodel.TableViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class TableFragmentTest: KoinTest {

    @Mock
    lateinit var viewModel: TableViewModel
    private var tables = MutableLiveData<DataResponse<TableDto>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_container)
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindZoneTitleItem() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 0),
                    getSponsors(1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("tournamentZoneName1")
    }

    //investigate why in some time this method throw error
    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindInstanceItem() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(0, 1, 0),
                    getSponsors(1)
                )
            )
        }
        Thread.sleep(1000)
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("tournamentInstanceName1")
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindPointItem() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(0, 2, 1),
                    getSponsors(1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("teamName")
    }

    //endregion

    //region ADAPTER
    //Use Robolectric ?????
    @Test
    fun testAdapterCountWhenHasSponsor_ShouldReturnEightAdapters() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 0),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(8, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterCountWhenHasNotSponsor_ShouldReturnSevenAdapters() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 0),
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(7, concatAdapter?.adapters?.size)
        }
    }

    @Test
    fun testAdapterWhenHasSponsors_ShouldHasSponsorAdapter() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 1),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val sponsorAdapter = concatAdapter?.adapters?.filterIsInstance(SponsorItemAdapter::class.java)?.first()
            assertNotNull(sponsorAdapter)
            assertEquals(1, sponsorAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldHasAdAdapter() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 1),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val adMobAdapter = concatAdapter?.adapters?.filterIsInstance(AdMobAdapter::class.java)?.first()
            assertNotNull(adMobAdapter)
            assertEquals(1, adMobAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterPointSortWhenIsFullInfo_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(0, 2, 2),
                    getSponsors(1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(8, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableGridAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is CommentAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is SourceAdapter)
            assertTrue(concatAdapter?.adapters?.get(6) is SponsorItemAdapter)
            assertTrue(concatAdapter?.adapters?.get(7) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterZoneSortWhenIsFullInfo_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 0),
                    getSponsors(1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(8, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is CommentAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is SourceAdapter)
            assertTrue(concatAdapter?.adapters?.get(6) is SponsorItemAdapter)
            assertTrue(concatAdapter?.adapters?.get(7) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenHasNotInstance_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(0, 0, 2),
                    getSponsors(1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(6, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableGridAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is CommentAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is SourceAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is SponsorItemAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenHasNotSponsor_ShouldBeSorted() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 0),
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(7, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is CommentAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is SourceAdapter)
            assertTrue(concatAdapter?.adapters?.get(6) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenHasNotComment_ShouldBeSorted() {
        val model = getTable(2, 2, 0).copy(comment = "")
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    model,
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(6, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is SourceAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenHasNotSource_ShouldBeSorted() {
        val model = getTable(2, 2, 0).copy(source = "")
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    model,
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(6, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is CommentAdapter)
            assertTrue(concatAdapter?.adapters?.get(5) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterSortWhenHasNotCommentAndSource_ShouldBeSorted() {
        val model = getTable(2, 2, 0).copy(comment = "", source = "")
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    model,
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }

            assertNotNull(concatAdapter)
            assertEquals(5, concatAdapter?.adapters?.size)
            assertTrue(concatAdapter?.adapters?.get(0) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(1) is TableInstanceAdapter)
            assertTrue(concatAdapter?.adapters?.get(2) is DividerTitleAdapter)
            assertTrue(concatAdapter?.adapters?.get(3) is TableZoneAdapter)
            assertTrue(concatAdapter?.adapters?.get(4) is AdMobAdapter)
        }
    }

    @Test
    fun testAdapterZoneGroupItemsCountWhenIsSuccess_ShouldReturnCount() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(3, 2, 0),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val zoneAdapter = concatAdapter?.adapters?.filterIsInstance(TableZoneAdapter::class.java)?.first()
            assertNotNull(zoneAdapter)
            assertEquals(1, zoneAdapter?.getGroupByPosition(0)?.tableDataZone?.size)
            assertEquals(2, zoneAdapter?.getGroupByPosition(1)?.tableDataZone?.size)
            assertEquals(3, zoneAdapter?.getGroupByPosition(2)?.tableDataZone?.size)
        }
    }

    @Test
    fun testAdapterInstanceGroupItemsCountWhenIsSuccess_ShouldReturnCount() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(3, 2, 0),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val instanceAdapter = concatAdapter?.adapters?.filterIsInstance(TableInstanceAdapter::class.java)?.first()
            assertNotNull(instanceAdapter)
            assertEquals(1, instanceAdapter?.getGroupByPosition(0)?.tableDataInstance?.size)
            assertEquals(2, instanceAdapter?.getGroupByPosition(1)?.tableDataInstance?.size)
        }
    }

    @Test
    fun testAdapterPointGroupItemsCountWhenIsSuccess_ShouldReturnCount() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(0, 2, 2),
                    getSponsors(1)
                )
            )
        }

        scenario.onFragment {
            val concatAdapter: ConcatAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as ConcatAdapter
                }
            val tableGridAdapter = concatAdapter?.adapters?.filterIsInstance(TableGridAdapter::class.java)?.first()
            val tableGridItemAdapter = tableGridAdapter?.getTableGridItemAdapter()
            assertNotNull(tableGridAdapter)
            assertNotNull(tableGridItemAdapter)
            assertEquals(1, tableGridAdapter?.itemCount)
            assertEquals(2, tableGridItemAdapter?.itemCount)
        }
    }
    //endregion

    //region EMPTY TEXT
    @Test
    fun testEmptyTextVisibilityWhenIsSuccessEmpty_ShouldBeVisible() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_empty)
    }

    @Test
    fun testEmptyTextVisibilityWhenIsSuccessNoEmpty_ShouldBeGone() {
        launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(1, 1, 0),
                    mutableListOf()
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_empty)
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchFragmentInContainer<TableFragment>()
        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(2, 2, 1),
                    getSponsors(5)
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchFragmentInContainer<TableFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.SUCCESS(
                TableDto(
                    getTable(3, 3, 0),
                    getSponsors(5)
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchFragmentInContainer<TableFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchFragmentInContainer<TableFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            tables.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.table).thenReturn(tables)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}