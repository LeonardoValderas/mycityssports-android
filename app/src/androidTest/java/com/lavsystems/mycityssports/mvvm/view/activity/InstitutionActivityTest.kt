package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getInstitutions
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.view.adapter.InstitutionAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.InstitutionViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class InstitutionActivityTest: KoinTest {

    @Mock
    lateinit var viewModel: InstitutionViewModel
    private var institutions = MutableLiveData<DataResponse<MutableList<InstitutionModel>>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindItem() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                    getInstitutions(2)
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("nameInstitution1")
    }

    @Test
    fun testAdapterWhenIsSuccess_ShouldTwoItems() {
        val scenario = launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(2)
            )
        }
        var institutionAdapter: InstitutionAdapter? = null
        scenario.onActivity {
            it.findViewById<RecyclerView>(R.id.rv_container)?.run {
                institutionAdapter = adapter as InstitutionAdapter
            }
        }

        assertNotNull(institutionAdapter)
        assertEquals(2, institutionAdapter?.itemCount)
    }

    @Test
    fun testRecyclerWhenClickItem_ShouldGoToInstitutionDetails() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(2)
            )
        }
        AndroidTestRobot.init().clickItemList(R.id.rv_container, 0)
        Intents.intended(IntentMatchers.hasComponent(InstitutionDetailsActivity::class.java.name))
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(2)
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchActivity<InstitutionActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(2)
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchActivity<InstitutionActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            institutions.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchActivity<InstitutionActivity>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onActivity {
            swipe = it.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        runOnUiThread {
            institutions.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.LOADING()
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_institutions)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_institutions)
    }

    @Test
    fun testProgressbarVisibilityWhenIsError_ShouldBeGone() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.ERROR("", null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_institutions)
    }

    @Test
    fun testProgressbarVisibilityWhenIsFailure_ShouldBeGone() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_institutions)
    }
    //endregion

    //region EMPTY TEXT
    @Test
    fun testTextEmptyWhenSponsorsIsSuccessEmpty_ShouldBeVisible() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                mutableListOf()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.cl_empty_text)
    }

    @Test
    fun testTextEmptyWhenSponsorsIsSuccess_ShouldBeGone() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                    getInstitutions(1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_empty_text)
    }
    //endregion

    //region FRIENDLY ERROR
    @Test
    fun testFriendlyErrorWhenIsSuccess_ShouldBeGone() {
        launchActivity<InstitutionActivity>()
        UiThreadStatement.runOnUiThread {
            institutions.value = DataResponse.SUCCESS(
                getInstitutions(1)
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsError_ShouldBeVisible() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.ERROR(null, null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenIsFailure_ShouldBeVisible() {
        val scenario = launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.FAILURE(null)
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_friendly_error)
    }

    @Test
    fun testFriendlyErrorWhenClickBtnAgain_ShouldThrowIntent() {
        launchActivity<InstitutionActivity>()
        runOnUiThread {
            institutions.value = DataResponse.FAILURE(null)
        }

        AndroidTestRobot.init()
            .viewOnClick(R.id.btn_try_again)

        Intents.intended(IntentMatchers.hasComponent(InstitutionActivity::class.java.name))
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.institutions).thenReturn(institutions)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}