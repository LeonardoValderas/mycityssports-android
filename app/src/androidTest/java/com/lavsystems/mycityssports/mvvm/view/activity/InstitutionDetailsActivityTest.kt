package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getInstitutions
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import androidx.test.rule.GrantPermissionRule
import com.google.android.gms.ads.AdView
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.AddressModel
import com.lavsystems.mycityssports.mvvm.model.InstitutionModel
import com.lavsystems.mycityssports.mvvm.model.PositionModel
import com.lavsystems.mycityssports.mvvm.view.utils.InformationContactIntentImpl
import com.lavsystems.mycityssports.mvvm.viewmodel.InstitutionDetailsViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.ConstantsUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class InstitutionDetailsActivityTest : KoinTest {

    @Mock
    lateinit var viewModel: InstitutionDetailsViewModel
    private var institution = MutableLiveData<DataResponse<InstitutionModel>>()
    private val intent = Intent(ApplicationProvider.getApplicationContext(),
        InstitutionDetailsActivity::class.java)
    @get:Rule
    var permissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        intent.putExtra(ConstantsUtils.INSTITUTION_ID_BUDLE, "id")
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region TOOLBAR
    @Test
    fun testToolbarWhenActivityStart_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_toolbar)
    }

    @Test
    fun testToolbarWhenActivityStart_ShouldHasTitle() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        AndroidTestRobot.init()
            .toolbarTitleText(R.id.i_toolbar, (ApplicationProvider.getApplicationContext() as Context).getString(R.string.institution_title))
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenActivityStart_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.LOADING()
        }
        //Thread.sleep(500)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_institution)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_institution)
    }
    //endregion

    //region ERROR
    @Test
    fun testActivityStateVisibilityWhenIsBundleNull_ShouldBeDestroy() {
        val scenario = launchActivity<InstitutionDetailsActivity>()
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenIsError_ShouldBeDestroy() {
        val scenario = launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.ERROR("", null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenIsFailure_ShouldBeDestroy() {
        val scenario = launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.FAILURE(null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }
    // Verify toast test when activity finish
//    @Test
//    fun testToastBarVisibilityWhenIsFailure_ShouldBeVisible() {
//        val scenario = launchActivity<InstitutionDetailsActivity>()
//        var activity: InstitutionDetailsActivity? = null
//        scenario.onActivity {
//            activity = it
//        }
//
//        runOnUiThread {
//            institution.value = DataResponse.FAILURE(null)
//        }
//
//        AndroidTestRobot.init()
//            .verifyToastDisplayed(activity!!, R.string.institution_error)
//    }
    //endregion

    //region INSTITUTION VIEWS
    //NAME
    @Test
    fun testInstitutionNameWhenInstitutionIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                getInstitutions(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_name)
    }
    @Test
    fun testInstitutionNameWhenInstitutionNameIsNotEmpty_ShouldShowName() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_name, model.name)
    }

    @Test
    fun testInstitutionImageWhenInstitutionIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                getInstitutions(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_institution_image)
    }
    //DESCRIPTION
    @Test
    fun testDescriptionWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                getInstitutions(1)[0]
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_description)
    }
    @Test
    fun testDescriptionWhenIsNotEmpty_ShouldShowDescription() {
        val model = getInstitutions(1)[0].copy(description = "Desc")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_description, model.description)
    }
    @Test
    fun testDescriptionWhenIsEmpty_ShouldBeEmpty() {
        val model = InstitutionModel()
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_description, "")
    }
    //ADDRESS
    @Test
    fun testAddressWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_address)
    }
    @Test
    fun testAddressWhenAddressIsNotEmpty_ShouldShowAddress() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_address, model.address.streetAndNumber)
    }
    @Test
    fun testAddressWhenAddressIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
           institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_address, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testAddressWhenClickAddress_ShouldGoToCallMap() {
        val model = getInstitutions(1)[0].copy(address = AddressModel(
            "street",
            "number",
            "neigh",
            PositionModel("-30", "-45"),
            ""))
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_address)

        val url = String.format(
            Locale.ENGLISH,
            InformationContactIntentImpl.GOOGLE_MAPS,
            model.address.position.latitude.toDouble(),
            model.address.position.longitude.toDouble(),
            model.name
        )

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(url))
            )
        )
    }
    @Test
    fun testAddressWhenClickEmptyAddress_ShouldNotGoToCallMap() {
        val model = InstitutionModel()
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_address)
            .viewIsDisplayed(R.id.tv_institution_address)
    }
    //PHONE
    @Test
    fun testPhoneWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_home_phone)
    }
    @Test
    fun testPhoneWhenPhoneIsNotEmpty_ShouldShowPhone() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_home_phone, model.phone)
    }
    @Test
    fun testPhoneWhenPhoneIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_home_phone, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testPhoneWhenClickPhone_ShouldGoToCallPanel() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_home_phone)

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_DIAL),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.TEL}${model.phone}"))
            )
        )
    }
    @Test
    fun testPhoneWhenClickEmptyPhone_ShouldNotGoToCallPanel() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_home_phone)
            .viewIsDisplayed(R.id.tv_institution_home_phone)
    }
    //MOBILE
    @Test
    fun testMobileWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_phone)
    }
    @Test
    fun testMobileWhenMobileIsNotEmpty_ShouldShowMobile() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_phone, model.mobile)
    }
    @Test
    fun testMobileWhenMobileIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_phone, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testMobileWhenClickMobile_ShouldGoToCallPanel() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_phone)

        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_DIAL),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.TEL}${model.mobile}"))
            )
        )
    }
    @Test
    fun testMobileWhenClickEmptyMobile_ShouldNotGoToCallPanel() {
        val model = getInstitutions(1)[0].copy(mobile = "")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_phone)
            .viewIsDisplayed(R.id.tv_institution_phone)
    }
    //EMAIL
    @Test
    fun testEmailWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_institution_email)
    }
    @Test
    fun testEmailWhenEmailIsNotEmpty_ShouldShowEmail() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_email, model.email)
    }
    @Test
    fun testEmailWhenEmailIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_email, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testEmailWhenClickEmail_ShouldGoToEmailOptions() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_email)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_SENDTO),
                IntentMatchers.hasData(Uri.parse("${InformationContactIntentImpl.MAIL_TO}${model.email}"))
            )
        )
    }
    @Test
    fun testEmailWhenClickEmptyEmail_ShouldNotGoToEmailOptions() {
        val model = getInstitutions(1)[0].copy(email = "")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_email)
            .viewIsDisplayedWithScroll(R.id.tv_institution_email)
    }
    //WEB
    @Test
    fun testWebWhenIsSuccess_ShouldBeVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_institution_web)
    }
    @Test
    fun testWebWhenWebIsNotEmpty_ShouldShowWeb() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_web, model.web)
    }
    @Test
    fun testWebWhenWebIsEmpty_ShouldBeWithoutInfo() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        Thread.sleep(500)
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_web, InstitutionModel.WITHOUT_INFO)
    }
    @Test
    fun testWebWhenClickValidWeb_ShouldGoToBrowser() {
        val model = getInstitutions(1)[0].copy(web = "https://www.google.com/")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_web)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.web))
            )
        )
    }
    @Test
    fun testWebWhenClickNotValidWeb_ShouldShowToast() {
        val model = getInstitutions(1)[0]
        val scenario = launchActivity<InstitutionDetailsActivity>(intent = intent)
        var activity: InstitutionDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_web)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    @Test
    fun testWebWhenClickEmptyWeb_ShouldNotGoToBrowser() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_web)
            .viewIsDisplayedWithScroll(R.id.tv_institution_web)
    }
    //FACEBOOK
    @Test
    fun testFacebookWhenIsSuccess_ShouldBeVisible() {
        val model = getInstitutions(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_institution_facebook)
    }
    @Test
    fun testFacebookWhenFacebookIsNotEmpty_ShouldBeVisible() {
        val model = getInstitutions(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_institution_facebook)
    }
    @Test
    fun testFacebookWhenFacebookIsEmpty_ShouldBeNotVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_institution_facebook)
    }
    @Test
    fun testFacebookWhenClickFacebook_ShouldGoToFacebook() {
        val model = getInstitutions(1)[0].copy(facebook = "https://www.facebook.com/leonardo.valderas.9")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_facebook)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.facebook))
            )
        )
    }
    @Test
    fun testFacebookWhenClickNotValidFacebook_ShouldShowToast() {
        val model = getInstitutions(1)[0]
        val scenario = launchActivity<InstitutionDetailsActivity>(intent = intent)
        var activity: InstitutionDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_facebook)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }
    //INSTAGRAM
    @Test
    fun testInstagramWhenIsSuccess_ShouldBeVisible() {
        val model = getInstitutions(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_institution_instagram)
    }
    @Test
    fun testInstagramWhenInstagramIsNotEmpty_ShouldBeVisible() {
        val model = getInstitutions(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayedWithScroll(R.id.tv_institution_instagram)
    }
    @Test
    fun testInstagramWhenInstagramIsEmpty_ShouldBeNotVisible() {
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                InstitutionModel()
            )
        }

        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_institution_instagram)
    }
    @Test
    fun testInstagramWhenClickInstagram_ShouldGoToInstagram() {
        val model = getInstitutions(1)[0].copy(instagram = "https://www.instagram.com/mycityssports/")
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_instagram)
        Intents.intended(
            AllOf.allOf(
                IntentMatchers.hasAction(Intent.ACTION_VIEW),
                IntentMatchers.hasData(Uri.parse(model.instagram))
            )
        )
    }
    @Test
    fun testInstagramWhenClickNotValidInstagram_ShouldShowToast() {
        val model = getInstitutions(1)[0]
        val scenario = launchActivity<InstitutionDetailsActivity>(intent = intent)
        var activity: InstitutionDetailsActivity? = null

        scenario.onActivity {
            activity = it
        }
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .viewOnClickScroll(R.id.tv_institution_instagram)
            .verifyToastDisplayed(activity!!, R.string.error_contatc_action)
    }

    //SOURCE
    //I dont know with the source is no displayed
    @Test
    fun testSourceWhenIsSuccess_ShouldBeVisible() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_institution_source)
    }

    @Test
    fun testSourceWhenIsNotEmpty_ShouldShowSource() {
        val model = getInstitutions(1)[0]
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_source, model.sourceFormatted)
    }
    @Test
    fun testSourceWhenIsEmpty_ShouldBeEmpty() {
        val model = InstitutionModel()
        launchActivity<InstitutionDetailsActivity>(intent = intent)
        runOnUiThread {
            institution.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_institution_source, "")
    }
    //endregion


    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.institution).thenReturn(institution)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }
//endregion
}