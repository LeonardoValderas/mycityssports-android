package com.lavsystems.mycityssports.mvvm.view.activity

import ProvidesTestAndroidModels.getCity
import ProvidesTestAndroidModels.getFixtureItemModel
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import androidx.test.rule.GrantPermissionRule
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.*
import com.lavsystems.mycityssports.mvvm.viewmodel.FixtureDetailsViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.ConstantsUtils
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class FixtureDetailsActivityTest: KoinTest {

    @Mock
    lateinit var viewModel: FixtureDetailsViewModel
    private var fixture = MutableLiveData<DataResponse<FixtureItemModel>>()
    private var cityModel = MutableLiveData<DataResponse<CityModel>>()

    private val intent = Intent(
        ApplicationProvider.getApplicationContext(),
        FixtureDetailsActivity::class.java)
    @get:Rule
    var permissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.CALL_PHONE)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        intent.putExtra(ConstantsUtils.FIXTURE_ID_BUDLE, "id")
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region TOOLBAR
    @Test
    fun testToolbarWhenActivityStart_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_toolbar)
    }

    @Test
    fun testToolbarTitleWhenIsZone_ShouldBeZoneName() {
        val model = getFixtureItemModel()
        val city = getCity()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                city
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .toolbarTitleText(
                R.id.i_toolbar, model.tournamentZone!!.name)
    }
    @Test
    fun testToolbarTitleWhenIsInstance_ShouldBeInstanceName() {
        val model = getFixtureItemModel().copy(tournamentZone = null)
        val city = getCity()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                city
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .toolbarTitleText(
                R.id.i_toolbar, model.tournamentInstance!!.name)
    }
    @Test
    fun testToolbarTitleWhenIsRound_ShouldBeRound() {
        val model = getFixtureItemModel().copy(tournamentZone = null, tournamentInstance = null)
        val city = getCity()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                city
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .toolbarTitleText(
                R.id.i_toolbar, model.roundString)
    }
    //endregion

    //region PROGRESS BAR
    @Test
    fun testProgressbarVisibilityWhenIsFixture_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            fixture.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_fixture)
    }

    @Test
    fun testProgressbarVisibilityWhenIsCity_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.LOADING()
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_fixture)
    }

    @Test
    fun testProgressbarVisibilityWhenCityIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.LOADING()
            cityModel.value = DataResponse.SUCCESS(
                CityModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.i_progress_bar_fixture)
    }

    @Test
    fun testProgressbarVisibilityWhenIsSuccess_ShouldBeGone() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                FixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.i_progress_bar_fixture)
    }
    //endregion

    //region ERROR
    @Test
    fun testActivityStateVisibilityWhenIsBundleNull_ShouldBeDestroy() {
        val scenario = launchActivity<FixtureDetailsActivity>()
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenCityIsError_ShouldBeDestroy() {
        val scenario = launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.ERROR("", null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenFixtureIsError_ShouldBeDestroy() {
        val scenario = launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.ERROR("", null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    @Test
    fun testActivityStateVisibilityWhenCityIsFailure_ShouldBeDestroy() {
        val scenario = launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.FAILURE(null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }
    @Test
    fun testActivityStateVisibilityWhenFixtureIsFailure_ShouldBeDestroy() {
        val scenario = launchActivity<FixtureDetailsActivity>(intent = intent)
        UiThreadStatement.runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.FAILURE(null)
        }
        assertEquals(Lifecycle.State.DESTROYED, scenario.state)
    }

    // Verify toast test when activity finish
//    @Test
//    fun testToastBarVisibilityWhenIsFailure_ShouldBeVisible() {
//        val scenario = launchActivity<FixtureDetailsActivity>()
//        var activity: FixtureDetailsActivity? = null
//        scenario.onActivity {
//            activity = it
//        }
//
//        runOnUiThread {
//            fixture.value = DataResponse.FAILURE(null)
//        }
//
//        AndroidTestRobot.init()
//            .verifyToastDisplayed(activity!!, R.string.fixture_error)
//    }
    //endregion

    //region FIXTURE VIEWS
    //ROUND
    @Test
    fun testRoundWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_round)
    }
    @Test
    fun testRoundWhenRoundIsNotEmpty_ShouldShowRound() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_round, model.roundString)
    }

    //DATE
    @Test
    fun testDateWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel().copy(dateTime = "2020-10-01T20:00:00")
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_date)
    }
    @Test
    fun testDateWhenRoundIsNotEmpty_ShouldShowDate() {
        //yyyy-MM-dd'T'HH:mm:ss
        val model = getFixtureItemModel().copy(dateTime = "2020-10-01T20:00:00")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_date, model.dateFormatted)
    }

    //SHIELD
    @Test
    fun testLocalShieldWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_local_team)
    }
    @Test
    fun testVisitorsShieldWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.iv_visit_team)
    }

    //RESULT
    @Test
    fun testLocalResultWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_result_local_team)
    }
    @Test
    fun testLocalResultWhenLocalResultIsNotEmpty_ShouldShowLocalResult() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_local_team, model.homeResult!!)
    }
    @Test
    fun testLocalResultWhenLocalResultIsEmpty_ShouldShowHyphen() {
        val model = getFixtureItemModel().copy(homeResult = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_local_team, "-")
    }
    @Test
    fun testVisitorsResultWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_result_visit_team)
    }
    @Test
    fun testVisitorsResultWhenVisitorsResultIsNotEmpty_ShouldShowVisitorsResult() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_visit_team, model.homeResult!!)
    }
    @Test
    fun testVisitorsResultWhenVisitorsResultIsEmpty_ShouldShowHyphen() {
        val model = getFixtureItemModel().copy(visitorsResult = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_visit_team, "-")
    }

    //PENALTY
    @Test
    fun testHomeResultPenaltyWhenIsNoEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_result_penalty_local_team)
    }
    @Test
    fun testHomeResultPenaltyWhenIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(homeResultPenalty = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_result_penalty_local_team)
    }
    @Test
    fun testHomeResultPenaltyWhenIsNotEmpty_ShouldShowHomeResultPenalty() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_penalty_local_team, model.homeResultPenaltyFormat)
    }
    @Test
    fun testVisitorsResultPenaltyWhenIsNoEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_result_penalty_visit_team)
    }
    @Test
    fun testVisitorsResultPenaltyWhenIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(visitorsResultPenalty = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_result_penalty_visit_team)
    }
    @Test
    fun testVisitorsResultPenaltyWhenIsNotEmpty_ShouldShowVisitorsResultPenalty() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_result_penalty_visit_team, model.visitorsResultPenaltyFormat)
    }

    //STATUS
    @Test
    fun testStatusWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_status_fixture)
    }
    @Test
    fun testStatusWhenIsFinish_ShouldShowText() {
        val model = getFixtureItemModel().copy(isFinish = true)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }

        AndroidTestRobot.init()
            .checkViewText(R.id.tv_status_fixture, (ApplicationProvider.getApplicationContext() as Context).getString(R.string.fixture_details_status_finished))
    }
    @Test
    fun testStatusWhenIsNotFinish_ShouldShowText() {
        val model = getFixtureItemModel().copy(isFinish = false)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_status_fixture, (ApplicationProvider.getApplicationContext() as Context).getString(R.string.fixture_details_status_no_finished))
    }

    //NAME TEAM
    @Test
    fun testLocalTeamNameWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_name_local_team)
    }
    @Test
    fun testLocalTeamNameWhenIsNotEmpty_ShouldShowName() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_name_local_team, model.homeTeam.name)
    }
    @Test
    fun testVisitorsTeamNameWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_name_visit_team)
    }
    @Test
    fun testVisitorsTeamNameWhenIsNotEmpty_ShouldShowName() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_name_visit_team, model.visitorsTeam.name)
    }

    //REFEREE
    @Test
    fun testRefereeContainerWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_referee)
    }
    @Test
    fun testRefereeContainerWhenIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(referees = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_referee)
    }
    @Test
    fun testRefereeWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_referees)
    }
    @Test
    fun testRefereeWhenIsNotEmpty_ShouldShowReferees() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_referees, model.refereeString)
    }

    //SCORER
    @Test
    fun testScorersContainerWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_scorers)
    }
    @Test
    fun testScorersContainerWhenHomeScorersIsEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel().copy(homeScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_scorers)
    }
    @Test
    fun testScorersContainerWhenVisitorsScorersIsEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel().copy(visitorsScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_scorers)
    }
    @Test
    fun testScorersContainerWhenVisitorsScorersAndHomeScorersIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(homeScorers = null, visitorsScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_scorers)
    }
    @Test
    fun testHomeScorersWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_local_scorers)
    }
    @Test
    fun testHomeScorersWhenIsNotEmpty_ShouldShowHomeScorers() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_local_scorers, model.homeScorersString)
    }
    @Test
    fun testVisitorsScorersWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_visitors_scorers)
    }
    @Test
    fun testVisitorsScorersWhenIsNotEmpty_ShouldShowVisitorsScorers() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_visitors_scorers, model.visitorsScorersString)
    }

    //CARD
    @Test
    fun testCardsContainerWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_cards)
    }
    @Test
    fun testCardsContainerWhenHomeScorersIsEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel().copy(homeScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_cards)
    }
    @Test
    fun testCardsContainerWhenVisitorsCardsIsEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel().copy(visitorsScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_cards)
    }
    @Test
    fun testCardsContainerWhenVisitorsCardsAndHomeCardsIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(homeCards = null, visitorsCards = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_cards)
    }
    @Test
    fun testHomeCardsWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_local_scorers)
    }
    @Test
    fun testHomeCardsWhenIsNotEmpty_ShouldShowHomeCards() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_local_scorers, model.homeScorersString)
    }
    @Test
    fun testVisitorsCardsWhenIsNotEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewIsDisplayed(R.id.tv_visitors_scorers)
    }
    @Test
    fun testVisitorsCardsWhenIsNotEmpty_ShouldShowVisitorsCards() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .checkViewText(R.id.tv_visitors_scorers, model.visitorsScorersString)
    }
    //TODO scorer format

    //CONTAINER VISIBILITY
    @Test
    fun testContainersVisibilityWhenIsNotEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel()
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_referee)
            .viewVisibilityVisible(R.id.cl_scorers)
            .viewVisibilityVisible(R.id.cl_cards)
    }
    @Test
    fun testContainersVisibilityWhenScorersIsEmpty_ShouldBeScorersGone() {
        val model = getFixtureItemModel().copy(homeScorers = null, visitorsScorers = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_scorers)
            .viewVisibilityVisible(R.id.cl_referee)
            .viewVisibilityVisible(R.id.cl_cards)
    }
    @Test
    fun testContainersVisibilityWhenRefereesIsEmpty_ShouldBeRefereesGone() {
        val model = getFixtureItemModel().copy(referees = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.cl_referee)
            .viewVisibilityVisible(R.id.cl_scorers)
            .viewVisibilityVisible(R.id.cl_cards)
    }

    @Test
    fun testContainersVisibilityWhenCardsIsEmpty_ShouldBeCardsGone() {
        val model = getFixtureItemModel().copy(homeCards = null, visitorsCards = null)
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.cl_referee)
            .viewVisibilityVisible(R.id.cl_scorers)
            .viewVisibilityGone(R.id.cl_cards)
    }

    //MAP
    @Test
    fun testMapWhenIsSuccess_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.map)
    }

    //COMMENT
    @Test
    fun testCommentWhenIsNoEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_comment)
    }
    @Test
    fun testCommentWhenIsEmpty_ShouldBeGone() {
        val model = getFixtureItemModel().copy(comment = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_comment)
    }

    //SOURCE
    @Test
    fun testSourceWhenIsNoEmpty_ShouldBeVisible() {
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                getFixtureItemModel()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_source)
    }
    @Test
    fun testSourceWhenIsEmpty_ShouldBeVisible() {
        val model = getFixtureItemModel().copy(source = "")
        launchActivity<FixtureDetailsActivity>(intent = intent)
        runOnUiThread {
            cityModel.value = DataResponse.SUCCESS(
                getCity()
            )
            fixture.value = DataResponse.SUCCESS(
                model
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_source)
    }
   //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.fixtureDetails).thenReturn(fixture)
        Mockito.`when`(viewModel.cityModel).thenReturn(cityModel)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }
//endregion
}