package com.lavsystems.mycityssports.mvvm.view.fragment

import ProvidesTestAndroidModels.getSanctions
import ProvidesTestAndroidModels.getSponsors
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.espresso.intent.Intents
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.lavsystems.mycityssports.R
import com.lavsystems.mycityssports.data.dto.SanctionDto
import com.lavsystems.mycityssports.data.network.api.utils.DataResponse
import com.lavsystems.mycityssports.mvvm.model.SponsorModel
import com.lavsystems.mycityssports.mvvm.view.adapter.SanctionAdapter
import com.lavsystems.mycityssports.mvvm.viewmodel.SanctionViewModel
import com.lavsystems.mycityssports.testUtils.AndroidTestRobot
import com.lavsystems.mycityssports.utils.adapter.AdMobItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4ClassRunner::class)
class SanctionFragmentTest : KoinTest {

    @Mock
    lateinit var viewModel: SanctionViewModel
    private var sanctions = MutableLiveData<DataResponse<SanctionDto>>()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Intents.init()
        initViewModel()
        initModule()
    }

    @After
    fun clear() {
        Intents.release()
    }

    //region RECYCLER
    @Test
    fun testRecyclerVisibilityWhenIsSuccessEmpty_ShouldBeGone() {
        launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.rv_container)
    }

    @Test
    fun testRecyclerWhenIsSuccess_ShouldFindItem() {
        launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(2),
                    getSponsors(1)
                )
            )
        }
        AndroidTestRobot.init()
            .itemIsDisplayedWithText("sanctionSanction1")
    }

    // is error
//    @Test
//    fun testRecyclerLayoutWhenIsSuccess_ShouldBeVertical() {
//        val scenario = launchFragmentInContainer<SanctionFragment>()
//        UiThreadStatement.runOnUiThread {
//            sanctions.value = DataResponse.SUCCESS(
//                SanctionDto(
//                    getNews(1),
//                    getSponsors(2)
//                )
//            )
//        }
//        var recycler: RecyclerView? = null
//        scenario.onFragment {
//             recycler = it.view?.findViewById(R.id.rv_container)
//        }
//        assertEquals(LinearLayoutManager.VERTICAL, recycler?.layoutManager?.layoutDirection?)
//    }

    //endregion

    //region ADAPTER
    //Use Robolectric ?????
    @Test
    fun testAdapterCountWhenIsTwoItem_ShouldReturnFour() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertEquals(4, sanctionsAdapter?.itemCount)
        }
    }

    @Test
    fun testAdapterCountWhenIsFiveItem_ShouldReturnEight() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(5),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertEquals(8, sanctionsAdapter?.itemCount)
        }
    }

    // Sponsor
    @Test
    fun testAdapterSponsorWhenIsEmptySponsors_ShouldHasNotSponsor() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(2),
                    mutableListOf()
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertTrue(sanctionsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).isEmpty())
            assertEquals(3, sanctionsAdapter.itemCount)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsOneItem_ShouldBeAnteUltimate() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(1),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            val count = sanctionsAdapter?.itemCount!!
            val index = count - 2
            assertTrue(sanctionsAdapter.getItemModel(index) is SponsorModel)
            assertEquals(3, count)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsTwoItems_ShouldHasOneItemSecondPos() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertEquals(1, sanctionsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).size)
            assertTrue(sanctionsAdapter?.getItemModel(2) is SponsorModel)
            assertEquals(4, sanctionsAdapter.itemCount)
        }
    }

    @Test
    fun testAdapterSponsorWhenIsFourItems_ShouldHasTwoItems() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(4),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertEquals(2, sanctionsAdapter?.getItems()!!.filterIsInstance(SponsorModel::class.java).size)
            assertTrue(sanctionsAdapter.getItemModel(2) is SponsorModel)
            assertTrue(sanctionsAdapter.getItemModel(6) is SponsorModel)
            assertEquals(7, sanctionsAdapter.itemCount)
        }
    }

    // Ads
    @Test
    fun testAdapterAdsPositionWhenIsLessToThreeItems_ShouldBeLastPos() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(2),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            val count = sanctionsAdapter?.itemCount!!
            assertNotNull(sanctionsAdapter)
            assertTrue(sanctionsAdapter.getItemModel(count - 1) is AdMobItem)
        }
    }

    @Test
    fun testAdapterAdsPositionWhenIsHigherToThreeItems_ShouldBeInLastPos() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(4),
                    getSponsors(3)
                )
            )
        }

        scenario.onFragment {
            val sanctionsAdapter: SanctionAdapter? =
                it.view?.findViewById<RecyclerView>(R.id.rv_container)?.run {
                    adapter as SanctionAdapter
                }

            assertNotNull(sanctionsAdapter)
            assertTrue(sanctionsAdapter?.getItemModel(3) is AdMobItem)
        }
    }
    //endregion

    //region EMPTY TEXT
    @Test
    fun testEmptyTextVisibilityWhenIsSuccessEmpty_ShouldBeVisible() {
        launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto()
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityVisible(R.id.tv_empty)
    }

    @Test
    fun testEmptyTextVisibilityWhenIsSuccessNoEmpty_ShouldBeGone() {
        launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(1),
                    mutableListOf()
                )
            )
        }
        AndroidTestRobot.init()
            .viewVisibilityGone(R.id.tv_empty)
    }
    //endregion

    //region SWIPE
    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDown_ShouldBeVisible() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(10),
                    getSponsors(5)
                )
            )
        }

        AndroidTestRobot.init()
            .viewSwipeDown(R.id.srl_container)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownSuccess_ShouldBeDone() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.SUCCESS(
                SanctionDto(
                    getSanctions(10),
                    getSponsors(5)
                )
            )
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownError_ShouldBeDone() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.ERROR("", null)
        }

        assertFalse(swipe!!.isRefreshing)
    }

    @Test
    fun testSwipeFreshLayoutWhenIsSwipeDownFailure_ShouldBeDone() {
        val scenario = launchFragmentInContainer<SanctionFragment>()
        var swipe: SwipeRefreshLayout? = null
        scenario.onFragment {
            swipe = it.view?.findViewById(R.id.srl_container)
            swipe?.isRefreshing = true
        }

        UiThreadStatement.runOnUiThread {
            sanctions.value = DataResponse.FAILURE(null)
        }

        assertFalse(swipe!!.isRefreshing)
    }
    //endregion

    //region PRIVATE
    private fun initViewModel() {
        Mockito.`when`(viewModel.sanctions).thenReturn(sanctions)
    }

    private fun initModule() {
        val mockedModule = module {
            single(override = true) { viewModel }
        }
        loadKoinModules(mockedModule)
    }

//endregion
}