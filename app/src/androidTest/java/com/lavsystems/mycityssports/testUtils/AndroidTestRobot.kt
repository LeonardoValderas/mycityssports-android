package com.lavsystems.mycityssports.testUtils

import android.app.Activity
import android.view.Gravity
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.contrib.DrawerMatchers.isOpen
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers.*


class AndroidTestRobot {
    companion object {
        fun init(): AndroidTestRobot {
            return AndroidTestRobot()
        }
    }

    fun viewIsDisplayed(id: Int): AndroidTestRobot {
        onView(withId(id))
            .check(matches(isDisplayed()))
        return this
    }

    fun viewIsDisplayedWithScroll(id: Int): AndroidTestRobot {
        onView(withId(id))
            .perform(scrollTo())
            .check(matches(isDisplayed()))
        return this
    }

    fun viewIsNotDisplayed(id: Int): AndroidTestRobot {
        onView(withId(id))
            .check(doesNotExist())
        return this
    }

    fun checkViewText(id: Int, text: String): AndroidTestRobot {
        onView(withId(id))
            .check(matches((withText(text))))
        return this
    }

    fun checkViewText(text: String): AndroidTestRobot {
        onView(withText(text)).check(matches(isDisplayed()))
        return this
    }

    fun checkNotViewText(text: String): AndroidTestRobot {
        onView(withText(text)).check(doesNotExist())
        return this
    }

    fun viewVisibilityGone(id: Int): AndroidTestRobot {
        onView(withId(id))
            .check(matches((withEffectiveVisibility(Visibility.GONE))))
        return this
    }

    fun viewVisibilityVisible(id: Int): AndroidTestRobot {
        onView(withId(id))
            .check(matches((withEffectiveVisibility(Visibility.VISIBLE))))
        return this
    }

    fun checkSnackBarDisplayed(resource: Int):AndroidTestRobot {
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(resource)))
        return this
    }

    fun checkSnackBarIsNotDisplayed(resource: Int) :AndroidTestRobot {
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(doesNotExist())
        return this
    }

    fun viewOnClick(id: Int):AndroidTestRobot {
        onView(withId(id))
            .perform(click())
        return this
    }

    fun viewOnClickScroll(id: Int):AndroidTestRobot {
        onView(withId(id))
            .perform(scrollTo(), click())
        return this
    }

    fun viewSwipeDown(id: Int):AndroidTestRobot {
        onView(withId(id))
            .perform(swipeDown())
            .check(matches(isDisplayed()))
        return this
    }

    fun childIsDisplayed(id: Int, idParent: Int) :AndroidTestRobot {
        onView(allOf(withId(id), withParent(withId(idParent))))
            .check(matches(isDisplayed()))
        return this
    }

    fun clickItemList(@IdRes id: Int, position: Int):AndroidTestRobot {
        onView(withId(id))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    position,
                    click()
                )
            )
        return this
    }

    fun itemIsDisplayedWithText(text: String):AndroidTestRobot {
        onView(withText(text)).check(matches(isDisplayed()))
        return this
    }

    fun toolbarTitleText(id: Int, text: String):AndroidTestRobot {
        onView(allOf(instanceOf(TextView::class.java), withParent(withId(id))))
            .check(matches(withText(text)))
        return this
    }

    fun verifyToastDisplayed(activity: Activity, res: Int) :AndroidTestRobot {
        onView(withText(res))
            .inRoot(withDecorView(not(`is`(activity.window.decorView))))
            .check(matches(isDisplayed()))
        return this
    }

    fun openDrawer(id: Int): AndroidTestRobot{
        onView(withId(id))
            .check(matches(isClosed(Gravity.START))) // Left Drawer should be closed.
            .perform(DrawerActions.open()); // Open Drawer
        return this
    }

    fun drawerIsClosed(id: Int): AndroidTestRobot{
        onView(withId(id))
            .check(matches(isClosed(Gravity.START)))
        return this
    }

    fun drawerIsOpen(id: Int): AndroidTestRobot{
        onView(withId(id))
            .check(matches(isOpen(Gravity.START)))
        return this
    }




/*

    fun verifyNavigationBottomVisibility(visibility: ViewMatchers.Visibility): NavigationBottomActivityRobot {
        onView(withId(R.id.navigationBottom))
            .check(matches((withEffectiveVisibility(visibility))))
        return this
    }

    fun verifyToolbarVisibility(visibility: ViewMatchers.Visibility): NavigationBottomActivityRobot {
        onView(withId(R.id.toolbar))
            .check(matches((withEffectiveVisibility(visibility))))
        return this
    }

    fun verifyToolbarDivisionTitleVisibility(visibility: ViewMatchers.Visibility): NavigationBottomActivityRobot {
        onView(withId(R.id.tv_toolbar_division))
            .check(matches((withEffectiveVisibility(visibility))))
        return this
    }

    fun verifyToolbarTournamentTitleVisibility(visibility: ViewMatchers.Visibility): NavigationBottomActivityRobot {
        onView(withId(R.id.tv_toolbar_tournament))
            .check(matches((withEffectiveVisibility(visibility))))
        return this
    }


    fun verifyFabMainVisibility(visibility: ViewMatchers.Visibility): NavigationBottomActivityRobot {
        onView(withId(R.id.fabMain))
            .check(matches((withEffectiveVisibility(visibility))))
        return this
    }

    fun verifyToolbarTitleText(division: String, tournament: String): NavigationBottomActivityRobot {
        verifyToolbarDivisionText(division)
        verifyToolbarTournamentText(tournament)
        return this
    }

    fun verifyToolbarDivisionText(division: String): NavigationBottomActivityRobot {
        onView(withId(R.id.tv_toolbar_division))
            .check(matches((withText(division))))
        return this
    }

    fun verifyToolbarTournamentText(tournament: String): NavigationBottomActivityRobot {
        onView(withId(R.id.tv_toolbar_tournament))
            .check(matches((withText(tournament))))
        return this
    }



    */
/* fun verifyRecyclersMenuTournamentDisplayed(): NavigationBottomActivityRobot {
         onView(allOf(withId(R.id.recycler_view_menu), isDescendantOfA(withId(R.id.vp_vertical_ntb))))
                 .check(matches(isDisplayed()))
         onView(allOf(withId(R.id.recycler_view_tournament), isDescendantOfA(withId(R.id.vp_vertical_ntb))))
                 .check(matches(isDisplayed()))
         return this
     }*//*


    fun verifyToastDisplayed(activity: Activity, res: Int): NavigationBottomActivityRobot {
        onView(withText(res))
            .inRoot(withDecorView(not(`is`(activity.window.decorView))))
            .check(matches(isDisplayed()))
        return this
    }

    fun verifySnackDisplayed(activity: Activity, res: Int): NavigationBottomActivityRobot {

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(activity.getString(res))))
            .check(matches(isDisplayed()))

        */
/*onView(withId(android.support.design.R.id.snackbar_text))
                .check(matches(withText(R.string.generic_error)))

        onView(withText(res))
                .inRoot(withDecorView(not(`is`(activity.window.decorView))))
                .check(matches(isDisplayed()))*//*

        return this
    }

*/
}